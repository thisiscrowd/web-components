const path = require('path');
const entryPlus = require('webpack-entry-plus');
const glob = require('glob');
const entries = [
    {
        entryFiles: glob.sync('./src/index.js'),
        outputName: 'index'
    }
];

module.exports = {
  mode: 'production',
  entry: entryPlus(entries),
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'docs'),
  },
};