const path = require('path');
const entryPlus = require('webpack-entry-plus');
const glob = require('glob');
const entries = [
    {
        entryFiles: glob.sync('./src/index.js'),
        outputName: 'index'
    },
    {
        entryFiles: glob.sync('./src/_accordion.js'),
        outputName: 'accordion'
    },
    {
        entryFiles: glob.sync('./src/_alert.js'),
        outputName: 'alert'
    },
    {
        entryFiles: glob.sync('./src/_badge.js'),
        outputName: 'badge'
    },
    {
        entryFiles: glob.sync('./src/_button.js'),
        outputName: 'button'
    },
    {
        entryFiles: glob.sync('./src/_checkbox.js'),
        outputName: 'checkbox'
    },
    {
        entryFiles: glob.sync('./src/_color-picker.js'),
        outputName: 'colorpicker'
    },
    {
        entryFiles: glob.sync('./src/_dialog.js'),
        outputName: 'dialog'
    },
    {
        entryFiles: glob.sync('./src/_drawer.js'),
        outputName: 'drawer'
    },
    {
        entryFiles: glob.sync('./src/_dropdown.js'),
        outputName: 'dropdown'
    },
    {
        entryFiles: glob.sync('./src/_form.js'),
        outputName: 'form'
    },
    {
        entryFiles: glob.sync('./src/_icon-button.js'),
        outputName: 'icon-button'
    },
    {
        entryFiles: glob.sync('./src/_icon.js'),
        outputName: 'icon'
    },
    {
        entryFiles: glob.sync('./src/_input.js'),
        outputName: 'input'
    },
    {
        entryFiles: glob.sync('./src/_menu-item.js'),
        outputName: 'menu-item'
    },
    {
        entryFiles: glob.sync('./src/_menu.js'),
        outputName: 'menu'
    },
    {
        entryFiles: glob.sync('./src/_progress-ring.js'),
        outputName: 'progress-ring'
    },
    {
        entryFiles: glob.sync('./src/_range.js'),
        outputName: 'range'
    },
    {
        entryFiles: glob.sync('./src/_select.js'),
        outputName: 'select'
    },
    {
        entryFiles: glob.sync('./src/_spinner.js'),
        outputName: 'spinner'
    },
    {
        entryFiles: glob.sync('./src/_switch.js'),
        outputName: 'switch'
    },
    {
        entryFiles: glob.sync('./src/_textarea.js'),
        outputName: 'textarea'
    },
    {
        entryFiles: glob.sync('./src/_toast-stack.js'),
        outputName: 'toast-stack'
    },
    {
        entryFiles: glob.sync('./src/_tooltip.js'),
        outputName: 'tooltip'
    },
    {
        entryFiles: glob.sync('./src/index.js'),
        outputName: 'index'
    }
];

module.exports = {
  mode: 'development',
  entry: entryPlus(entries),
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dev/js'),
  },
};