/*! For license information please see select.js.LICENSE.txt */
(()=>{"use strict";var t,e={692:(t,e,o)=>{var i,r;o.d(e,{Jb:()=>y,sY:()=>E});const n=globalThis.trustedTypes,s=n?n.createPolicy("lit-html",{createHTML:t=>t}):void 0,l=`lit$${(Math.random()+"").slice(9)}$`,a="?"+l,d=`<${a}>`,c=document,h=(t="")=>c.createComment(t),p=t=>null===t||"object"!=typeof t&&"function"!=typeof t,u=Array.isArray,v=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,g=/-->/g,$=/>/g,f=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,m=/'/g,w=/"/g,b=/^(?:script|style|textarea)$/i,_=t=>(e,...o)=>({_$litType$:t,strings:e,values:o}),y=(_(1),_(2),Symbol.for("lit-noChange")),A=Symbol.for("lit-nothing"),x=new WeakMap,E=(t,e,o)=>{var i,r;const n=null!==(i=null==o?void 0:o.renderBefore)&&void 0!==i?i:e;let s=n._$litPart$;if(void 0===s){const t=null!==(r=null==o?void 0:o.renderBefore)&&void 0!==r?r:null;n._$litPart$=s=new P(e.insertBefore(h(),t),t,void 0,null!=o?o:{})}return s._$AI(t),s},S=c.createTreeWalker(c,129,null,!1),C=(t,e)=>{const o=t.length-1,i=[];let r,n=2===e?"<svg>":"",a=v;for(let e=0;e<o;e++){const o=t[e];let s,c,h=-1,p=0;for(;p<o.length&&(a.lastIndex=p,c=a.exec(o),null!==c);)p=a.lastIndex,a===v?"!--"===c[1]?a=g:void 0!==c[1]?a=$:void 0!==c[2]?(b.test(c[2])&&(r=RegExp("</"+c[2],"g")),a=f):void 0!==c[3]&&(a=f):a===f?">"===c[0]?(a=null!=r?r:v,h=-1):void 0===c[1]?h=-2:(h=a.lastIndex-c[2].length,s=c[1],a=void 0===c[3]?f:'"'===c[3]?w:m):a===w||a===m?a=f:a===g||a===$?a=v:(a=f,r=void 0);const u=a===f&&t[e+1].startsWith("/>")?" ":"";n+=a===v?o+d:h>=0?(i.push(s),o.slice(0,h)+"$lit$"+o.slice(h)+l+u):o+l+(-2===h?(i.push(void 0),e):u)}const c=n+(t[o]||"<?>")+(2===e?"</svg>":"");return[void 0!==s?s.createHTML(c):c,i]};class T{constructor({strings:t,_$litType$:e},o){let i;this.parts=[];let r=0,s=0;const d=t.length-1,c=this.parts,[p,u]=C(t,e);if(this.el=T.createElement(p,o),S.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(i=S.nextNode())&&c.length<d;){if(1===i.nodeType){if(i.hasAttributes()){const t=[];for(const e of i.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(l)){const o=u[s++];if(t.push(e),void 0!==o){const t=i.getAttribute(o.toLowerCase()+"$lit$").split(l),e=/([.?@])?(.*)/.exec(o);c.push({type:1,index:r,name:e[2],strings:t,ctor:"."===e[1]?H:"?"===e[1]?N:"@"===e[1]?O:z})}else c.push({type:6,index:r})}for(const e of t)i.removeAttribute(e)}if(b.test(i.tagName)){const t=i.textContent.split(l),e=t.length-1;if(e>0){i.textContent=n?n.emptyScript:"";for(let o=0;o<e;o++)i.append(t[o],h()),S.nextNode(),c.push({type:2,index:++r});i.append(t[e],h())}}}else if(8===i.nodeType)if(i.data===a)c.push({type:2,index:r});else{let t=-1;for(;-1!==(t=i.data.indexOf(l,t+1));)c.push({type:7,index:r}),t+=l.length-1}r++}}static createElement(t,e){const o=c.createElement("template");return o.innerHTML=t,o}}function k(t,e,o=t,i){var r,n,s,l;if(e===y)return e;let a=void 0!==i?null===(r=o._$Cl)||void 0===r?void 0:r[i]:o._$Cu;const d=p(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(n=null==a?void 0:a._$AO)||void 0===n||n.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,o,i)),void 0!==i?(null!==(s=(l=o)._$Cl)&&void 0!==s?s:l._$Cl=[])[i]=a:o._$Cu=a),void 0!==a&&(e=k(t,a._$AS(t,e.values),a,i)),e}class U{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:o},parts:i}=this._$AD,r=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:c).importNode(o,!0);S.currentNode=r;let n=S.nextNode(),s=0,l=0,a=i[0];for(;void 0!==a;){if(s===a.index){let e;2===a.type?e=new P(n,n.nextSibling,this,t):1===a.type?e=new a.ctor(n,a.name,a.strings,this,t):6===a.type&&(e=new M(n,this,t)),this.v.push(e),a=i[++l]}s!==(null==a?void 0:a.index)&&(n=S.nextNode(),s++)}return r}m(t){let e=0;for(const o of this.v)void 0!==o&&(void 0!==o.strings?(o._$AI(t,o,e),e+=o.strings.length-2):o._$AI(t[e])),e++}}class P{constructor(t,e,o,i){var r;this.type=2,this._$AH=A,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=o,this.options=i,this._$Cg=null===(r=null==i?void 0:i.isConnected)||void 0===r||r}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=k(this,t,e),p(t)?t===A||null==t||""===t?(this._$AH!==A&&this._$AR(),this._$AH=A):t!==this._$AH&&t!==y&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.S(t):(t=>{var e;return u(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.M(t):this.$(t)}A(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}S(t){this._$AH!==t&&(this._$AR(),this._$AH=this.A(t))}$(t){this._$AH!==A&&p(this._$AH)?this._$AA.nextSibling.data=t:this.S(c.createTextNode(t)),this._$AH=t}T(t){var e;const{values:o,_$litType$:i}=t,r="number"==typeof i?this._$AC(t):(void 0===i.el&&(i.el=T.createElement(i.h,this.options)),i);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===r)this._$AH.m(o);else{const t=new U(r,this),e=t.p(this.options);t.m(o),this.S(e),this._$AH=t}}_$AC(t){let e=x.get(t.strings);return void 0===e&&x.set(t.strings,e=new T(t)),e}M(t){u(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let o,i=0;for(const r of t)i===e.length?e.push(o=new P(this.A(h()),this.A(h()),this,this.options)):o=e[i],o._$AI(r),i++;i<e.length&&(this._$AR(o&&o._$AB.nextSibling,i),e.length=i)}_$AR(t=this._$AA.nextSibling,e){var o;for(null===(o=this._$AP)||void 0===o||o.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class z{constructor(t,e,o,i,r){this.type=1,this._$AH=A,this._$AN=void 0,this.element=t,this.name=e,this._$AM=i,this.options=r,o.length>2||""!==o[0]||""!==o[1]?(this._$AH=Array(o.length-1).fill(new String),this.strings=o):this._$AH=A}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,o,i){const r=this.strings;let n=!1;if(void 0===r)t=k(this,t,e,0),n=!p(t)||t!==this._$AH&&t!==y,n&&(this._$AH=t);else{const i=t;let s,l;for(t=r[0],s=0;s<r.length-1;s++)l=k(this,i[o+s],e,s),l===y&&(l=this._$AH[s]),n||(n=!p(l)||l!==this._$AH[s]),l===A?t=A:t!==A&&(t+=(null!=l?l:"")+r[s+1]),this._$AH[s]=l}n&&!i&&this.k(t)}k(t){t===A?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class H extends z{constructor(){super(...arguments),this.type=3}k(t){this.element[this.name]=t===A?void 0:t}}class N extends z{constructor(){super(...arguments),this.type=4}k(t){t&&t!==A?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name)}}class O extends z{constructor(t,e,o,i,r){super(t,e,o,i,r),this.type=5}_$AI(t,e=this){var o;if((t=null!==(o=k(this,t,e,0))&&void 0!==o?o:A)===y)return;const i=this._$AH,r=t===A&&i!==A||t.capture!==i.capture||t.once!==i.once||t.passive!==i.passive,n=t!==A&&(i===A||r);r&&this.element.removeEventListener(this.name,this,i),n&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,o;"function"==typeof this._$AH?this._$AH.call(null!==(o=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==o?o:this.element,t):this._$AH.handleEvent(t)}}class M{constructor(t,e,o){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=o}get _$AU(){return this._$AM._$AU}_$AI(t){k(this,t)}}null===(i=globalThis.litHtmlPolyfillSupport)||void 0===i||i.call(globalThis,T,P),(null!==(r=globalThis.litHtmlVersions)&&void 0!==r?r:globalThis.litHtmlVersions=[]).push("2.0.0")},392:(t,e,o)=>{o.d(e,{iv:()=>a});const i=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,r=Symbol(),n=new Map;class s{constructor(t,e){if(this._$cssResult$=!0,e!==r)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=n.get(this.cssText);return i&&void 0===t&&(n.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const l=t=>new s("string"==typeof t?t:t+"",r),a=(t,...e)=>{const o=1===t.length?t[0]:e.reduce(((e,o,i)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(o)+t[i+1]),t[0]);return new s(o,r)},d=i?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const o of t.cssRules)e+=o.cssText;return l(e)})(t):t;var c,h;const p={toAttribute(t,e){switch(e){case Boolean:t=t?"":null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let o=t;switch(e){case Boolean:o=null!==t;break;case Number:o=null===t?null:Number(t);break;case Object:case Array:try{o=JSON.parse(t)}catch(t){o=null}}return o}},u=(t,e)=>e!==t&&(e==e||t==t),v={attribute:!0,type:String,converter:p,reflect:!1,hasChanged:u};class g extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,o)=>{const i=this._$Eh(o,e);void 0!==i&&(this._$Eu.set(i,o),t.push(i))})),t}static createProperty(t,e=v){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const o="symbol"==typeof t?Symbol():"__"+t,i=this.getPropertyDescriptor(t,o,e);void 0!==i&&Object.defineProperty(this.prototype,t,i)}}static getPropertyDescriptor(t,e,o){return{get(){return this[e]},set(i){const r=this[t];this[e]=i,this.requestUpdate(t,r,o)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||v}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const o of e)this.createProperty(o,t[o])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const o=new Set(t.flat(1/0).reverse());for(const t of o)e.unshift(d(t))}else void 0!==t&&e.push(d(t));return e}static _$Eh(t,e){const o=e.attribute;return!1===o?void 0:"string"==typeof o?o:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ev=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Ep(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,o;(null!==(e=this._$Em)&&void 0!==e?e:this._$Em=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(o=t.hostConnected)||void 0===o||o.call(t))}removeController(t){var e;null===(e=this._$Em)||void 0===e||e.splice(this._$Em.indexOf(t)>>>0,1)}_$Ep(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{i?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const o=document.createElement("style"),i=window.litNonce;void 0!==i&&o.setAttribute("nonce",i),o.textContent=e.cssText,t.appendChild(o)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,o){this._$AK(t,o)}_$Eg(t,e,o=v){var i,r;const n=this.constructor._$Eh(t,o);if(void 0!==n&&!0===o.reflect){const s=(null!==(r=null===(i=o.converter)||void 0===i?void 0:i.toAttribute)&&void 0!==r?r:p.toAttribute)(e,o.type);this._$Ei=t,null==s?this.removeAttribute(n):this.setAttribute(n,s),this._$Ei=null}}_$AK(t,e){var o,i,r;const n=this.constructor,s=n._$Eu.get(t);if(void 0!==s&&this._$Ei!==s){const t=n.getPropertyOptions(s),l=t.converter,a=null!==(r=null!==(i=null===(o=l)||void 0===o?void 0:o.fromAttribute)&&void 0!==i?i:"function"==typeof l?l:null)&&void 0!==r?r:p.fromAttribute;this._$Ei=s,this[s]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,o){let i=!0;void 0!==t&&(((o=o||this.constructor.getPropertyOptions(t)).hasChanged||u)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===o.reflect&&this._$Ei!==t&&(void 0===this._$ES&&(this._$ES=new Map),this._$ES.set(t,o))):i=!1),!this.isUpdatePending&&i&&(this._$Ev=this._$EC())}async _$EC(){this.isUpdatePending=!0;try{await this._$Ev}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const o=this._$AL;try{e=this.shouldUpdate(o),e?(this.willUpdate(o),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(o)):this._$ET()}catch(t){throw e=!1,this._$ET(),t}e&&this._$AE(o)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Em)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$ET(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ev}shouldUpdate(t){return!0}update(t){void 0!==this._$ES&&(this._$ES.forEach(((t,e)=>this._$Eg(e,this[e],t))),this._$ES=void 0),this._$ET()}updated(t){}firstUpdated(t){}}g.finalized=!0,g.elementProperties=new Map,g.elementStyles=[],g.shadowRootOptions={mode:"open"},null===(c=globalThis.reactiveElementPolyfillSupport)||void 0===c||c.call(globalThis,{ReactiveElement:g}),(null!==(h=globalThis.reactiveElementVersions)&&void 0!==h?h:globalThis.reactiveElementVersions=[]).push("1.0.0");var $,f,m,w=o(692);class b extends g{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const o=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=o.firstChild),o}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=(0,w.sY)(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return w.Jb}}b.finalized=!0,b._$litElement$=!0,null===($=globalThis.litElementHydrateSupport)||void 0===$||$.call(globalThis,{LitElement:b}),null===(f=globalThis.litElementPolyfillSupport)||void 0===f||f.call(globalThis,{LitElement:b}),(null!==(m=globalThis.litElementVersions)&&void 0!==m?m:globalThis.litElementVersions=[]).push("3.0.0")}},o={};function i(t){var r=o[t];if(void 0!==r)return r.exports;var n=o[t]={exports:{}};return e[t](n,n.exports,i),n.exports}i.d=(t,e)=>{for(var o in e)i.o(e,o)&&!i.o(t,o)&&Object.defineProperty(t,o,{enumerable:!0,get:e[o]})},i.o=(t,e)=>Object.prototype.hasOwnProperty.call(t,e),t=i(392),Boolean,Boolean,Boolean,Boolean,Boolean,Boolean,Boolean,Boolean,t.iv`
        :host {
            display: inline-block;
        }
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        .wrapper {
            padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
            max-width: 100%;
            position:relative;
        }
        label {
            display: inline-block;
            color: var(--crowd-input-label-color, inherit);
            margin-bottom: var(--crowd-input-label-spacing, 0.5em);
        }
        .select-dropdown {
            box-sizing: border-box;
            position: absolute;
            top:calc(100% - var(--crowd-input-wrapper-padding-vertical, 0) + var(--crowd-select-dropdown-spacing,2px));
            left: 0;
            width: 100%;
            max-height: var(--crowd-select-dropdown-max-height, 50vh);
            overflow-y: scroll;
            padding: var(--crowd-select-dropdown-padding-vertical, 0.5em) var(--crowd-select-dropdown-padding-horizontal, 0.5em);
            pointer-events: none;
            transition-property: opacity, transform;
            transition-duration: var(--crowd-select-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-select-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-transition-delay, 0s);
            opacity: 0;
            transform: scaleY(0.5);
            transform-origin: top center;
            background-color: var(--crowd-select-dropdown-background-color, white);
            z-index: var(--crowd-select-dropdown-z-index, 999);
            box-shadow: var(
                --crowd-select-dropdown-box-shadow,
                0 2px 8px rgba(0, 0, 0, 0.1)
            );
            border: var(--crowd-select-dropdown-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-select-dropdown-border-color, transparent);
        }
        :host([hoist]) .select-dropdown {
            position:fixed;
            top: auto;
            left: auto;
        }
        :host([show]) .select-dropdown {
            opacity: 1;
            transform: scale(1);
            pointer-events: all;
        }
        input,.multiple-items {
            -webkit-appearance: none;
            background-color: transparent;
            border: none;
            height: 100%;
            width: calc(100% - (2 * var(--crowd-input-padding-horizontal, 1em)));
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            padding: 0 var(--crowd-input-padding-horizontal, 1em);
            font-weight: var(--crowd-input-font-weight,400);
            height: calc(
                var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
            );
            caret-color: transparent;
            cursor: pointer;
            text-transform: var(--crowd-input-text-transform);
        }
        input.multi {
            opacity: 0;
        }
        input:focus-visible, input:active {
            outline: none;
        }
        input::placeholder {
            color: var(--crowd-input-placeholder-color, inherit);
        }
        .input-container {
            color: var(--crowd-input-color, inherit);
            background-color: var(--crowd-input-background, white);
            border: var(--crowd-input-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, transparent);
            border-radius: var(--crowd-input-border-radius, 0px);
            font-size: var(--crowd-input-font-size,1rem);
            padding: var(--crowd-input-padding-vertical, 1em) 0;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: stretch;
            position: relative;
        }
        .input-container:focus-within, .input-container:focus-visible {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        [part='overlay'] {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            pointer-events: none;
            z-index: calc(var(--crowd-select-dropdown-z-index, 999) - 1);
        }
        :host([show]) [part='overlay'] {
            pointer-events: all;
        }
        button {
            -webkit-appearance: none;
            background-color: transparent;
            color: var(--crowd-select-icon-color, inherit);
            border: none;
            padding: 0;
            display: grid;
            place-items: center;
            transition-property: transform,color;
            transition-duration: var(--crowd-select-icon-transition-duration,0.15s);
            transition-timing-function: var(--crowd-select-icon-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-icon-transition-delay, 0s);
            margin-right: var(--crowd-input-padding-horizontal, 1em);
            cursor: pointer;
            position:relative;
            z-index: 2;
        }
        .error,.success {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        .success {
            color: var(--crowd-input-success-message-color, lime);
        }
        @media (hover: hover) {
            button:hover {
                color: var(--crowd-select-icon-hover-color, inherit);
            }
        }
        :host([show]) button[part='toggle'] {
            transform: rotate(180deg);
        }
        :host([invalid]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-error-message-color, red);
        }
        :host([success]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-success-message-color, lime);
        }
        .multiple-items {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            bottom: 0;
            left: 0;
            right: 0;
            overflow-y: hidden;
            overflow-x: scroll;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
            gap: 4px;
            z-index: 1;
        }
        @supports (::-moz-range-track) {
            .multiple-items {
                scrollbar-width: thin;
                scrollbar-color: var(--crowd-select-multi-scrollbar-color,#000);
            }
        }
        .multiple-items::-webkit-scrollbar {
            height: 2px;
            background-color: var(--crowd-select-multi-scrollbar-background,#aaa);
        }
        .multiple-items::-moz-scrollbar-button, .multiple-items::-webkit-scrollbar-button {
            width: 0px;
            display: none;
        }
        .multiple-items::-webkit-scrollbar-thumb {
            background: var(--crowd-select-multi-scrollbar-color,#000);
        }
        .multiple-items crowd-badge {
            pointer-events: auto;
            gap: 2px;
        }
    `,Boolean,Boolean,t.iv`
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        [part='container'] {
            padding: var(--crowd-option-padding-vertical, 0.2em) var(--crowd-option-padding-horizontal, 1em);
            font-family: var(--crowd-option-font-family, inherit);
            font-size: var(--crowd-option-font-size, inherit);
            font-weight: var(--crowd-option-font-weight, inherit);
            color: var(--crowd-option-color, inherit);
            background-color: var(--crowd-option-background-color, transparent);
            width: calc(100% - (1 * var(--crowd-option-padding-horizontal, 1em)));
            margin: 0 calc(-1 * var(--crowd-select-dropdown-padding-horizontal, 0.5em));
            cursor: pointer;
            transition-property: color, background-color;
            transition-duration: var(--crowd-option-transition-duration,0.15s);
            transition-timing-function: var(--crowd-option-transition-timing-function, ease-in-out);
            transition-delay: var(--crowd-option-transition-delay, 0s);
            border-radius: var(--crowd-option-border-radius,0px);
        }
        @media  (hover: hover) {
            [part='container']:hover {
                color: var(--crowd-option-hover-color, inherit);
                background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
            }
        }
        [part='container']:focus-visible, :host([focus]) [part='container'],:host([active]) [part='container'] {
            color: var(--crowd-option-hover-color, inherit);
            background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
        }
    `})();