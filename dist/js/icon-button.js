/*! For license information please see icon-button.js.LICENSE.txt */
(()=>{"use strict";var t={565:(t,e,i)=>{i.d(e,{z:()=>o});var s=i(392);class o extends s.oi{static properties={href:{type:String},pill:{type:Boolean},loading:{type:Boolean},disabled:{type:Boolean},caret:{type:Boolean},target:{type:String},circle:{type:Boolean}};static styles=s.iv`
        :host {
            display: inline-block;
            width: auto;
            cursor: pointer;
            line-height: 1;
            height:min-content;
        }
        button,a {
            -webkit-appearance: none;
            background-color: var(--crowd-button-background-color, #eeeeee);
            padding: var(--crowd-button-padding-vertical,0.5em) var(--crowd-button-padding-horizontal, 1em);
            color: var(--crowd-button-color, inherit);
            border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-button-border-color, #aeaeae);
            border-radius: var(--crowd-button-border-radius, 3px);
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            text-align: var(--crowd-button-text-align, center);
            display: inline-flex;
            width: 100%;
            height: var(--crowd-button-height, 2.5em);
            flex-flow: row nowrap;
            justify-content: var(--crowd-button-justify,center);
            align-items: center;
            gap: var(--crowd-button-gap, 0.5em);
            cursor: pointer;
            margin: 0;
            text-decoration: none;
            text-transform: inherit;
            box-sizing: border-box;
            transition-property: background-color, border-color, color;
            transition-duration: var(--crowd-button-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-button-transition-ease, ease-in-out);
            transition-delay: var(--crowd-button-transition-delay, 0s);
            position:relative;
            text-transform: var(--crowd-button-text-transform, inherit);
        }
        button:focus-visible, button:active, a:focus-visible,a:active {
            outline: none;
        }
        button:focus-visible,button:active, a:focus-visible,a:active {
            box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-button-focus-color, rgba(0,0,0,0.3));
        }
        :host([pill]) button, :host([pill]) a {
            border-radius: var(--crowd-button-pill-border-radius, 999px);
        }
        @media (hover: hover) {
            button:hover, a:hover {
                background-color: var(--crowd-button-hover-background-color, #aeaeae);
                border-color: var(--crowd-button-hover-border-color, #aeaeae);
                color: var(--crowd-button-hover-color, #fff);
            }
        }
        :host([disabled]) {
            opacity: 0.5;
            pointer-events: none;
        }
        :host([circle]) button, :host([circle]) a {
            aspect-ratio: 1/1;
            height: var(--crowd-button-width, auto);
            width: var(--crowd-button-width, auto);
            border-radius: 50%;
        }
        slot[name='prefix'] svg,
        slot[name='suffix'] svg {
            height: 1.5em;
        }
        .prefix,.suffix {
            display: inline-grid;
            place-items:center;
        }
        .label {
            display: flex;
            flex-flow: row nowrap;
            justfy-content: flex-start;
            align-items:center;
        }
        :host([loading]) button, :host([loading]) a {
            display: inline-grid;
            place-items: center;
        }
        crowd-spinner {
            position: absolute;
            top:50%;
            left:50%;
            transform: translate(-50%,-50%);
        }
        .loading {
            display: inline-flex;
            flex-flow: row nowrap;
            justify-content: var(--crowd-button-justify,center);
            align-items: center;
            gap: var(--crowd-button-gap, 0.5em);
            opacity: 0;
        }
    `;constructor(){super()}render(){let t="";this.caret&&(t=s.dy`
                <crowd-icon name='chevron-down'></crowd-icon>
            `);let e=s.dy`
            <span class='prefix' part='prefix'>
                <slot name='prefix'></slot>
            </span>
            <span class='label' part='label'>
                <slot></slot>
            </span>
            <span class='suffix' part='suffix'>
                <slot name='suffix'>${t}</slot>
            </span>
        `;this.loading&&(e=s.dy`<crowd-spinner></crowd-spinner><span class='loading'>${e}</span>`);let i=s.dy`
            <button part='button'>
                ${e}
            </button>
        `;return this.href&&(i=s.dy`
                <a part='button' href='${this.href}' target='${this.target}'>
                    ${e}
                </a>
            `),s.dy`
            ${i}
        `}}},692:(t,e,i)=>{var s,o;i.d(e,{dy:()=>A,Jb:()=>w,sY:()=>S});const n=globalThis.trustedTypes,r=n?n.createPolicy("lit-html",{createHTML:t=>t}):void 0,l=`lit$${(Math.random()+"").slice(9)}$`,a="?"+l,h=`<${a}>`,d=document,c=(t="")=>d.createComment(t),u=t=>null===t||"object"!=typeof t&&"function"!=typeof t,p=Array.isArray,v=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,$=/-->/g,f=/>/g,g=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,b=/'/g,_=/"/g,m=/^(?:script|style|textarea)$/i,y=t=>(e,...i)=>({_$litType$:t,strings:e,values:i}),A=y(1),w=(y(2),Symbol.for("lit-noChange")),E=Symbol.for("lit-nothing"),x=new WeakMap,S=(t,e,i)=>{var s,o;const n=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:e;let r=n._$litPart$;if(void 0===r){const t=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:null;n._$litPart$=r=new N(e.insertBefore(c(),t),t,void 0,null!=i?i:{})}return r._$AI(t),r},C=d.createTreeWalker(d,129,null,!1),T=(t,e)=>{const i=t.length-1,s=[];let o,n=2===e?"<svg>":"",a=v;for(let e=0;e<i;e++){const i=t[e];let r,d,c=-1,u=0;for(;u<i.length&&(a.lastIndex=u,d=a.exec(i),null!==d);)u=a.lastIndex,a===v?"!--"===d[1]?a=$:void 0!==d[1]?a=f:void 0!==d[2]?(m.test(d[2])&&(o=RegExp("</"+d[2],"g")),a=g):void 0!==d[3]&&(a=g):a===g?">"===d[0]?(a=null!=o?o:v,c=-1):void 0===d[1]?c=-2:(c=a.lastIndex-d[2].length,r=d[1],a=void 0===d[3]?g:'"'===d[3]?_:b):a===_||a===b?a=g:a===$||a===f?a=v:(a=g,o=void 0);const p=a===g&&t[e+1].startsWith("/>")?" ":"";n+=a===v?i+h:c>=0?(s.push(r),i.slice(0,c)+"$lit$"+i.slice(c)+l+p):i+l+(-2===c?(s.push(void 0),e):p)}const d=n+(t[i]||"<?>")+(2===e?"</svg>":"");return[void 0!==r?r.createHTML(d):d,s]};class U{constructor({strings:t,_$litType$:e},i){let s;this.parts=[];let o=0,r=0;const h=t.length-1,d=this.parts,[u,p]=T(t,e);if(this.el=U.createElement(u,i),C.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(s=C.nextNode())&&d.length<h;){if(1===s.nodeType){if(s.hasAttributes()){const t=[];for(const e of s.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(l)){const i=p[r++];if(t.push(e),void 0!==i){const t=s.getAttribute(i.toLowerCase()+"$lit$").split(l),e=/([.?@])?(.*)/.exec(i);d.push({type:1,index:o,name:e[2],strings:t,ctor:"."===e[1]?M:"?"===e[1]?R:"@"===e[1]?k:O})}else d.push({type:6,index:o})}for(const e of t)s.removeAttribute(e)}if(m.test(s.tagName)){const t=s.textContent.split(l),e=t.length-1;if(e>0){s.textContent=n?n.emptyScript:"";for(let i=0;i<e;i++)s.append(t[i],c()),C.nextNode(),d.push({type:2,index:++o});s.append(t[e],c())}}}else if(8===s.nodeType)if(s.data===a)d.push({type:2,index:o});else{let t=-1;for(;-1!==(t=s.data.indexOf(l,t+1));)d.push({type:7,index:o}),t+=l.length-1}o++}}static createElement(t,e){const i=d.createElement("template");return i.innerHTML=t,i}}function P(t,e,i=t,s){var o,n,r,l;if(e===w)return e;let a=void 0!==s?null===(o=i._$Cl)||void 0===o?void 0:o[s]:i._$Cu;const h=u(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==h&&(null===(n=null==a?void 0:a._$AO)||void 0===n||n.call(a,!1),void 0===h?a=void 0:(a=new h(t),a._$AT(t,i,s)),void 0!==s?(null!==(r=(l=i)._$Cl)&&void 0!==r?r:l._$Cl=[])[s]=a:i._$Cu=a),void 0!==a&&(e=P(t,a._$AS(t,e.values),a,s)),e}class H{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:s}=this._$AD,o=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:d).importNode(i,!0);C.currentNode=o;let n=C.nextNode(),r=0,l=0,a=s[0];for(;void 0!==a;){if(r===a.index){let e;2===a.type?e=new N(n,n.nextSibling,this,t):1===a.type?e=new a.ctor(n,a.name,a.strings,this,t):6===a.type&&(e=new z(n,this,t)),this.v.push(e),a=s[++l]}r!==(null==a?void 0:a.index)&&(n=C.nextNode(),r++)}return o}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class N{constructor(t,e,i,s){var o;this.type=2,this._$AH=E,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=s,this._$Cg=null===(o=null==s?void 0:s.isConnected)||void 0===o||o}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=P(this,t,e),u(t)?t===E||null==t||""===t?(this._$AH!==E&&this._$AR(),this._$AH=E):t!==this._$AH&&t!==w&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.S(t):(t=>{var e;return p(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.M(t):this.$(t)}A(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}S(t){this._$AH!==t&&(this._$AR(),this._$AH=this.A(t))}$(t){this._$AH!==E&&u(this._$AH)?this._$AA.nextSibling.data=t:this.S(d.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:s}=t,o="number"==typeof s?this._$AC(t):(void 0===s.el&&(s.el=U.createElement(s.h,this.options)),s);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===o)this._$AH.m(i);else{const t=new H(o,this),e=t.p(this.options);t.m(i),this.S(e),this._$AH=t}}_$AC(t){let e=x.get(t.strings);return void 0===e&&x.set(t.strings,e=new U(t)),e}M(t){p(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,s=0;for(const o of t)s===e.length?e.push(i=new N(this.A(c()),this.A(c()),this,this.options)):i=e[s],i._$AI(o),s++;s<e.length&&(this._$AR(i&&i._$AB.nextSibling,s),e.length=s)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class O{constructor(t,e,i,s,o){this.type=1,this._$AH=E,this._$AN=void 0,this.element=t,this.name=e,this._$AM=s,this.options=o,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=E}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,s){const o=this.strings;let n=!1;if(void 0===o)t=P(this,t,e,0),n=!u(t)||t!==this._$AH&&t!==w,n&&(this._$AH=t);else{const s=t;let r,l;for(t=o[0],r=0;r<o.length-1;r++)l=P(this,s[i+r],e,r),l===w&&(l=this._$AH[r]),n||(n=!u(l)||l!==this._$AH[r]),l===E?t=E:t!==E&&(t+=(null!=l?l:"")+o[r+1]),this._$AH[r]=l}n&&!s&&this.k(t)}k(t){t===E?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class M extends O{constructor(){super(...arguments),this.type=3}k(t){this.element[this.name]=t===E?void 0:t}}class R extends O{constructor(){super(...arguments),this.type=4}k(t){t&&t!==E?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name)}}class k extends O{constructor(t,e,i,s,o){super(t,e,i,s,o),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=P(this,t,e,0))&&void 0!==i?i:E)===w)return;const s=this._$AH,o=t===E&&s!==E||t.capture!==s.capture||t.once!==s.once||t.passive!==s.passive,n=t!==E&&(s===E||o);o&&this.element.removeEventListener(this.name,this,s),n&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class z{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){P(this,t)}}null===(s=globalThis.litHtmlPolyfillSupport)||void 0===s||s.call(globalThis,U,N),(null!==(o=globalThis.litHtmlVersions)&&void 0!==o?o:globalThis.litHtmlVersions=[]).push("2.0.0")},392:(t,e,i)=>{i.d(e,{oi:()=>m,iv:()=>a,dy:()=>_.dy});const s=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,o=Symbol(),n=new Map;class r{constructor(t,e){if(this._$cssResult$=!0,e!==o)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=n.get(this.cssText);return s&&void 0===t&&(n.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const l=t=>new r("string"==typeof t?t:t+"",o),a=(t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,s)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[s+1]),t[0]);return new r(i,o)},h=s?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return l(e)})(t):t;var d,c;const u={toAttribute(t,e){switch(e){case Boolean:t=t?"":null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},p=(t,e)=>e!==t&&(e==e||t==t),v={attribute:!0,type:String,converter:u,reflect:!1,hasChanged:p};class $ extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const s=this._$Eh(i,e);void 0!==s&&(this._$Eu.set(s,i),t.push(s))})),t}static createProperty(t,e=v){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,i,e);void 0!==s&&Object.defineProperty(this.prototype,t,s)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(s){const o=this[t];this[e]=s,this.requestUpdate(t,o,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||v}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(h(t))}else void 0!==t&&e.push(h(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ev=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Ep(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Em)&&void 0!==e?e:this._$Em=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Em)||void 0===e||e.splice(this._$Em.indexOf(t)>>>0,1)}_$Ep(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{s?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),s=window.litNonce;void 0!==s&&i.setAttribute("nonce",s),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$Eg(t,e,i=v){var s,o;const n=this.constructor._$Eh(t,i);if(void 0!==n&&!0===i.reflect){const r=(null!==(o=null===(s=i.converter)||void 0===s?void 0:s.toAttribute)&&void 0!==o?o:u.toAttribute)(e,i.type);this._$Ei=t,null==r?this.removeAttribute(n):this.setAttribute(n,r),this._$Ei=null}}_$AK(t,e){var i,s,o;const n=this.constructor,r=n._$Eu.get(t);if(void 0!==r&&this._$Ei!==r){const t=n.getPropertyOptions(r),l=t.converter,a=null!==(o=null!==(s=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==s?s:"function"==typeof l?l:null)&&void 0!==o?o:u.fromAttribute;this._$Ei=r,this[r]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let s=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||p)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$ES&&(this._$ES=new Map),this._$ES.set(t,i))):s=!1),!this.isUpdatePending&&s&&(this._$Ev=this._$EC())}async _$EC(){this.isUpdatePending=!0;try{await this._$Ev}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$ET()}catch(t){throw e=!1,this._$ET(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Em)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$ET(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ev}shouldUpdate(t){return!0}update(t){void 0!==this._$ES&&(this._$ES.forEach(((t,e)=>this._$Eg(e,this[e],t))),this._$ES=void 0),this._$ET()}updated(t){}firstUpdated(t){}}$.finalized=!0,$.elementProperties=new Map,$.elementStyles=[],$.shadowRootOptions={mode:"open"},null===(d=globalThis.reactiveElementPolyfillSupport)||void 0===d||d.call(globalThis,{ReactiveElement:$}),(null!==(c=globalThis.reactiveElementVersions)&&void 0!==c?c:globalThis.reactiveElementVersions=[]).push("1.0.0");var f,g,b,_=i(692);class m extends ${constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=(0,_.sY)(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return _.Jb}}m.finalized=!0,m._$litElement$=!0,null===(f=globalThis.litElementHydrateSupport)||void 0===f||f.call(globalThis,{LitElement:m}),null===(g=globalThis.litElementPolyfillSupport)||void 0===g||g.call(globalThis,{LitElement:m}),(null!==(b=globalThis.litElementVersions)&&void 0!==b?b:globalThis.litElementVersions=[]).push("3.0.0")}},e={};function i(s){var o=e[s];if(void 0!==o)return o.exports;var n=e[s]={exports:{}};return t[s](n,n.exports,i),n.exports}i.d=(t,e)=>{for(var s in e)i.o(e,s)&&!i.o(t,s)&&Object.defineProperty(t,s,{enumerable:!0,get:e[s]})},i.o=(t,e)=>Object.prototype.hasOwnProperty.call(t,e),(()=>{var t=i(392),e=i(565);super.properties,e.z.styles,t.iv`
            :host {
                line-height: 0;
            }
            button {
                background-color: transparent;
                border: none;
                padding: var(--crowd-icon-button-padding, 0.25em);
                width: 1em;
                height: 1em;
            }
            @media (hover: hover) {
                button:hover {
                    background-color: transparent;
                    border: none;
                }
            }
        `})()})();