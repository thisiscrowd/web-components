/*! For license information please see dialog.js.LICENSE.txt */
(()=>{"use strict";var t,e={692:(t,e,i)=>{var o,s;i.d(e,{Jb:()=>b,sY:()=>S});const r=globalThis.trustedTypes,n=r?r.createPolicy("lit-html",{createHTML:t=>t}):void 0,l=`lit$${(Math.random()+"").slice(9)}$`,a="?"+l,d=`<${a}>`,h=document,c=(t="")=>h.createComment(t),u=t=>null===t||"object"!=typeof t&&"function"!=typeof t,p=Array.isArray,v=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,g=/-->/g,$=/>/g,_=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,f=/'/g,m=/"/g,y=/^(?:script|style|textarea)$/i,A=t=>(e,...i)=>({_$litType$:t,strings:e,values:i}),b=(A(1),A(2),Symbol.for("lit-noChange")),w=Symbol.for("lit-nothing"),E=new WeakMap,S=(t,e,i)=>{var o,s;const r=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:e;let n=r._$litPart$;if(void 0===n){const t=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:null;r._$litPart$=n=new H(e.insertBefore(c(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n},x=h.createTreeWalker(h,129,null,!1),C=(t,e)=>{const i=t.length-1,o=[];let s,r=2===e?"<svg>":"",a=v;for(let e=0;e<i;e++){const i=t[e];let n,h,c=-1,u=0;for(;u<i.length&&(a.lastIndex=u,h=a.exec(i),null!==h);)u=a.lastIndex,a===v?"!--"===h[1]?a=g:void 0!==h[1]?a=$:void 0!==h[2]?(y.test(h[2])&&(s=RegExp("</"+h[2],"g")),a=_):void 0!==h[3]&&(a=_):a===_?">"===h[0]?(a=null!=s?s:v,c=-1):void 0===h[1]?c=-2:(c=a.lastIndex-h[2].length,n=h[1],a=void 0===h[3]?_:'"'===h[3]?m:f):a===m||a===f?a=_:a===g||a===$?a=v:(a=_,s=void 0);const p=a===_&&t[e+1].startsWith("/>")?" ":"";r+=a===v?i+d:c>=0?(o.push(n),i.slice(0,c)+"$lit$"+i.slice(c)+l+p):i+l+(-2===c?(o.push(void 0),e):p)}const h=r+(t[i]||"<?>")+(2===e?"</svg>":"");return[void 0!==n?n.createHTML(h):h,o]};class T{constructor({strings:t,_$litType$:e},i){let o;this.parts=[];let s=0,n=0;const d=t.length-1,h=this.parts,[u,p]=C(t,e);if(this.el=T.createElement(u,i),x.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(o=x.nextNode())&&h.length<d;){if(1===o.nodeType){if(o.hasAttributes()){const t=[];for(const e of o.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(l)){const i=p[n++];if(t.push(e),void 0!==i){const t=o.getAttribute(i.toLowerCase()+"$lit$").split(l),e=/([.?@])?(.*)/.exec(i);h.push({type:1,index:s,name:e[2],strings:t,ctor:"."===e[1]?N:"?"===e[1]?O:"@"===e[1]?M:k})}else h.push({type:6,index:s})}for(const e of t)o.removeAttribute(e)}if(y.test(o.tagName)){const t=o.textContent.split(l),e=t.length-1;if(e>0){o.textContent=r?r.emptyScript:"";for(let i=0;i<e;i++)o.append(t[i],c()),x.nextNode(),h.push({type:2,index:++s});o.append(t[e],c())}}}else if(8===o.nodeType)if(o.data===a)h.push({type:2,index:s});else{let t=-1;for(;-1!==(t=o.data.indexOf(l,t+1));)h.push({type:7,index:s}),t+=l.length-1}s++}}static createElement(t,e){const i=h.createElement("template");return i.innerHTML=t,i}}function U(t,e,i=t,o){var s,r,n,l;if(e===b)return e;let a=void 0!==o?null===(s=i._$Cl)||void 0===s?void 0:s[o]:i._$Cu;const d=u(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,i,o)),void 0!==o?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[o]=a:i._$Cu=a),void 0!==a&&(e=U(t,a._$AS(t,e.values),a,o)),e}class P{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:o}=this._$AD,s=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:h).importNode(i,!0);x.currentNode=s;let r=x.nextNode(),n=0,l=0,a=o[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new H(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new R(r,this,t)),this.v.push(e),a=o[++l]}n!==(null==a?void 0:a.index)&&(r=x.nextNode(),n++)}return s}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class H{constructor(t,e,i,o){var s;this.type=2,this._$AH=w,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=o,this._$Cg=null===(s=null==o?void 0:o.isConnected)||void 0===s||s}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=U(this,t,e),u(t)?t===w||null==t||""===t?(this._$AH!==w&&this._$AR(),this._$AH=w):t!==this._$AH&&t!==b&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.S(t):(t=>{var e;return p(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.M(t):this.$(t)}A(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}S(t){this._$AH!==t&&(this._$AR(),this._$AH=this.A(t))}$(t){this._$AH!==w&&u(this._$AH)?this._$AA.nextSibling.data=t:this.S(h.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:o}=t,s="number"==typeof o?this._$AC(t):(void 0===o.el&&(o.el=T.createElement(o.h,this.options)),o);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===s)this._$AH.m(i);else{const t=new P(s,this),e=t.p(this.options);t.m(i),this.S(e),this._$AH=t}}_$AC(t){let e=E.get(t.strings);return void 0===e&&E.set(t.strings,e=new T(t)),e}M(t){p(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,o=0;for(const s of t)o===e.length?e.push(i=new H(this.A(c()),this.A(c()),this,this.options)):i=e[o],i._$AI(s),o++;o<e.length&&(this._$AR(i&&i._$AB.nextSibling,o),e.length=o)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class k{constructor(t,e,i,o,s){this.type=1,this._$AH=w,this._$AN=void 0,this.element=t,this.name=e,this._$AM=o,this.options=s,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=w}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,o){const s=this.strings;let r=!1;if(void 0===s)t=U(this,t,e,0),r=!u(t)||t!==this._$AH&&t!==b,r&&(this._$AH=t);else{const o=t;let n,l;for(t=s[0],n=0;n<s.length-1;n++)l=U(this,o[i+n],e,n),l===b&&(l=this._$AH[n]),r||(r=!u(l)||l!==this._$AH[n]),l===w?t=w:t!==w&&(t+=(null!=l?l:"")+s[n+1]),this._$AH[n]=l}r&&!o&&this.k(t)}k(t){t===w?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class N extends k{constructor(){super(...arguments),this.type=3}k(t){this.element[this.name]=t===w?void 0:t}}class O extends k{constructor(){super(...arguments),this.type=4}k(t){t&&t!==w?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name)}}class M extends k{constructor(t,e,i,o,s){super(t,e,i,o,s),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=U(this,t,e,0))&&void 0!==i?i:w)===b)return;const o=this._$AH,s=t===w&&o!==w||t.capture!==o.capture||t.once!==o.once||t.passive!==o.passive,r=t!==w&&(o===w||s);s&&this.element.removeEventListener(this.name,this,o),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class R{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){U(this,t)}}null===(o=globalThis.litHtmlPolyfillSupport)||void 0===o||o.call(globalThis,T,H),(null!==(s=globalThis.litHtmlVersions)&&void 0!==s?s:globalThis.litHtmlVersions=[]).push("2.0.0")},392:(t,e,i)=>{i.d(e,{iv:()=>a});const o=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,s=Symbol(),r=new Map;class n{constructor(t,e){if(this._$cssResult$=!0,e!==s)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=r.get(this.cssText);return o&&void 0===t&&(r.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const l=t=>new n("string"==typeof t?t:t+"",s),a=(t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,o)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[o+1]),t[0]);return new n(i,s)},d=o?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return l(e)})(t):t;var h,c;const u={toAttribute(t,e){switch(e){case Boolean:t=t?"":null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},p=(t,e)=>e!==t&&(e==e||t==t),v={attribute:!0,type:String,converter:u,reflect:!1,hasChanged:p};class g extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const o=this._$Eh(i,e);void 0!==o&&(this._$Eu.set(o,i),t.push(o))})),t}static createProperty(t,e=v){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,o=this.getPropertyDescriptor(t,i,e);void 0!==o&&Object.defineProperty(this.prototype,t,o)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(o){const s=this[t];this[e]=o,this.requestUpdate(t,s,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||v}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(d(t))}else void 0!==t&&e.push(d(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ev=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Ep(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Em)&&void 0!==e?e:this._$Em=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Em)||void 0===e||e.splice(this._$Em.indexOf(t)>>>0,1)}_$Ep(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{o?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),o=window.litNonce;void 0!==o&&i.setAttribute("nonce",o),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$Eg(t,e,i=v){var o,s;const r=this.constructor._$Eh(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(s=null===(o=i.converter)||void 0===o?void 0:o.toAttribute)&&void 0!==s?s:u.toAttribute)(e,i.type);this._$Ei=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$Ei=null}}_$AK(t,e){var i,o,s;const r=this.constructor,n=r._$Eu.get(t);if(void 0!==n&&this._$Ei!==n){const t=r.getPropertyOptions(n),l=t.converter,a=null!==(s=null!==(o=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==o?o:"function"==typeof l?l:null)&&void 0!==s?s:u.fromAttribute;this._$Ei=n,this[n]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let o=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||p)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$ES&&(this._$ES=new Map),this._$ES.set(t,i))):o=!1),!this.isUpdatePending&&o&&(this._$Ev=this._$EC())}async _$EC(){this.isUpdatePending=!0;try{await this._$Ev}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$ET()}catch(t){throw e=!1,this._$ET(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Em)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$ET(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ev}shouldUpdate(t){return!0}update(t){void 0!==this._$ES&&(this._$ES.forEach(((t,e)=>this._$Eg(e,this[e],t))),this._$ES=void 0),this._$ET()}updated(t){}firstUpdated(t){}}g.finalized=!0,g.elementProperties=new Map,g.elementStyles=[],g.shadowRootOptions={mode:"open"},null===(h=globalThis.reactiveElementPolyfillSupport)||void 0===h||h.call(globalThis,{ReactiveElement:g}),(null!==(c=globalThis.reactiveElementVersions)&&void 0!==c?c:globalThis.reactiveElementVersions=[]).push("1.0.0");var $,_,f,m=i(692);class y extends g{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=(0,m.sY)(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return m.Jb}}y.finalized=!0,y._$litElement$=!0,null===($=globalThis.litElementHydrateSupport)||void 0===$||$.call(globalThis,{LitElement:y}),null===(_=globalThis.litElementPolyfillSupport)||void 0===_||_.call(globalThis,{LitElement:y}),(null!==(f=globalThis.litElementVersions)&&void 0!==f?f:globalThis.litElementVersions=[]).push("3.0.0")}},i={};function o(t){var s=i[t];if(void 0!==s)return s.exports;var r=i[t]={exports:{}};return e[t](r,r.exports,o),r.exports}o.d=(t,e)=>{for(var i in e)o.o(e,i)&&!o.o(t,i)&&Object.defineProperty(t,i,{enumerable:!0,get:e[i]})},o.o=(t,e)=>Object.prototype.hasOwnProperty.call(t,e),t=o(392),new Event("crowdDialogHide",{bubbles:!0,composed:!0}),new Event("crowdDialogShow",{bubbles:!0,composed:!0}),Boolean,t.iv`
        :host .dialog-container {
            pointer-events: none;
            visibility:hidden;
            background-color: transparent;
            transition-property: visibility;
            transition-duration: var(--crowd-dialog-container-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-dialog-container-transition-function, ease-in-out);
            transition-delay: var(--crowd-dialog-container-transition-delay, 0.3s);
        }
        :host([open]) .dialog-container {
            pointer-events: all;
            visibility:visible;
            transition-property: none;
        }
        :host .dialog-overlay {
            transition-property: background-color;
            transition-duration: var(--crowd-dialog-overlay-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-dialog-overlay-transition-function, ease-in-out);
            transition-delay: var(--crowd-dialog-overlay-transition-delay, 0.3s);
        }
        :host([open]) .dialog-overlay {
            background-color: var(--crowd-dialog-overlay-background, rgba(0,0,0,0.3));
        }
        dialog::backdrop {
            background-color: var(--crowd-dialog-overlay-background, rgba(0,0,0,0.3));
        }
        :host dialog {
            transform: scale(0);
        }
        :host([open]) dialog {
            transform: scale(1);
            transition-delay: var(--crowd-dialog-transition-delay, 0.2s);
        }
        dialog {
            all:unset;
            display: block;
        }
        dialog {
            background-color: var(--crowd-dialog-background, white);
            padding: var(--crowd-dialog-padding-vertical,1em) var(--crowd-dialog-padding-horizontal,1em);
            height: min(var(--height,calc(100% - (2 * var(--crowd-dialog-spacing-vertical,1em)))), 100%);
            width: min(var(--width,calc(100% - (2 * var(--crowd-dialog-spacing-horiztonal,1em)))), 100%);
            overflow-y: scroll;
            transition-property: transform;
            transition-duration: var(--crowd-dialog-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-dialog-transition-function, ease-in-out);
            position: relative;
            border-radius: var(--crowd-dialog-border-radius,0px);
            box-shadow: var(--crowd-dialog-box-shadow,
                0 1px 1px hsl(0deg 0% 0% / 0.075),
                0 2px 2px hsl(0deg 0% 0% / 0.075),
                0 4px 4px hsl(0deg 0% 0% / 0.075),
                0 8px 8px hsl(0deg 0% 0% / 0.075),
                0 16px 16px hsl(0deg 0% 0% / 0.075)
            );
                
        }
        h2 {
            margin: var(--crowd-dialog-heading-margin-top, 0px) 0 var(--crowd-dialog-heading-margin-bottom, 0px);
        }
        .dialog-container {
            display: grid;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            place-items: center;
            padding: var(--crowd-dialog-spacing-vertical, 2em) var(--crowd-dialog-spacing-horizontal, 2em);
            z-index: var(--crowd-dialog-z-index,9999);
        }
        .dialog-overlay {
            position:absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            cursor: pointer;
        }
        dialog > button.dialog-close {
            -webkit-appearance: none;
            background-color: var(--crowd-dialog-close-background,transparent);
            color: var(--crowd-dialog-close-color, black);
            border: var(--crowd-dialog-close-border-width,0px) var(--crowd-dialog-close-border-type,solid) var(--crowd-dialog-close-border-color,transparent);
            font-size: var(--crowd-dialog-close-font-size,2rem);
            line-height: 1;
            position: absolute;
            top: 0;
            right: 0;
            cursor: pointer;
        }
        @media (hover: hover) {
            dialog > button.dialog-close:hover {
                opacity: var(--crowd-dialog-close-hover-opacity, 0.6);
            }
        }
        @media (prefers-reduced-motion: reduce) {
            :host {
                --crowd-dialog-container-transition-duration: 0s;
                --crowd-dialog-container-transition-delay: 0s;
                --crowd-dialog-overlay-transition-duration: 0s;
                --crowd-dialog-overlay-transition-delay: 0s;
                --crowd-dialog-transition-duration: 0s;
                --crowd-dialog-transition-delay: 0s;
            }
        }
    `})();