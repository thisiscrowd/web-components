/*! For license information please see dropdown.js.LICENSE.txt */
(()=>{"use strict";var t,e,i={515:(t,e,i)=>{i.d(e,{P:()=>r});var o=i(392),s=i(414);class r extends o.oi{static properties={id:{type:String},show:{type:Boolean,reflect:!0},value:{type:String,reflect:!0},selectedLabel:{type:String},name:{type:String},placeholder:{type:String},required:{type:Boolean},label:{type:String},clearable:{type:Boolean},multiple:{type:Boolean},hoist:{type:Boolean},errorMessage:{type:String},showSuccess:{type:Boolean},success:{type:Boolean,reflect:!0},successMessage:{type:String},invalid:{type:Boolean,reflect:!0},_childIndex:{type:Number},_multiSelect:{type:Array},_multiLabel:{type:Array}};static styles=o.iv`
        :host {
            display: inline-block;
        }
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        .wrapper {
            padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
            max-width: 100%;
            position:relative;
        }
        label {
            display: inline-block;
            color: var(--crowd-input-label-color, inherit);
            margin-bottom: var(--crowd-input-label-spacing, 0.5em);
        }
        .select-dropdown {
            box-sizing: border-box;
            position: absolute;
            top:calc(100% - var(--crowd-input-wrapper-padding-vertical, 0) + var(--crowd-select-dropdown-spacing,2px));
            left: 0;
            width: 100%;
            max-height: var(--crowd-select-dropdown-max-height, 50vh);
            overflow-y: scroll;
            padding: var(--crowd-select-dropdown-padding-vertical, 0.5em) var(--crowd-select-dropdown-padding-horizontal, 0.5em);
            pointer-events: none;
            transition-property: opacity, transform;
            transition-duration: var(--crowd-select-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-select-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-transition-delay, 0s);
            opacity: 0;
            transform: scaleY(0.5);
            transform-origin: top center;
            background-color: var(--crowd-select-dropdown-background-color, white);
            z-index: var(--crowd-select-dropdown-z-index, 999);
            box-shadow: var(
                --crowd-select-dropdown-box-shadow,
                0 2px 8px rgba(0, 0, 0, 0.1)
            );
            border: var(--crowd-select-dropdown-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-select-dropdown-border-color, transparent);
        }
        :host([hoist]) .select-dropdown {
            position:fixed;
            top: auto;
            left: auto;
        }
        :host([show]) .select-dropdown {
            opacity: 1;
            transform: scale(1);
            pointer-events: all;
        }
        input,.multiple-items {
            -webkit-appearance: none;
            background-color: transparent;
            border: none;
            height: 100%;
            width: calc(100% - (2 * var(--crowd-input-padding-horizontal, 1em)));
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            padding: 0 var(--crowd-input-padding-horizontal, 1em);
            font-weight: var(--crowd-input-font-weight,400);
            height: calc(
                var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
            );
            caret-color: transparent;
            cursor: pointer;
            text-transform: var(--crowd-input-text-transform);
        }
        input.multi {
            opacity: 0;
        }
        input:focus-visible, input:active {
            outline: none;
        }
        input::placeholder {
            color: var(--crowd-input-placeholder-color, inherit);
        }
        .input-container {
            color: var(--crowd-input-color, inherit);
            background-color: var(--crowd-input-background, white);
            border: var(--crowd-input-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, transparent);
            border-radius: var(--crowd-input-border-radius, 0px);
            font-size: var(--crowd-input-font-size,1rem);
            padding: var(--crowd-input-padding-vertical, 1em) 0;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: stretch;
            position: relative;
        }
        .input-container:focus-within, .input-container:focus-visible {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        [part='overlay'] {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            pointer-events: none;
            z-index: calc(var(--crowd-select-dropdown-z-index, 999) - 1);
        }
        :host([show]) [part='overlay'] {
            pointer-events: all;
        }
        button {
            -webkit-appearance: none;
            background-color: transparent;
            color: var(--crowd-select-icon-color, inherit);
            border: none;
            padding: 0;
            display: grid;
            place-items: center;
            transition-property: transform,color;
            transition-duration: var(--crowd-select-icon-transition-duration,0.15s);
            transition-timing-function: var(--crowd-select-icon-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-icon-transition-delay, 0s);
            margin-right: var(--crowd-input-padding-horizontal, 1em);
            cursor: pointer;
            position:relative;
            z-index: 2;
        }
        .error,.success {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        .success {
            color: var(--crowd-input-success-message-color, lime);
        }
        @media (hover: hover) {
            button:hover {
                color: var(--crowd-select-icon-hover-color, inherit);
            }
        }
        :host([show]) button[part='toggle'] {
            transform: rotate(180deg);
        }
        :host([invalid]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-error-message-color, red);
        }
        :host([success]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-success-message-color, lime);
        }
        .multiple-items {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            bottom: 0;
            left: 0;
            right: 0;
            overflow-y: hidden;
            overflow-x: scroll;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
            gap: 4px;
            z-index: 1;
        }
        @supports (::-moz-range-track) {
            .multiple-items {
                scrollbar-width: thin;
                scrollbar-color: var(--crowd-select-multi-scrollbar-color,#000);
            }
        }
        .multiple-items::-webkit-scrollbar {
            height: 2px;
            background-color: var(--crowd-select-multi-scrollbar-background,#aaa);
        }
        .multiple-items::-moz-scrollbar-button, .multiple-items::-webkit-scrollbar-button {
            width: 0px;
            display: none;
        }
        .multiple-items::-webkit-scrollbar-thumb {
            background: var(--crowd-select-multi-scrollbar-color,#000);
        }
        .multiple-items crowd-badge {
            pointer-events: auto;
            gap: 2px;
        }
    `;constructor(){super(),this.placeholder="Please select",this.show=!1,this.childIndex=-1,this.invalid=!1}connectedCallback(){super.connectedCallback(),this.id="select-"+Date.now(),new s.z(this),this._multiSelect=[],this._multiLabel=[],this.multiple&&this.value&&(this._multiSelect=this.value.split(","),this._multiLabel=this.value.split(","),console.log(this._multiSelect),this._multiSelect.length>0?(this.value=this._multiSelect.join(),this.selectedLabel=this._multiLabel.join()):(this.value="",this.selectedLabel=""),console.log(this.value))}firstUpdated(){let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{t.addEventListener("click",(()=>{this._selectOption(t),this.multiple||this.close()}),!1),console.log(t.value.toUpperCase()),this.value&&this.value.toUpperCase().includes(t.value.toUpperCase())&&t.click()}))}updated(){let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{this.value&&this.value==t.value?(t.isActive=!0,this.selectedLabel!=t.innerHTML.replace(/(<([^>]+)>)/gi,"")&&(this.selectedLabel=t.innerHTML.replace(/(<([^>]+)>)/gi,""))):this.multiple&&this.value&&this._multiSelect.indexOf(t.value)>-1?t.isActive=!0:t.isActive=!1}))}open(){this.show=!0}close(){this.show=!1,this.childIndex=-1,this._blurOptions()}_blurOptions(){let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{t.blur()}))}validate(){this.invalid=!1,this.success=!1,this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0)),!this.invalid&&this.showSuccess&&(this.success=!0);const t=new CustomEvent("crowdValidate",{detail:{value:this.value}});this.dispatchEvent(t)}toggle(){this.show=!this.show,this.show||(this.childIndex=-1,this._blurOptions())}clear(){this._multiSelect=[],this._multiLabel=[],this.value="",this.selectedLabel="";let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{t.isActive=!1})),this.validate(),this._dispatchChange()}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onInput(t){t.preventDefault(),this.invalid=!1}_keyDown(t){if("Tab"===t.key)return;t.preventDefault();let e=[].slice.call(this.querySelectorAll("crowd-option"));"Enter"===t.key?(this.open(),-1!=this.childIndex&&e[this.childIndex].click()):"ArrowDown"===t.key?(this.open(),e[this.childIndex]&&e[this.childIndex].blur(),e.length==this.childIndex+1?this.childIndex=0:this.childIndex=this.childIndex+1,e[this.childIndex]&&e[this.childIndex].focus()):"ArrowUp"===t.key?(this.open(),e[this.childIndex]&&e[this.childIndex].blur(),0==this.childIndex?this.childIndex=e.length-1:this.childIndex=this.childIndex-1,e[this.childIndex]&&e[this.childIndex].focus()):(this.open(),e&&e.forEach(((i,o)=>{if(i.value.toUpperCase()[0]==t.key.toUpperCase())return e.forEach((t=>t.blur())),i.focus(),void(this.childIndex=o)})))}_selectOption(t){t&&t.value&&t.innerHTML||console.warn("<crowd-select>: Could not find element for option",t);let e=t.value,i=t.innerHTML.replace(/(<([^>]+)>)/gi,"");this.multiple?(this._multiSelect.indexOf(e)<0?(this._multiSelect.push(e),this._multiLabel.push(i)):(this._multiSelect.splice(this._multiSelect.indexOf(e),1),this._multiLabel.splice(this._multiLabel.indexOf(i),1)),this._multiSelect.length>0?(this.value=this._multiSelect.join(),this.selectedLabel=this._multiLabel.join()):(this.value="",this.selectedLabel="")):(this.value=e,this.selectedLabel=i),this.validate(),this._dispatchChange()}_hoistPosition(){let t=this.renderRoot.querySelector(".select-dropdown"),e=this.renderRoot.querySelector(".input-container");t&&e&&(t.style.top=e.getBoundingClientRect().bottom+"px",t.style.left=e.getBoundingClientRect().left+"px",t.style.width=e.getBoundingClientRect().width+"px"),requestAnimationFrame((()=>{this._hoistPosition()}))}render(){let t="";this.clearable&&this.value&&(t=o.dy`
                <button @click='${this.clear}' part="clear" name="clear" aria-label="clear select">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </button>
            `);let e=o.dy`
            <div class='select-dropdown'>
                <slot></slot>
            </div>
        `;this.hoist&&requestAnimationFrame((()=>{this._hoistPosition()}));let i="";this.label&&(i=o.dy`
                <label @click='${this.open}' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `);let s="";this.invalid&&(s=o.dy`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let r="";this.success&&(r=o.dy`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `);let n=o.dy``;return this.multiple&&this._multiSelect.length&&(n=o.dy`
                <div class='multiple-items' @click='${this.open}'>
                    ${this._multiSelect.map((t=>o.dy`<crowd-badge pill @click='${e=>{e.stopImmediatePropagation(),this._selectOption(this.querySelector(`crowd-option[value='${t}']`))}}'>${t}<crowd-icon name='x'></crowd-icon></crowd-badge>`))}
                </div>
            `),o.dy`
            <div part='wrapper' class='wrapper'>
                ${i}
                <div part='container' class='input-container' @click='${this.open}'>
                    <input inputmode='none' class='${this._multiSelect.length?"multi":""}' @change='${this._dispatchChange}' @keydown='${this._keyDown}' @input='${this._onInput}' id='${this.id}' type='text' name='${this.name}' value='${this.selectedLabel}' placeholder='${this.placeholder}' required='${this.required}' />
                    ${n}
                    ${t}
                    <slot name="icon"></slot>
                    <button @click='${this.toggle}' part="toggle" name="toggle" aria-label="toggle dropdown">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </button>
                </div>
                ${s}
                ${r}
                ${this.hoist?"":e}
            </div>
            <div @click='${this.close}' part='overlay'></div>
            ${this.hoist?e:""}
        `}}Boolean,Boolean,o.iv`
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        [part='container'] {
            padding: var(--crowd-option-padding-vertical, 0.2em) var(--crowd-option-padding-horizontal, 1em);
            font-family: var(--crowd-option-font-family, inherit);
            font-size: var(--crowd-option-font-size, inherit);
            font-weight: var(--crowd-option-font-weight, inherit);
            color: var(--crowd-option-color, inherit);
            background-color: var(--crowd-option-background-color, transparent);
            width: calc(100% - (1 * var(--crowd-option-padding-horizontal, 1em)));
            margin: 0 calc(-1 * var(--crowd-select-dropdown-padding-horizontal, 0.5em));
            cursor: pointer;
            transition-property: color, background-color;
            transition-duration: var(--crowd-option-transition-duration,0.15s);
            transition-timing-function: var(--crowd-option-transition-timing-function, ease-in-out);
            transition-delay: var(--crowd-option-transition-delay, 0s);
            border-radius: var(--crowd-option-border-radius,0px);
        }
        @media  (hover: hover) {
            [part='container']:hover {
                color: var(--crowd-option-hover-color, inherit);
                background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
            }
        }
        [part='container']:focus-visible, :host([focus]) [part='container'],:host([active]) [part='container'] {
            color: var(--crowd-option-hover-color, inherit);
            background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
        }
    `},414:(t,e,i)=>{i.d(e,{z:()=>o});class o{constructor(t){this.element=t,this.form=t.closest("form"),this.bindHandleFormData=this.handleFormData.bind(this),this.form&&this.element.name&&this.form.addEventListener("formdata",this.bindHandleFormData,!1),this.bindHandleFormSubmit=this.handleFormSubmit.bind(this),this.form&&this.form.addEventListener("submit",this.bindHandleFormSubmit,!1)}handleFormSubmit(t){const e=this.element.disabled;this.element.validate&&this.element.validate(),this.form&&!this.form.noValidate&&!e&&this.element.invalid&&(t.preventDefault(),t.stopImmediatePropagation())}handleFormData(t){for(var e of(null==this.element.value?t.formData.append(this.element.name,null):Array.isArray(this.element.value)?this.element.value.forEach((e=>{t.formData.append(this.element.name,e.toString())})):(console.log(this.element.name,this.element.value.toString()),t.formData.append(this.element.name,this.element.value.toString())),t.formData.entries()))console.log(e[0]+", "+e[1])}}},692:(t,e,i)=>{var o,s;i.d(e,{dy:()=>y,Jb:()=>A,sY:()=>E});const r=globalThis.trustedTypes,n=r?r.createPolicy("lit-html",{createHTML:t=>t}):void 0,l=`lit$${(Math.random()+"").slice(9)}$`,a="?"+l,c=`<${a}>`,h=document,d=(t="")=>h.createComment(t),p=t=>null===t||"object"!=typeof t&&"function"!=typeof t,u=Array.isArray,v=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,m=/-->/g,g=/>/g,w=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,f=/'/g,b=/"/g,$=/^(?:script|style|textarea)$/i,_=t=>(e,...i)=>({_$litType$:t,strings:e,values:i}),y=_(1),A=(_(2),Symbol.for("lit-noChange")),x=Symbol.for("lit-nothing"),S=new WeakMap,E=(t,e,i)=>{var o,s;const r=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:e;let n=r._$litPart$;if(void 0===n){const t=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:null;r._$litPart$=n=new L(e.insertBefore(d(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n},C=h.createTreeWalker(h,129,null,!1),k=(t,e)=>{const i=t.length-1,o=[];let s,r=2===e?"<svg>":"",a=v;for(let e=0;e<i;e++){const i=t[e];let n,h,d=-1,p=0;for(;p<i.length&&(a.lastIndex=p,h=a.exec(i),null!==h);)p=a.lastIndex,a===v?"!--"===h[1]?a=m:void 0!==h[1]?a=g:void 0!==h[2]?($.test(h[2])&&(s=RegExp("</"+h[2],"g")),a=w):void 0!==h[3]&&(a=w):a===w?">"===h[0]?(a=null!=s?s:v,d=-1):void 0===h[1]?d=-2:(d=a.lastIndex-h[2].length,n=h[1],a=void 0===h[3]?w:'"'===h[3]?b:f):a===b||a===f?a=w:a===m||a===g?a=v:(a=w,s=void 0);const u=a===w&&t[e+1].startsWith("/>")?" ":"";r+=a===v?i+c:d>=0?(o.push(n),i.slice(0,d)+"$lit$"+i.slice(d)+l+u):i+l+(-2===d?(o.push(void 0),e):u)}const h=r+(t[i]||"<?>")+(2===e?"</svg>":"");return[void 0!==n?n.createHTML(h):h,o]};class T{constructor({strings:t,_$litType$:e},i){let o;this.parts=[];let s=0,n=0;const c=t.length-1,h=this.parts,[p,u]=k(t,e);if(this.el=T.createElement(p,i),C.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(o=C.nextNode())&&h.length<c;){if(1===o.nodeType){if(o.hasAttributes()){const t=[];for(const e of o.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(l)){const i=u[n++];if(t.push(e),void 0!==i){const t=o.getAttribute(i.toLowerCase()+"$lit$").split(l),e=/([.?@])?(.*)/.exec(i);h.push({type:1,index:s,name:e[2],strings:t,ctor:"."===e[1]?H:"?"===e[1]?O:"@"===e[1]?I:z})}else h.push({type:6,index:s})}for(const e of t)o.removeAttribute(e)}if($.test(o.tagName)){const t=o.textContent.split(l),e=t.length-1;if(e>0){o.textContent=r?r.emptyScript:"";for(let i=0;i<e;i++)o.append(t[i],d()),C.nextNode(),h.push({type:2,index:++s});o.append(t[e],d())}}}else if(8===o.nodeType)if(o.data===a)h.push({type:2,index:s});else{let t=-1;for(;-1!==(t=o.data.indexOf(l,t+1));)h.push({type:7,index:s}),t+=l.length-1}s++}}static createElement(t,e){const i=h.createElement("template");return i.innerHTML=t,i}}function P(t,e,i=t,o){var s,r,n,l;if(e===A)return e;let a=void 0!==o?null===(s=i._$Cl)||void 0===s?void 0:s[o]:i._$Cu;const c=p(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==c&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===c?a=void 0:(a=new c(t),a._$AT(t,i,o)),void 0!==o?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[o]=a:i._$Cu=a),void 0!==a&&(e=P(t,a._$AS(t,e.values),a,o)),e}class U{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:o}=this._$AD,s=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:h).importNode(i,!0);C.currentNode=s;let r=C.nextNode(),n=0,l=0,a=o[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new L(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new M(r,this,t)),this.v.push(e),a=o[++l]}n!==(null==a?void 0:a.index)&&(r=C.nextNode(),n++)}return s}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class L{constructor(t,e,i,o){var s;this.type=2,this._$AH=x,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=o,this._$Cg=null===(s=null==o?void 0:o.isConnected)||void 0===s||s}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=P(this,t,e),p(t)?t===x||null==t||""===t?(this._$AH!==x&&this._$AR(),this._$AH=x):t!==this._$AH&&t!==A&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.S(t):(t=>{var e;return u(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.M(t):this.$(t)}A(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}S(t){this._$AH!==t&&(this._$AR(),this._$AH=this.A(t))}$(t){this._$AH!==x&&p(this._$AH)?this._$AA.nextSibling.data=t:this.S(h.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:o}=t,s="number"==typeof o?this._$AC(t):(void 0===o.el&&(o.el=T.createElement(o.h,this.options)),o);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===s)this._$AH.m(i);else{const t=new U(s,this),e=t.p(this.options);t.m(i),this.S(e),this._$AH=t}}_$AC(t){let e=S.get(t.strings);return void 0===e&&S.set(t.strings,e=new T(t)),e}M(t){u(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,o=0;for(const s of t)o===e.length?e.push(i=new L(this.A(d()),this.A(d()),this,this.options)):i=e[o],i._$AI(s),o++;o<e.length&&(this._$AR(i&&i._$AB.nextSibling,o),e.length=o)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class z{constructor(t,e,i,o,s){this.type=1,this._$AH=x,this._$AN=void 0,this.element=t,this.name=e,this._$AM=o,this.options=s,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=x}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,o){const s=this.strings;let r=!1;if(void 0===s)t=P(this,t,e,0),r=!p(t)||t!==this._$AH&&t!==A,r&&(this._$AH=t);else{const o=t;let n,l;for(t=s[0],n=0;n<s.length-1;n++)l=P(this,o[i+n],e,n),l===A&&(l=this._$AH[n]),r||(r=!p(l)||l!==this._$AH[n]),l===x?t=x:t!==x&&(t+=(null!=l?l:"")+s[n+1]),this._$AH[n]=l}r&&!o&&this.k(t)}k(t){t===x?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class H extends z{constructor(){super(...arguments),this.type=3}k(t){this.element[this.name]=t===x?void 0:t}}class O extends z{constructor(){super(...arguments),this.type=4}k(t){t&&t!==x?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name)}}class I extends z{constructor(t,e,i,o,s){super(t,e,i,o,s),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=P(this,t,e,0))&&void 0!==i?i:x)===A)return;const o=this._$AH,s=t===x&&o!==x||t.capture!==o.capture||t.once!==o.once||t.passive!==o.passive,r=t!==x&&(o===x||s);s&&this.element.removeEventListener(this.name,this,o),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class M{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){P(this,t)}}null===(o=globalThis.litHtmlPolyfillSupport)||void 0===o||o.call(globalThis,T,L),(null!==(s=globalThis.litHtmlVersions)&&void 0!==s?s:globalThis.litHtmlVersions=[]).push("2.0.0")},392:(t,e,i)=>{i.d(e,{oi:()=>$,iv:()=>a,dy:()=>b.dy});const o=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,s=Symbol(),r=new Map;class n{constructor(t,e){if(this._$cssResult$=!0,e!==s)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=r.get(this.cssText);return o&&void 0===t&&(r.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const l=t=>new n("string"==typeof t?t:t+"",s),a=(t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,o)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[o+1]),t[0]);return new n(i,s)},c=o?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return l(e)})(t):t;var h,d;const p={toAttribute(t,e){switch(e){case Boolean:t=t?"":null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},u=(t,e)=>e!==t&&(e==e||t==t),v={attribute:!0,type:String,converter:p,reflect:!1,hasChanged:u};class m extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const o=this._$Eh(i,e);void 0!==o&&(this._$Eu.set(o,i),t.push(o))})),t}static createProperty(t,e=v){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,o=this.getPropertyDescriptor(t,i,e);void 0!==o&&Object.defineProperty(this.prototype,t,o)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(o){const s=this[t];this[e]=o,this.requestUpdate(t,s,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||v}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(c(t))}else void 0!==t&&e.push(c(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ev=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Ep(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Em)&&void 0!==e?e:this._$Em=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Em)||void 0===e||e.splice(this._$Em.indexOf(t)>>>0,1)}_$Ep(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{o?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),o=window.litNonce;void 0!==o&&i.setAttribute("nonce",o),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$Eg(t,e,i=v){var o,s;const r=this.constructor._$Eh(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(s=null===(o=i.converter)||void 0===o?void 0:o.toAttribute)&&void 0!==s?s:p.toAttribute)(e,i.type);this._$Ei=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$Ei=null}}_$AK(t,e){var i,o,s;const r=this.constructor,n=r._$Eu.get(t);if(void 0!==n&&this._$Ei!==n){const t=r.getPropertyOptions(n),l=t.converter,a=null!==(s=null!==(o=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==o?o:"function"==typeof l?l:null)&&void 0!==s?s:p.fromAttribute;this._$Ei=n,this[n]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let o=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||u)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$ES&&(this._$ES=new Map),this._$ES.set(t,i))):o=!1),!this.isUpdatePending&&o&&(this._$Ev=this._$EC())}async _$EC(){this.isUpdatePending=!0;try{await this._$Ev}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$ET()}catch(t){throw e=!1,this._$ET(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Em)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$ET(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ev}shouldUpdate(t){return!0}update(t){void 0!==this._$ES&&(this._$ES.forEach(((t,e)=>this._$Eg(e,this[e],t))),this._$ES=void 0),this._$ET()}updated(t){}firstUpdated(t){}}m.finalized=!0,m.elementProperties=new Map,m.elementStyles=[],m.shadowRootOptions={mode:"open"},null===(h=globalThis.reactiveElementPolyfillSupport)||void 0===h||h.call(globalThis,{ReactiveElement:m}),(null!==(d=globalThis.reactiveElementVersions)&&void 0!==d?d:globalThis.reactiveElementVersions=[]).push("1.0.0");var g,w,f,b=i(692);class $ extends m{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=(0,b.sY)(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return b.Jb}}$.finalized=!0,$._$litElement$=!0,null===(g=globalThis.litElementHydrateSupport)||void 0===g||g.call(globalThis,{LitElement:$}),null===(w=globalThis.litElementPolyfillSupport)||void 0===w||w.call(globalThis,{LitElement:$}),(null!==(f=globalThis.litElementVersions)&&void 0!==f?f:globalThis.litElementVersions=[]).push("3.0.0")}},o={};function s(t){var e=o[t];if(void 0!==e)return e.exports;var r=o[t]={exports:{}};return i[t](r,r.exports,s),r.exports}s.d=(t,e)=>{for(var i in e)s.o(e,i)&&!s.o(t,i)&&Object.defineProperty(t,i,{enumerable:!0,get:e[i]})},s.o=(t,e)=>Object.prototype.hasOwnProperty.call(t,e),t=s(392),e=s(515),Boolean,Boolean,e.P.styles,t.iv`
            :host {
                transform: none !important;
            }
            .input-container {
                background-color: transparent;
                border: none;
            }
            .select-dropdown {
                min-width: 100%;
                width: max-content;
            }
            :host([hoist]) .select-dropdown {
                min-width: auto;
                width: max-content;
            }
            :host([position*="top"]) .select-dropdown {
                top: auto;
                bottom: calc(100% + var(--crowd-input-wrapper-padding-vertical, 0px) - var(--crowd-select-dropdown-spacing,2px));
                transform-origin: center bottom;
            }
            :host([position*="right"]) .select-dropdown {
                top: auto;
                left: auto;
                right: 0px;
                bottom: calc(100% + var(--crowd-input-wrapper-padding-vertical, 0px) - var(--crowd-select-dropdown-spacing,2px));
                transform-origin: center bottom;
            }
        `})();