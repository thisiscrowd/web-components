import { LitElement, html, css } from 'lit';
const { DateTime } = require("luxon")
import {FormController} from './helpers/form-controller.js'

export class DatePicker extends LitElement {

    static properties = {
        value: { type: String, reflect: true },
        open: { type: Boolean, reflect: true },
        month: { type: Number },
        year: { type: Number },
        selected: { type: Object },
        label: {type: String},
        required: {type: Boolean},
        errorMessage: {type: String},
        invalid: {type: Boolean, reflect: true},
        format: { type: String, reflect: true },
        range: { type: Boolean },
        dateRange: {type: Array},
        rangeState: {type: Number},
        weekFormat: {type: String}
    }
    static styles = [
        css`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .container {
                position: relative;
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            label.label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            .calendar {
                display: grid;
                grid-template-columns: repeat(7,1fr);
                padding-top: var(--crowd-date-picker-calendar-padding-top,1em);
            }
            .panel {
                position: absolute;
                top: 100%;
                left: 0;
                width:max-content;
                transform-origin: top center;
                transform: scaleY(0);
                transition-property: transform;
                transition-duration: var(--crowd-date-picker-transition-duration, 0.15s);
                transition-delay: var(--crowd-date-picker-transition-delay, 0s);
                transition-timing-function: var(--crowd-date-picker-transition-ease, ease-in-out);
            }
            :host([open]) .panel {
                transform: scaleY(1);
            }
            .panel {
                font-size: var(--crowd-date-time-picker-font-size,0.7rem);
                --crowd-input-font-size: var(--crowd-date-time-picker-font-size,0.7rem);
                --crowd-input-padding-vertical: 0px;
                background-color: var(--crowd-date-picker-panel-background-color, #fff);
                padding: var(--crowd-date-picker-panel-padding-vertical, 1em) var(--crowd-date-picker-panel-padding-horizontal, 1em);
                box-shadow: var(
                    --crowd-date-picker-panel-box-shadow,
                    0 2px 8px rgba(0, 0, 0, 0.1)
                );
                z-index: var(--crowd-date-picker-z-index, 999);
            }
            .calendar-nav {
                display: flex;
                justify-content: var(--crowd-date-picker-calendar-nav-justify, center);
                align-items: center;
                padding: var(--crowd-date-picker-calendar-nav-padding-vertical, 0) var(--crowd-date-picker-calendar-nav-padding-horizontal, 1em);
            }
            crowd-select::part(toggle) {
                font-size: 0.5em;
            }
            .month-picker {
                width: var(--crowd-date-picker-month-picker-width,13ch);
            }
            .year-picker {
                width: var(--crowd-date-picker-year-picker-width,12ch);
            }
            .calendar-nav crowd-button {
                margin: 0 auto;
                -webkit-appearance: none;
                cursor: pointer;
                --crowd-button-width: 1.5em;
                --crowd-button-padding-vertical: 0px;
                --crowd-button-padding-horizontal: 0px;
                --crowd-button-background-color: transparent;
                --crowd-button-hover-background-color: var(--crowd-date-picker-calendar-nav-button-hover-background-color, #f9f9f9);
                --crowd-button-border-width: 0px;
                --crowd-button-color: var(--crowd-menu-item-hover-color,inherit);
                --crowd-button-hover-color: var(--crowd-menu-item-hover-color,inherit);
            }
            .calendar-nav .active {
                text-decoration: underline;
            }
            .calendar-nav .today-button {
                margin-right: 0;
            }

            .weekday,.day {
                aspect-ratio: 1/1;
                padding: 5px;
            }

            .weekday {
                font-size: 0.5em;
            }

            .day {
                cursor: pointer;
                transition-property: background-color;
                transition-duration: var(--crowd-date-picker-transition-duration, 0.15s);
                transition-timing-function: var(--crowd-date-picker-transition-ease, ease-in-out);
                transition-delay: var(--crowd-date-picker-transition-delay, 0s);
                border: var(--crowd-date-picker-day-border-width,1px) var(--crowd-date-picker-day-border-style,solid) var(--crowd-date-picker-day-border-color,#f7f7f7);
            }

            @media (hover: hover) {
                .day:hover {
                    background-color: var(--crowd-date-picker-day-hover-background-color,#f9f9f9);
                }
            }
            .day:focus-visible {
                background-color: var(--crowd-date-picker-day-hover-background-color,#f9f9f9);
            }

            .day.today {
                text-decoration: underline;
            }
            .day.ranged.from ~ * {
                background-color: var(--crowd-date-picker-day-hover-background-color,#f9f9f9);
            }
            .day.ranged.from.to ~ *,.day.ranged.to ~ * {
                background-color: transparent;
            }

            .day.ranged,.day.ranged.from,.day.ranged.to, .day.active, .day:active {
                background-color: var(--crowd-date-picker-day-selected-background-color, #eee);
            }
            .action-buttons {
                margin-top: 0.5em;
                display: flex;
                gap: 1em;
                justify-content: space-between;
            }
        `
    ]

    _generateCalendar () {
        let lastDateOfCurrentMonth = new Date(this.year, this.month+1, 0).getDate()
        let days = []
        for (let index = 0; index < lastDateOfCurrentMonth; index++) {
            days[index] = DateTime.fromObject({
                day: index + 1,
                month: this.month + 1,
                year: this.year
            })
        }

        return days
    }

    _prevMonth() {
        if (this.month == 0) {
            this.month = 11
            this.year -= 1
        } else {
            this.month -= 1
        }
    }

    _nextMonth() {
        if (this.month == 11) {
            this.month = 0
            this.year += 1
        } else {
            this.month += 1
        }
    }

    _selectDay(day) {
        this.selected = day
        this.month = day.month - 1
        this.year = day.year
        if (this.range) {
            this.dateRange[this.rangeState] = this.selected
        }
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _onInput(e) {
        this.invalid = false
        this._dispatchChange()
    }

    _setValue() {
        if (this.range) {
            this.value = this.dateRange[0].toFormat(this.format) + ' - ' + this.dateRange[1].toFormat(this.format)
        } else {
            let dt = this.selected
            this.value = dt.toFormat(this.format)
        }
        this._dispatchChange()
    }

    _onKeydown(e) {
        if (e.key != 'Tab') {
            e.preventDefault()
        }
    }

    _showPicker() {
        this.open = true
    }

    _hidePicker() {
        this.open = false
    }

    _swapRange(range) {
        this.rangeState = range
        this.selected = this.dateRange[range]
    }

    _isSelected(day) {
        return day.hasSame(this.selected,'day') && day.hasSame(this.selected,'month') && day.hasSame(this.selected,'year')
    }

    _isRanged(day) {
        if (!this.range) {
            return false
        }
        let isFrom = day.hasSame(this.dateRange[0],'day') && day.hasSame(this.dateRange[0],'month') && day.hasSame(this.dateRange[0],'year')
        let isTo = day.hasSame(this.dateRange[1],'day') && day.hasSame(this.dateRange[1],'month') && day.hasSame(this.dateRange[1],'year')
        return isFrom || isTo
    }

    _dayRangeState(day) {
        let isFrom = day.hasSame(this.dateRange[0],'day') && day.hasSame(this.dateRange[0],'month') && day.hasSame(this.dateRange[0],'year')
        let isTo = day.hasSame(this.dateRange[1],'day') && day.hasSame(this.dateRange[1],'month') && day.hasSame(this.dateRange[1],'year')
        if (isFrom && isTo) {
            return 'from to'
        }
        if (isFrom) {
            return 'from'
        }
        if (isTo) {
            return 'to'
        }
    }

    connectedCallback() {
        super.connectedCallback()
        new FormController(this)
    }

    constructor() {
        super()
        this.year = new Date().getFullYear()
        this.month = new Date().getMonth()
        this.weekdays = new Array(7).fill(0)
        this.monthList = new Array(12).fill(0)
        let today = DateTime.now()
        this.selected = today
        this.format = 'dd/MM/y'
        this.dateRange = [today,today]
        this.rangeState = 0
        this._setValue()
        this.weekFormat = 'cccc'
    }

    render() {
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let label = ''
        if (this.label) {
            label = html`
                <label class='label' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        return html`
            <div class='container' @focusin='${this._showPicker}' @focusout='${this._hidePicker}'>
                ${label}
                <div class='input'>
                    <crowd-input part='input' @keydown='${this._onKeydown}' @crowdChange='${this._onInput}' type='text' value='${this.value}'></crowd-input>
                </div>
                <div class='panel' part='panel' tabindex='0'>
                    <div class='calendar-nav'>
                        <crowd-button circle @click='${this._prevMonth}'>
                            <slot name='prev-icon'>
                                <crowd-icon name='chevron-left'></crowd-icon>
                            </slot>
                        </crowd-button>
                        <p>
                            <crowd-select class='month-picker' @crowdChange='${(e) => this.month = parseInt(e.currentTarget.value)}' value='${this.month}'>
                                ${this.monthList.map((o,index) => {
                                    let month = DateTime.fromObject({month:index + 1})
                                    return html`<crowd-option value='${index}'>${month.monthLong}</crowd-option>`
                                })}
                            </crowd-select>
                            <crowd-select class='year-picker' @crowdChange='${(e) => this.year = parseInt(e.currentTarget.value)}' value='${this.year}'>
                                ${new Array(5).fill(0).map((o,index) => (
                                    html`
                                        <crowd-option value='${this.year - 5 + index}'>${this.year - 5 + index}</crowd-option>
                                    `
                                ))}
                                ${new Array(5).fill(0).map((o,index) => (
                                    html`
                                        <crowd-option value='${this.year + index}'>${this.year + index}</crowd-option>
                                    `
                                ))}
                            </crowd-select>
                        </p>
                        <crowd-button circle @click='${this._nextMonth}'>
                            <slot name='next-icon'>
                                <crowd-icon name='chevron-right'></crowd-icon>
                            </slot>
                        </crowd-button>
                        <crowd-button class='today-button' circle @click='${() => this._selectDay(DateTime.now())}'>
                            <slot name='today-icon'>
                                <crowd-icon name='calendar-event'></crowd-icon>
                            </slot>
                        </crowd-button>
                    </div>
                    ${this.range ? html`
                        <div class='calendar-nav'>
                            <crowd-button @click='${() => this._swapRange(0)}' class='${this.rangeState == 0 ? 'active' : ''}'>FROM</crowd-button>
                            <crowd-button @click='${() => this._swapRange(1)}' class='${this.rangeState == 1 ? 'active' : ''}'>TO</crowd-button>
                        </div>
                    ` : ''}
                    <div class='calendar'>
                        ${this.weekdays.map((o,index) => {
                            let weekday = DateTime.fromObject({weekday:index + 1})
                            return html`<div class='weekday'>${weekday.toFormat(this.weekFormat)}</div>`
                        })}
                        ${new Array(this._generateCalendar()[0].weekday - 1).fill(0).map((i,index) => (
                            html`<div class='day' @click='${this._prevMonth}'></div>`
                        ))}
                        ${this._generateCalendar().map((day,index) => {
                            return html`
                                <div tabindex='0' @click='${() => this._selectDay(day)}' class='day ${this._isRanged(day) ? `ranged ${this._dayRangeState(day)}` : ''} ${this._isSelected(day) ? 'active' : ''} ${day.hasSame(DateTime.now(),'day') ? 'today' : ''}' part='day'>
                                    ${day.day}
                                </div>
                            `
                        })}
                        ${new Array(7 - this._generateCalendar()[this._generateCalendar().length - 1].weekday).fill(0).map((i,index) => (
                            html`<div class='day' @click='${this._nextMonth}'></div>`
                        ))}
                    </div>
                    <div class='action-buttons'>
                        <crowd-button @click='${this._hidePicker}'>CANCEL</crowd-button>
                        <crowd-button @click='${() => {this._setValue();this._hidePicker()}}'>OK</crowd-button>
                    </div>
                </div>
                ${error}
                <slot name="help-text"></slot>
            </div>
        `;
    }
}
