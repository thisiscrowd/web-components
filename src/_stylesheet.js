import { LitElement, html, css } from 'lit';

export class Stylesheet extends LitElement {
    static properties = {
        _content: {type: String}
    }
    static styles = [
        css`
            :host {
                display: block;
            }
            pre {
                padding: 1em;
                border: 1px solid #eeeeee;
            }
        `
    ];

    _loop() {

    }

    _updateStylesheet() {
        this._content = document.documentElement.style.cssText.replace(/; /g,';\n')
    }

    connectedCallback() {
        super.connectedCallback()
        let customisers = [].slice.call(document.querySelectorAll('crowd-customiser'))
        if (customisers) {
            customisers.forEach(customiser => {
                customiser.addEventListener('crowdChange',() => this._updateStylesheet())
            })
        }
    }

    render() {
        return html`
            <pre>${this._content}</pre>
        `;
    }
}