import { AccordionGroup, AccordionItem } from './_accordion';
import { Alert } from './_alert';
import { Badge } from './_badge';
import { Button } from './_button';
import { Checkbox } from './_checkbox';
import { ColorPicker } from './_color-picker';
import { ContextMenu } from './_context-menu';
import { Customiser } from './_customiser';
import { DatePicker } from './_date-picker';
import { Dialog } from './_dialog';
import { Drawer } from './_drawer';
import { Dropdown } from './_dropdown';
import { Form } from './_form';
import { IconButton } from './_icon-button';
import { Icon } from './_icon';
import { Input } from './_input';
import { MenuItem } from './_menu-item';
import { Menu } from './_menu';
import { MultiToggle, MultiToggleItem } from './_multi-toggle';
import { Number } from './_number';
import { ProgressRing } from './_progress-ring';
import { Range } from './_range';
import { Select, Option } from './_select';
import { Spinner } from './_spinner';
import { Stylesheet } from './_stylesheet';
import { Switch } from './_switch';
import { TabGroup,Tab } from './_tabs';
import { TextArea } from './_textarea';
import { TimePicker } from './_time-picker';
import { ToastStack } from './_toast-stack';
import { Tooltip } from './_tooltip';
import { Video } from './_video';

import { Enhancer } from './_enhancer';

customElements.define('crowd-accordion-group', AccordionGroup);
customElements.define('crowd-accordion-item', AccordionItem);
customElements.define('crowd-alert', Alert);
customElements.define('crowd-badge', Badge);
customElements.define('crowd-button', Button)
customElements.define('crowd-checkbox', Checkbox);
customElements.define('crowd-color-picker', ColorPicker);
customElements.define('crowd-context-menu', ContextMenu);
customElements.define('crowd-customiser', Customiser);
customElements.define('crowd-date-picker', DatePicker);
customElements.define('crowd-dialog', Dialog);
customElements.define('crowd-drawer', Drawer);
customElements.define('crowd-dropdown', Dropdown);
customElements.define('crowd-form', Form);
customElements.define('crowd-icon-button', IconButton)
customElements.define('crowd-icon', Icon)
customElements.define('crowd-input', Input);
customElements.define('crowd-menu-item', MenuItem);
customElements.define('crowd-menu', Menu);
customElements.define('crowd-multi-toggle', MultiToggle);
customElements.define('crowd-multi-toggle-item', MultiToggleItem);
customElements.define('crowd-number', Number);
customElements.define('crowd-progress-ring', ProgressRing);
customElements.define('crowd-range', Range);
customElements.define('crowd-select', Select);
customElements.define('crowd-stylesheet', Stylesheet);
customElements.define('crowd-switch', Switch);
customElements.define('crowd-option', Option);
customElements.define('crowd-spinner', Spinner);
customElements.define('crowd-tab', Tab);
customElements.define('crowd-tab-group', TabGroup);
customElements.define('crowd-textarea', TextArea);
customElements.define('crowd-time-picker', TimePicker);
customElements.define('toast-stack', ToastStack);
customElements.define('crowd-tooltip', Tooltip);
customElements.define('crowd-video', Video);

new Enhancer().init()