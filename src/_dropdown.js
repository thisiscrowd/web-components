import { LitElement, html, css } from 'lit';
import {Select} from './_select'

export class Dropdown extends LitElement {

    static properties = {
        show: {type: Boolean, reflect: true},
        hoist: {type: Boolean},
        position: {type: String, reflect: true}
    };

    static styles = [
        Select.styles,
        css`
            :host {
                transform: none !important;
            }
            .input-container {
                background-color: transparent;
                border: none;
            }
            .select-dropdown {
                min-width: 100%;
                width: max-content;
            }
            :host([hoist]) .select-dropdown {
                min-width: auto;
                width: max-content;
            }
            :host([position*="top"]) .select-dropdown {
                top: auto;
                bottom: calc(100% + var(--crowd-input-wrapper-padding-vertical, 0px) - var(--crowd-select-dropdown-spacing,2px));
                transform-origin: center bottom;
            }
            :host([position*="right"]) .select-dropdown {
                top: auto;
                left: auto;
                right: 0px;
                bottom: calc(100% + var(--crowd-input-wrapper-padding-vertical, 0px) - var(--crowd-select-dropdown-spacing,2px));
                transform-origin: center bottom;
            }
        `
    ];

    open() {
        this.show = true
        this._dispatchOpen()
    }

    close() {
        this.show = false
        this._dispatchClose()
    }

    toggle() {
        this.show = !this.show
        if (this.show) {
            this._dispatchOpen()
        } else {
            this._dispatchClose()
        }
    }

    _dispatchOpen() {
        const event = new CustomEvent('crowdOpen');
        this.dispatchEvent(event);
    }

    _dispatchClose() {
        const event = new CustomEvent('crowdClose');
        this.dispatchEvent(event);
    }

    _hoistPosition() {
        let dropdownEl = this.renderRoot.querySelector('.select-dropdown')
        let container = this.renderRoot.querySelector('.input-container')
        if (dropdownEl && container) {
            dropdownEl.style.top = container.getBoundingClientRect().bottom + 'px';
            dropdownEl.style.left = container.getBoundingClientRect().left + 'px';
            dropdownEl.style.width = container.getBoundingClientRect().width + 'px';
        }
        requestAnimationFrame(() => {this._hoistPosition()})
    }

    constructor() {
        super()
        this.position = 'bottom'
    }

    render() {
        let dropdown = html`
            <div class='select-dropdown' part='dropdown'>
                <slot></slot>
            </div>
        `
        if (this.hoist) {
            requestAnimationFrame(() => {this._hoistPosition()})
        }
        return html`
            <div part='wrapper' class='wrapper'>
                <div @click='${this.open}' part='container' class='input-container'>
                    <slot name='trigger'></slot>
                </div>
                ${this.hoist ? '' : dropdown}
            </div>
            <div @click='${this.close}' part='overlay'></div>
            ${this.hoist ? dropdown : ''}
        `
    }
}
