import { LitElement, html, css } from 'lit';
import {FormController} from './helpers/form-controller.js'

export class Range extends LitElement {
    static properties = {
        value: {type: Number,reflect: true},
        min: {type: Number},
        max: {type: Number},
        step: {type: Number},
        notches: {type: Boolean},
        numbers: {type: Boolean},
        tooltip: {type: Boolean},
        required: {type: Boolean},
        label: {type: String},
        invalid: {type: Boolean},
        errorMessage: {type: String},
        hideMinMax: {type: Boolean}
    }
    static styles = [
        css`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
                width: var(--crowd-range-width,300px);

            }
            .container {
                position: relative;
                display: flex;
                flex-flow: row nowrap;
                justify-content: stretch;
                align-items: center;
                gap: var(--crowd-range-spacing,0.2em);
            }
            .container > span {
                flex: 0 1 auto;
            }
            .track {
                flex: 1 0 auto;
                position: relative;
                height: var(--crowd-range-track-height, 0.5em);
                border: var(--crowd-range-track-border-width, 1px) var(--crowd-range-track-border-style, solid) var(--crowd-range-track-border-color, #000);
                border-radius: var(--crowd-range-track-border-radius, 0px);
                background-color: var(--crowd-range-track-background-color,#eee);
                cursor: pointer;
            }
            .track::before {
                content: '';
                display: block;
                position:absolute;
                top: 0;
                right: calc(100% - var(--value));
                bottom: 0;
                left: 0;
                background-color: var(--crowd-range-indicator-color,#000);
            }
            .thumb {
                height: calc(var(--crowd-range-thumb-scale, 2) * var(--crowd-range-track-height, 0.5em));
                width: calc(var(--crowd-range-thumb-scale, 2) * var(--crowd-range-track-height, 0.5em));
                border: var(--crowd-range-track-border-width, 1px) var(--crowd-range-thumb-border-style, solid) var(--crowd-range-thumb-border-color, #000);
                border-radius: var(--crowd-range-thumb-border-radius, 50%);
                background-color: var(--crowd-range-thumb-background-color,#fff);
                position: absolute;
                top:50%;
                left: var(--value);
                transform: translate(-50%,-50%);
                pointer-events: none;
            }
            .track:focus-within .thumb {
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            input {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                -webkit-appearance: none;
                padding: 0;
                margin: 0;
                height: 0;
                width: 0;
                opacity: 0;
            }
            :host([notches]) .container {
                margin-top: calc(1ex + var(--crowd-range-notch-height,0.5em));
                
            }
            .notches {
                width: 100%;
                position: absolute;
                bottom: 100%;
                height: var(--crowd-range-notch-height,0.5em);
                left: 0;
                right: 0;
                pointer-events:none;
            }
            .notches > div:first-child,.notches > div:last-child {
                opacity: 0;
            }
            .notches > div {
                position: absolute;
                top: 0;
                height: 100%;
                left: var(--left);
                color: var(--crowd-range-number-color, inherit);
            }
            .notches > div.notched::before {
                content: '';
                display:block;
                position: absolute;
                top: 0;
                height: 100%;
                width: var(--crowd-range-notch-width,1px);
                background-color: var(--crowd-range-notch-color,#000);
            }
            .notches > div span {
                position: absolute;
                left: 50%;
                bottom: 100%;
                transform: translateX(-50%);
            }
            label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
        `
    ];

    constructor() {
        super()
        this.min = '0'
        this.max = '100'
        this.step = 1
        this.notches = false
        this.value = this.min
        this.tooltipTimer = null
        this.invalid = false
        this.hideMinMax = false
    }

    connectedCallback() {
        super.connectedCallback()
        this.id = 'range-' + Date.now()
        new FormController(this)
    }

    _dispatchPreChange() {
        const event = new CustomEvent('crowdMove');
        this.dispatchEvent(event);
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    // _onPointerDown(e) {
    //     this._setValue(e)
    //     let onMove = (e) => {
    //         this._setValue(e)
    //     }
    //     e.currentTarget.addEventListener('pointermove', onMove,false)
    //     let remove = (e) => {
    //         e.currentTarget.removeEventListener('pointermove', onMove, false)
    //         e.currentTarget.removeEventListener('pointerup', remove, false)
    //     }
    //     e.currentTarget.addEventListener('pointerup', remove,false)
    // }

    _roundToStep(number, step) {
        return Math.round(number / step) * step
    }

    _setValue(e) {
        this.value = this._roundToStep((e.offsetX / e.currentTarget.getBoundingClientRect().width) * this.max,this.step)
        if (this.value > this.max) {
            this.value = this.max
        }
        if (this.value < this.min) {
            this.value = this.min
        }
        let input = this.renderRoot.querySelector('input')
        if (input) {
            input.focus()
        }
        this.invalid = false
        this._showTooltip()
        this._dispatchChange()
    }

    _onMove(e) {
        if (Math.round(e.pressure) == 1) {
            this._dispatchPreChange()
            this._setValue(e)
        }
    }

    _showTooltip() {
        let tooltip = this.renderRoot.querySelector('crowd-tooltip')
        if (tooltip) {
            tooltip.setAttribute('show',true)
            if (this.tooltipTimer) {
                clearTimeout(this.tooltipTimer)
            }
            this.tooltipTimer = setTimeout(() => {
                tooltip.removeAttribute('show')
            },4000)
        }
    }

    _keyDown(e) {
        e.preventDefault()
        switch(e.key) {
            case 'ArrowLeft':
                this._dispatchPreChange()
                if (this.value > this.min) {
                    this.value = this._roundToStep(this.value - this.step,this.step)
                    this._dispatchChange()
                }
                break
            case 'ArrowRight':
                this._dispatchPreChange()
                if (this.value < this.max) {
                    this.value = this._roundToStep(this.value + this.step,this.step)
                    this._dispatchChange()
                }
                break
            default:
                break
        }
    }

    validate() {
        this.invalid = false
        if (this.required) {
            if (this.value === '' || this.value == null) {
                this.invalid = true
            }
        }
    }

    render() {
        let min = ''
        if (!this.hideMinMax) {
            min = html`<span>${this.min}</span>`
        }
        let max = ''
        if (this.max && !this.hideMinMax) {
            max = html`<span>${this.max}</span>`
        }
        let notches = ''
        if (this.notches || this.numbers) {
            let notchArray = []
            for (let i = 0; i <= this.max; i+=this.step) {
                notchArray.push(i)
            }
            notches = html`
                <div class='notches' part='notches'>
                    ${notchArray.map(notch => {
                        let number = ''
                        if (this.numbers) {
                            number = html`<span>${notch}</span>`
                        }
                        return html`<div class='${this.notches ? 'notched' : ''}' style='--left: ${(notch/this.max) * 100}%;'>${number}</div>`
                    })}
                </div>
            `
        }
        let thumb = ''
        if (this.tooltip) {
            thumb = html`
                <crowd-tooltip content='${this.value}'>
                    <div aria-label='Slider thumb' class='thumb' part='thumb'></div>
                </crowd-tooltip>
            `
        } else {
            thumb = html`
                <div aria-label='Slider thumb' class='thumb' part='thumb'></div>
            `
        }
        let label = ''
        if (this.label) {
            label = html`
                <label for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        return html`
            <div part='wrapper' class='wrapper'>
                ${label}
                <div class='container' part='container'>
                    ${min}
                    <div @pointermove='${e => this._onMove(e)}' @pointerdown='${e => this._setValue(e)}' class='track' part='track' style='--value: ${(this.value / this.max) * 100}%;'>
                        ${notches}
                        ${thumb}
                        <input inputmode='none' id='${this.id}' @keydown='${e => this._keyDown(e)}' type='text' value='${this.value}' />
                    </div>
                    ${max}
                </div>
                ${error}
            </div>
        `;
    }
}
