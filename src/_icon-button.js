import {LitElement, css, html} from 'lit'
import {Button} from './_button'

export class IconButton extends Button {

    static properties = {
        ...super.properties,
        name: {type: String},
        src: {type: String},
        count: { type: Number }
    }

    static styles = [
        Button.styles,
        css`
            :host {
                line-height: 0;
            }
            button {
                background-color: transparent;
                border: none;
                padding: var(--crowd-icon-button-padding, 0.25em);
                width: 1em;
                height: 1em;
            }
            @media (hover: hover) {
                button:hover {
                    background-color: transparent;
                    border: none;
                }
            }
        `
    ]

    constructor() {
        super()
    }

    render() {
        let content = html`
            <slot part='label' class='label'>
                <slot><crowd-icon count='${this.count}' name='${this.name}' src='${this.src}'></crowd-icon></slot>
            </slot>
        `
        let element = html`
            <button part='button'>
                ${content}
            </button>
        `
        if (this.href) {
            element = html`
                <a part='button' href='${this.href}' target='${this.target}'>
                    ${content}
                </a>
            `
        }
        return html`
            ${element}
        `
    }
}