import { LitElement, html, css } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
require('@google-web-components/google-youtube/google-youtube')

export class Video extends LitElement {
    static properties = {
        src: {type: String},
        'src-youtube': {type: String},
        'src-vimeo': {type: String},
        type: {type: String},
        playState: {
            type: Boolean,
            reflect: true
        },
        initialPlay: {
            type: Boolean
        },
        progress: {
            type: Number
        },
        volume: {
            type: Number
        },
        muted: {
            type: Boolean,
            reflect: true
        },
        loop: {
            type: Boolean,
            reflect: true
        },
        fullscreen: {
            type: Boolean
        },
        _hideControls: {
            type: Boolean,
            reflect: true
        },
        controlHideTimeoutDuration: {
            type: Number
        },
        playbackRate: {
            type: Number
        },
        pip: {
            type: Boolean
        },
        poster: {
            type: String
        },
        _embedWidth: {
            type: Number
        },
        playsupported: {
            type: Boolean
        },
        controls: {
            type: Boolean
        }
    }
    static styles = [
        css`

            *,*::before, *::after {
                box-sizing: border-box;
                transition-property: none;
                transition-duration: var(--crowd-video-transition-duration, 0.15s);
                transition-delay: var(--crowd-video-transition-delay, 0s);
                transition-timing-function: var(--crowd-video-transition-ease, ease-in-out);
            }

            :host {
                display: inline-block;
                --crowd-range-indicator-color: var(--crowd-video-theme-color, #000);
                --crowd-button-background-color: var(--crowd-video-theme-color, #000);
                --crowd-button-border-color: var(--crowd-video-theme-color, #000);
                --crowd-button-color: var(--crowd-video-alt-color, #fff);
                --crowd-button-hover-background-color: var(--crowd-video-button-hover-color, #aaa);
                --crowd-button-hover-border-color: var(--crowd-video-button-hover-color, #000);
                --crowd-menu-item-color: var(--crowd-video-theme-color, #000);
            }

            :host([src-youtube]) {
                display: block;
                width: 100%;
            }

            video {
                display: inline-block;
                max-width: 100%;
                width:100%;
            }

            .container {
                display: grid;
                place-items: center;
                position: relative;
                overflow: hidden;
            }
            :host([src-youtube]) .container {
                width: 100%;
            }

            .media, .play, .pause {
                grid-area: 1/1;
            }

            .media {
                width: 100%;
            }

            .pause {
                opacity: 0;
                transition-property: opacity;
            }

            @media (hover: hover) {
                .container:hover .pause {
                    opacity: 1;
                }
                :host([_hideControls]) .container:hover .pause {
                    opacity: 0;
                }
            }

            .controls {
                position: absolute;
                bottom: 0;
                left: 0;
                right: 0;
                color: #fff;
                padding: var(--crowd-video-controls-padding, 1em);
                background: var(--crowd-video-controls-background,linear-gradient(0deg, var(--crowd-video-controls-background-color,rgba(0,0,0,1)) 0%, rgba(255,255,255,0) 100%));
                transition-property: transform opacity;
                transform: translateY(100%);
                opacity: 0;
                display: flex;
                align-items: center;
                gap: var(--crowd-video-controls-gap, 1em);
            }

            @media (hover: hover) {
                .container:hover .controls {
                    transform: translateY(0);
                    opacity: 1;
                }
                :host([_hideControls]) .container:hover .controls {
                    transform: translateY(100%);
                    opacity: 0;
                }
            }

            .controls crowd-icon-button {
                font-size: var(--crowd-video-controls-button-size, 1.2em);
            }

            .controls .progress {
                width: 100%;
                --crowd-range-width: 100%;
            }
            .controls .volume {
                --crowd-range-width: 75px;
            }
            .audio {
                display: flex;
                align-items: center;
                gap: 0.5em;
            }
            crowd-icon-button {
                --crowd-button-hover-color: var(--crowd-video-button-hover-color, #aaa);
            }

            .settings {
                --crowd-input-padding-vertical: 0px;
                --crowd-input-wrapper-padding-vertical: 5px;
            }

            .settings::part(dropdown) {
                overflow: visible;
            }

            .playback span {
                display: inline-block;
                padding-left: 0.5em;
                font-style: italic;
                opacity: 0.5;
            }
            .container:not(.no-play) google-youtube {
                pointer-events: none;
            }
            .container.no-play .play {
                display: none;
            }
        `
    ];

    async play() {
        this.video = this.renderRoot.querySelector('.video')
        this.youtube = this.renderRoot.querySelector('google-youtube')
        if (!this.initialPlay && this.video) {
            this.seeking = false
        }
        if (this.video) {
            this.initialPlay = true
            this.video.play().then(() => {
                this.playState = true
            })
        } else if (this.youtube) {
            this.initialPlay = true
            if (this.youtube.playsupported) {
                this.youtube.play()
                this.playState = true
            } else {
                this.renderRoot.querySelector('.container').classList.add('no-play')
            }
        }
    }

    pause() {
        this.video = this.renderRoot.querySelector('.video')
        this.youtube = this.renderRoot.querySelector('google-youtube')
        if (this.video) {
            this.video.pause()
            this.playState = false
        } else if (this.youtube) {
            this.youtube.pause()
            this.playState = false
            if (!this.youtube.playsupported) {
                this.renderRoot.querySelector('.container').classList.add('no-play')
            }
        }
    }

    _saveVolume() {
        localStorage.setItem('crowd-video-volume', this.volume)
        localStorage.setItem('crowd-video-muted', this.muted)
    }

    _toggleFullscreen() {
        let container = this.renderRoot.querySelector('.container')
        if (this.fullscreen) {
            document.exitFullscreen()
        } else {
            container.requestFullscreen()
        }
    }

    _startControlTimeout() {
        this._hideControls = false
        this._clearControlTimeout()
        this.controlTimeout = setTimeout(() => {
            this._hideControls = true
        }, this.controlHideTimeoutDuration)
    }

    _clearControlTimeout() {
        if (this.controlTimeout) {
            clearTimeout(this.controlTimeout)
        }
    }

    _setPlaybackRate(rate) {
        this.playbackRate = rate
    }

    _togglePiP() {
        if (this.pip && document.pictureInPictureElement) {
            document.exitPictureInPicture().then(() => {
                this.pip = false
            })
        } else {
            this.video = this.renderRoot.querySelector('.video')
            if (this.video) {
                this.video.requestPictureInPicture().then(() => {
                    this.pip = true
                })
                this.video.addEventListener('leavepictureinpicture', () => {
                    this.pip = false
                })
            }
        }
    }

    _checkPlaySupported() {
            // Run a new playback test.
        var timeout;
        var videoElement = document.createElement('video');
    
        if ('play' in videoElement) {
            videoElement.id = 'playtest';
            // Some browsers will refuse to play videos with 'display: none' set,
            // so position the video well offscreen instead.
            // Modify the .style property directly instead of using CSS to work around polyfill
            // issues; see https://github.com/GoogleWebComponents/google-youtube/issues/49
            videoElement.style.position = 'absolute';
            videoElement.style.top = '-9999px';
            videoElement.style.left = '-9999px';
    
            var mp4Source = document.createElement('source');
            mp4Source.src = "data:video/mp4;base64,AAAAFGZ0eXBNU05WAAACAE1TTlYAAAOUbW9vdgAAAGxtdmhkAAAAAM9ghv7PYIb+AAACWAAACu8AAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAnh0cmFrAAAAXHRraGQAAAAHz2CG/s9ghv4AAAABAAAAAAAACu8AAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAFAAAAA4AAAAAAHgbWRpYQAAACBtZGhkAAAAAM9ghv7PYIb+AAALuAAANq8AAAAAAAAAIWhkbHIAAAAAbWhscnZpZGVBVlMgAAAAAAABAB4AAAABl21pbmYAAAAUdm1oZAAAAAAAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAVdzdGJsAAAAp3N0c2QAAAAAAAAAAQAAAJdhdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAFAAOABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAAEmNvbHJuY2xjAAEAAQABAAAAL2F2Y0MBTUAz/+EAGGdNQDOadCk/LgIgAAADACAAAAMA0eMGVAEABGjuPIAAAAAYc3R0cwAAAAAAAAABAAAADgAAA+gAAAAUc3RzcwAAAAAAAAABAAAAAQAAABxzdHNjAAAAAAAAAAEAAAABAAAADgAAAAEAAABMc3RzegAAAAAAAAAAAAAADgAAAE8AAAAOAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA4AAAAOAAAAFHN0Y28AAAAAAAAAAQAAA7AAAAA0dXVpZFVTTVQh0k/Ou4hpXPrJx0AAAAAcTVREVAABABIAAAAKVcQAAAAAAAEAAAAAAAAAqHV1aWRVU01UIdJPzruIaVz6ycdAAAAAkE1URFQABAAMAAAAC1XEAAACHAAeAAAABBXHAAEAQQBWAFMAIABNAGUAZABpAGEAAAAqAAAAASoOAAEAZABlAHQAZQBjAHQAXwBhAHUAdABvAHAAbABhAHkAAAAyAAAAA1XEAAEAMgAwADAANQBtAGUALwAwADcALwAwADYAMAA2ACAAMwA6ADUAOgAwAAABA21kYXQAAAAYZ01AM5p0KT8uAiAAAAMAIAAAAwDR4wZUAAAABGjuPIAAAAAnZYiAIAAR//eBLT+oL1eA2Nlb/edvwWZflzEVLlhlXtJvSAEGRA3ZAAAACkGaAQCyJ/8AFBAAAAAJQZoCATP/AOmBAAAACUGaAwGz/wDpgAAAAAlBmgQCM/8A6YEAAAAJQZoFArP/AOmBAAAACUGaBgMz/wDpgQAAAAlBmgcDs/8A6YEAAAAJQZoIBDP/AOmAAAAACUGaCQSz/wDpgAAAAAlBmgoFM/8A6YEAAAAJQZoLBbP/AOmAAAAACkGaDAYyJ/8AFBAAAAAKQZoNBrIv/4cMeQ==";
            videoElement.appendChild(mp4Source);
    
            var webmSource = document.createElement('source');
            webmSource.src = "data:video/webm;base64,GkXfo49CgoR3ZWJtQoeBAUKFgQEYU4BnAQAAAAAAF60RTZt0vE27jFOrhBVJqWZTrIIQA027jFOrhBZUrmtTrIIQbE27jFOrhBFNm3RTrIIXmU27jFOrhBxTu2tTrIIWs+xPvwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUmpZuQq17GDD0JATYCjbGliZWJtbCB2MC43LjcgKyBsaWJtYXRyb3NrYSB2MC44LjFXQY9BVlNNYXRyb3NrYUZpbGVEiYRFnEAARGGIBc2Lz1QNtgBzpJCy3XZ0KNuKNZS4+fDpFxzUFlSua9iu1teBAXPFhL4G+bmDgQG5gQGIgQFVqoEAnIEAbeeBASMxT4Q/gAAAVe6BAIaFVl9WUDiqgQEj44OEE95DVSK1nIN1bmTgkbCBULqBPJqBAFSwgVBUuoE87EQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9DtnVB4eeBAKC4obaBAAAAkAMAnQEqUAA8AABHCIWFiIWEiAICAAamYnoOC6cfJa8f5Zvda4D+/7YOf//nNefQYACgnKGWgQFNANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQKbANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQPoANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQU1ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQaDANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQfQANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQkdANEBAAEQEBRgAGFgv9AAIiGAAPuC/rOgnKGWgQprANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQu4ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ0FANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ5TANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQ+gANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRDtANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRI7ANEBAAEQEAAYABhYL/QACIhgAPuC/rIcU7trQOC7jLOBALeH94EB8YIUzLuNs4IBTbeH94EB8YIUzLuNs4ICm7eH94EB8YIUzLuNs4ID6LeH94EB8YIUzLuNs4IFNbeH94EB8YIUzLuNs4IGg7eH94EB8YIUzLuNs4IH0LeH94EB8YIUzLuNs4IJHbeH94EB8YIUzLuNs4IKa7eH94EB8YIUzLuNs4ILuLeH94EB8YIUzLuNs4INBbeH94EB8YIUzLuNs4IOU7eH94EB8YIUzLuNs4IPoLeH94EB8YIUzLuNs4IQ7beH94EB8YIUzLuNs4ISO7eH94EB8YIUzBFNm3SPTbuMU6uEH0O2dVOsghTM";
            videoElement.appendChild(webmSource);
    
            document.body.appendChild(videoElement);
    
            // Ideally, we'll get a 'playing' event if we're on a browser that supports
            // programmatic play().
            videoElement.onplaying = (e) => {
                clearTimeout(timeout);

                this.playsupported = (e && e.type === 'playing') || videoElement.currentTime !== 0;

                videoElement.onplaying = null;

                document.body.removeChild(videoElement);
            };

            // If we haven't received a 'playing' event within 500ms, then we're most likely on a browser that doesn't
            // support programmatic plays. Do a final check after 500ms and set this.playsupported at that point.
            timeout = setTimeout(videoElement.onplaying, 500);

            // Try to initiate playback...
            videoElement.play().catch((e) => {

            });
        } else {
            // If there's no play() method then we know that it's not supported.
            this.playsupported = false;
        }
    }

    constructor() {
        super()
        this.playState = false
        this.initialPlay = false
        this.loop = false
        this.controls = false
        if (localStorage.getItem('crowd-video-volume')) {
            this.volume = Number(localStorage.getItem('crowd-video-volume'))
        } else {
            this.volume = 1
        }
        if (localStorage.getItem('crowd-video-muted')) {
            this.muted = localStorage.getItem('crowd-video-muted') == 'true'
        }
        this._saveVolume()
        this.fullscreen = false
        this.controlHideTimeoutDuration = 5000
        this.playbackRate = 1
        this.pip = false
    }

    connectedCallback() {
        super.connectedCallback()
        document.addEventListener('fullscreenchange', () => {
            if (document.fullscreenElement) {
                this.fullscreen = true
            } else {
                this.fullscreen = false
            }
        })
        this._checkPlaySupported()
        console.log(this.controls)
    }

    firstUpdated() {
        let container = this.renderRoot.querySelector('.container')
        this._embedWidth = container.getBoundingClientRect().width
    
        new ResizeObserver((entries) => {
            for (let entry of entries) {
                this._embedWidth = entry.contentRect.width
            }
        }).observe(container)
    }

    render() {
        let media = ''
        if (this.src) {
            media = html`
                <video class='video' muted=${ifDefined(this.muted ? this.muted : undefined)} loop=${ifDefined(this.loop ? this.loop : undefined)} poster='${this.poster}' loading='lazy' preload="metadata" playsinline part='video' @timeupdate='${(e) => {
                    if (this.video) {
                        requestAnimationFrame(() => {
                            if (!this.seeking) {
                                this.progress = (this.video.currentTime / this.video.duration) * 100
                                this.video.volume = this.volume
                                this.video.muted = this.muted
                                this.video.playbackRate = this.playbackRate
                            }
                        })
                    }
                }}'>
                    <source src='${this.src}' type='${this.type}' />
                </video>
            `
        } else if (this['src-youtube']) {
            media = html`
                <google-youtube
                    video-id="${this['src-youtube']}"
                    height="${(this._embedWidth / 16) * 9}px"
                    width="${this._embedWidth}px"
                    autoplay="0"
                    playsupported="${this.playsupported}"
                    chromeless
                    class='youtube'
                    part='youtube'
                    @google-youtube-state-change='${(e) => {

                        // Has the video ended?
                        if (e.detail.data === 0) {
                            this.pause()
                            this.youtube._videoIdChanged()
                            if (this.loop) {
                                this.play()
                            }
                        }
                    }}'
                    @playbackstarted-changed='${(e) => {
                        this.renderRoot.querySelector('.container').classList.remove('no-play')
                        this.playState = e.detail.value
                    }}'
                    @currenttime-changed='${(e) => {
                        this.initialPlay = true
                        if (this.youtube) {
                            requestAnimationFrame(() => {
                                if (!this.seeking) {
                                    this.progress = (this.youtube.currenttime / this.youtube.duration) * 100
                                    this.youtube.setVolume(this.volume * 100)
                                    if (this.muted) {
                                        this.youtube.mute()
                                    } else {
                                        this.youtube.unMute()
                                    }
                                    this.youtube.setPlaybackRate(this.playbackRate)
                                }
                            })
                        } else {
                            this.youtube = this.renderRoot.querySelector('google-youtube')
                        }
                    }}'
                >
                </google-youtube>
            `
        }
        return html`
            <div class='container' part='container' @pointerleave='${() => this._clearControlTimeout()}' @pointerenter='${() => this._startControlTimeout()}' @pointermove='${() => this._startControlTimeout()}' @pointerover='${() => this._startControlTimeout()}'>
                <div class='media' part='media'>
                    ${media}
                </div>
                ${
                    !this.playState && this.controls ? html`
                        <crowd-button @click='${() => this.play()}' circle part='play' class='play'>
                            <slot name='play'>
                                <crowd-icon name='play-fill'></crowd-icon>
                            </slot>
                        </crowd-button>
                    ` : ''
                }
                ${
                    this.playState && this.controls ? html`
                        <crowd-button @click='${() => this.pause()}' circle part='pause' class='pause'>
                            <slot name='pause'>
                                <crowd-icon name='pause-fill'></crowd-icon>
                            </slot>
                        </crowd-button>
                    ` : ''
                }
                ${
                    this.initialPlay && this.controls ? html`
                        <div class='controls' part='controls'>
                            <crowd-icon-button @click='${() => {
                                if(this.playState) {
                                    this.pause()
                                 } else {
                                     this.play()
                                 }
                            }}' name='${this.playState ? 'pause-fill' : 'play-fill'}'></crowd-icon-button>
                            <crowd-range part='progress' class='progress' hideMinMax='true' min='0' max='100' value='${this.progress}' step='1' @crowdMove='${(e) => {
                                this.pause()
                                this.seeking = true
                                if (this.video) {
                                    this.video.currentTime = (e.currentTarget.value / 100) * this.video.duration
                                }
                                if (this.youtube) {

                                    if (this.youtube.state != 3) {
                                        let newDurationPercent = (e.currentTarget.value / 100) * this.youtube.duration
                                        let deltaPercent = newDurationPercent - this.progress
                                        let deltaDuration = (deltaPercent / 100) * this.youtube.duration
                                        deltaDuration = Math.round(deltaDuration)
                                        this.youtube.seekTo(deltaDuration)
                                    }
                                }
                                // requestAnimationFrame(() => {
                                //     this.seeking = false
                                //     this.play()
                                // })
                                setTimeout(() => {
                                    this.seeking = false
                                    this.play()
                                }, 500)
                            }}'></crowd-range>
                            <div class='audio' part='audio'>
                                <crowd-icon-button @click='${() => {
                                    this.muted = !this.muted
                                    this._saveVolume()
                                }}' name='${this.volume == 0 || this.muted ? 'volume-mute-fill' : ''}${this.volume > 0 && this.volume < 0.5 && !this.muted ? 'volume-down-fill' : ''}${this.volume >= 0.5 && !this.muted ? 'volume-up-fill' : ''}'></crowd-icon-button>
                                <crowd-range part='volume' class='volume' hideMinMax='true' min='0' max='1' value='${this.volume}' step='0.1' @crowdMove='${(e) => {
                                    this.volume = e.currentTarget.value
                                    this._saveVolume()
                                }}'></crowd-range>
                            </div>
                            <crowd-dropdown position="top right" class='settings' part='settings'>
                                <crowd-icon-button name='gear-fill' slot='trigger'></crowd-icon-button>
                                <crowd-menu>
                                    <crowd-menu-item class='playback' position='left bottom'>
                                        Playback Speed <span>${this.playbackRate}x</span>
                                        <crowd-menu slot="submenu">
                                            <crowd-menu-item @click='${() => this._setPlaybackRate(0.25)}'>0.25x</crowd-menu-item>
                                            <crowd-menu-item @click='${() => this._setPlaybackRate(0.5)}'>0.5x</crowd-menu-item>
                                            <crowd-menu-item @click='${() => this._setPlaybackRate(1)}'>1x</crowd-menu-item>
                                            <crowd-menu-item @click='${() => this._setPlaybackRate(1.5)}'>1.5x</crowd-menu-item>
                                            <crowd-menu-item @click='${() => this._setPlaybackRate(2)}'>2x</crowd-menu-item>
                                        </crowd-menu>
                                    </crowd-menu-item>
                                    <crowd-menu-item @click='${() => this._togglePiP()}'>Picture in picture</crowd-menu-item>
                                </crowd-menu>
                            </crowd-dropdown>
                            <crowd-icon-button @click='${() => this._toggleFullscreen()}' name='${!this.fullscreen ? 'fullscreen' : 'fullscreen-exit' }'></crowd-icon-button>
                        </div>
                    ` : ''
                }
            </div>
        `;
    }
}