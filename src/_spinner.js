import { LitElement, html, css } from 'lit';

export class Spinner extends LitElement {
    static styles = [
        css`
            :host {
                display: inline-block;
            }
            :host,:host * {
                box-sizing: inherit;
            }
            figure {
                margin: 0;
                width: 1em;
                height: 1em;
            }
            svg {
                width: 100%;
                height: 100%;
                transform-origin: 50% 50%;
                animation: spin var(--crowd-spinner-animation-duration, 1s) forwards infinite linear;
            }
            .track,.indicator {
                fill: none;
                stroke-width: var(--track-width,2px);
                r: calc(0.5em - var(--track-width,2px) / 2);
                cx: 0.5em;
                cy: 0.5em;
                stroke: var(--track-color,rgba(0,0,0,0.2));
            }
            .indicator {
                stroke: var(--indicator-color,black);
                stroke-linecap: round;
                stroke-dasharray: 25 250;
                transform-origin: 50% 50%;
                animation: pulse var(--crowd-spinner-animation-duration, 1s) forwards infinite alternate linear;
            }
            @keyframes pulse {
                0% {
                    stroke-dasharray: 25 250;
                }
                100% {
                    stroke-dasharray: 3 250;
                }
            }
            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }
                50% {
                    transform: rotate(180deg);
                }
                0% {
                    transform: rotate(360deg);
                }
            }
        `
    ];

    render() {
        return html`
            <figure>
                <svg>
                    <circle class='track'></circle>
                    <circle class='indicator'></circle>
                </svg>
            </figure>
        `;
    }
}
