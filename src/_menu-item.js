import { LitElement, html, css } from 'lit';
// const supportsContainerQueries = "container" in document.documentElement.style;
// if (!supportsContainerQueries) {
//     import("container-query-polyfill");
// }

export class MenuItem extends LitElement {
    static styles = [
        css`
            :host {
                display: inline-block;
            }
            li {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                padding: var(--crowd-menu-item-padding-vertical,0.2em) var(--crowd-menu-item-padding-horizontal,1em);
                color: var(--crowd-menu-item-color, inherit);
                background-color: var(--crowd-menu-item-background-color, transparent);
                transition-property: color, background-color;
                transition-duration: var(--crowd-menu-item-transition-duration,0.15s);
                transition-timing-function: var(--crowd-menu-item-transition-ease,ease-in-out);
                transition-delay: var(--crowd-menu-item-transition-delay, 0s);
                cursor: pointer;
                position: relative;
                container-type: inline-size;
                container-name: submenu-container;
            }
            slot[name='submenu'] {
                display: block;
                position: absolute;
                top: 0;
                left: 100%;
                transform: scale(0);
                opacity: 0;
                transition-duration: var(--crowd-menu-item-submenu-transition-duration, 0.15s);
                transition-timing-function: var(--crowd-menu-item-submenu-transition-ease, ease-in-out);
                transition-delay: var(--crowd-menu-item-submenu-transition-delay, 0s);
            }
            @media (hover: hover) {
                li:hover {
                    background-color: var(--crowd-menu-item-hover-background-color, rgba(0,0,0,0.1));
                    color: var(--crowd-menu-item-hover-color, inherit);
                }
                li:hover slot[name='submenu'] {
                    transform: scale(1);
                    opacity: 1;
                }
            }
            li:focus-visible,li:focus-within {
                outline: none;
                background-color: var(--crowd-menu-item-hover-background-color, rgba(0,0,0,0.1));
                color: var(--crowd-menu-item-hover-color, inherit);
            }

            li:focus-visible slot[name='submenu'],li:active slot[name='submenu'] {
                transform: scale(1);
                opacity: 1;
            }

            span.shortcut {
                opacity: 0.5;
                padding-left: 0.5em;
            }
            :host([position*="left"]) slot[name='submenu'] {
                left: auto;
                right: 100%;
            }
            :host([position*="bottom"]) slot[name='submenu'] {
                top: auto;
                bottom: 0;
            }
        `
    ];

    static properties = {
        shortcut: {
            type: String
        },
        position: {
            type: String,
            reflect: true
        }
    }

    _executeShortcut(e) {
        if (e.ctrlKey || e.metaKey) {
            if (e.key == this.shortcut) {
                e.preventDefault()
                this.triggerFocus()
                this.click()
            }
        }
    }

    _listenForShortcut() {
        if (this.parentMenu) {
            document.addEventListener(`keydown`, this.bindExecuteShortcut, false)
        }
    }

    _stopListenForShortcut() {
        if (this.parentMenu) {
            document.removeEventListener(`keydown`, this.bindExecuteShortcut, false)
        }
    }

    triggerFocus() {
        this.renderRoot.querySelector('li').focus()
    }

    _executeNavigation(e) {
        const nextMenuItem = () => {
            if (this.nextElementSibling && this.nextElementSibling.nodeName == 'CROWD-MENU-ITEM') {
                this.blur()
                this.nextElementSibling.triggerFocus()
            }
        }
        const previousMenuItem = () => {
            if (this.previousElementSibling && this.previousElementSibling.nodeName == 'CROWD-MENU-ITEM') {
                this.blur()
                this.previousElementSibling.triggerFocus()
            }
        }
        const childMenuItem = () => {
            if (this.querySelector('crowd-menu crowd-menu-item')) {
                this.blur()
                this.querySelector('crowd-menu crowd-menu-item').triggerFocus()
            }
        }
        switch (e.key) {
            case "Down": // IE/Edge specific value
            case "ArrowDown":
                e.preventDefault()
                nextMenuItem()
                break;
            case "Up": // IE/Edge specific value
            case "ArrowUp":
                e.preventDefault()
                previousMenuItem()
                break;
            case "Right":
            case "ArrowRight":
                e.preventDefault()
                childMenuItem()
                break;
            case "Enter":
                e.preventDefault()
                this.click()
            default:
                return; // Quit when this doesn't handle the key event.
          }
    }

    _listenForNavigation() {
        this.triggerFocus()
        document.addEventListener(`keydown`, this.bindExecuteNavigation, false)
    }

    _stopListenForNavigation() {
        document.removeEventListener(`keydown`, this.bindExecuteNavigation, false)
    }

    constructor() {
        super()
        this.position = 'right'
    }

    connectedCallback() {
        super.connectedCallback()
        this.bindExecuteShortcut = this._executeShortcut.bind(this)
        this.bindExecuteNavigation = this._executeNavigation.bind(this)
        this.parentMenu = this.parentElement
        if (this.parentMenu && this.parentMenu.nodeName == 'CROWD-MENU' && this.shortcut) {
            this.parentMenu.addEventListener('focusin',() => this._listenForShortcut(),false)
            this.parentMenu.addEventListener('mouseover',() => this._listenForShortcut(),false)
            this.parentMenu.addEventListener('focusout',() => this._stopListenForShortcut(),false)
            this.parentMenu.addEventListener('mouseleave',() => this._stopListenForShortcut(),false)
        }

        this.addEventListener('focusin',() => this._listenForNavigation(), false)
        this.addEventListener('focus',() => this._listenForNavigation(), false)
        this.addEventListener('focusout',() => this._stopListenForNavigation(), false)
        this.addEventListener('mouseover',() => this._listenForNavigation(), false)
        this.addEventListener('mouseleave',() => this._stopListenForNavigation(), false)
    }

    render() {
        return html`
            <li part='item' tabindex='0'>
                <span><slot></slot></span>
                <div class='submenu'>
                    <slot name='submenu'>
                </div>
                </slot>
                <span class='shortcut'>
                    ${this.shortcut ? 
                        `Ctrl+${this.shortcut.toUpperCase()}`
                        :
                        ``
                    }
                </span>
            </li>
        `;
    }
}
