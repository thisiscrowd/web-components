import { LitElement, html, css } from 'lit';

export class Tab extends LitElement {

    static properties = {
        title: {type: String},
        _tabIndex: {type: Number},
        active: {type: Boolean, reflect: true}
    }

    static styles = [
        css`
            :host {
                display: contents;
            }
            * {
                box-sizing: border-box;
            }
            :host(:not([active])) .container {
                opacity: 0;
                pointer-events: none;
            }
            :host(:not([active])) button {
                opacity: 0.4;
            }
            article {
                position: relative;
                grid-area: 1 / 1;
                max-width: 100%;
            }
        `
    ];

    _dispatchChange() {
        const event = new CustomEvent('crowdShowTab');
        this.dispatchEvent(event);
    }

    show() {
        this.active = true
        this._dispatchChange()
    }

    hide() {
        this.active = false
    }

    render() {
        return html`
            <article part='wrapper'>
                <div class='container' part='container'>
                    <slot></slot>
                </div>
            </article>
        `;
    }
}
export class TabGroup extends LitElement {
    static properties = {
        _tabs: {type: Array},
        _activeTab: {type: Number}
    }
    static styles = [
        css`
            :host {
                display: block;
            }
            * {
                box-sizing: border-box;
            }
            .panels {
                display: grid;
            }
            .buttons {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                overflow-x: scroll;
                scrollbar-width: none;
                gap: var(--crowd-tabs-gap,0px);
                width:100%;
                border-bottom: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
            }
            .buttons::-webkit-scrollbar {
                display: none;
                width: 0px;
                height: 0px;
                opacity: 0;
            }
            .buttons button {
                -webkit-appearance: none;
                background-color: var(--crowd-tab-button-background-color, #fff);
                border: var(--crowd-tab-button-border-width, 0px) var(--crowd-tab-button-border-style, solid) var(--crowd-tab-button-border-color, #eee);
                padding: var(--crowd-tab-button-padding-vertical, 0.3em) var(--crowd-tab-button-padding-horizontal, 0.5em);
                font-size: var(--crowd-tab-button-font-size, inherit);
                cursor: pointer;
                border-top-left-radius: var(--crowd-tab-button-border-radius, 3px);
                border-top-right-radius: var(--crowd-tab-button-border-radius, 3px);
                opacity: 0.4;
            }
            @media (hover: hover) {
                .buttons button:hover {
                    background-color: var(--crowd-tab-button-hover-background-color, #fafafa);
                }
            }
            .buttons button:active,.buttons button:focus-visible {
                outline: none;
                background-color: var(--crowd-tab-button-hover-background-color, #fafafa);
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            .buttons button.active {
                opacity: 1;
                background-color: var(--crowd-tab-button-active-background-color, #fff);
                border-top: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
                border-left: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
                border-right: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
                border-bottom: none;
                position: relative;
            }
            // .buttons button.active::after {
            //     content: '';
            //     display: inline-block;
            //     width: 100%;
            //     left: 0;
            //     height: var(--crowd-tab-button-active-border-width, 1px);
            //     background-color: var(--crowd-tab-button-active-background-color, #fff);
            //     position: absolute;
            //     bottom: 0;
            //     transform: translateY(100%);
            // }
        `
    ];

    _hideOtherTabs(excludeTab) {
        let tabs = [].slice.call(this.querySelectorAll('crowd-tab'))
        tabs.forEach(tab => {
            if (tab != excludeTab) {
                tab.hide()
            }
        })
    }

    showTab(index) {
        let tabs = [].slice.call(this.querySelectorAll('crowd-tab'))
        if (tabs) {
            this._activeTab = index
            tabs[index].show()
        }
    }

    connectedCallback() {
        super.connectedCallback()
        let tabs = [].slice.call(this.querySelectorAll('crowd-tab'))
        if (tabs) {
            this._tabs = tabs
            tabs.forEach(tab => {
                tab.addEventListener('crowdShowTab', () => this._hideOtherTabs(tab), false);
            });
            this.showTab(0)
        }
    }

    constructor() {
        super()
        this._activeTab = 0
    }

    render() {
        return html`
            <article class='tab-group'>
                <div class='buttons'>
                    ${this._tabs.map((tab,index) => html`
                        <button class='${index === this._activeTab ? "active" : ""}' @click='${() => this.showTab(index)}'>${tab.title}</button>
                    `)}
                </div>
                <div class='panels'>
                    <slot></slot>
                </div>
            </article>
        `;
    }
}
