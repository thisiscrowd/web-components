import { LitElement, html, css } from 'lit';

export class ContextMenu extends LitElement {

    static properties = {
        show: {
            type: Boolean,
            reflect: true
        }
    }

    toggle() {
        this.show = !this.show
    }

    open() {
        this.show = true
        if (this.querySelector('crowd-menu') && this.querySelector('crowd-menu').querySelector('crowd-menu-item')) {
            this.querySelector('crowd-menu').querySelector('crowd-menu-item').triggerFocus()
        }
    }

    close() {
        this.show = false
    }

    static styles = [
        css`
            :host {
                display: contents;
                --mouse-y: 0px;
                --mouse-x: 0px;
            }
            * {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
                transition-duration: var(--context-menu-transition-duration, 0.15s);
                transition-property: none;
            }
            @media (prefers-reduced-motion: reduced) {
                * {
                    transition-duration: none;
                }
            }
            .context {
                position: fixed;
                top: calc(var(--mouse-y) + 0.5em);
                left: calc(var(--mouse-x) + 0.5em);
                opacity: 0;
                transform: scale(0);
                transition-property: opacity, transform;
                background-color: var(--context-background-color, white);
                padding: var(--context-padding-vertical,1em) var(--context-padding-horizontal,1em);
                box-shadow: var(--context-padding-box-shadow, 0px 10px 15px -3px rgba(0,0,0,0.1));
                z-index: var(--context-z-index,999);
                width: var(--context-width,250px);
            }
            :host([show]) .context {
                opacity: 1;
                transform: scale(1);
            }
            .overlay {
                position: fixed;
                inset: 0;
                pointer-events: none;
                z-index: calc(var(--context-z-index,999) - 1);
            }
            :host([show]) .overlay {
                pointer-events: all;
            }
        `
    ];

    connectedCallback() {
        super.connectedCallback()
        window.addEventListener('mousemove', e => {
            if (!this.show) {
                this.style.setProperty('--mouse-x', e.clientX + "px")
                this.style.setProperty('--mouse-y', e.clientY + "px")
            }
        })
        document.addEventListener('contextmenu', e => {
            e.preventDefault()
            this.open()
        }, false)
    }

    render() {
        return html`
            <div class='overlay' @click='${() => this.close()}'></div>
            <div class='context'>
                <slot></slot>
            </div>
        `;
    }
}
