import {LitElement, css, html} from 'lit';

export class Button extends LitElement {

    static properties = {
        href: {type: String},
        pill: {type: Boolean},
        loading: {type: Boolean},
        disabled: {type: Boolean},
        caret: {type: Boolean},
        target: {type: String},
        circle: {type: Boolean}
    }

    static styles = css`
        :host {
            display: inline-block;
            width: auto;
            cursor: pointer;
            line-height: 1;
            height:min-content;
        }
        button,a {
            -webkit-appearance: none;
            background-color: var(--crowd-button-background-color, #eeeeee);
            padding: var(--crowd-button-padding-vertical,0.5em) var(--crowd-button-padding-horizontal, 1em);
            color: var(--crowd-button-color, inherit);
            border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-button-border-color, #aeaeae);
            border-radius: var(--crowd-button-border-radius, 3px);
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            text-align: var(--crowd-button-text-align, center);
            display: inline-flex;
            width: 100%;
            height: var(--crowd-button-height, 2.5em);
            flex-flow: row nowrap;
            justify-content: var(--crowd-button-justify,center);
            align-items: center;
            gap: var(--crowd-button-gap, 0.5em);
            cursor: pointer;
            margin: 0;
            text-decoration: none;
            text-transform: inherit;
            box-sizing: border-box;
            transition-property: background-color, border-color, color;
            transition-duration: var(--crowd-button-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-button-transition-ease, ease-in-out);
            transition-delay: var(--crowd-button-transition-delay, 0s);
            position:relative;
            text-transform: var(--crowd-button-text-transform, inherit);
        }
        button:focus-visible, button:active, a:focus-visible,a:active {
            outline: none;
        }
        button:focus-visible,button:active, a:focus-visible,a:active {
            box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-button-focus-color, rgba(0,0,0,0.3));
        }
        :host([pill]) button, :host([pill]) a {
            border-radius: var(--crowd-button-pill-border-radius, 999px);
        }
        @media (hover: hover) {
            button:hover, a:hover {
                background-color: var(--crowd-button-hover-background-color, #aeaeae);
                border-color: var(--crowd-button-hover-border-color, #aeaeae);
                color: var(--crowd-button-hover-color, #fff);
            }
        }
        :host([disabled]) {
            opacity: 0.5;
            pointer-events: none;
        }
        :host([circle]) button, :host([circle]) a {
            aspect-ratio: 1/1;
            height: var(--crowd-button-width, auto);
            width: var(--crowd-button-width, auto);
            border-radius: 50%;
        }
        slot[name='prefix'] svg,
        slot[name='suffix'] svg {
            height: 1.5em;
        }
        .prefix,.suffix {
            display: inline-grid;
            place-items:center;
        }
        .label {
            display: flex;
            flex-flow: row nowrap;
            justfy-content: flex-start;
            align-items:center;
        }
        :host([loading]) button, :host([loading]) a {
            display: inline-grid;
            place-items: center;
        }
        crowd-spinner {
            position: absolute;
            top:50%;
            left:50%;
            transform: translate(-50%,-50%);
        }
        .loading {
            display: inline-flex;
            flex-flow: row nowrap;
            justify-content: var(--crowd-button-justify,center);
            align-items: center;
            gap: var(--crowd-button-gap, 0.5em);
            opacity: 0;
        }
    `

    constructor() {
        super()
    }

    render() {
        let caret = '';
        if (this.caret) {
            caret = html`
                <crowd-icon name='chevron-down'></crowd-icon>
            `
        }
        let content = html`
            <span class='prefix' part='prefix'>
                <slot name='prefix'></slot>
            </span>
            <span class='label' part='label'>
                <slot></slot>
            </span>
            <span class='suffix' part='suffix'>
                <slot name='suffix'>${caret}</slot>
            </span>
        `
        if (this.loading) {
            content = html`<crowd-spinner></crowd-spinner><span class='loading'>${content}</span>`
        }
        let element = html`
            <button part='button'>
                ${content}
            </button>
        `
        if (this.href) {
            element = html`
                <a part='button' href='${this.href}' target='${this.target}'>
                    ${content}
                </a>
            `
        }
        return html`
            ${element}
        `
    }
}