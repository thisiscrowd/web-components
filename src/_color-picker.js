import { LitElement, html, css } from 'lit';
const tinycolor = require("tinycolor2")
const roundToX = (num = 0, X = 20) => +(Math.round(num + `e${X}`)  + `e-${X}`)

export class ColorPicker extends LitElement {

    static properties = {
        value: {type: String, reflect: true},
        name: {type: String},
        label: {type: String},
        _unit: {type: String},
        _hue: {type: Number},
        _sat: {type: Number},
        _v: {type: Number},
        _alpha: {type: Number},
        _recentColors: {type: Array}
    }

    static styles = [
        css`
            :host {
                display: inline-block;
            }
            :host,:host * {
                box-sizing: border-box;
                margin: 0;
                padding: 0;
            }
            .pallete {
                background: linear-gradient(to top, hsla(0,0%,0%,calc(var(--a))), transparent), linear-gradient(to left, hsla(calc(var(--h)),100%,50%,calc(var(--a))),hsla(0,0%,100%,calc(var(--a)))),linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 ),linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 );
                background-position: 0 0, 0 0,0 0,5px 5px;
                background-size: 100% 100%, 100% 100%, 10px 10px, 10px 10px;
                user-select: none;
                cursor: crosshair;
                min-width: 150px;
                min-height: 150px;
                position:relative;
            }
            .hue-range,.alpha-range {
                -webkit-appearance: none;
                display: block;
                border-radius: 999px;
                width: 100%;
            }
            input[type="range"]::-webkit-slider-thumb {
                -webkit-appearance: none;
                position: relative;
                width: 10px;
                height: 10px;
                transform: scale(1.2);
                border-radius: 50%;
                box-shadow: 0 0 10px rgb(0 0 0 / 10%);
                background: #fff;
                transition: .2s cubic-bezier(.12, .4, .29, 1.46);
            }
            .hue-range {
                background: linear-gradient(to right, red, yellow, lime, cyan, blue, magenta, red);
            }
            .alpha-range {
                background: linear-gradient(to right, hsla(calc(var(--h)),100%,50%,0), hsla(calc(var(--h)),100%,50%,1)),linear-gradient( 
                    45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 ),linear-gradient( 
                    45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 );
                        background-position: 0 0,0 0,5px 5px;
                        background-size: 100% 100%,10px 10px,10px 10px;
            }
            .indicator {
                position: relative;
            }
            .indicator::after,.indicator::before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
            }
            .indicator::after {
                background: var(--background);
            }
            .indicator::before {
                background: linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 ),linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 );
                background-position: 0 0,5px 5px;
                background-size: 10px 10px, 10px 10px;
            }
            .container {
                display: flex;
                flex-flow: column;
                align-items: stretch;
                gap: var(--crowd-color-picker-spacing,0.2em);
            }
            .recent-colors {
                display: flex;
                gap: 5px;
            }
            .recent-colors button {
                -webkit-appearance: none;
                border: 1px solid #eee;
                padding: 0;
            }
            .pallete-indicator {
                display: inline-block;
                position: absolute;
                left: var(--s);
                top: calc(100% - var(--v));
                width: 5px;
                height: 5px;
                transform: translate(-50%,-50%);
                border-radius: 50%;
                background-color: white;
                border: 1px solid #eee;
            }
        `
    ];

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _setValue() {
        let hsva = tinycolor(`hsva(${this._hue},${roundToX(this._sat,2)}%,${roundToX(this._v,2)}%,${this._alpha}%)`)
        hsva.setAlpha(this._alpha / 100)
        this.value = hsva.toHslString()
        let recentColors = JSON.parse(localStorage.getItem('recentColors'))
        if (!recentColors) {
            recentColors = []
        }
        if (recentColors.indexOf(this.value) < 0) {
            recentColors.push(this.value)
        }
        localStorage.setItem('recentColors', JSON.stringify(recentColors))
        this._dispatchChange()
    }

    _chooseColor(e) {
        let width = e.currentTarget.getBoundingClientRect().width
        let height = e.currentTarget.getBoundingClientRect().height
        let sat = (e.offsetX / width) * 100
        let v = (1 - (e.offsetY / height)) * 100
        this._sat = sat
        this._v = v
        this._setValue()
    }

    constructor() {
        super()
        this._hue = 0
        this._sat = 0
        this._v = 0
        this._alpha = 100
        this._recentColors = []
    }

    _loop() {
        this._recentColors = JSON.parse(localStorage.getItem('recentColors'))
        requestAnimationFrame(() => this._loop())
    }

    connectedCallback() {
        super.connectedCallback()
        if (!localStorage.getItem('recentColors')) {
            localStorage.setItem('recentColors', '[]')
        }
        this._loop()
    }

    _redraw() {
        let hsla = tinycolor(this.value)
        let hsva = hsla.toHsv()
        this._hue = hsva.h
        this._sat = hsva.s * 100
        this._v = hsva.v * 100
        this._alpha = hsva.a * 100
        this._dispatchChange()
    }

    render() {
        let recentHTML = ''
        if (this._recentColors) {
            recentHTML = html`
                <div class='recent-colors'>
                    ${this._recentColors.slice(-8).reverse().map(color => html`<button @click='${() => {this.value = color;this._redraw()}}' style='background-color: ${color};width:1em;height: 1em;'></button>`)}
                </div>
            `
        }
        return html`
            <crowd-dropdown style='--h: ${this._hue};--s:${this._sat}%;--v: ${this._v}%;--a: ${this._alpha}%;'>
                <crowd-button slot='trigger' style='--crowd-button-background-color:var(--crowd-color-picker-background-color,#eee);--crowd-button-gap:0px;--crowd-button-padding-vertical:10px;--crowd-button-padding-horizontal: 10px;'>
                    <div style='width: 1em;height: 1em;--background: ${this.value};' class='indicator'></div>
                </crowd-button>
                <div part='container' class='container'>
                    <div @mouseup='${(e) => this._chooseColor(e)}' part='pallete' class='pallete'>
                        <span class='pallete-indicator'></span>
                    </div>
                    <input class='hue-range' type='range' min='0' max='360' step='1' value='${this._hue}' @change='${(e) => {this._hue = e.currentTarget.value;this._setValue()}}' />
                    <input class='alpha-range' type='range' min='0' max='100' step='1' value='${this._alpha}' @change='${e => {this._alpha = e.currentTarget.value;this._setValue()}}' />
                    ${recentHTML}
                    <small>${this.value}</small>
                </div>
            </crowd-dropdown>
        `
    }
}