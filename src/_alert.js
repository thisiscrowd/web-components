import { LitElement, html, css, nothing } from 'lit';

export class Alert extends LitElement {

    static properties = {
        open: {
            type: Boolean,
            reflect: true
        },
        closable: {
            type: Boolean
        },
        duration: {
            type: Number
        },
        toast: {
            type: Boolean
        },
        title: {
            type: String
        }
    }

    static styles = [
        css`
            :host {
                display: block;
            }
            :host, :host * {
                box-sizing: inherit;
            }
            .alert {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                background-color: var(--crowd-alert-background-color, white);
                padding: var(--crowd-alert-padding-vertical, 1em) var(--crowd-alert-padding-horizontal,1em);
                gap: var(--crowd-alert-spacing, 1em);
                color: inherit;
                border: 1px solid var(--crowd-alert-outline-color,#eee);
                border-top: var(--crowd-alert-border-width, 3px) var(--crowd-alert-border-style, solid) var(--crowd-alert-border-color,black);
                animation: close 0.2s forwards;
                border-radius: var(--crowd-alert-border-radius,2px);
                font-size: var(--crowd-alert-font-size, inherit);
                pointer-events: var(--crowd-alert-pointer-events, all);
            }
            .alert > div {
                flex: 1 1 auto;
            }
            slot[name='icon'] {
                color: var(--crowd-alert-color, black);
            }

            :host([open]) .alert {
                animation: open 0.2s forwards;
            }
            :host([toast]) .alert {
                box-shadow: var(
                    --crowd-alert-box-shadow,
                    0 2px 8px rgba(0, 0, 0, 0.1)
                );
                animation: none;
            }
            [popover] {
                margin: var(--alert-toast-position-top, 1em) var(--alert-toast-position-right, 1em) var(--alert-toast-position-bottom, auto) var(--alert-toast-position-left, auto);
                padding: 0;
                border: none;
            }

            .close {
                margin-left: auto;
            }

            h3 {
                display: flex;
                gap: 0.5em;
                align-items: center;
                margin: 0;
            }

            @keyframes open {
                0% {
                    height: 0px;
                    transform: scale(0);
                    opacity: 0;
                    padding: 0;
                    border-width: 0px;
                }
                1% {
                    height: auto;
                    transform: scale(0.5);
                    padding: 0;
                    border-width: 0px;
                    opacity: 0;
                }
                100% {
                    transform: scale(1);
                    opacity: 1;
                }
            }

            @keyframes close {
                0% {
                    height: auto;
                    transform: scale(1);
                    opacity: 1;
                }
                99% {
                    height: auto;
                    transform: scale(0.5);
                    opacity: 0;
                    padding: 0;
                    border-width: 0px;
                }
                100% {
                    height: 0px;
                    transform: scale(0);
                    opacity: 0;
                    padding: 0;
                    border-width: 0px;
                }
            }
        `
    ];

    connectedCallback() {
        super.connectedCallback()
        if (this.duration) {
            setTimeout(() => this.hide(),this.duration)
        }
        if (this.toast) {
            // this._setupToast()
            console.log(this.alert)
            this.toastEl.hidePopover()
        }
    }

    // _createToastStack() {
    //     let toastStack = document.createElement('toast-stack')
    //     document.body.appendChild(toastStack)
    //     return toastStack
    // }

    // _setupToast() {
    //     let toastStack = document.querySelector('toast-stack')
    //     if (!toastStack) {
    //         toastStack = this._createToastStack()
    //     }
    //     if (this.parentElement != toastStack) {
    //         this.parentElement.removeChild(this)
    //         toastStack.appendChild(this)
    //     }
    // }

    get toastEl() {
        console.log('getting')
        return this.renderRoot.querySelector('[popover]')
    }

    show() {
        if (this.toast) {
            this.toastEl.showPopover()
        } else {
            this.open = true
            if (this.duration) {
                setTimeout(() => this.hide(),this.duration)
            }
        }
    }

    hide() {
        this.open = false
        if (this.toast) {
            this.toastEl.hidePopover()
        }
    }

    constructor() {
        super()
        this.toast = false
    }

    render() {
        let closeBtn = ''
        if (this.closable) {
            closeBtn = html`
                <crowd-icon-button class='close' name='x' @click='${() => this.hide()}'></crowd-icon-button>
            `
        }
        const article = html`
            <article class='alert' part='alert'>
                <div>
                    <h3>
                        <slot name='icon'></slot>
                        ${this.title}
                    </h3>
                    <slot></slot>
                </div>
                ${closeBtn}
            </article>
        `
        if (this.toast) {
            return html`<div popover>
                ${article}
            </div>`
        }
        return html`
            ${article}
        `;
    }
}
