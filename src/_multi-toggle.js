import { LitElement, html, css } from 'lit';
import {FormController} from './helpers/form-controller.js'

export class MultiToggle extends LitElement {

    static properties = {
        value: {
            type: String,
            reflect: true
        },
        _selectedItems: {
            type: Array
        },
        name: {
            type: String
        },
        label: {type: String},
        required: {type: Boolean},
        errorMessage: {type: String},
        invalid: {type: Boolean, reflect: true},
    }

    static styles = [
        css`
            :host {
                display: inline-block;
            }
            .input-container {
                color: var(--crowd-input-color, inherit);
                font-size: var(--crowd-input-font-size,1rem);
                display: inline-flex;
                flex-flow: row wrap;
                justify-content: flex-start;
                align-items: center;
                gap: var(--crowd-multi-toggle-row-gap, 1em);
            }
            label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .input-container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
        `
    ];

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _setValue() {
        this.value = this._selectedItems.join(',')
        this._dispatchChange()
    }

    constructor() {
        super()
        this._selectedItems = []
    }

    connectedCallback() {
        super.connectedCallback()
        new FormController(this)
        let items = [].slice.call(this.querySelectorAll('crowd-multi-toggle-item'))
        if (items) {
            items.forEach(item => {
                item.addEventListener('crowdChange', () => {
                    if (item.checked) {
                        if (this._selectedItems.indexOf(item.value) < 0) {
                            this._selectedItems.push(item.value)
                        } 
                    } else {
                        if (this._selectedItems.indexOf(item.value) != -1) {
                            let index = this._selectedItems.indexOf(item.value)
                            this._selectedItems.splice(index, 1)
                        }
                    }
                    this._setValue()
                }, false)
            })
        }
    }

    validate() {
        this.invalid = false
        if (this.required) {
            if (this.value === '' || this.value == null) {
                this.invalid = true
            }
        }
    }

    render() {
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let label = ''
        if (this.label) {
            label = html`
                <label part='label'>
                    ${this.label}
                </label>
            `
        }
        return html`
            <div part='wrapper' class='wrapper'>
                ${label}
                <div part='container' class='input-container'>
                    <slot></slot>
                </div>
                ${error}
                <slot name="help-text"></slot>
            </div>
        `;
    }
}

export class MultiToggleItem extends LitElement {

    static properties = {
        checked: {
            type: Boolean,
            reflect: true
        },
        'show-icon': {
            type: Boolean,
            converter: (value, type) => {
                if (typeof value == 'string') {
                    value = value != 'false'
                }
                return value
            }
        },
        'icon-position': {
            type: String
        },
        value: {
            type: String
        }
    }

    static styles = [
        css`
            *,*::before, *::after {
                box-sizing: border-box;
                transition-property: none;
                transition-duration: var(--crowd-multi-toggle-transition-duration, 0.15s);
                transition-delay: var(--crowd-multi-toggle-transition-delay, 0s);
                transition-timing-function: var(--crowd-multi-toggle-transition-ease, ease-in-out);
            }

            :host {
                display: inline-block;
                width: auto;
                line-height: 1;
                height:min-content;
            }

            button {
                -webkit-appearance: none;
                background-color: var(--crowd-multi-toggle-off-background-color, transparent);
                color: var(--crowd-multi-toggle-off-color, inherit);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-off-border-color, #000);
                border-radius: var(--crowd-button-border-radius, 3px);
                font-family: inherit;
                font-size: inherit;
                font-weight: inherit;
                display: inline-flex;
                justify-content: center;
                align-items: center;
                gap: var(--crowd-button-gap, 0.5em);
                padding: var(--crowd-button-padding-vertical,0.5em) var(--crowd-button-padding-horizontal, 1em);
                cursor: pointer;
                transition-property: color, border-color, background-color;
            }

            button > span {
                display: inherit;
                min-width: 1ch;
            }

            @media (hover: hover) {
                button:hover {
                    background-color: var(--crowd-multi-toggle-off-hover-background-color, transparent);
                    color: var(--crowd-multi-toggle-off-hover-color, inherit);
                    border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-off-hover-border-color, #000);
                }
            }

            button:focus-visible,button:active {
                background-color: var(--crowd-multi-toggle-off-hover-background-color, transparent);
                color: var(--crowd-multi-toggle-off-hover-color, inherit);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-off-hover-border-color, #000);
                box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-multi-toggle-off-focus-color, rgba(0,0,0,0.3));
            }

            button.toggled {
                background-color: var(--crowd-multi-toggle-on-background-color, rgba(133, 255, 102,0.3));
                color: var(--crowd-multi-toggle-on-color, #85ff66);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-on-border-color, #85ff66);
                border-radius: var(--crowd-multi-toggle-border-radius, 3px);
            }

            @media (hover: hover) {
                button.toggled:hover {
                    background-color: var(--crowd-multi-toggle-on-hover-background-color, rgba(133, 255, 102,0.3));
                    color: var(--crowd-multi-toggle-on-hover-color, #85ff66);
                    border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-on-hover-border-color, #85ff66);
                }
            }

            button.toggled:focus-visible, button.toggled:active {
                background-color: var(--crowd-multi-toggle-on-hover-background-color, rgba(133, 255, 102,0.3));
                color: var(--crowd-multi-toggle-on-hover-color, #85ff66);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-on-hover-border-color, #85ff66);
                box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-multi-toggle-on-focus-color, rgba(133, 255, 102,0.3));
            }
        `
    ];

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    toggle() {
        this.checked = !this.checked
        this._dispatchChange()
    }

    constructor() {
        super()
        this['icon-position'] = 'after'
        this['show-icon'] = true
    }

    render() {
        let icon = ''
        if (this['show-icon']) {
            icon = html`<span>${this.checked ? 
                html`<slot name='on'><crowd-icon name='x'></crowd-icon></slot>`
                : html`<slot name='off'><crowd-icon name='plus'></crowd-icon></slot>`
            }</span>`
        }
        return html`
            <button part='button' class='${this.checked ? 'toggled' : ''}' @click='${() => this.toggle()}'>
                ${this['icon-position'] == 'before' ? 
                    icon : ''
                }
                <slot></slot>
                ${this['icon-position'] == 'after' ? 
                    icon : ''
                }
            </button>
        `;
    }
}