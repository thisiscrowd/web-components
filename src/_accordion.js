import { LitElement, html, css } from 'lit';

export class AccordionGroup extends LitElement {
    static properties = {
        multiple: {type: Boolean}
    }
    static styles = [
        css`
            :host {
                display: block;
            }
            .container {
                border: var(--crowd-accordion-border-width, 1px) var(--crowd-accordion-border-style, solid) var(--crowd-accordion-border-color, #ddd);
                border-radius: var(--crowd-accordion-border-radius, 4px);
            }
        `
    ];

    connectedCallback() {
        super.connectedCallback()
        let items = [].slice.call(this.querySelectorAll('crowd-accordion-item'))
        if (items) {
            items.forEach(item => {
                item.addEventListener('crowdOpen', () => {
                    items.forEach(oItem => {
                        if (oItem !== item && !this.multiple) {
                            oItem.close()
                        }
                    })
                }, false)
            })
        }
    }

    render() {
        return html`
            <div class='container' part='container'>
                <slot></slot>
            </div>
        `;
    }
}

export class AccordionItem extends LitElement {
    static properties = {
        open: {type: Boolean, reflect: true}
    }
    static styles = [
        css`
            :host {
                display: block;
            }
            * {
                margin: 0;
                padding: 0;
            }
            *,*::before, *::after {
                box-sizing: border-box;
            }
            .tray {
                max-height: 0px;
                transition-property: max-height;
                transition-duration: var(--accordion-item-transition-duration, 0.15s);
                transition-delay: var(--accordion-item-transition-delay, 0s);
                transition-timing-function: var(--accordion-item-ease, ease-in-out);
                overflow-y: scroll;
                padding: 0 var(--crowd-accordion-item-tray-padding-horizontal,1em);
                background-color: var(--crowd-accordion-tray-background-color, #fff);
                pointer-events: none;
            }
            :host([open]) .tray {
                max-height: 900vh;
                padding: var(--crowd-accordion-item-tray-padding-vertical,0.5em) var(--crowd-accordion-item-tray-padding-horizontal,1em);
                pointer-events: all;
            }
            h3 {
                display: flex;
                flex-flow: row nowrap;
                justify-content: space-between;
                align-items: center;
                cursor: pointer;
                padding: var(--crowd-accordion-item-title-padding-vertical,1em) var(--crowd-accordion-item-title-padding-horizontal,1em);
                color: var(--crowd-accordion-item-title-color, #000);
                background-color: var(--crowd-accordion-item-title-background-color, #fff);
                font-weight: var(--crowd-accordion-item-title-weight, 400);
                font-size: var(--crowd-accordion-item-title-font-size, 1.15em);
                font-family: var(--crowd-accordion-title-font-family, inherit);
            }
            @media (hover: hover) {
                h3:hover {
                    color: var(--crowd-accordion-item-title-hover-color, #000);
                    background-color: var(--crowd-accordion-item-title-hover-background-color, #eee);
                }
            }
            input {
                opacity: 0;
                position: absolute;
                -webkit-appearance: none;
            }
            .container:focus-within h3 {
                color: var(--crowd-accordion-item-title-hover-color, #000);
                background-color: var(--crowd-accordion-item-title-hover-background-color, #eee);
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }

            :host([open]) h3 {
                color: var(--crowd-accordion-item-title-open-color, #000);
                background-color: var(--crowd-accordion-item-title-open-background-color, #eee);
                border-bottom: var(--crowd-accordion-border-width, 1px) var(--crowd-accordion-border-style, solid) var(--crowd-accordion-border-color, #ddd);
            }

            h3 crowd-icon {
                transition-property: transform;
                transition-duration: var(--accordion-item-transition-duration, 0.15s);
                transition-delay: var(--accordion-item-transition-delay, 0s);
                transition-timing-function: var(--accordion-item-ease, ease-in-out);
            }
            :host([open]) h3 crowd-icon {
                transform: rotate(180deg);
            }
            :host(:first-child) .container,:host(:first-child) .container h3 {
                border-top-left-radius: var(--crowd-accordion-border-radius, 4px);
                border-top-right-radius: var(--crowd-accordion-border-radius, 4px);
            }
            :host(:last-child) .container,:host(:last-child) .container .tray, :host(:last-child:not([open])) .container h3 {
                border-bottom-left-radius: var(--crowd-accordion-border-radius, 4px);
                border-bottom-right-radius: var(--crowd-accordion-border-radius, 4px);
            }
            :host(:not(:first-child)) .container {
                border-top: var(--crowd-accordion-border-width, 1px) var(--crowd-accordion-border-style, solid) var(--crowd-accordion-border-color, #ddd);
            }
            :host(:nth-child(2):last-child) .container {
                border-top: none;
            }
        `
    ];

    

    show() {
        this.open = true;
        const event = new CustomEvent('crowdOpen');
        this.dispatchEvent(event);
    }

    close() {
        this.open = false;
        const event = new CustomEvent('crowdClose');
        this.dispatchEvent(event);
    }

    toggle() {
        this.open = !this.open;
        let event
        if (this.open) {
            event = new CustomEvent('crowdOpen');
        } else {
            event = new CustomEvent('crowdClose');
        }
        this.dispatchEvent(event);
    }

    _focus() {
        let input = this.renderRoot.querySelector('input')
        input.focus()
    }

    _keyDown(e) {
        switch (e.key) {
            case ' ':
                this.toggle()
                break;
            case 'Enter':
                this.toggle()
                break;
            case 'Tab':
                break;
            default:
                e.preventDefault();
                break;
        }
    }

    render() {
        return html`
            <div class='container' part='container'>
                <input inputmode='none' @keydown='${(e) => this._keyDown(e)}' type='text' />
                <h3 @click='${() => {this._focus();this.toggle()}}'>${this.title}<crowd-icon name='chevron-down'></crowd-icon></h3>
                <div class='tray' part='tray'>
                    <slot></slot>
                </div>
            </div>
        `
    }
}