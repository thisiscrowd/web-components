import {LitElement, css, html} from 'lit';

import {Input} from './_input'

export class TextArea extends Input {
    static styles = css
        `
            ${Input.styles}
            .input-container {
                position: relative;
            }
            .icon {
                position: absolute;
                top: var(--crowd-textarea-icon-top,1ex);
                right: var(--crowd-textarea-icon-right,1ex);
                bottom: var(--crowd-textarea-icon-bottom,auto);
                left: var(--crowd-textarea-icon-left,auto);
            }
        `
    constructor() {
        super()
    }
    render() {
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let success = ''
        if (this.success) {
            success = html`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `
        }
        let maxlength = ''
        if (this.maxlength) {
            maxlength = `maxlength='${this.maxlength}'`
        }
        return html`
            <div part='wrapper' class='wrapper'>
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
                <div part='container' class='input-container'>
                    <textarea rows='4' ${maxlength} @change='${this._dispatchChange}' @input='${this._onInput}' id='${this.id}' name='${this.name}' value='${this.value}' placeholder='${this.placeholder}' required='${this.required}'>${this.value}</textarea>
                    <div class='icon'>
                        <slot name="icon"></slot>
                    </div>
                </div>
                ${error}
                ${success}
                <slot name="help-text"></slot>
            </div>
        `
    }
}