import {LitElement, css, html} from 'lit';

const hideEvent = new Event('crowdDrawerHide', {bubbles: true, composed: true});
const showEvent = new Event('crowdDrawerShow', {bubbles: true, composed: true});

export class Drawer extends LitElement {
    static properties = {
        open: {type: Boolean, reflect: true},
        placement: {type: String}
    };
    static styles = css`
        :host .drawer-container {
            pointer-events: none;
            visibility:hidden;
            background-color: transparent;
            transition-property: visibility;
            transition-duration: var(--crowd-drawer-container-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-drawer-container-ease, ease-in-out);
            transition-delay: var(--crowd-drawer-container-transition-delay, 0.3s);
        }
        .drawer-container {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: var(--crowd-drawer-z-index,9999);
        }
        :host([open]) .drawer-container {
            pointer-events: all;
            visibility: visible;
        }
        .drawer-overlay {
            width: 100%;
            height: 100%;
        }
        :host .drawer-overlay {
            transition-property: background-color;
            transition-duration: var(--crowd-drawer-overlay-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-drawer-overlay-transition-function, ease-in-out);
            transition-delay: var(--crowd-drawer-overlay-transition-delay, 0.3s);
            cursor: pointer;
        }
        :host([open]) .drawer-overlay {
            background-color: var(--crowd-drawer-overlay-background, rgba(0,0,0,0.3));
        }
        .drawer {
            background-color: var(--crowd-drawer-background,white);
            position: absolute;
            top:var(--crowd-drawer-position-top, 0);
            left:var(--crowd-drawer-position-left, 0);
            right:var(--crowd-drawer-position-right, 0);
            bottom:var(--crowd-drawer-position-bottom, 0);
            padding: var(--crowd-drawer-padding-vertical, 2em) var(--crowd-drawer-padding-horizontal, 1em);
            transition-property: transform;
            transition-duration: var(--crowd-drawer-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-drawer-transition-function, ease-in-out);
            overflow-y: scroll;
        }
        .drawer--top {
            transform: translateY(-100%);
        }
        .drawer--right {
            transform: translateX(100%);
        }
        .drawer--left {
            transform: translateX(-100%);
        }
        .drawer--bottom {
            transform: translateY(100%);
        }
        :host([open]) .drawer {
            transform: none;
            transition-delay: var(--crowd-drawer-transition-delay, 0.3s);
        }
    `
    constructor() {
        super()
    }
    connectedCallback() {
        super.connectedCallback()
        this.classList.add('hydrated')
    }
    toggle() {
        this.open = !this.open
        if (!this.open) {
            this.dispatchEvent(hideEvent);
        } else {
            this.dispatchEvent(showEvent);
        }
    }

    show() {
        this.open = true
        this.dispatchEvent(showEvent);
    }

    hide() {
        this.open = false
        this.dispatchEvent(hideEvent);
    }
    render() {
        return html`
            <div part="container" class='drawer-container'>
                <div @click='${() => this.hide()}' part="overlay" class='drawer-overlay'></div>
                <aside part="drawer" class='drawer drawer--${this.placement}'>
                    <slot></slot>
                </aside>
            </div>
        `
    }
}
