import {LitElement, css, html} from 'lit';
import {FormController} from './helpers/form-controller.js'

export class Select extends LitElement {

    static properties = {
        id: {type: String},
        show: {type: Boolean, reflect: true},
        value: {type: String, reflect: true},
        selectedLabel: {type: String},
        name: {type: String},
        placeholder: {type: String},
        required: {type: Boolean},
        label: {type: String},
        clearable: {type: Boolean},
        multiple: {type: Boolean},
        hoist: {type: Boolean},
        errorMessage: {type: String},
        showSuccess: {type: Boolean},
        success: {type: Boolean,reflect: true},
        successMessage: {type: String},
        invalid: {type: Boolean,reflect: true},
        _childIndex: {type: Number},
        _multiSelect: {type: Array},
        _multiLabel: {type: Array},
    };

    static styles = css`
        :host {
            display: inline-block;
        }
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        .wrapper {
            padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
            max-width: 100%;
            position:relative;
        }
        label {
            display: inline-block;
            color: var(--crowd-input-label-color, inherit);
            margin-bottom: var(--crowd-input-label-spacing, 0.5em);
        }
        .select-dropdown {
            box-sizing: border-box;
            position: absolute;
            top:calc(100% - var(--crowd-input-wrapper-padding-vertical, 0) + var(--crowd-select-dropdown-spacing,2px));
            left: 0;
            width: 100%;
            max-height: var(--crowd-select-dropdown-max-height, 50vh);
            overflow-y: scroll;
            padding: var(--crowd-select-dropdown-padding-vertical, 0.5em) var(--crowd-select-dropdown-padding-horizontal, 0.5em);
            pointer-events: none;
            transition-property: opacity, transform;
            transition-duration: var(--crowd-select-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-select-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-transition-delay, 0s);
            opacity: 0;
            transform: scaleY(0.5);
            transform-origin: top center;
            background-color: var(--crowd-select-dropdown-background-color, white);
            z-index: var(--crowd-select-dropdown-z-index, 999);
            box-shadow: var(
                --crowd-select-dropdown-box-shadow,
                0 2px 8px rgba(0, 0, 0, 0.1)
            );
            border: var(--crowd-select-dropdown-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-select-dropdown-border-color, transparent);
        }
        :host([hoist]) .select-dropdown {
            position:fixed;
            top: auto;
            left: auto;
        }
        :host([show]) .select-dropdown {
            opacity: 1;
            transform: scale(1);
            pointer-events: all;
        }
        input,.multiple-items {
            -webkit-appearance: none;
            background-color: transparent;
            border: none;
            height: 100%;
            width: calc(100% - (2 * var(--crowd-input-padding-horizontal, 1em)));
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            padding: 0 var(--crowd-input-padding-horizontal, 1em);
            font-weight: var(--crowd-input-font-weight,400);
            height: calc(
                var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
            );
            caret-color: transparent;
            cursor: pointer;
            text-transform: var(--crowd-input-text-transform);
        }
        input.multi {
            opacity: 0;
        }
        input:focus-visible, input:active {
            outline: none;
        }
        input::placeholder {
            color: var(--crowd-input-placeholder-color, inherit);
        }
        .input-container {
            color: var(--crowd-input-color, inherit);
            background-color: var(--crowd-input-background, white);
            border: var(--crowd-input-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, transparent);
            border-radius: var(--crowd-input-border-radius, 0px);
            font-size: var(--crowd-input-font-size,1rem);
            padding: var(--crowd-input-padding-vertical, 1em) 0;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: stretch;
            position: relative;
        }
        .input-container:focus-within, .input-container:focus-visible {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        [part='overlay'] {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            pointer-events: none;
            z-index: calc(var(--crowd-select-dropdown-z-index, 999) - 1);
        }
        :host([show]) [part='overlay'] {
            pointer-events: all;
        }
        button {
            -webkit-appearance: none;
            background-color: transparent;
            color: var(--crowd-select-icon-color, inherit);
            border: none;
            padding: 0;
            display: grid;
            place-items: center;
            transition-property: transform,color;
            transition-duration: var(--crowd-select-icon-transition-duration,0.15s);
            transition-timing-function: var(--crowd-select-icon-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-icon-transition-delay, 0s);
            margin-right: var(--crowd-input-padding-horizontal, 1em);
            cursor: pointer;
            position:relative;
            z-index: 2;
        }
        .error,.success {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        .success {
            color: var(--crowd-input-success-message-color, lime);
        }
        @media (hover: hover) {
            button:hover {
                color: var(--crowd-select-icon-hover-color, inherit);
            }
        }
        :host([show]) button[part='toggle'] {
            transform: rotate(180deg);
        }
        :host([invalid]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-error-message-color, red);
        }
        :host([success]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-success-message-color, lime);
        }
        .multiple-items {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            bottom: 0;
            left: 0;
            right: 0;
            overflow-y: hidden;
            overflow-x: scroll;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
            gap: 4px;
            z-index: 1;
        }
        @supports (::-moz-range-track) {
            .multiple-items {
                scrollbar-width: thin;
                scrollbar-color: var(--crowd-select-multi-scrollbar-color,#000);
            }
        }
        .multiple-items::-webkit-scrollbar {
            height: 2px;
            background-color: var(--crowd-select-multi-scrollbar-background,#aaa);
        }
        .multiple-items::-moz-scrollbar-button, .multiple-items::-webkit-scrollbar-button {
            width: 0px;
            display: none;
        }
        .multiple-items::-webkit-scrollbar-thumb {
            background: var(--crowd-select-multi-scrollbar-color,#000);
        }
        .multiple-items crowd-badge {
            pointer-events: auto;
            gap: 2px;
        }
    `

    constructor() {
        super()
        this.placeholder = 'Please select'
        this.show = false
        this.childIndex = -1;
        this.invalid = false;
    }

    connectedCallback() {
        super.connectedCallback()
        this.id = 'select-' + Date.now()
        new FormController(this)
        this._multiSelect = [];
        this._multiLabel = [];
        if (this.multiple && this.value) {
            this._multiSelect = this.value.split(',')
            this._multiLabel = this.value.split(',')
            console.log(this._multiSelect);
            if (this._multiSelect.length > 0) {
                this.value = this._multiSelect.join()
                this.selectedLabel = this._multiLabel.join()
            } else {
                this.value = ''
                this.selectedLabel = ''
            }
            console.log(this.value);
        }
    }

    firstUpdated() {
        let options = [].slice.call(this.querySelectorAll('crowd-option'));
        if (options) {
            options.forEach(option => {
                option.addEventListener('click', () => {
                    this._selectOption(option)
                    if (!this.multiple) {
                        this.close()
                    }
                }, false);
                console.log(option.value.toUpperCase());
                if (this.value && this.value.toUpperCase().includes(option.value.toUpperCase())) {
                    option.click()
                }
            })
        }
    }

    updated() {
        let options = [].slice.call(this.querySelectorAll('crowd-option'));
        if (options) {
            options.forEach(option => {
                if (this.value && this.value == option.value) {
                    option.isActive = true
                    if (this.selectedLabel != option.innerHTML.replace(/(<([^>]+)>)/gi, "")) {
                        this.selectedLabel = option.innerHTML.replace(/(<([^>]+)>)/gi, "")
                    }
                } else if (this.multiple && this.value) {
                    if (this._multiSelect.indexOf(option.value) > -1) {
                        option.isActive = true
                    } else {
                        option.isActive = false
                    }
                } else {
                    option.isActive = false
                }
            })
        }
    }

    open() {
        this.show = true
    }

    close() {
        this.show = false
        this.childIndex = -1
        this._blurOptions()
    }

    _blurOptions() {
        let options = [].slice.call(this.querySelectorAll('crowd-option'));
        if (options) {
            options.forEach(option => {
                option.blur()
            })
        }
    }

    validate() {
        this.invalid = false
        this.success = false
        if (this.required) {
            if (this.value === '' || this.value == null) {
                this.invalid = true
            }
        }
        if (!this.invalid && this.showSuccess) {
            this.success = true
        }
        const event = new CustomEvent('crowdValidate', {
            detail: {
                value: this.value
            }
        });
        this.dispatchEvent(event);
    }

    toggle() {
        this.show = !this.show
        if (!this.show) {
            this.childIndex = -1
            this._blurOptions()
        }
    }

    clear() {
        this._multiSelect = []
        this._multiLabel = []
        this.value = ''
        this.selectedLabel = ''
        let options = [].slice.call(this.querySelectorAll('crowd-option'));
        if (options) {
            options.forEach(option => {
                option.isActive = false
            })
        }
        this.validate()
        this._dispatchChange()
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _onInput(e) {
        e.preventDefault()
        this.invalid = false
        // this.value = e.currentTarget.value
        // this._dispatchChange()
    }

    _keyDown(e) {
        if (e.key === 'Tab') {
            return
        }
        e.preventDefault()
        let options = [].slice.call(this.querySelectorAll('crowd-option'));
        if (e.key === "Enter") {
            this.open()
            if (this.childIndex != -1) {
                options[this.childIndex].click()
            }
        } else if (e.key === 'ArrowDown') {
            this.open()
            if (options[this.childIndex]) {
                options[this.childIndex].blur()
            }
            if (options.length == this.childIndex + 1) {
                this.childIndex = 0
            } else {
                this.childIndex = this.childIndex + 1
            }
            if (options[this.childIndex]) {
                options[this.childIndex].focus()
            }
        } else if (e.key === 'ArrowUp') {
            this.open()
            if (options[this.childIndex]) {
                options[this.childIndex].blur()
            }
            if (0 == this.childIndex) {
                this.childIndex = options.length - 1
            } else {
                this.childIndex = this.childIndex - 1
            }
            if (options[this.childIndex]) {
                options[this.childIndex].focus()
            }
        } else {
            this.open()
            if (options) {
                options.forEach((option,index) => {
                    if (option.value.toUpperCase()[0] == e.key.toUpperCase()) {
                        options.forEach(o => o.blur())
                        option.focus()
                        this.childIndex = index
                        return
                    }
                })
            }
        }
    }

    _selectOption(option) {
        if (!option || !option.value || !option.innerHTML) {
            console.warn('<crowd-select>: Could not find element for option',option)
        }
        let value = option.value
        let label = option.innerHTML.replace(/(<([^>]+)>)/gi, "")
        if (this.multiple) {
            if (this._multiSelect.indexOf(value) < 0) {
                this._multiSelect.push(value)
                this._multiLabel.push(label)
            } else {
                this._multiSelect.splice(this._multiSelect.indexOf(value),1)
                this._multiLabel.splice(this._multiLabel.indexOf(label),1)
            }
            if (this._multiSelect.length > 0) {
                this.value = this._multiSelect.join()
                this.selectedLabel = this._multiLabel.join()
            } else {
                this.value = ''
                this.selectedLabel = ''
            }
        } else {
            this.value = value
            this.selectedLabel = label
        }
        this.validate()
        this._dispatchChange()
    }

    _hoistPosition() {
        let dropdownEl = this.renderRoot.querySelector('.select-dropdown')
        let container = this.renderRoot.querySelector('.input-container')
        if (dropdownEl && container) {
            dropdownEl.style.top = container.getBoundingClientRect().bottom + 'px';
            dropdownEl.style.left = container.getBoundingClientRect().left + 'px';
            dropdownEl.style.width = container.getBoundingClientRect().width + 'px';
        }
        requestAnimationFrame(() => {this._hoistPosition()})
    }

    render() {
        let clearBtn = '';
        if (this.clearable && this.value) {
            clearBtn = html`
                <button @click='${this.clear}' part="clear" name="clear" aria-label="clear select">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </button>
            `
        }
        let dropdown = html`
            <div class='select-dropdown'>
                <slot></slot>
            </div>
        `
        if (this.hoist) {
            requestAnimationFrame(() => {this._hoistPosition()})
        }
        let label = '';
        if (this.label) {
            label = html`
                <label @click='${this.open}' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let success = ''
        if (this.success) {
            success = html`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `
        }
        let showMultiple = html``
        if (this.multiple && this._multiSelect.length) {
            showMultiple = html`
                <div class='multiple-items' @click='${this.open}'>
                    ${this._multiSelect.map(item => {
                        return html`<crowd-badge pill @click='${(e) => {e.stopImmediatePropagation();this._selectOption(this.querySelector(`crowd-option[value='${item}']`))}}'>${item}<crowd-icon name='x'></crowd-icon></crowd-badge>`
                    })}
                </div>
            `
        }
        return html`
            <div part='wrapper' class='wrapper'>
                ${label}
                <div part='container' class='input-container' @click='${this.open}'>
                    <input inputmode='none' class='${this._multiSelect.length ? "multi" : ""}' @change='${this._dispatchChange}' @keydown='${this._keyDown}' @input='${this._onInput}' id='${this.id}' type='text' name='${this.name}' value='${this.selectedLabel}' placeholder='${this.placeholder}' required='${this.required}' />
                    ${showMultiple}
                    ${clearBtn}
                    <slot name="icon"></slot>
                    <button @click='${this.toggle}' part="toggle" name="toggle" aria-label="toggle dropdown">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </button>
                </div>
                ${error}
                ${success}
                ${this.hoist ? '' : dropdown}
            </div>
            <div @click='${this.close}' part='overlay'></div>
            ${this.hoist ? dropdown : ''}
        `
    }
}

export class Option extends LitElement {
    static properties = {
        value: {type: String},
        isFocus: {type: Boolean,reflect: true,attribute: 'focus'},
        isActive: {type: Boolean,reflect: true,attribute: 'active'}
    }

    static styles = css`
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        [part='container'] {
            padding: var(--crowd-option-padding-vertical, 0.2em) var(--crowd-option-padding-horizontal, 1em);
            font-family: var(--crowd-option-font-family, inherit);
            font-size: var(--crowd-option-font-size, inherit);
            font-weight: var(--crowd-option-font-weight, inherit);
            color: var(--crowd-option-color, inherit);
            background-color: var(--crowd-option-background-color, transparent);
            width: calc(100% - (1 * var(--crowd-option-padding-horizontal, 1em)));
            margin: 0 calc(-1 * var(--crowd-select-dropdown-padding-horizontal, 0.5em));
            cursor: pointer;
            transition-property: color, background-color;
            transition-duration: var(--crowd-option-transition-duration,0.15s);
            transition-timing-function: var(--crowd-option-transition-timing-function, ease-in-out);
            transition-delay: var(--crowd-option-transition-delay, 0s);
            border-radius: var(--crowd-option-border-radius,0px);
        }
        @media  (hover: hover) {
            [part='container']:hover {
                color: var(--crowd-option-hover-color, inherit);
                background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
            }
        }
        [part='container']:focus-visible, :host([focus]) [part='container'],:host([active]) [part='container'] {
            color: var(--crowd-option-hover-color, inherit);
            background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
        }
    `

    constructor() {
        super()
        this.isFocus = false
        this.isActive = false
    }

    focus() {
        this.isFocus = true
    }

    blur() {
        this.isFocus = false
    }

    render() {
        return html`
            <div part='container'>
                <slot></slot>
            </div>
        `
    }
}