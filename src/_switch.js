import { LitElement, html, css } from 'lit';

export class Switch extends LitElement {
    static properties = {
        checked: {type: Boolean,reflect: true},
        label: {type: String},
        value: {type: String, reflect: true},
        name: {type: String},
        true: {type: String},
        false: {type: String}
    }
    static styles = [
        css`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            .container {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                gap: var(--crowd-switch-spacing, 0.5em);
            }
            .container > span {
                cursor: pointer;
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            :host label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin: 0 0 var(--crowd-input-label-spacing, 0.5em);
            }
            .track {
                position:relative;
                padding: var(--crowd-switch-track-spacing, 3px);
                border-radius: var(--crowd-switch-border-radius, 999px);
                width: calc((2 * var(--crowd-switch-size, 1em)) + (2 * var(--crowd-switch-track-spacing, 3px)));
                height: calc((var(--crowd-switch-size, 1em)) + (2 * var(--crowd-switch-track-spacing, 3px)));
                background-color: var(--crowd-switch-track-background-color, #eeeeee);
                cursor: pointer;
                transition-property: background-color;
                transition-duration: var(--crowd-switch-transition-duration,0.15s);
                transition-delay: var(--crowd-switch-transition-delay, 0s);
                transition-timing-function: var(--crowd-switch-ease, ease-in-out);
            }
            .track:focus-within {
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            .thumb {
                position: absolute;
                display: block;
                width: var(--crowd-switch-size, 1em);
                height: var(--crowd-switch-size, 1em);
                border-radius: var(--crowd-switch-border-radius, 999px);
                left: var(--crowd-switch-track-spacing, 3px);
                right: auto;
                background-color: var(--crowd-switch-thumb-background-color, #ffffff);
                transition-property: left, right;
                transition-duration: var(--crowd-switch-transition-duration,0.15s);
                transition-delay: var(--crowd-switch-transition-delay, 0s);
                transition-timing-function: var(--crowd-switch-ease, ease-in-out);
            }
            .on {
                color: var(--crowd-switch-unswitched-text-color, rgba(0,0,0,0.5));
            }
            :host([checked]) .on {
                color: inherit;
            }
            :host([checked]) .off {
                color: var(--crowd-switch-unswitched-text-color, rgba(0,0,0,0.5));
            }

            :host([checked]) .thumb {
                left: 50%;
            }
            :host([checked]) .track {
                background-color: var(--crowd-switch-track-background-color-active, #000000);
            }
            input[type='text'] {
                -webkit-appearance: none;
                opacity: 0;
                position: absolute;
                width: 0;
                height: 0;
            }
        `
    ];

    _updateValue() {
        if (this.checked && this.true) {
            this.value = this.true
        } else if (this.checked) {
            this.value = 'true'
        } else if (!this.checked && this.false) {
            this.value = this.false
        } else if (!this.checked) {
            this.value = 'false'
        }
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _toggle() {
        this.checked = !this.checked;
        let input = this.renderRoot.querySelector('input')
        if (input) {
            input.focus()
        }
        this._updateValue();
        this._dispatchChange();
    }

    _check() {
        this.checked = true;
        let input = this.renderRoot.querySelector('input')
        if (input) {
            input.focus()
        }
        this._updateValue();
        this._dispatchChange();
    }

    _unCheck() {
        this.checked = false;
        let input = this.renderRoot.querySelector('input')
        if (input) {
            input.focus()
        }
        this._updateValue();
        this._dispatchChange();
    }

    _keyDown(e) {
        e.preventDefault()
        switch (e.key) {
            case ' ':
                this._toggle()
                break;
            case 'ArrowLeft':
                this._unCheck()
                break;
            case 'ArrowRight':
                this._check()
                break;
            default:
                break;
        }
    }

    constructor() {
        super()
        this.checked = false;
        this.value = 'false';
    }
    
    connectedCallback() {
        super.connectedCallback()
        this.id = 'switch-' + Date.now()
    }

    render() {
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let label = ''
        if (this.label) {
            label = html`
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        let offText = ''
        if (this.false) {
            offText = html`
                <span class='off' part='off' @click='${() => this._unCheck()}'>${this.false}</span>
            `
        }
        let onText = ''
        if (this.true) {
            onText = html`
                <span class='on' part='on' @click='${() => this._check()}'>${this.true}</span>
            `
        }
        return html`
            <div part='wrapper' class='wrapper'>
                ${label}
                <div class='container' part='container'>
                    ${offText}
                    <div @click='${() => this._toggle()}' class='track' part='track'>
                        <span class='thumb' part='thumb'></span>
                        <input inputmode='none' aria-label='Toggle switch' @keydown='${(e) => this._keyDown(e)}' id='${this.id}' type='text' />
                    </div>
                    ${onText}
                </div>
                ${error}
                <slot name="help-text"></slot>
            </div>
        `;
    }
}