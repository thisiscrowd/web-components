import { LitElement, html, css } from 'lit';

export class Badge extends LitElement {

    static properties = {
        pill: {type: Boolean},
        pulse: {type: Boolean},
        type: {type: String}
    }
    
    static styles = [
        css`
            :host {
                display: inline-grid;
                place-items: center;
            }

            :host([type='danger']) {
                --crowd-badge-color: #f92f06;
            }

            :host([type='warning']) {
                --crowd-badge-color: #ffd000;
            }

            :host([type='success']) {
                --crowd-badge-color: #65d672;
            }

            .badge {
                color: var(--crowd-badge-text-color, inherit);
                background-color: var(--crowd-badge-color);
                display: inline-flex;
                align-items: center;
                justify-content: center;
                font-size: inherit;
                font-weight: inherit;
                line-height: 1;
                white-space: nowrap;
                padding: var(--crowd-badge-padding-vertical,3px) var(--crowd-badge-padding-horizontal, 6px);
                user-select: none;
                cursor: inherit;
            }

            :host([pill]) .badge {
                border-radius: 999px;
            }

            :host([pulse]) .badge {
                animation: 1.5s ease 0s infinite normal none running pulse;
            }

            .badge {
                border-radius: var(--crowd-badge-border-radius, 0px);
            }

            @keyframes pulse {
                0% {
                    box-shadow: 0 0 0 0 var(--pulse-color, var(--crowd-badge-color));
                }
                70% {
                    box-shadow: 0 0 0 0.5rem transparent;
                }
                100% {
                    box-shadow: 0 0 0 0 transparent;
                }
            }
        `
    ];

    render() {
        return html`
            <span class='badge' part='badge'>
                <slot></slot>
            </span>
        `;
    }
}
