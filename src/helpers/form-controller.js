export class FormController {
    constructor(element) {
        this.element = element
        this.form = element.closest('form')
        this.bindHandleFormData = this.handleFormData.bind(this)
        if (this.form && this.element.name) {
            this.form.addEventListener('formdata', this.bindHandleFormData, false)
        }
        this.bindHandleFormSubmit = this.handleFormSubmit.bind(this)
        if (this.form) {
            this.form.addEventListener('submit', this.bindHandleFormSubmit, false)
        }
    }

    handleFormSubmit(e) {
        const disabled = this.element.disabled;
        if (this.element.validate) {
            this.element.validate()
        }
        if (this.form && !this.form.noValidate && !disabled && this.element.invalid) {
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    }

    handleFormData(e) {
        if (this.element.value == undefined) {
            e.formData.append(this.element.name, null)
        } else {
            if (Array.isArray(this.element.value)) {
                this.element.value.forEach(val => {
                    e.formData.append(this.element.name, val.toString())
                })
            } else {
                console.log(this.element.name, this.element.value.toString())
                e.formData.append(this.element.name, this.element.value.toString())
            }
        }
        for(var pair of e.formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        }
    }
}