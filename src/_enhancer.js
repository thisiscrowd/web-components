class Enhancer {
    constructor() {
        
    }

    _initEl(element) {
        const elType = element.getAttribute('cc-element')
        const newEl = document.createElement(elType)
        let attributes = [].slice.call(element.attributes)
        attributes.forEach(attr => {
            newEl.setAttribute(attr.nodeName,attr.nodeValue)
        })
        newEl.innerHTML = element.innerHTML
        newEl.removeAttribute('cc-element')
        element.parentElement.replaceChild(newEl,element)
    }

    init() {
        const elsForEnhancing = [].slice.call(document.querySelectorAll('[cc-element]'))

        if (elsForEnhancing) {
            elsForEnhancing.reverse().forEach(element => {
                this._initEl(element)
            })
        }
    }
}

export {
    Enhancer
}