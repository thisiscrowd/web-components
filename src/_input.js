import {LitElement, css, html} from 'lit';
import {FormController} from './helpers/form-controller.js'

export class Input extends LitElement {

    static properties = {
        type: {type: String, reflect: true},
        name: {type: String},
        value: {
            type: String, reflect: true
        },
        placeholder: {type: String},
        label: {type: String},
        required: {type: Boolean},
        togglePassword: {type: Boolean},
        showPassword: {type: Boolean},
        errorMessage: {type: String},
        showSuccess: {type: Boolean},
        success: {type: Boolean,reflect: true},
        successMessage: {type: String},
        invalid: {type: Boolean, reflect: true},
        maxlength: {type: String}
    };

    static styles = css`
        :host {
            display: block;
        }
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        input,textarea {
            -webkit-appearance: none;
            background-color: transparent;
            border: none;
            height: 100%;
            width: calc(100% - (2 * var(--crowd-input-padding-horizontal, 1em)));
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            padding: 0 var(--crowd-input-padding-horizontal, 1em);
            font-weight: var(--crowd-input-font-weight,400);
            height: calc(
                var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
            );
            text-transform: var(--crowd-input-text-transform);
            flex: 1 1 auto;
        }
        textarea {
            height: auto;
            padding: var(--crowd-textarea-padding-vertical, 1em) var(--crowd-textarea-padding-horizontal, 1em);
        }
        input:focus-visible, input:active, textarea:focus-visible, textarea:active {
            outline: none;
        }
        input::placeholder,textarea::placeholder {
            color: var(--crowd-input-placeholder-color, inherit);
        }
        .input-container {
            color: var(--crowd-input-color, inherit);
            background-color: var(--crowd-input-background, white);
            border: var(--crowd-input-border-width,1px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, #eee);
            border-radius: var(--crowd-input-border-radius, 0px);
            font-size: var(--crowd-input-font-size,1rem);
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: stretch;
        }
        .input-container:focus-within {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        label {
            display: inline-block;
            color: var(--crowd-input-label-color, inherit);
            margin-bottom: var(--crowd-input-label-spacing, 0.5em);
        }
        .wrapper {
            padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
            max-width: 100%;
        }
        .password-toggle {
            -webkit-appearance: none;
            background-color: transparent;
            color: var(--crowd-input-password-toggle-color, inherit);
            font-family: inherit;
            padding: 0;
            border: none;
            display: grid;
            place-items:center;
            margin-right: var(--crowd-input-padding-horizontal, 1em);
            cursor: pointer;
        }
        .error,.success {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        .success {
            color: var(--crowd-input-success-message-color, lime);
        }
        :host([invalid]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-error-message-color, red);
        }
        :host([success]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-success-message-color, lime);
        }
        slot[name='help-text'] {
            font-size:var(--crowd-input-error-message-font-size, 0.8em);
        }
    `

    constructor() {
        super()
        this.showPassword = false
        this.invalid = false
        this.showSuccess = false
        this.success = false
    }

    connectedCallback() {
        super.connectedCallback()
        this.id = 'input-' + Date.now()
        this.classList.add('hydrated')
        new FormController(this)
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _onInput(e) {
        this.invalid = false
        this.value = e.currentTarget.value
        this.validate()
        this._dispatchChange()
    }

    _togglePassword() {
        this.showPassword = !this.showPassword
    }

    _getType() {
        if (this.type === 'password' && this.showPassword) {
            return 'text'
        }
        return this.type
    }

    _validateEmail() {
        if (this.value && this.value.match(/^.+@\w+\.\w+/g) == null) {
            this.invalid = true
        }
    }

    validate() {
        this.invalid = false
        this.success = false
        if (this.type == 'email') {
            this._validateEmail()
        }
        if (this.required) {
            if (this.value === '' || this.value == null) {
                this.invalid = true
            }
        }
        if (!this.invalid && this.showSuccess) {
            this.success = true
        }
        const event = new CustomEvent('crowdValidate', {
            detail: {
                value: this.value
            }
        });
        this.dispatchEvent(event);
    }

    render() {
        let passwordBtn = ''
        if (this.type === 'password' && this.togglePassword) {
            passwordBtn = html`
                <button tabindex='-1' part='password-toggle' class='password-toggle' @click='${this._togglePassword}'>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"></path>
                        <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"></path>
                    </svg>
                </button>
            `
        }
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let success = ''
        if (this.success) {
            success = html`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `
        }
        let maxlength = ''
        if (this.maxlength) {
            maxlength = `maxlength='${this.maxlength}'`
        }
        let label = ''
        if (this.label) {
            label = html`
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        return html`
            <div part='wrapper' class='wrapper'>
                ${label}
                <div part='container' class='input-container'>
                    <input ${maxlength} @change='${this._dispatchChange}' @input='${this._onInput}' id='${this.id}' type='${this._getType()}' name='${this.name}' value='${this.value}' placeholder='${this.placeholder}' required='${this.required}' />
                    <slot name="icon"></slot>
                    ${passwordBtn}
                </div>
                ${error}
                ${success}
                <slot name="help-text"></slot>
            </div>
        `
    }
}