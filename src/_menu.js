import { LitElement, html, css } from 'lit';

export class Menu extends LitElement {
    static styles = [
        css`
            :host, :host * {
                box-sizing: inherit;
            }
            ul {
                list-style: none;
                margin: 0;
                padding: var(--crowd-menu-padding-vertical,0.5em) var(--crowd-menu-padding-horizontal,0.5em);
                background-color: var(--crowd-menu-background-color,white);
                border: var(--crowd-menu-border-width, 0px) var(--crowd-menu-border-style, solid) var(--crowd-menu-border-color, black);
                border-radius: var(--crowd-menu-border-radius, 0px);
                display: flex;
                flex-flow: column nowrap;
                justify-content: flex-start;
                align-items: stretch;
                max-height: 50vh;
                overflow: visible;
            }
        `
    ];

    render() {
        return html`
            <ul part='menu'>
                <slot></slot>
            </ul>
        `;
    }
}
