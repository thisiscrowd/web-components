import {LitElement, css, html} from 'lit';

const hideEvent = new Event('crowdDialogHide', {bubbles: true, composed: true});
const showEvent = new Event('crowdDialogShow', {bubbles: true, composed: true});

export class Dialog extends LitElement {
    static properties = {
        open: {type: Boolean, reflect: true},
        label: {type: String}
    }
    static styles = css`
        :host .dialog-container {
            pointer-events: none;
            visibility:hidden;
            background-color: transparent;
            transition-property: visibility;
            transition-duration: var(--crowd-dialog-container-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-dialog-container-transition-function, ease-in-out);
            transition-delay: var(--crowd-dialog-container-transition-delay, 0.3s);
        }
        :host([open]) .dialog-container {
            pointer-events: all;
            visibility:visible;
            transition-property: none;
        }
        :host .dialog-overlay {
            transition-property: background-color;
            transition-duration: var(--crowd-dialog-overlay-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-dialog-overlay-transition-function, ease-in-out);
            transition-delay: var(--crowd-dialog-overlay-transition-delay, 0.3s);
        }
        :host([open]) .dialog-overlay {
            background-color: var(--crowd-dialog-overlay-background, rgba(0,0,0,0.3));
        }
        dialog::backdrop {
            background-color: var(--crowd-dialog-overlay-background, rgba(0,0,0,0.3));
        }
        :host dialog {
            transform: scale(0);
        }
        :host([open]) dialog {
            transform: scale(1);
            transition-delay: var(--crowd-dialog-transition-delay, 0.2s);
        }
        dialog {
            all:unset;
            display: block;
        }
        dialog {
            background-color: var(--crowd-dialog-background, white);
            padding: var(--crowd-dialog-padding-vertical,1em) var(--crowd-dialog-padding-horizontal,1em);
            height: min(var(--height,calc(100% - (2 * var(--crowd-dialog-spacing-vertical,1em)))), 100%);
            width: min(var(--width,calc(100% - (2 * var(--crowd-dialog-spacing-horiztonal,1em)))), 100%);
            overflow-y: scroll;
            transition-property: transform;
            transition-duration: var(--crowd-dialog-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-dialog-transition-function, ease-in-out);
            position: relative;
            border-radius: var(--crowd-dialog-border-radius,0px);
            box-shadow: var(--crowd-dialog-box-shadow,
                0 1px 1px hsl(0deg 0% 0% / 0.075),
                0 2px 2px hsl(0deg 0% 0% / 0.075),
                0 4px 4px hsl(0deg 0% 0% / 0.075),
                0 8px 8px hsl(0deg 0% 0% / 0.075),
                0 16px 16px hsl(0deg 0% 0% / 0.075)
            );
                
        }
        h2 {
            margin: var(--crowd-dialog-heading-margin-top, 0px) 0 var(--crowd-dialog-heading-margin-bottom, 0px);
        }
        .dialog-container {
            display: grid;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            place-items: center;
            padding: var(--crowd-dialog-spacing-vertical, 2em) var(--crowd-dialog-spacing-horizontal, 2em);
            z-index: var(--crowd-dialog-z-index,9999);
        }
        .dialog-overlay {
            position:absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            cursor: pointer;
        }
        dialog > button.dialog-close {
            -webkit-appearance: none;
            background-color: var(--crowd-dialog-close-background,transparent);
            color: var(--crowd-dialog-close-color, black);
            border: var(--crowd-dialog-close-border-width,0px) var(--crowd-dialog-close-border-type,solid) var(--crowd-dialog-close-border-color,transparent);
            font-size: var(--crowd-dialog-close-font-size,2rem);
            line-height: 1;
            position: absolute;
            top: 0;
            right: 0;
            cursor: pointer;
        }
        @media (hover: hover) {
            dialog > button.dialog-close:hover {
                opacity: var(--crowd-dialog-close-hover-opacity, 0.6);
            }
        }
        @media (prefers-reduced-motion: reduce) {
            :host {
                --crowd-dialog-container-transition-duration: 0s;
                --crowd-dialog-container-transition-delay: 0s;
                --crowd-dialog-overlay-transition-duration: 0s;
                --crowd-dialog-overlay-transition-delay: 0s;
                --crowd-dialog-transition-duration: 0s;
                --crowd-dialog-transition-delay: 0s;
            }
        }
    `;
    constructor() {
        super()
        this.open = false
    }

    toggle() {
        this.open = !this.open
        this.renderRoot.querySelector('dialog').show()
        if (!this.open) {
            this.dispatchEvent(hideEvent);
        } else {
            this.dispatchEvent(showEvent);
        }
    }

    show() {
        this.open = true
        this.renderRoot.querySelector('dialog').show()
        this.dispatchEvent(showEvent);
    }

    hide() {
        this.open = false
        this.renderRoot.querySelector('dialog').close()
        this.dispatchEvent(hideEvent);
    }

    render() {
        let label
        if (this.label) {
            label = html`<h2>${this.label}</h2>`
        }
        return html`
            <div class='dialog-container' part="container">
            <div class='dialog-overlay' part="overlay" @click="${() => this.hide()}"></div>
                <dialog ${this.open ? 'open' : ''} @close='${this.dispatchEvent(hideEvent)}' @cancel='${this.dispatchEvent(hideEvent)}'>
                    <button part="close" name='close dialog' aria-label='close dialog' class='dialog-close' @click="${() => this.hide()}"
                        ><crowd-icon name='x'></crowd-icon></button>
                    ${label}
                    <slot></slot>
                </dialog>
            </div>
        `
    }
}