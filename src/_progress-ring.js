import { LitElement, html, css } from 'lit';

export class ProgressRing extends LitElement {

    static properties = {
        percentage: {type: String},
        size: {type: String},
        trackWidth: {type: String, attribute: "stroke-width"}
    }

    static styles = [
        css`
            :host, :host * {
                box-sizing: inherit;
            }
            .progress-ring {
                display: inline-flex;
                align-items: center;
                justify-content: center;
                position: relative;
            }
            .progress-ring__image {
                width: var(--size,1em);
                height: var(--size,1em);
                transform: rotate(-90deg);
                transform-origin: 50% 50%;
            }
            .progress-ring__track {
                stroke: var(--track-color,rgba(0,0,0,0.2));
            }
            .progress-ring__track, .progress-ring__indicator {
                --radius: calc(var(--size,1em) / 2 - var(--track-width,2px) * 2);
                --circumference: calc(var(--radius) * 2 * 3.14159);
                fill: none;
                stroke-width: var(--track-width,2px);
                r: var(--radius);
                cx: calc(var(--size,1em) / 2);
                cy: calc(var(--size,1em) / 2);
            }
            .progress-ring__indicator {
                stroke: var(--indicator-color,black);
                stroke-linecap: round;
                transition: stroke-dashoffset 0.35s ease 0s;
                stroke-dasharray: var(--circumference) var(--circumference);
                stroke-dashoffset: calc(var(--circumference) - var(--percentage) * var(--circumference));
            }
            .progress-ring__label {
                display: flex;
                align-items: center;
                justify-content: center;
                position: absolute;
                top: 0px;
                left: 0px;
                width: 100%;
                height: 100%;
                text-align: center;
                user-select: none;
            }
        `
    ];

    render() {
        return html`
            <div part="base" class="progress-ring" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="${this.percentage}">
                <svg class="progress-ring__image" style='${this.size ? `--size: ${this.size};` : ``} ${this.trackWidth ? `--track-width:${this.trackWidth}` : ``}'>
                    <circle class="progress-ring__track"></circle>
                    <circle class="progress-ring__indicator" style="--percentage: ${this.percentage / 100};"></circle>
                </svg>

                <span part="label" class="progress-ring__label">
                    <slot></slot>
                </span>
            </div>
        `;
    }
}
