import {LitElement, css, html} from 'lit';

// const submitEvent = new Event('crowdFormSubmit', {bubbles: true, composed: true});
function navigateChildren(element) {
    let children = []
    if (element.children.length) {
        let childrenArray = Array.prototype.slice.call(element.children)
        childrenArray.forEach(child => {
            children.push(child)
            if (child.children.length) {
                children = [
                    ...children,
                    ...navigateChildren(child)
                ]
            }
        })
    }
    return children
}

export class Form extends LitElement {

    static styles = css`
        :host {
            display: block;
        }
        .form-container {
            padding: var(--crowd-form-padding-vertical, 1em) var(--crowd-form-padding-horizontal, 1em);
        }
    `

    constructor() {
        super()
    }

    connectedCallback() {
        super.connectedCallback()
        let submits = [].slice.call(this.querySelectorAll('[submit]'))
        if (submits) {
            submits.forEach(submit => {
                submit.addEventListener('click', () => {this.submit()}, false)
            })
        }
    }

    validate() {
        return new Promise((res,rej) => {
            this.getFormControls().then(controls => {
                let allValid = true
                controls.forEach(control => {
                    if (control.validate) {
                        control.validate()
                        if (control.invalid) {
                            allValid = false
                        }
                    }
                })
                if (allValid) {
                    res()
                }
                rej()
            })
        })
    }

    getFormControls() {
        let children = navigateChildren(this)
        return new Promise((res,rej) => {
            res(
                children.filter((child) => {
                    return child.name
                })
            )
        })
    }

    getFormData() {
        return new Promise((res, rej) => {
            let formData = new FormData();
            this.getFormControls().then(controls => {
                controls.forEach(control => {
                    if (control.tagName == 'CROWD-CHECKBOX' && control.checked) {
                        formData.append(control.name,control.value)
                    } else if (control.tagName != 'CROWD-CHECKBOX') {
                        formData.append(control.name,control.value)
                    }
                })
                res(formData)
            })
        })
    }

    submit() {
        this.validate().then(() => {
            this.getFormData().then(formData => {
                const event = new CustomEvent('crowdFormSubmit', {
                    detail: {
                        formData: formData
                    }
                });
                this.dispatchEvent(event);
            })
        })
    }

    render() {
        return html`
            <div part='container' class='form-container'>
                <form part='form'>
                    <slot></slot>
                </form>
            </div>
        `
    }
}
