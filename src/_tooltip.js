import { LitElement, html, css } from 'lit';

export class Tooltip extends LitElement {
    static properties = {
        _hidden: {type: Boolean,reflect: true},
        show: {type: Boolean},
        content: {type: String},
        delay: {type: Number},
        hoist: {type: Boolean}
    }
    static styles = [
        css`
            :host {
                display: contents;
                position:relative;
            }
            :host, :host * {
                box-sizing: inherit;
            }
            .positioner {
                position: absolute;
                z-index: var(--crowd-tooltip-z-index,1);
                pointer-events: none;
            }
            :host([hoist]) .positioner {
                position: fixed;
            }
            .tooltip {
                display: inline-block;
                font-size: 0.8em;
                padding: var(--crowd-tooltip-padding-vertical,0.5em) var(--crowd-tooltip-padding-horizontal,0.8em);
                color: var(--crowd-tooltip-color, inherit);
                background-color: var(--crowd-tooltip-background-color, white);
                border: var(--crowd-tooltip-border-width, 1px) var(--crowd-tooltip-border-style, solid) var(--crowd-tooltip-border-color, #eee);
                border-radius: var(--crowd-tooltip-border-radius, 4px);
                transform: translate(-50%,calc(-100% - 1em)) scale(1);
                opacity: 1;
                transition-property: transform, opacity;
                transition-duration: var(--crowd-tooltip-transition-duration, 0.15s);
                transition-timing-function: var(--crowd-tooltip-transition-ease, ease-in-out);
                transition-delay: var(--tooltip-delay);
            }
            :host([_hidden]) .tooltip {
                pointer-events: none;
                transform: translate(-50%,calc(-100% - 1em)) scale(0.5);
                opacity: 0;
                transition-delay: 0s;
            }
            :host([show]) .tooltip {
                pointer-events: all !important;
                transform: translate(-50%,calc(-100% - 1em)) scale(1) !important;
                opacity: 1 !important;
            }
        `
    ];

    constructor() {
        super()
        this._hidden = true
        this.delay = 0
    }
    
    _positionTooltip() {
        let positioner = this.renderRoot.querySelector('.positioner')
        if (!positioner) {
            return
        }
        let top = null;
        let center = null;
        if (this.hoist) {
            for (let child of this.children) {
                let pos = child.getBoundingClientRect()
                if (top === null || pos.top < top) {
                    top = pos.top
                }
                let posCenter = pos.left + (pos.width / 2)
                if (center === null || posCenter > center) {
                    center = posCenter
                }
            }
        } else {
            for (let child of this.children) {
                if (top === null || child.offsetTop < top) {
                    top = child.offsetTop
                }
                let posCenter = child.offsetLeft + (child.offsetWidth / 2)
                if (center === null || posCenter > center) {
                    center = posCenter
                }
            }
        }
        positioner.style.top = top + 'px'
        positioner.style.left = center + 'px'
        requestAnimationFrame(() => this._positionTooltip())
    }

    connectedCallback() {
        super.connectedCallback()
        for (let child of this.children) {
            child.addEventListener('pointerover', () => this._show(), false)
            child.addEventListener('pointerleave', () => this._hide(), false)
        }
    }

    _show() {
        this._hidden = false
    }

    _hide() {
        this._hidden = true
    }

    render() {
        this._positionTooltip()
        return html`
            <div class='positioner' part='positioner'>
                <article part='tooltip' style='--tooltip-delay: ${this.delay / 1000}s;' class='tooltip' ${this._hidden ? 'hidden aria-hidden="true"' : ''}>
                    ${this.content}
                </article>
            </div>
            <slot></slot>
        `;
    }
}
