import { LitElement, html, css } from 'lit';

export class ToastStack extends LitElement {
    static styles = [
        css`
            :host {
                display: block;
            }
            .toast-stack {
                position: fixed;
                top: 1rem;
                right: 1rem;
                display: flex;
                flex-flow: column nowrap;
                justify-content: flex-start;
                align-items: stretch;
                gap: var(--toast-stack-gap,1rem);
                z-index: var(--toast-stack-z-index, 9999);
                max-width: var(--toast-stack-max-width, min(500px,100%));
                pointer-events: none;
            }
        `
    ];

    render() {
        return html`
            <article class='toast-stack'>
                <slot></slot>
            </article>
        `;
    }
}
