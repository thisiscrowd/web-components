import { LitElement, html, css } from 'lit';
import {FormController} from './helpers/form-controller.js'

export class Number extends LitElement {

    static properties = {
        value: {
            type: Number,
            reflect: true
        },
        name: {type: String},
        min: {type: Number},
        max: {type: Number},
        step: {type: Number},
        fixedPoint: {type: Number},
        label: {type: String},
        required: {type: Boolean},
        errorMessage: {type: String},
        invalid: {type: Boolean, reflect: true},
        placement: {
            type: String,
            hasChanged(newVal,oldVal) {
                return newVal == 'before' || newVal == 'after' ? newVal : oldVal
            }
        },
        unit: {type: String}
    }

    static styles = [
        css`
            :host {
                display: block;
            }
            *,*::before, *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .input-container {
                color: var(--crowd-input-color, inherit);
                background-color: var(--crowd-input-background, white);
                border: var(--crowd-input-border-width,1px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, #eee);
                border-radius: var(--crowd-input-border-radius, 0px);
                font-size: var(--crowd-input-font-size,1rem);
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: stretch;
                width: max-content;
            }
            .input-container:focus-within {
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .input-container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            input {
                -webkit-appearance: none;
                background-color: transparent;
                border: none;
                height: 100%;
                width: max-content;
                min-width: 2ch;
                color: inherit;
                font-family: inherit;
                font-size: inherit;
                padding: 0 var(--crowd-input-padding-horizontal, 1em);
                font-weight: var(--crowd-input-font-weight,400);
                height: calc(
                    var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
                );
                text-transform: var(--crowd-input-text-transform);
                flex: 1 1 auto;
            }
            input:focus-visible {
                outline: none;
            }
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
            }

            /* Firefox */
            input[type=number] {
            -moz-appearance: textfield;
            }

            .controls {
                display: flex;
                flex-flow: column;
                font-size: 0.5em;
                justify-content: space-around;
                margin: 0.5em 0 0.5em 0.5em;
                padding: 2px;
                background-color: var(--crowd-number-controls-background, #efefef);
                color: var(--crowd-number-controls-color, #dedede);
                --crowd-button-hover-color: #cecece;
            }
            :host([placement="after"]) .controls {
                margin: 0.5em 0.5em 0.5em 0;
            }

            .unit {
                display: flex;
                flex-flow: column;
                justify-content: center;
                padding: 0 0.3em;
                color: var(--crowd-number-unit-color, #dedede);
                font-size: 0.8em;
            }
        `
    ]

    connectedCallback() {
        super.connectedCallback()
        new FormController(this)
        this.id = 'input-' + Date.now()
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _onInput(e) {
        this.invalid = false
        this.value = parseFloat(e.currentTarget.value).toFixed(this.fixedPoint)
    }

    _increment() {
        this.value = (parseFloat(this.value) + parseFloat(this.step)).toFixed(this.fixedPoint)
    }

    _decrement() {
        this.value = (parseFloat(this.value) - parseFloat(this.step)).toFixed(this.fixedPoint)
    }

    validate() {
        this.invalid = false
        if (this.required) {
            if (this.value === '' || this.value == null) {
                this.invalid = true
            }
        }
    }

    shouldUpdate(changedProperties) {
        if (changedProperties.has('value')) {
            if (
                // true if no max, or newval is less than max
                (this.max == null || parseFloat(this.value) <= parseFloat(this.max))
                &&
                // true if no min, or newval is greater than min
                (this.min == null || parseFloat(this.value) >= parseFloat(this.min))
            ) {
                this._dispatchChange()
            } else {
                this.value = changedProperties.get('value')
                this._syncValue(changedProperties.get('value'))
                return false
            }
        }
        return true
    }

    _syncValue(value) {
        let input = this.renderRoot.querySelector('input')
        if (input) {
            input.value = value
        }
    }

    updated(changedProperties) {
        if (changedProperties.has('value')) {
            this._syncValue(this.value)
        }
    }

    constructor() {
        super()
        this.placement = 'before'
        this.value = 0
        this.step = 1
        this.fixedPoint = 1
    }

    render() {
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let maxlength = ''
        if (this.maxlength) {
            maxlength = `maxlength='${this.maxlength}'`
        }
        let label = ''
        if (this.label) {
            label = html`
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        let controls = html`
            <div class='controls' part='controls'>
                <crowd-icon-button @click='${this._increment}' name='caret-up-fill'></crowd-icon-button>
                <crowd-icon-button @click='${this._decrement}' name='caret-down-fill'></crowd-icon-button>
            </div>
        `
        let unit = ''
        if (this.unit) {
            unit = html`
                <div class='unit'>${this.unit}</div>
            `
        }
        return html`
            <div part='wrapper' class='wrapper'>
                ${label}
                <div part='container' class='input-container'>
                    ${this.placement == 'before' ? controls : ''}
                    <input @change='${this._dispatchChange}' @input='${this._onInput}' id='${this.id}' type='text' min='${this.min}' max='${this.max}' step='${this.step}' name='${this.name}' value='${this.value}' required='${this.required}' />
                    ${unit}
                    ${this.placement == 'after' ? controls : ''}
                </div>
                ${error}
                <slot name="help-text"></slot>
            </div>
        `;
    }
}