import {LitElement, css, html} from 'lit';
import {FormController} from './helpers/form-controller.js'

export class Checkbox extends LitElement {

    static properties = {
        name: {type: String},
        value: {type: String},
        checked: {type: Boolean,reflect:true},
        required: {type: Boolean},
        errorMessage: {type: String},
        invalid: {type: Boolean, reflect: true},
    }

    static styles = css`
        :host {
            display: block;
            position:relative;
        }
        input {
            -webkit-apperance: none;
            height: 0px;
            width: 0px;
            opacity: 0;
            position: absolute;
            margin: 0;
        }
        label {
            color: var(--crowd-input-color, inherit);
            font-family: inherit;
            font-weight: inherit;
            font-size: inherit;
            cursor: pointer;
            display: inline-flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: var(--crowd-checkbox-align, center);
            gap: var(--crowd-checkbox-spacing,0.5em);
        }
        .box {
            border: var(--crowd-checkbox-border-width,1px) var(--crowd-checkbox-border-style,solid) var(--crowd-checkbox-border-color,black);
            min-width: calc(1.2em - (2 * var(--crowd-checkbox-border-width,1px)));
            min-height: calc(1.2em - (2 * var(--crowd-checkbox-border-height,1px)));
            max-width: calc(1.2em - (2 * var(--crowd-checkbox-border-width,1px)));
            max-height: calc(1.2em - (2 * var(--crowd-checkbox-border-height,1px)));
            background-color: var(--crowd-checkbox-background-color, transparent);
            color: var(--crowd-checkbox-color, black);
            line-height: 1;
            display: grid;
            place-items: center;
            font-size: 1em;
            position: relative;
            transition-property: color, background-color;
            transition-duration: var(--crowd-checkbox-transition-duration,0.15s);
            transition-timing-function: var(--crowd-checkbox-transition-ease, ease-in-out);
            transition-delay: var(--crowd-checkbox-transition-delay,0s);
        }
        .container {
            max-width: 100%;
        }
        .box svg {
            position:absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        input:focus-visible + label .box {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        .error {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        :host([checked]) .box {
            background-color: var(--crowd-checkbox-checked-background-color, black);
            color: var(--crowd-checkbox-checked-color, white);
        }
        :host([invalid]) input {
            outline: 1px solid var(--crowd-input-error-message-color, red);
        }
    `

    constructor() {
        super()
        this.checked = false
        this.invalid = false
    }
    
    connectedCallback() {
        super.connectedCallback()
        this.id = 'checkbox-' + Date.now()
        new FormController(this)
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _onChange(e) {
        this.checked = e.currentTarget.checked
        this.invalid = false
        if (this.checked) {
            this.value = e.currentTarget.value
        } else {
            this.value = null
        }
        this._dispatchChange()
    }

    validate() {
        if (this.required) {
            if (!this.checked) {
                this.invalid = true
            }
        }
    }

    render() {
        let checkIcon = ''
        if (this.checked) {
            checkIcon = html`
                <slot name='check-icon'>
                    <svg viewBox="0 0 16 16"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"><g stroke="currentColor" stroke-width="2"><g transform="translate(3.428571, 3.428571)"><path d="M0,5.71428571 L3.42857143,9.14285714"></path><path d="M9.14285714,0 L3.42857143,9.14285714"></path></g></g></g></svg>
                </slot>
            `
        }
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        return html`
            <div class='container' part='container'>
                <input @change='${this._onChange}' type='checkbox' id='${this.id}' name='${this.name}' value='${this.value}' required='${this.required}' />
                <label part='label' for='${this.id}'>
                    <div class='box' part='box'>
                        ${checkIcon}
                    </div>
                    <slot></slot>
                </label>
                ${error}
            </div>
        `
    }
}