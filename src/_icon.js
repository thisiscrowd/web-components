import { LitElement,html,css } from "lit"
import {until} from 'lit/directives/until.js';
import {unsafeSVG} from 'lit/directives/unsafe-svg.js';

export class Icon extends LitElement {

    static properties = {
        name: {type: String},
        style: {type: String},
        src: {type: String},
        count: { type: Number }
    }
    static styles = css`
        :host {
            display: inline-grid;
            place-items: center;
            position: relative;
            margin: var(--crowd-icon-vertical-spacing,0px) var(--crowd-icon-horizontal-spacing,0px);
        }
        img,i,svg {
            width: 1em;
            height: 1em;
            color: inherit;
        }
        img {
            width: 100%;
            height: 100%;
            aspect-ratio: 1/1;
            object-fit: contain;
        }
        .count {
            position:absolute;
            top: 0;
            right: 0;
            aspect-ratio: 1/1;
            display: inline-grid;
            place-items: center;
            border-radius: 50%;
            background-color: var(--crowd-icon-count-background-color, red);
            color: var(--crowd-icon-count-color, white);
            font-size: 0.5em;
            transform: translate(50%,-50%);
            width: 1em;
            height: 1em;
            padding: 0.2em;
            line-height: 1;
        }
    `

    constructor() {
        super()
    }

    _fetchSrc() {
        return new Promise((res,rej) => {
            // let icon = html`<i class='bi bi-${this.name}' style='${this.style}'></i>`
            if (this.src) {
                // fetch(this.src).then(r => r.text())
                    // .then(svg => res(html`${unsafeSVG(svg)}`))
                    res(html`<img src='${this.src}' />`)
            } else if (this.name) {
                fetch(`https://icons.getbootstrap.com/assets/icons/${this.name}.svg`).then(r => r.text())
                    .then(svg => res(html`${unsafeSVG(svg)}`))
            }
        })
    }

    render() {
        return html`
            ${until(
                this._fetchSrc().then(icon => html`${icon}`),
                html``
            )}
            ${this.count ? html`<span class='count'>${this.count < 10 ? this.count : '•'}</span>` : ''}
        `
    }
}