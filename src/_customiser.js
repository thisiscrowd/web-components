import { LitElement, html, css } from 'lit';

export class Customiser extends LitElement {

    static properties = {
        property: {type: String},
        type: {type: String},
        min: {type: Number},
        max: {type: Number},
        value: {type: String, reflect: true},
        unit: {type: String},
        step: {type: Number},
        options: {type: Array}
    }

    static styles = [
        css`
            :host {
                display: inline-block;
            }
        `
    ];

    _onChange(e) {
        this.value = e.target.value
        let propertyVal
        if (this.type == 'Number') {
            propertyVal = this.value + this.unit
        } else {
            propertyVal = this.value
        }
        document.documentElement.style.setProperty(this.property, propertyVal)
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    constructor() {
        super()
    }
    
    render() {
        if (!this.property) {
            throw '"property" attribute is not defined.';
        }
        if (!this.type) {
            throw '"type" attribute is not defined.';
        }
        if (this.type === 'Number' && this.unit === null) {
            throw '"unit" attribute is not defined.';
        }
        let control = ''
        if (this.type == 'Number') {
            control = html`
                <!-- <crowd-range @crowdChange='${e => this._onChange(e)}' step='${this.step}' tooltip min='${this.min}' max='${this.max}' value='${this.value}' ></crowd-range> -->
                <crowd-number @crowdChange='${e => this._onChange(e)}' placement='after' unit="${this.unit}" value='${this.value}' step='${this.step}' min='${this.min}' max='${this.max}'></crowd-number>
            `
        } else if (this.type == 'Color') {
            control = html`
                <crowd-color-picker @crowdChange='${e => this._onChange(e)}' value='${this.value}'></crowd-color-picker>
            `
        } else if (this.type == 'Select') {
            control = html`
                <crowd-select @crowdChange='${e => this._onChange(e)}' value='${this.value}'>
                    ${this.options.map(option => html`<crowd-option value='${option}'>${option}</crowd-option>`)}
                </crowd-select>
            `
        } else if (this.type == 'Text') {
            control = html`
                <crowd-input @crowdChange='${e => this._onChange(e)}' type='text' value='${this.value}'></crowd-input>
            `
        }
        return html`
            ${control}
        `;
    }
}
