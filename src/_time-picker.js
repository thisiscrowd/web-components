import { LitElement, html, css } from 'lit'
const { DateTime } = require("luxon")
import {FormController} from './helpers/form-controller.js'

export class TimePicker extends LitElement {
    static properties = {
        value: { type: String, reflect: true },
        open: { type: Boolean, reflect: true },
        _angle: { type: Number },
        hours: { type: Number },
        minutes: { type: Number },
        meridiem: { type: String },
        selecting: { type: String },
        format: { type: String, reflect: true },
        label: {type: String},
        required: {type: Boolean},
        errorMessage: {type: String},
        invalid: {type: Boolean, reflect: true},
    }
    static styles = [
        css`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .container {
                position: relative;
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            label.label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            .picker {
                position: absolute;
                top: 100%;
                left: 0;
                transform-origin: top center;
                transform: scaleY(0);
                transition-property: transform;
                transition-duration: var(--crowd-time-picker-transition-duration, 0.15s);
                transition-delay: var(--crowd-time-picker-transition-delay, 0s);
                transition-timing-function: var(--crowd-time-picker-transition-ease, ease-in-out);
                background-color: var(--crowd-time-picker-background-color, white);
                padding: var(--crowd-time-picker-padding-vertical, 1em) var(--crowd-time-picker-padding-horizontal, 1em);
                box-shadow: var(
                    --crowd-select-dropdown-box-shadow,
                    0 2px 8px rgba(0, 0, 0, 0.1)
                );
                z-index: var(--crowd-time-picker-z-index, 999);
            }
            :host([open]) .picker {
                transform: scaleY(1);
            }

            .time-display {
                display: flex;
                flex-flow: row nowrap;
                gap: 0.5em;
                padding-bottom: 0.5em;
            }

            .time-digital {
                flex: 1 0 auto;
                display: flex;
                flex-flow: row nowrap;
                align-items: center;
                gap: 0.25em;
                height: 100%;
            }
            .time-digital crowd-button {
                font-size: 2em;
                min-width: 3em;
                --crowd-button-padding-vertical: 0.25em
                --crowd-button-padding-horizontal: 0.5em
            }
            .time-display crowd-button.active {
                --crowd-button-background-color: var(--crowd-time-picker-active-button-background-color, #ccc);
            }
            .time-digital span {
                font-size: 2em;
            }

            .meridiem {
                flex: 0 1 0;
                height: 100%;
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-button-border-color, #aeaeae);
                border-radius: var(--crowd-button-border-radius, 3px);
            }

            .meridiem crowd-button {
                --crowd-button-border-width: 0px;
                --crowd-button-border-radius: 0px;
            }

            .clock {
                width: 100%;
                aspect-ratio: 1/1;
                display: grid;
                grid-template-area: 100% / 100%;
            }

            .clock-face,.clock-hand,.clock-thumb {
                grid-area: 1/1;
            }

            .clock-face {
                width: 100%;
                height: 100%;
                background-color: var(--crowd-time-picker-clock-face-background-color, #eee);
                border-radius: 50%;
                position: relative;
                display: grid;
            }

            .clock-face > span {
                grid-area: 1/1;
                place-self: center;
                display: block;
                width: 100%;
                aspect-ratio: 1/1;
                position: relative;
                z-index: 2;
                pointer-events: none;
                user-select: none;
            }

            .clock-face > span > span {
                display: inline-block;
            }

            .clock-hand {
                place-self: center;
                height: 50%;
                width: var(--crowd-time-picker-clock-hand-width, 2px);
                background-color: var(--crowd-time-picker-clock-hand-background-color, #ccc);
            }

            .clock-thumb {
                aspect-ratio: 1/1;
                transform-origin:center;
                position: relative;
                place-self: center;
                cursor: pointer;
            }

            .clock-thumb::before {
                content: '';
                position: absolute;
                width: var(--crowd-time-picker-clock-thumb-size, 2em);
                background-color: var(--crowd-time-picker-clock-hand-background-color, #ccc);
                aspect-ratio: 1/1;
                transform: translate(-25%,-25%);
                border-radius: 50%;
                top: 0;
                left: 0;
            }
            .action-buttons {
                margin-top: 0.5em;
                display: flex;
                gap: 1em;
                justify-content: space-between;
            }
        `
    ]

    _setValue() {
        let hours = this.hours
        if (this.meridiem == 'pm') {
            if (this.hours == 12) {
                hours = this.hours
            } else {
                hours = this.hours + 12
            }
        } else {
            if (this.hours == 12) {
                hours = 0
            }
        }
        let dt = DateTime.fromObject({
            hour: hours,
            minute: this.minutes
        })
        this.value = dt.toFormat(this.format)
        this._dispatchChange()
    }

    _showPicker() {
        this.open = true
    }

    _hidePicker() {
        this.open = false
    }

    _onPointerDown(e) {
        this._onMouseMove(e)
        e.target.addEventListener('pointermove', this.bindMouseMove, false);
    }

    _onPointerUp(e) {
        e.target.removeEventListener('pointermove', this.bindMouseMove, false);
    }

    _onMouseMove(e) {
        let clockFace = this.shadowRoot.querySelector('.clock')
        let center = {
            x: clockFace.getBoundingClientRect().x + (clockFace.getBoundingClientRect().width / 2),
            y: clockFace.getBoundingClientRect().y + (clockFace.getBoundingClientRect().height / 2)
        }
        let diffX = (e.clientX - center.x)
        let diffY = (center.y - e.clientY)
        let angle = (Math.PI / 2) - Math.atan2(diffY,diffX)
        angle = angle * (180/Math.PI) 
        angle = (( angle + 360) % 360)
        this._angle = angle
        if (this.selecting == 'hours') {
            this.hours = Math.round(((angle) / 360) * 12);
            if (this.hours == 0) {
                this.hours = 12
            }
        }
        if (this.selecting == 'minutes') {
            this.minutes = Math.round(((angle) / 360) * 60);
            if (this.minutes == 60) {
                this.minutes = 0
            }
        }
    }

    _getHoursDegree(index) {
        return (( (index + 1)/12 ) * 360) + 45
    }

    _getMinutesDegree(index) {
        return (( (index + 1)/6 ) * 360) + 45
    }

    _addPaddingZero(number) {
        return number < 10 ? '0' + number : number
    }

    _dispatchChange() {
        const event = new CustomEvent('crowdChange');
        this.dispatchEvent(event);
    }

    _onInput(e) {
        this.invalid = false
        this._dispatchChange()
    }

    _onKeydown(e) {
        if (e.key != 'Tab') {
            e.preventDefault()
        }
    }

    validate() {
        this.invalid = false
        if (this.type == 'email') {
            this._validateEmail()
        }
        if (this.required) {
            if (this.value === '' || this.value == null) {
                this.invalid = true
            }
        }
    }

    connectedCallback() {
        super.connectedCallback()
        new FormController(this)
        this.bindMouseMove = this._onMouseMove.bind(this)
        this.textInput = this.renderRoot.querySelector('crowd-input')
    }
    
    constructor() {
        super()
        let hours = new Date().getHours()
        if (hours > 12) {
            this.meridiem = 'pm'
            hours = hours - 12
        } else {
            this.meridiem = 'am'
        }
        this.hours = hours
        this.minutes = new Date().getMinutes()
        this._angle = (hours/12) * 360
        this.selecting = 'hours'
        this.format = 'HH:mm a'
        this._setValue()
    }

    render() {
        let error = ''
        if (this.invalid) {
            error = html`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `
        }
        let label = ''
        if (this.label) {
            label = html`
                <label class='label' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `
        }
        return html`
            <div class='container' @focusin='${this._showPicker}' @focusout='${this._hidePicker}'>
                ${label}
                <div part='input' class='input'>
                    <crowd-input @keydown='${this._onKeydown}' @crowdChange='${this._onInput}' type='text' value='${this.value}'></crowd-input>
                </div>
                <div part='picker' class='picker' tabindex='0'>
                    <div class='time-display'>
                        <div class='time-digital'>
                            <div class='hours'>
                                <crowd-button class='${this.selecting == 'hours' ? 'active' : ''}' @click=${() => this.selecting = 'hours'}>${this._addPaddingZero(this.hours)}</crowd-button>
                            </div>
                            <span>:</span>
                            <div class='minutes'>
                                <crowd-button class='${this.selecting == 'minutes' ? 'active' : ''}' @click=${() => this.selecting = 'minutes'}>${this._addPaddingZero(this.minutes)}</crowd-button>
                            </div>
                        </div>
                        <div class='meridiem'>
                            <crowd-button @click='${() => this.meridiem = 'am'}' class='${this.meridiem == 'am' ? 'active' : ''}'>AM</crowd-button>
                            <crowd-button @click='${() => this.meridiem = 'pm'}' class='${this.meridiem == 'pm' ? 'active' : ''}'>PM</crowd-button>
                        </div>
                    </div>
                    <div class='clock' @pointerdown='${this._onPointerDown}' @pointerup='${this._onPointerUp}' @pointerout='${this._onPointerUp}' @pointerleave='${this._onPointerUp}'>
                        <div class='clock-face'>
                            ${this.selecting == 'hours' ? new Array(12).fill(0).map((item,index) => (
                                html`
                                    <span style='
                                        width: ${Math.sqrt(5000)}%;
                                        transform: rotate(${this._getHoursDegree(index)}deg);
                                    '>
                                        <span style='transform: rotate(${this._getHoursDegree(index) * -1}deg);'>
                                            ${index + 1}
                                        </span>
                                    </span>
                                `
                            )) : ''}
                            ${this.selecting == 'minutes' ? new Array(6).fill(0).map((item,index) => (
                                html`
                                    <span style='
                                        width: ${Math.sqrt(5000)}%;
                                        transform: rotate(${this._getMinutesDegree(index)}deg);
                                    '>
                                        <span style='transform: rotate(${this._getMinutesDegree(index) * -1}deg);'>
                                            ${10 * (index + 1)}
                                        </span>
                                    </span>
                                `
                            )) : ''}
                        </div>
                        <div class='clock-hand' style='
                            transform: rotate(${this._angle}deg) translateY(-50%);
                        '></div>
                        <div class='clock-thumb' style='
                            width: ${Math.sqrt(5000)}%;
                            transform: rotate(${this._angle + 45}deg);
                        '></div>
                    </div>
                    <div class='action-buttons'>
                        <crowd-button @click='${this._hidePicker}'>CANCEL</crowd-button>
                        <crowd-button @click='${() => {this._setValue();this._hidePicker()}}'>OK</crowd-button>
                    </div>
                </div>
                ${error}
                <slot name="help-text"></slot>
            </div>
        `
    }
}