# Textarea

Textarea for forms.

## Example

Textarea inherits common attribute and style properties from `<crowd-input>`

```
<crowd-textarea placeholder='Type something' label='Bio' maxlength='50' name='bio' required></crowd-textarea>
```
<crowd-textarea placeholder='Type something' label='Bio' maxlength='50' name='bio' required></crowd-textarea>

### Help text

Add content in the `help-text` slot to show it below the textarea.

```
<crowd-textarea label='Bio' placeholder='Type something'><p slot='help-text'>A bit about yourself</p>
```
<crowd-textarea label='Bio' placeholder='Type something'><p slot='help-text'>A bit about yourself</p>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-input-padding-horizontal`|Interior padding on left/right|<crowd-customiser property='--crowd-input-padding-horizontal' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-font-weight`|Font weight of the input|<crowd-customiser property='--crowd-input-font-weight' type='Number' unit='' value='400' min='0' max='900' step='100'></crowd-customiser>|
|`--crowd-input-height`|Height of the input|<crowd-customiser property='--crowd-input-height' type='Number' unit='em' value='2' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-color`|Input text color|<crowd-customiser property='--crowd-input-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-label-color`|Label color|<crowd-customiser property='--crowd-input-label-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-placeholder-color`|Input placeholder color|<crowd-customiser property='--crowd-input-placeholder-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-background`|Input background color|<crowd-customiser property='--crowd-input-background' value='#ffffff' type='Color'></crowd-customiser>|
|`--crowd-input-border-width`|Border width|<crowd-customiser property='--crowd-input-border-width' type='Number' value='1' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-input-border-style`|Border style|<crowd-customiser property='--crowd-input-border-style' type='Select' value='solid' options='["solid", "dashed", "dotted", "none"]'></crowd-customiser>|
|`--crowd-input-border-color`|Input border color|<crowd-customiser property='--crowd-input-border-color' value='#eeeeee' type='Color'></crowd-customiser>|
|`--crowd-input-border-radius`|Border radius|<crowd-customiser property='--crowd-input-border-radius' type='Number' value='0' unit='px' min='0' max='16' step='1'></crowd-customiser>|
|`--crowd-input-font-size`|Font size of the input|<crowd-customiser property='--crowd-input-font-size' type='Number' unit='rem' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-focus-width`|Width of the focus ring|<crowd-customiser property='--crowd-input-focus-width' type='Number' value='2' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-input-focus-color`|Input focus ring color|<crowd-customiser property='--crowd-input-focus-color' value='rgba(0,0,0,0.3)' type='Color'></crowd-customiser>|
|`--crowd-input-label-spacing`|Font size of the input|<crowd-customiser property='--crowd-input-label-spacing' type='Number' unit='em' value='0.5' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-wrapper-padding-vertical`|Space above/below the input|<crowd-customiser property='--crowd-input-wrapper-padding-vertical' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-wrapper-padding-horizontal`|Space left/right of the input|<crowd-customiser property='--crowd-input-wrapper-padding-horizontal' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-password-toggle-color`|Color of the password toggle icon|<crowd-customiser property='--crowd-input-password-toggle-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-error-message-font-size`|Error message font size|<crowd-customiser property='--crowd-input-error-message-font-size' type='Number' value='0.8' unit='em' min='0' max='3' step='0.1'></crowd-customiser>|
|`--crowd-input-error-message-color`|Color of the error message & outline|<crowd-customiser property='--crowd-input-error-message-color' value='red' type='Color'></crowd-customiser>|