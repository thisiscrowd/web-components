# Jet

Sleek and dark

<style>
    body {
        background-color: #1e1e1e;
        color: #eee;
    }
    :root {
        --crowd-alert-background-color:hsl(0, 1%, 11%);
        --crowd-alert-color:hsl(0, 5%, 94%);
        --crowd-alert-box-shadow:0 2px 8px rgba(255, 255, 255, 0.1);
        --crowd-alert-border-radius:4px;
        --crowd-badge-color:hsl(0, 1%, 8%);
        --crowd-badge-text-color:hsl(0, 5%, 94%);
        --crowd-badge-border-radius:4px;
        --crowd-button-background-color:hsl(0, 1%, 8%);
        --crowd-button-color:hsl(0, 5%, 94%);
        --crowd-button-border-width:1px;
        --crowd-button-border-color:hsla(0, 5%, 94%, 0.12);
        --crowd-button-border-radius:5px;
        --crowd-button-hover-background-color:hsl(0, 0%, 0%);
        --crowd-button-hover-border-color:hsla(0, 5%, 94%, 0.12);
        --crowd-dialog-background:hsl(0, 1%, 11%);
        --crowd-drawer-background:hsl(0, 1%, 11%);
        --crowd-input-background:hsl(0, 1%, 8%);
        --crowd-input-color:hsl(0, 5%, 94%);
        --crowd-input-placeholder-color:hsla(0, 5%, 94%, 0.12);
        --crowd-input-border-width:1px;
        --crowd-input-border-style:solid;
        --crowd-input-border-color:hsla(0, 5%, 94%, 0.12);
        --crowd-input-border-radius:4px;
        --crowd-input-wrapper-padding-vertical:1em;
        --crowd-input-focus-width:1px;
        --crowd-input-font-size:1rem;
        --crowd-menu-background-color:hsl(0, 1%, 8%);
        --crowd-menu-item-color:hsl(0, 5%, 94%);
        --crowd-menu-item-background-color:hsl(0, 1%, 8%);
        --crowd-menu-item-hover-background-color:hsl(0, 1%, 11%);
        --track-color:hsla(0, 5%, 94%, 0.12);
        --indicator-color:hsl(0, 100%, 0%);
        --crowd-range-spacing:0.6em;
        --crowd-range-track-border-width:0px;
        --crowd-range-track-border-radius:4px;
        --crowd-range-track-background-color:hsla(0, 5%, 94%, 0.12);
        --crowd-range-indicator-color:hsl(0, 100%, 0%);
        --crowd-range-thumb-background-color:hsl(0, 5%, 94%);
        --crowd-range-notch-color:hsl(0, 5%, 94%);
        --crowd-range-number-color:hsl(0, 5%, 94%);
        --crowd-select-dropdown-background-color:hsl(0, 1%, 8%);
        --crowd-switch-track-background-color:hsla(0, 5%, 94%, 0.12);
        --crowd-switch-track-background-color-active:hsl(0, 100%, 0%);
        --crowd-tooltip-background-color:hsl(0, 1%, 8%);
        --crowd-tooltip-color:hsl(0, 5%, 94%);
        --crowd-tooltip-border-color:hsla(0, 5%, 94%, 0.12);
    }
</style>

<crowd-alert open>
    <crowd-icon slot='icon' name='info-circle'></crowd-icon>
    <h3>Alert</h3>
    <p>Ut varius tincidunt libero.</p>
</crowd-alert>

<crowd-badge>Lorem Ipsum</crowd-badge>
<crowd-badge pulse>Lorem Ipsum</crowd-badge>

<crowd-button>Click me!</crowd-button>

<crowd-checkbox>Tick me!</crowd-checkbox>

<crowd-dialog style='--height:50vh;--width: 500px;' label='Itsa me!' id='demoDialog'>
    <p>Lorem Ipsum dialog text</p>
</crowd-dialog>
<crowd-button onClick='demoDialog.show()'>Show Dialog</crowd-button>

<crowd-drawer style='--crowd-drawer-position-right: 60vw;' id='demoDrawer'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer.show()'>Open Drawer</crowd-button>

<crowd-dropdown>
    <crowd-button caret slot='trigger'>Open</crowd-button>
    Presto!
</crowd-dropdown>

<crowd-input type='text' name='First Name' placeholder='Jane Smith' label='First Name'></crowd-input>
<crowd-input type='text' password toggle name='Password' togglePassword label='Password'></crowd-input>
<crowd-input type='text' invalid errorMessage='Something is wrong' toggle name='Password' togglePassword label='Error'></crowd-input>

<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>

<crowd-progress-ring size='3em' percentage='45'></crowd-progress-ring>

<crowd-range tooltip notches numbers step='10'></crowd-range>

<crowd-select label='Select an option' placeholder='Choose something'>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>

<crowd-spinner></crowd-spinner>

<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>

<crowd-textarea placeholder='Type something' label='Bio' maxlength='50' name='bio' required></crowd-textarea>

<crowd-tooltip content='Wassup?'>
    <crowd-button>Hover me!</crowd-button>
</crowd-tooltip>

---

<pre style='border: 1px solid #eee;padding:1em;'>
    --crowd-alert-background-color:hsl(0, 1%, 11%);
    --crowd-alert-color:hsl(0, 5%, 94%);
    --crowd-alert-box-shadow:0 2px 8px rgba(255, 255, 255, 0.1);
    --crowd-alert-border-radius:4px;
    --crowd-badge-color:hsl(0, 1%, 8%);
    --crowd-badge-text-color:hsl(0, 5%, 94%);
    --crowd-badge-border-radius:4px;
    --crowd-button-background-color:hsl(0, 1%, 8%);
    --crowd-button-color:hsl(0, 5%, 94%);
    --crowd-button-border-width:1px;
    --crowd-button-border-color:hsla(0, 5%, 94%, 0.12);
    --crowd-button-border-radius:5px;
    --crowd-button-hover-background-color:hsl(0, 0%, 0%);
    --crowd-button-hover-border-color:hsla(0, 5%, 94%, 0.12);
    --crowd-dialog-background:hsl(0, 1%, 11%);
    --crowd-drawer-background:hsl(0, 1%, 11%);
    --crowd-input-background:hsl(0, 1%, 8%);
    --crowd-input-color:hsl(0, 5%, 94%);
    --crowd-input-placeholder-color:hsla(0, 5%, 94%, 0.12);
    --crowd-input-border-width:1px;
    --crowd-input-border-style:solid;
    --crowd-input-border-color:hsla(0, 5%, 94%, 0.12);
    --crowd-input-border-radius:4px;
    --crowd-input-wrapper-padding-vertical:1em;
    --crowd-input-focus-width:1px;
    --crowd-input-font-size:1rem;
    --crowd-menu-background-color:hsl(0, 1%, 8%);
    --crowd-menu-item-color:hsl(0, 5%, 94%);
    --crowd-menu-item-background-color:hsl(0, 1%, 8%);
    --crowd-menu-item-hover-background-color:hsl(0, 1%, 11%);
    --track-color:hsla(0, 5%, 94%, 0.12);
    --indicator-color:hsl(0, 100%, 0%);
    --crowd-range-spacing:0.6em;
    --crowd-range-track-border-width:0px;
    --crowd-range-track-border-radius:4px;
    --crowd-range-track-background-color:hsla(0, 5%, 94%, 0.12);
    --crowd-range-indicator-color:hsl(0, 100%, 0%);
    --crowd-range-thumb-background-color:hsl(0, 5%, 94%);
    --crowd-range-notch-color:hsl(0, 5%, 94%);
    --crowd-range-number-color:hsl(0, 5%, 94%);
    --crowd-select-dropdown-background-color:hsl(0, 1%, 8%);
    --crowd-switch-track-background-color:hsla(0, 5%, 94%, 0.12);
    --crowd-switch-track-background-color-active:hsl(0, 100%, 0%);
    --crowd-tooltip-background-color:hsl(0, 1%, 8%);
    --crowd-tooltip-color:hsl(0, 5%, 94%);
    --crowd-tooltip-border-color:hsla(0, 5%, 94%, 0.12);
</pre>