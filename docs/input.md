# Input

Classic form field.

## Example

```
<crowd-input type='text' name='First Name' placeholder='John Smith' label='First Name'></crowd-input>
```
<crowd-input type='text' name='First Name' placeholder='Jane Smith' label='First Name'></crowd-input>

### Type

Use the type field to adjust the expected input type

```
<crowd-input type='email' name='Email Address' placeholder='example@mail.com' label='Email Address'></crowd-input>
<crowd-input type='tel' name='Telephone' placeholder='+447542347485' label='Phone Number'></crowd-input>
<crowd-input type='password' name='Password' label='Create Password' togglePassword></crowd-input>
```
<crowd-input type='email' name='Email Address' placeholder='example@mail.com' label='Email Address'></crowd-input>
<crowd-input type='tel' name='Telephone' placeholder='+447542347485' label='Phone Number'></crowd-input>
<crowd-input type='password' name='Password' label='Create Password' togglePassword></crowd-input>

### Number

You can use `type` of 'number' to have a native number picker, or you can use `<crowd-number>`. It shares the same properties as `<crowd-input>` but has the `unit` and `placement` attributes.

```
<crowd-number placement='before' label="Enter a number" unit="px"></crowd-number>
<crowd-number placement='after' label="Enter a number" unit="em"></crowd-number>
```
<crowd-number placement='before' label="Enter a number" unit="px"></crowd-number>
<crowd-number placement='after' label="Enter a number" unit="em"></crowd-number>

### Error Message

Use the `errorMessage` attribute to add an error message that appears when the input is invalid. The `invalid` attribute can be toggled automatically by calling `validate()`, or manually by setting the attribute. `invalid` is cleared after the input value changes.

```
<crowd-input invalid errorMessage='Please enter something' placeholder='Type please'></crowd-input>
```
<crowd-input style='--crowd-input-wrapper-padding-vertical: 0.5em;' invalid errorMessage='Please enter something' placeholder='Type please'></crowd-input>
<crowd-button onClick='this.previousElementSibling.invalid = true'>Invalidate Field</crowd-button>

## Methods

|Method|Action|
|---|---|
|`validate`|Validates the field, showing any errors with the input|

## Events

|Event|Called when|
|---|---|
|`crowdChange`|The value of the input changes|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-input-padding-horizontal`|Interior padding on left/right|<crowd-customiser property='--crowd-input-padding-horizontal' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-font-weight`|Font weight of the input|<crowd-customiser property='--crowd-input-font-weight' type='Number' unit='' value='400' min='0' max='900' step='100'></crowd-customiser>|
|`--crowd-input-height`|Height of the input|<crowd-customiser property='--crowd-input-height' type='Number' unit='em' value='2' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-color`|Input text color|<crowd-customiser property='--crowd-input-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-label-color`|Label color|<crowd-customiser property='--crowd-input-label-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-placeholder-color`|Input placeholder color|<crowd-customiser property='--crowd-input-placeholder-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-background`|Input background color|<crowd-customiser property='--crowd-input-background' value='#ffffff' type='Color'></crowd-customiser>|
|`--crowd-input-border-width`|Border width|<crowd-customiser property='--crowd-input-border-width' type='Number' value='1' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-input-border-style`|Border style|<crowd-customiser property='--crowd-input-border-style' type='Select' value='solid' options='["solid", "dashed", "dotted", "none"]'></crowd-customiser>|
|`--crowd-input-border-color`|Input border color|<crowd-customiser property='--crowd-input-border-color' value='#eeeeee' type='Color'></crowd-customiser>|
|`--crowd-input-border-radius`|Border radius|<crowd-customiser property='--crowd-input-border-radius' type='Number' value='0' unit='px' min='0' max='16' step='1'></crowd-customiser>|
|`--crowd-input-text-transform`|Transform the input text|<crowd-customiser property='--crowd-input-text-transform' type='Select' value='none' options='["none", "uppercase", "capitalize"]'></crowd-customiser>|
|`--crowd-input-font-size`|Font size of the input|<crowd-customiser property='--crowd-input-font-size' type='Number' unit='rem' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-focus-width`|Width of the focus ring|<crowd-customiser property='--crowd-input-focus-width' type='Number' value='2' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-input-focus-color`|Input focus ring color|<crowd-customiser property='--crowd-input-focus-color' value='rgba(0,0,0,0.3)' type='Color'></crowd-customiser>|
|`--crowd-input-label-spacing`|Font size of the input|<crowd-customiser property='--crowd-input-label-spacing' type='Number' unit='em' value='0.5' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-wrapper-padding-vertical`|Space above/below the input|<crowd-customiser property='--crowd-input-wrapper-padding-vertical' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-wrapper-padding-horizontal`|Space left/right of the input|<crowd-customiser property='--crowd-input-wrapper-padding-horizontal' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-password-toggle-color`|Color of the password toggle icon|<crowd-customiser property='--crowd-input-password-toggle-color' value='#000000' type='Color'></crowd-customiser>|
|`--crowd-input-error-message-font-size`|Error message font size|<crowd-customiser property='--crowd-input-error-message-font-size' type='Number' value='0.8' unit='em' min='0' max='3' step='0.1'></crowd-customiser>|
|`--crowd-input-error-message-color`|Color of the error message & outline|<crowd-customiser property='--crowd-input-error-message-color' value='red' type='Color'></crowd-customiser>|

