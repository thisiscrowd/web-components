# Time Picker

Pick a time from a clock.

## Example

```
<crowd-time-picker label="Pick a time"></crowd-time-picker>
```
<crowd-time-picker label="Pick a time"></crowd-time-picker>

## Attributes

Date Picker inherits a lot of standard attributes from `<crowd-input>`, like `invalid`, `required`, and `errorMessage`.

## Events

|Event|Called when|
|---|---|
|`crowdChange`|The value of the input changes|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-time-picker-background-color`|Background color of the picker panel|<crowd-customiser property='--crowd-time-picker-background-color' value='#ffffff' type='Color'></crowd-customiser>|
|`--crowd-time-picker-padding-vertical`|Padding around the picker|<crowd-customiser property='--crowd-time-picker-padding-vertical' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-time-picker-padding-horizontal`|Padding around the calendar|<crowd-customiser property='--crowd-time-picker-padding-horizontal' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-time-picker-clock-face-background-color`|Background color of the clock face.|<crowd-customiser property='--crowd-time-picker-clock-face-background-color' value='#eeeeee' type='Color'></crowd-customiser>|
|`--crowd-time-picker-clock-hand-background-color`|Background color of the clock hand.|<crowd-customiser property='--crowd-time-picker-clock-hand-background-color' value='#eeeeee' type='Color'></crowd-customiser>|
