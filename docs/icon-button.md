# Icon Button

An icon button is an icon that acts like a button. It uses the [Bootstrap Icons](https://icons.getbootstrap.com/) library.

## Example

Use the `name` prop to name the icon

```
<crowd-icon-button name='emoji-laughing'></crowd-icon-button>
```
<crowd-icon-button name='emoji-laughing'></crowd-icon-button>

### Count

Add a notification-style count to your icon using the `count` prop. Change the colors using the `--crowd-icon-count-background-color` and `--crowd-icon-count-color` CSS properties.

```
<crowd-icon-button count="8" name="bell-fill"></crowd-icon-button>
<crowd-icon-button count="11" name="bell-fill"></crowd-icon-button>
```
<crowd-icon-button count="8" name="bell-fill"></crowd-icon-button>
<crowd-icon-button count="11" name="bell-fill"></crowd-icon-button>

## Customisation

The icon button inherits most properties from the [Button](/button.md) component.

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-icon-button-padding`|Padding around the icon|<crowd-customiser property='--crowd-icon-button-padding' type='Number' value='0.25' unit='em' min='0' max='5' step='0.25'></crowd-customiser>|