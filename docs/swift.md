# Swift

A stylish white & blue theme.

<style>
    :root {
        --crowd-alert-padding-vertical:1em;
        --crowd-alert-spacing:1em;
        --crowd-alert-color:hsl(206, 99%, 50%);
        --crowd-alert-border-radius:4px;
        --crowd-badge-color:hsl(206, 99%, 50%);
        --crowd-badge-text-color:hsl(186, 50%, 99%);
        --crowd-badge-border-radius:4px;
        --pulse-color:hsl(206, 99%, 50%);
        --crowd-button-background-color:hsl(206, 99%, 50%);
        --crowd-button-color:hsl(186, 50%, 99%);
        --crowd-button-border-style:none;
        --crowd-button-border-radius:4px;
        --crowd-button-hover-background-color:hsl(206, 97%, 74%);
        --crowd-button-focus-color:hsla(206, 99%, 50%, 0.47);
        --crowd-checkbox-checked-background-color:hsl(206, 99%, 50%);
        --crowd-checkbox-color:hsl(186, 50%, 99%);
        --crowd-checkbox-checked-color:hsl(186, 50%, 99%);
        --crowd-date-picker-day-selected-background-color: hsla(206, 99%, 50%, 0.1);
        --crowd-date-picker-calendar-nav-button-hover-background-color: hsla(206, 99%, 50%, 0.1);
        --crowd-input-placeholder-color:hsla(206, 99%, 50%, 0.47);
        --crowd-input-border-width:1px;
        --crowd-input-border-style:solid;
        --crowd-input-border-color:hsl(0, 5%, 94%);
        --crowd-input-border-radius:4px;
        --crowd-input-focus-color:hsla(206, 99%, 50%, 0.47);
        --crowd-input-wrapper-padding-vertical:0.5em;
        --crowd-input-padding-vertical: 0px;
        --crowd-input-focus-width:1px;
        --crowd-input-error-message-color:hsl(0, 100%, 78%);
        --crowd-menu-item-hover-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-menu-item-hover-color:hsl(206, 99%, 50%);
        --track-color:hsla(206, 99%, 50%, 0.1);
        --indicator-color:hsl(206, 99%, 50%);
        --crowd-range-spacing:0.5em;
        --crowd-range-track-height:0.5em;
        --crowd-range-track-border-width:1px;
        --crowd-range-track-border-color:hsla(206, 99%, 50%, 0.18);
        --crowd-range-track-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-range-thumb-border-color:hsla(206, 99%, 50%, 0.47);
        --crowd-range-notch-color:hsla(206, 99%, 50%, 0.47);
        --crowd-range-track-border-radius:4px;
        --crowd-range-indicator-color:hsl(206, 99%, 50%);
        --crowd-option-hover-color:hsl(206, 99%, 50%);
        --crowd-option-hover-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-switch-track-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-switch-track-background-color-active:hsl(206, 99%, 50%);
        --crowd-tooltip-color:hsl(206, 99%, 50%);
        --crowd-tooltip-border-color:hsla(206, 99%, 50%, 0.18);
        --crowd-tooltip-border-radius:4px;
    }
</style>

<crowd-alert open>
    <crowd-icon slot='icon' name='info-circle'></crowd-icon>
    <h3>Alert</h3>
    <p>Ut varius tincidunt libero.</p>
</crowd-alert>

<crowd-badge>Lorem Ipsum</crowd-badge>
<crowd-badge pulse>Lorem Ipsum</crowd-badge>

<crowd-button>Click me!</crowd-button>

<crowd-checkbox>Tick me!</crowd-checkbox>

<crowd-date-picker label="Pick a day"></crowd-date-picker>

<crowd-dialog style='--height:50vh;--width: 500px;' label='Itsa me!' id='demoDialog'>
    <p>Lorem Ipsum dialog text</p>
</crowd-dialog>
<crowd-button onClick='demoDialog.show()'>Show Dialog</crowd-button>

<crowd-drawer style='--crowd-drawer-position-right: 60vw;' id='demoDrawer'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer.show()'>Open Drawer</crowd-button>

<crowd-dropdown>
    <crowd-button caret slot='trigger'>Open</crowd-button>
    Presto!
</crowd-dropdown>

<crowd-input type='text' name='First Name' placeholder='Jane Smith' label='First Name'></crowd-input>
<crowd-input type='password' password toggle name='Password' togglePassword label='Password'></crowd-input>
<crowd-input type='text' invalid errorMessage='Something is wrong' toggle name='Password' togglePassword label='Error'></crowd-input>

<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>

<crowd-progress-ring size='3em' percentage='45'></crowd-progress-ring>

<crowd-range tooltip notches numbers step='10'></crowd-range>

<crowd-select label='Select an option' placeholder='Choose something'>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>

<crowd-spinner></crowd-spinner>

<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>

<crowd-textarea placeholder='Type something' label='Bio' maxlength='50' name='bio' required></crowd-textarea>

<crowd-tooltip content='Wassup?'>
    <crowd-button>Hover me!</crowd-button>
</crowd-tooltip>

---

<pre style='border: 1px solid #eee;padding: 1em;'>
    :root {
        --crowd-alert-padding-vertical:1em;
        --crowd-alert-spacing:1em;
        --crowd-alert-color:hsl(206, 99%, 50%);
        --crowd-alert-border-radius:4px;
        --crowd-badge-color:hsl(206, 99%, 50%);
        --crowd-badge-text-color:hsl(186, 50%, 99%);
        --crowd-badge-border-radius:4px;
        --pulse-color:hsl(206, 99%, 50%);
        --crowd-button-background-color:hsl(206, 99%, 50%);
        --crowd-button-color:hsl(186, 50%, 99%);
        --crowd-button-border-style:none;
        --crowd-button-border-radius:4px;
        --crowd-button-hover-background-color:hsl(206, 97%, 74%);
        --crowd-button-focus-color:hsla(206, 99%, 50%, 0.47);
        --crowd-checkbox-checked-background-color:hsl(206, 99%, 50%);
        --crowd-checkbox-color:hsl(186, 50%, 99%);
        --crowd-checkbox-checked-color:hsl(186, 50%, 99%);
        --crowd-date-picker-day-selected-background-color: hsla(206, 99%, 50%, 0.1);
        --crowd-date-picker-calendar-nav-button-hover-background-color: hsla(206, 99%, 50%, 0.1);
        --crowd-input-placeholder-color:hsla(206, 99%, 50%, 0.47);
        --crowd-input-border-width:1px;
        --crowd-input-border-style:solid;
        --crowd-input-border-color:hsl(0, 5%, 94%);
        --crowd-input-border-radius:4px;
        --crowd-input-focus-color:hsla(206, 99%, 50%, 0.47);
        --crowd-input-wrapper-padding-vertical:0.5em;
        --crowd-input-padding-vertical: 0px;
        --crowd-input-focus-width:1px;
        --crowd-input-error-message-color:hsl(0, 100%, 78%);
        --crowd-menu-item-hover-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-menu-item-hover-color:hsl(206, 99%, 50%);
        --track-color:hsla(206, 99%, 50%, 0.1);
        --indicator-color:hsl(206, 99%, 50%);
        --crowd-range-spacing:0.5em;
        --crowd-range-track-height:0.5em;
        --crowd-range-track-border-width:1px;
        --crowd-range-track-border-color:hsla(206, 99%, 50%, 0.18);
        --crowd-range-track-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-range-thumb-border-color:hsla(206, 99%, 50%, 0.47);
        --crowd-range-notch-color:hsla(206, 99%, 50%, 0.47);
        --crowd-range-track-border-radius:4px;
        --crowd-range-indicator-color:hsl(206, 99%, 50%);
        --crowd-option-hover-color:hsl(206, 99%, 50%);
        --crowd-option-hover-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-switch-track-background-color:hsla(206, 99%, 50%, 0.1);
        --crowd-switch-track-background-color-active:hsl(206, 99%, 50%);
        --crowd-tooltip-color:hsl(206, 99%, 50%);
        --crowd-tooltip-border-color:hsla(206, 99%, 50%, 0.18);
        --crowd-tooltip-border-radius:4px;
    }
</pre>