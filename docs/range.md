# Range

A range slider for picking numbers

## Example

Use `min`, `max`, and `step` to define what values can be selected.
The range component has all shared input properties like `invalid`, `required`, `label`, and `errorMessage`.

```
<crowd-range label='Slide me!' min='0' max='50' step='10'></crowd-range>
```
<crowd-range label='Slide me!' min='0' max='50' step='10'></crowd-range>

### Notches

Add the `notches` attribute to show notches at each step

```
<crowd-range min='0' max='50' step='10' notches></crowd-range>
```
<crowd-range min='0' max='50' step='10' notches></crowd-range>

### Numbers

Add the `numbers` attribute to show numbers at each step. Can be combined with `notches`.

```
<crowd-range min='0' max='50' step='10' numbers></crowd-range>
<crowd-range min='0' max='50' step='10' numbers notches></crowd-range>
```
<crowd-range min='0' max='50' step='10' numbers></crowd-range>
<crowd-range min='0' max='50' step='10' numbers notches></crowd-range>

### Tooltip

Add the `tooltip` attribute to show a tooltip containing the current value when sliding.

```
<crowd-range min='0' max='100' tooltip></crowd-range>
```
<crowd-range min='0' max='100' tooltip></crowd-range>

## Events

|Event|Called by|
|---|---|
|`crowdChange`|Changing the value|

## Customisation

A lot of the `<crowd-input>` properties apply to the range.

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-range-width`|Width of the range|<crowd-customiser property='--crowd-range-width' type='Number' min='0' max='500' step='100' value='300' unit='px'></crowd-customiser>|
|`--crowd-range-spacing`|Gap between the range track and numbers|<crowd-customiser property='--crowd-range-spacing' type='Number' min='0' max='3' unit='em' step='0.1' value='0.2'></crowd-customiser>|
|`--crowd-range-track-height`|Height of the range track|<crowd-customiser property='--crowd-range-track-height' type='Number' value='0.5' unit='em' min='0' max='3' step='0.5'></crowd-customiser>|
|`--crowd-range-track-border-width`|Width of the range track border|<crowd-customiser property='--crowd-range-track-border-width' type='Number' min='0' max='5' step='1' value='1' unit='px'></crowd-customiser>|
|`--crowd-range-track-border-style`|Style of the range track border|<crowd-customiser property='--crowd-range-track-border-style' type='Select' value='solid' options='["solid","dashed","dotted","none"]'></crowd-customiser>|
|`--crowd-range-track-border-color`|Color of the range track border|<crowd-customiser property='--crowd-range-track-border-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-range-track-border-radius`|Border Radius of the range track|<crowd-customiser property='--crowd-range-track-border-radius' type='Number' value='0' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-range-track-background-color`|Background color of the range track|<crowd-customiser property='--crowd-range-track-background-color' type='Color' value='#eeeeee'></crowd-customiser>|
|`--crowd-range-indicator-color`|Color of the range indicator|<crowd-customiser property='--crowd-range-indicator-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-range-thumb-border-style`|Style of the range thumb border|<crowd-customiser property='--crowd-range-thumb-border-style' type='Select' value='solid' options='["solid","dashed","dotted","none"]'></crowd-customiser>|
|`--crowd-range-thumb-border-color`|Color of the range thumb border|<crowd-customiser property='--crowd-range-thumb-border-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-range-thumb-border-radius`|Border Radius of the range thumb|<crowd-customiser property='--crowd-range-thumb-border-radius' type='Number' value='50' unit='%' min='0' max='50' step='1'></crowd-customiser>|
|`--crowd-range-thumb-background-color`|Background color of the range thumb|<crowd-customiser property='--crowd-range-thumb-background-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-range-notch-height`|Height of the range notch|<crowd-customiser property='--crowd-range-notch-height' type='Number' value='0.5' unit='em' min='0' max='3' step='0.5'></crowd-customiser>|
|`--crowd-range-notch-width`|Width of the range notch|<crowd-customiser property='--crowd-range-notch-width' type='Number' value='1' unit='px' min='0' max='3' step='1'></crowd-customiser>|
|`--crowd-range-notch-color`|Color of the range notch|<crowd-customiser property='--crowd-range-notch-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-range-number-color`|Color of the range numbers|<crowd-customiser property='--crowd-range-number-color' type='Color' value='#000000'></crowd-customiser>|
