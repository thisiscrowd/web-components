# Spinner

The spinner is an animated loading indicator.

## Example

Use CSS Properties to style the spinner.

```
<crowd-spinner></crowd-spinner>
<crowd-spinner style='--track-color: #eee;--indicator-color: #58ff4f;--track-width: 4px;font-size: 2em;'></crowd-spinner>
```
<crowd-spinner></crowd-spinner>
<crowd-spinner style='--track-color: #eee;--indicator-color: #58ff4f;--track-width: 4px;font-size: 2em;'></crowd-spinner>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-spinner-animation-duration`|Duration of the spinning animation|<crowd-customiser property='--crowd-spinner-animation-duration' type='Number' unit='s' min='0' max='3' value='1' step='1'></crowd-customiser>|
|`--track-width`|Width of the track|<crowd-customiser property='--track-width' type='Number' unit='px' min='0' max='5' value='2' step='1'></crowd-customiser>|
|`--track-color`|Color of the track|<crowd-customiser property='--track-color' type='Color' value='#000000'></crowd-customiser>|
|`--indicator-color`|Color of the indicator|<crowd-customiser property='--indicator-color' type='Color' value='#000000'></crowd-customiser>|