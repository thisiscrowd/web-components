# Switch

Toggle between two values with a stylised switch.

## Example

```
<crowd-switch label='Flick me!'></crowd-switch>
```
<crowd-switch label='Flick me!'></crowd-switch>

### True/False

Use the `true` and `false` attributes to label the values being toggled.

```
<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>
```
<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-switch-spacing`|Spacing between switch and the side labels|<crowd-customiser property='--crowd-switch-spacing' type="Number" step='0.5' min='0' max='3' value='0.5' unit='em'></crowd-customiser>|
|`--crowd-switch-track-spacing`|Spacing around the inside of the track|<crowd-customiser property='--crowd-switch-track-spacing' type="Number" step='1' min='0' max='5' value='2' unit='px'></crowd-customiser>|
|`--crowd-switch-border-radius`|Border radius of the switch|<crowd-customiser property='--crowd-switch-border-radius' type='Number' step='1' min='0' max='1000' value='999' unit='px'></crowd-customiser>|
|`--crowd-switch-size`|Size of the switch thumb|<crowd-customiser property='--crowd-switch-size' type='Number' step='0.5' unit='em' min='0' max='3' value='1'></crowd-customiser>|
|`--crowd-switch-track-background-color`|Background color of the track|<crowd-customiser property='--crowd-switch-track-background-color' type='Color' value='#eeeeee'></crowd-customiser>|
|`--crowd-switch-thumb-background-color`|Background color of the thumb|<crowd-customiser property='--crowd-switch-thumb-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-switch-thumb-background-color-active`|Background color of the thumb when active|<crowd-customiser property='--crowd-switch-thumb-background-color-active' type='Color' value='#000000'></crowd-customiser>|