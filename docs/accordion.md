# Accordion

Showcase content in an accordion. Use the `crowd-accordion-group` and `crowd-accordion-item`.

## Example

```
<crowd-accordion-group>
    <crowd-accordion-item title='Item #1'>
        <p>Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Vestibulum suscipit nulla quis orci.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #2'>
        <p>Phasellus a est. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #3'>
        <p>Phasellus viverra nulla ut metus varius laoreet. Vivamus elementum semper nisi. Fusce commodo aliquam arcu.</p>
    </crowd-accordion-item>
</crowd-accordion-group>
        
```
<crowd-accordion-group>
    <crowd-accordion-item title='Item #1'>
        <p>Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Vestibulum suscipit nulla quis orci.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #2'>
        <p>Phasellus a est. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #3'>
        <p>Phasellus viverra nulla ut metus varius laoreet. Vivamus elementum semper nisi. Fusce commodo aliquam arcu.</p>
    </crowd-accordion-item>
</crowd-accordion-group>

### Multiple

Add the `multiple` attribute to the `crowd-accordion-group` to allow more than one accordion to be open.

```
<crowd-accordion-group multiple>
    <crowd-accordion-item title='Item #1'>
        <p>Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Vestibulum suscipit nulla quis orci.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #2'>
        <p>Phasellus a est. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #3'>
        <p>Phasellus viverra nulla ut metus varius laoreet. Vivamus elementum semper nisi. Fusce commodo aliquam arcu.</p>
    </crowd-accordion-item>
</crowd-accordion-group>
        
```
<crowd-accordion-group multiple>
    <crowd-accordion-item title='Item #1'>
        <p>Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Vestibulum suscipit nulla quis orci.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #2'>
        <p>Phasellus a est. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Item #3'>
        <p>Phasellus viverra nulla ut metus varius laoreet. Vivamus elementum semper nisi. Fusce commodo aliquam arcu.</p>
    </crowd-accordion-item>
</crowd-accordion-group>

## Events

|Event|Called by|
|---|---|
|`crowdOpen`|A `<crowd-accordion-item>` being opened|
|`crowdClose`|A `<crowd-accordion-item>` being closed|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-accordion-border-width`|Width of the borders|<crowd-customiser property='--crowd-accordion-border-width' type='Number' min='0' max='5' step='1' unit='px' value='1'></crowd-customiser>|
|`--crowd-accordion-border-style`|Style of the accordion border|<crowd-customiser property='--crowd-accordion-border-style' type='Select' options='["solid","dashed","dotted","none"]'></crowd-customiser>|
|`--crowd-accordion-border-color`|Color of the borders|<crowd-customiser property='--crowd-accordion-border-color' type='Color' value='#ddd'></crowd-customiser>|
|`--crowd-accordion-border-radius`|Border radius|<crowd-customiser property='--crowd-accordion-border-radius' type='Number' min='0' max='16' step='1' unit='px' value='1'></crowd-customiser>|
|`--crowd-accordion-item-tray-padding-vertical`|Top/bottom padding of the accordion tray|<crowd-customiser property='--crowd-accordion-item-tray-padding-vertical' type='Number' min='0' max='3' unit='em' value='0.5' step='0.5'></crowd-customiser>|
|`--crowd-accordion-item-tray-padding-horizontal`|Left/right padding of the accordion tray|<crowd-customiser property='--crowd-accordion-item-tray-padding-horizontal' value='1' type='Number' min='0' max='3' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-accordion-item-tray-background-color`|Background color of the accordion tray|<crowd-customiser property='--crowd-accordion-item-tray-background-color' type='Color' value='#fff'></crowd-customiser>|
|`--crowd-accordion-item-title-padding-vertical`|Top/bottom padding of the accordion title|<crowd-customiser property='--crowd-accordion-item-title-padding-vertical' type='Number' min='0' max='3' unit='em' value='1' step='0.5'></crowd-customiser>|
|`--crowd-accordion-item-title-padding-horizontal`|Left/right padding of the accordion title|<crowd-customiser property='--crowd-accordion-item-title-padding-horizontal' value='1' type='Number' min='0' max='3' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-accordion-item-title-color`|Color of the accordion title|<crowd-customiser property='--crowd-accordion-item-title-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-accordion-item-title-background-color`|Color of the accordion title|<crowd-customiser property='--crowd-accordion-item-title-background-color' type='Color' value='#fff'></crowd-customiser>|
|`--crowd-accordion-item-title-weight`|Left/right padding of the accordion title|<crowd-customiser property='--crowd-accordion-item-title-weight' value='400' type='Number' min='0' max='900' unit='' step='100'></crowd-customiser>|
|`--crowd-accordion-item-title-font-size`|Left/right padding of the accordion title|<crowd-customiser property='--crowd-accordion-item-title-font-size' value='1.15' type='Number' min='0' max='3' unit='em' step='0.05'></crowd-customiser>|
|`--crowd-accordion-item-title-hover-color`|Color of the accordion title on hover|<crowd-customiser property='--crowd-accordion-item-title-hover-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-accordion-item-hover-background-color`|Background color of the accordion title on hover|<crowd-customiser property='--crowd-accordion-item-title-hover-background-color' type='Color' value='#eee'></crowd-customiser>|
|`--crowd-accordion-item-title-open-color`|Color of the accordion title when open|<crowd-customiser property='--crowd-accordion-item-title-open-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-accordion-item-open-background-color`|Background color of the accordion title when open|<crowd-customiser property='--crowd-accordion-item-title-open-background-color' type='Color' value='#eee'></crowd-customiser>|