# Context Menu

Open a custom context menu on right-click.

## Example

```
<crowd-context-menu>
    ...
</crowd-context-menu>
```
<crowd-context-menu>
    ...
</crowd-context-menu>
