# Crowd

Crowd's brand colors/style

<style>
    :root {
        --crowd-alert-color:hsl(22, 97%, 49%);
        --crowd-badge-color:hsl(22, 97%, 49%);
        --crowd-badge-text-color:hsl(0, 100%, 100%);
        --crowd-badge-padding-vertical:5px;
        --crowd-badge-padding-horizontal:9px;
        --crowd-button-background-color:hsla(22, 33%, 99%, 0);
        --crowd-button-color:hsl(22, 97%, 49%);
        --crowd-button-border-width:1px;
        --crowd-button-border-color:hsl(22, 97%, 49%);
        --crowd-button-border-radius:0px;
        --crowd-button-hover-background-color:hsl(22, 97%, 49%);
        --crowd-button-hover-color:hsl(22, 33%, 99%);
        --crowd-button-hover-border-color:hsl(22, 97%, 49%);
        --crowd-button-focus-color:hsla(22, 97%, 49%, 0.15);
        --crowd-checkbox-checked-background-color:hsl(22, 97%, 49%);
        --crowd-checkbox-border-color:hsl(22, 97%, 49%);
        --crowd-date-picker-day-selected-background-color: hsla(22, 97%, 49%, 0.15);
        --crowd-date-picker-calendar-nav-button-hover-background-color: hsla(22, 97%, 49%, 0.15);
        --crowd-input-placeholder-color:hsla(0, 0%, 0%, 0.07);
        --crowd-input-border-style:none;
        --crowd-input-border-width:0px;
        --crowd-input-focus-color:hsla(22, 97%, 49%, 0.15);
        --crowd-input-wrapper-padding-vertical:0.5em;
        --crowd-input-focus-width:0px;
        --crowd-menu-item-hover-background-color:hsla(22, 97%, 49%, 0.15);
        --crowd-menu-item-hover-color:hsl(22, 97%, 49%);
        --track-color:#EDF3F3;
        --indicator-color:hsl(22, 97%, 49%);
        --crowd-range-track-border-width:0px;
        --crowd-range-track-background-color:#EDF3F3;
        --crowd-range-indicator-color:hsl(22, 97%, 49%);
        --crowd-option-hover-color:hsl(22, 97%, 49%);
        --crowd-option-hover-background-color:hsla(22, 97%, 49%, 0.15);
        --crowd-switch-track-background-color:#EDF3F3;
        --crowd-switch-track-background-color-active:hsl(22, 97%, 49%);
        --crowd-tooltip-color:hsl(22, 97%, 49%);
        --crowd-tooltip-border-color:hsla(22, 97%, 49%, 0.15);
        --crowd-input-padding-vertical: 0.625em;
    }
    crowd-select {
        --crowd-input-background: #EDF3F3;
    }
    crowd-input::part(container),crowd-select::part(container),crowd-textarea::part(container),crowd-date-picker:part(input):part(container) {
        border-bottom: 1px solid #969696;
    }
    crowd-input::part(container):focus-within,crowd-select::part(container):focus-within,crowd-textarea::part(container):focus-within,crowd-date-picker:part(input):part(container):focus-within {
        border-bottom: 1px solid hsl(22, 97%, 49%);
    }
</style>

<crowd-alert open>
    <crowd-icon slot='icon' name='info-circle'></crowd-icon>
    <h3>Alert</h3>
    <p>Ut varius tincidunt libero.</p>
</crowd-alert>

<crowd-badge>Lorem Ipsum</crowd-badge>
<crowd-badge pulse>Lorem Ipsum</crowd-badge>

<crowd-button>Click me!</crowd-button>

<crowd-checkbox>Tick me!</crowd-checkbox>

<crowd-date-picker label="Pick a day"></crowd-date-picker>

<crowd-dialog style='--height:50vh;--width: 500px;' label='Itsa me!' id='demoDialog'>
    <p>Lorem Ipsum dialog text</p>
</crowd-dialog>
<crowd-button onClick='demoDialog.show()'>Show Dialog</crowd-button>

<crowd-drawer style='--crowd-drawer-position-right: 60vw;' id='demoDrawer'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer.show()'>Open Drawer</crowd-button>

<crowd-dropdown>
    <crowd-button caret slot='trigger'>Open</crowd-button>
    Presto!
</crowd-dropdown>

<crowd-input type='text' name='First Name' placeholder='Jane Smith' label='First Name'></crowd-input>
<crowd-input type='text' password toggle name='Password' togglePassword label='Password'></crowd-input>
<crowd-input type='text' invalid errorMessage='Something is wrong' toggle name='Password' togglePassword label='Error'></crowd-input>

<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>

<crowd-progress-ring size='3em' percentage='45'></crowd-progress-ring>

<crowd-range tooltip notches numbers step='10'></crowd-range>

<crowd-select label='Select an option' placeholder='Choose something'>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>

<crowd-spinner></crowd-spinner>

<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>

<crowd-textarea placeholder='Type something' label='Bio' maxlength='50' name='bio' required></crowd-textarea>

<crowd-tooltip content='Wassup?'>
    <crowd-button>Hover me!</crowd-button>
</crowd-tooltip>

---