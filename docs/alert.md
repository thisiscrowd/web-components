# Alert

The Alert component creates a banner that notifies a user of something.

## Markup

```
<crowd-alert title='I am an alert'>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>Draw attention to me!</p>
</crowd-alert>
```

Use the `icon` slot to add an icon.

## Examples

<crowd-alert open title='I am an alert'>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>Draw attention to me!</p>
</crowd-alert>

### Closable

Add the `closable` attribute to allow users to close the alert

<crowd-alert closable open title='I am an alert'>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>Click the x to close me!</p>
</crowd-alert>

### Duration

Add the `duration` attribute to auto-hide the alert after a specified time (ms).

<crowd-button onClick='autohide.show()'>Show Alert</crowd-button>

<crowd-alert id='autohide' duration='2000'>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>Byeeeee!</p>
</crowd-alert>

### Toast

Add the `toast` prop to put this alert into a toast container that stays over the screen, like a notification

<crowd-button onClick='toaster.show()'>Show Alert</crowd-button>

<crowd-alert id='toaster' closable toast>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>Wassup</p>
</crowd-alert>

Toasts stack nicely in their container

<crowd-button onClick='toaster2.show()'>Show Alert #2</crowd-button>
<crowd-button onClick='toaster3.show()'>Show Alert #3</crowd-button>

<crowd-alert id='toaster2' closable toast>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>#2</p>
</crowd-alert>

<crowd-alert id='toaster3' closable toast>
    <crowd-icon slot='icon' name='exclamation-triangle'></crowd-icon>
    <p>#3</p>
</crowd-alert>

## Customisation

Use the following custom properties to customise the alert:

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-alert-background-color`|Background Color|<crowd-customiser property='--crowd-alert-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-alert-padding-vertical`|Vertical Padding|<crowd-customiser property='--crowd-alert-padding-vertical' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser>|
|`--crowd-alert-padding-horiztonal`|Horizontal Padding|<crowd-customiser property='--crowd-alert-padding-horizontal' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser>|
|`--crowd-alert-spacing`|Spacing between icon and content|<crowd-customiser property='--crowd-alert-spacing' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser>|
|`--crowd-alert-border-width`|Top border width|<crowd-customiser property='--crowd-alert-border-width' type='Number' unit='px' value='2' min='0' max='16' step='1'></crowd-customiser>|
|`--crowd-alert-border-style`|Top border style|<crowd-customiser property='--crowd-alert-border-style' type='Select' options='["solid", "dashed", "dotted", "none"]' value='solid'></crowd-customiser>|
|`--crowd-alert-color`|Top border & icon color|<crowd-customiser property='--crowd-alert-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-alert-border-radius`|Border radius|<crowd-customiser property='--crowd-alert-border-radius' type='Number' unit='px' value='0' min='0' max='16' step='1'></crowd-customiser>|
|`--crowd-alert-font-size`|Font size|<crowd-customiser property='--crowd-alert-font-size' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser>|
|`--crowd-alert-box-shadow`|Box shadow around alert|<crowd-customiser property='--crowd-alert-box-shadow' type='Text' value='0 2px 8px rgba(0, 0, 0, 0.1)'></crowd-customiser>|