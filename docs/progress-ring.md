# Progress Ring

Show progress with a circular chart.

## Example

```
<crowd-progress-ring size='3em' percentage='45'></crowd-progress-ring>
<crowd-progress-ring size='3em' stroke-width='3px' percentage='70'></crowd-progress-ring>
```
<crowd-progress-ring size='3em' percentage='45'></crowd-progress-ring>
<crowd-progress-ring size='3em' stroke-width='3px' percentage='70'></crowd-progress-ring>

You can update the percentage easily with javascript:

```
<crowd-progress-ring id='myProgress' size='3em' stroke-width='3px' percentage='0'></crowd-progress-ring>
<script>
    setInterval(() => {
        if (myProgress.percentage == 100) {
            myProgress.percentage = 0;
        } else {
            myProgress.percentage = parseInt(myProgress.percentage) + 1;
        }
    },100)
</script>
```
<crowd-progress-ring id='myProgress' size='3em' stroke-width='3px' percentage='0'></crowd-progress-ring>
<script>
    setInterval(() => {
        if (myProgress.percentage == 100) {
            myProgress.percentage = 0;
        } else {
            myProgress.percentage = parseInt(myProgress.percentage) + 1;
        }
    },100)
</script>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--track-color`|Ring track color|<crowd-customiser property='--track-color' type='Color' value='#000000'></crowd-customiser>|
|`--indicator-color`|Ring indicator color|<crowd-customiser property='--indicator-color' type='Color' value='#000000'></crowd-customiser>|