# Drawer

A sliding drawer of content. Good for menus.

## Example

Use the `show()` method to reveal the drawer, and the `hide()` method to hide it.

```
<crowd-drawer style='--crowd-drawer-position-right: 60vw;' id='demoDrawer'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer.show()'>Open Drawer</crowd-button>
```
<crowd-drawer style='--crowd-drawer-position-right: 40vw;' id='demoDrawer'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer.show()'>Open Drawer</crowd-button>

### Placement

Use the `placement` prop to adjust where the drawer slides in from. Values are `top`, `right`,`bottom`,`left`. Default is `left`. Use position properties to change the drawer size.

```
<crowd-drawer style='--crowd-drawer-position-right: 60vw;' id='demoDrawer2'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer2.show()'>Open Drawer</crowd-button>
```
<crowd-drawer style='--crowd-drawer-position-top: 40vh;' id='demoDrawer2' placement='bottom'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer2.show()'>Open Drawer</crowd-button>

## Methods

|Method|Action|
|---|---|
|`show`|Open the drawer|
|`hide`|Close the drawer|
|`toggle`|Toggle open/close|

## Events

|Event|Called when|
|---|---|
|`crowdDrawerShow`|Drawer opened|
|`crowdDrawerHide`|Drawer closed|

## Customisation

|Property|Changes|
|---|---|
|`--crowd-drawer-background`|Drawer background color|
|`--crowd-drawer-position-top`|Drawer top edge position|
|`--crowd-drawer-position-right`|Drawer right edge position|
|`--crowd-drawer-position-bottom`|Drawer bottom edge position|
|`--crowd-drawer-position-left`|Drawer left edge position|
|`--crowd-drawer-padding-vertical`|Drawer top/bottom padding|
|`--crowd-drawer-padding-horizontal`|Drawer left/right padding|
|`--crowd-drawer-container-transition-duration`|Open/Close transition duration|
|`--crowd-drawer-container-transition-ease`|Open/Close transition ease|
|`--crowd-drawer-container-transition-delay`|Open/Close transition delay|
|`--crowd-drawer-z-index`|Drawer z-index|
|`--crowd-drawer-overlay-background`|Overlay background|