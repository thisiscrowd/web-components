# Badge

The badge is a quick component for showcasing small text items, like tags.

## Example

```
<crowd-badge>Lorem Ipsum</crowd-badge>
<crowd-badge type='success'>Lorem Ipsum</crowd-badge>
<crowd-badge type='warning'>Lorem Ipsum</crowd-badge>
<crowd-badge type='danger' style='color: white;'>Lorem Ipsum</crowd-badge>
```

<crowd-badge>Lorem Ipsum</crowd-badge>
<crowd-badge type='success'>Lorem Ipsum</crowd-badge>
<crowd-badge type='warning'>Lorem Ipsum</crowd-badge>
<crowd-badge type='danger' style='color: white;'>Lorem Ipsum</crowd-badge>

Use the `type` attribute to choose pre-defined color schemes.

### Pill

Use the `pill` attribute to make the badge a pill.

<crowd-badge pill>Lorem Ipsum</crowd-badge>
<crowd-badge type='success' pill>Lorem Ipsum</crowd-badge>

### Pulse

Use the `pulse` attribute to add a pulse animation to the badge.

<crowd-badge pill pulse>Lorem Ipsum</crowd-badge>
<crowd-badge type='danger' pill pulse style='color: white;'>Lorem Ipsum</crowd-badge>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-badge-color`|Background color|<crowd-customiser property='--crowd-badge-color' value='#fff' type='Color'></crowd-customiser>|
|`--crowd-badge-text-color`|Text color|<crowd-customiser property='--crowd-badge-text-color' value='#000' type='Color'></crowd-customiser>|
|`--crowd-badge-padding-vertical`|Vertical padding|<crowd-customiser property='--crowd-badge-padding-vertical' type='Number' unit='px' step='1' min='0' max='16' value='3'></crowd-customiser>|
|`--crowd-badge-padding-horizontal`|Horizontal Padding|<crowd-customiser property='--crowd-badge-padding-horizontal' type='Number' unit='px' step='1' min='0' max='16' value='6'></crowd-customiser>|
|`--crowd-badge-border-radius`|Border radius (when not a pill)|<crowd-customiser property='--crowd-badge-border-radius' type='Number' unit='px' step='1' min='0' max='16' value='0'></crowd-customiser>|
|`--pulse-color`|Color of the animated pulse|<crowd-customiser property='--pulse-color' value='#000' type='Color'></crowd-customiser>|