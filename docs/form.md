# Form

---
## Depreciation notice

This element is now depreciated. Custom form elements can now be added into native `HTMLFormElement`s due to `FormController`.

---

Most custom form elements cannot be submitted with regular HTML `<form>` elements, so this acts as a wrapper for it.

## Example

```
<crowd-form id='myForm'>
    <crowd-input type='text' name='Full Name' required label='Full Name' placeholder='John Smith'></crowd-input>
    <crowd-input label='Email Address' type='email' name='Email Address' required placeholder='mail@example.com'></crowd-input>
    <crowd-textarea name='message' label='Message' placeholder='Type something' required></crowd-textarea>
    <crowd-select name='choice' required label='Choice' placeholder='Select something'>
        <crowd-option value='one'>One</crowd-option>
        <crowd-option value='two'>Two</crowd-option>
        <crowd-option value='three'>Three</crowd-option>
    </crowd-select>
    <crowd-button onClick='this.parentElement.submit()'>Submit</crowd-button>
</crowd-form>
```
<style>
    :root {
        --crowd-input-border-width: 1px;
        --crowd-input-border-color: #eeeeee;
        --crowd-input-placeholder-color: rgba(0,0,0,0.4);
        --crowd-input-wrapper-padding-vertical: 1em;
    }
    crowd-form {
        box-shadow: 0px 1px 3px rgba(0,0,0,0.2);
    }
    crowd-select {
        width: 100%;
    }
</style>
<crowd-form id='myForm'>
    <crowd-input type='text' name='Full Name' required label='Full Name' placeholder='John Smith'></crowd-input>
    <crowd-input label='Email Address' type='email' name='Email Address' required placeholder='mail@example.com'></crowd-input>
    <crowd-textarea name='message' label='Message' placeholder='Type something' required></crowd-textarea>
    <crowd-select name='choice' required label='Choice' placeholder='Select something'>
        <crowd-option value='one'>One</crowd-option>
        <crowd-option value='two'>Two</crowd-option>
        <crowd-option value='three'>Three</crowd-option>
    </crowd-select>
    <crowd-button onClick='this.parentElement.submit()'>Submit</crowd-button>
</crowd-form>

## Methods

|Method|Action|
|---|---|
|`validate`|Validates all form fields (shows any input errors). Returns a Promise which resolves if no fields are invalid|
|`getFormControls`|Return an Promise which resolves with an Array of form field elements|
|`getFormData`|Returns a Promise which resolves with a FormData object containg all data in the form|
|`submit`|Validates the form, and if valid dispatches the `crowdFormSubmit` event|

## Events 

|Event|Called when|
|---|---|
|`crowdFormSubmit`|After `submit()` is called, if the form is valid|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-form-padding-vertical`|Form top/bottom padding|<crowd-customiser property='--crowd-form-padding-vertical' value='1' unit='em' min='0' max='5' step='0.5' type='Number'></crowd-customiser>|
|`--crowd-form-padding-horizontal`|Form left/right padding|<crowd-customiser property='--crowd-form-padding-horizontal' value='1' unit='em' min='0' max='5' step='0.5' type='Number'></crowd-customiser>|