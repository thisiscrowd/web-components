# Icon

Display an icon. Can use icon from the [Bootstrap Icons](https://icons.getbootstrap.com/) library, or a custom src.

## Example

Use `name` to show an icon from the library. Use `src` to show an external image. Since it's an icon, it will always be square.

```
<crowd-icon name='hand-thumbs-up'></crowd-icon>
<crowd-icon src='https://thisiscrowd.com/wp-content/themes/crowdtwentyone-package/dist/images/logo.svg'></crowd-icon>
```
<crowd-icon name='hand-thumbs-up'></crowd-icon>
<crowd-icon src='https://thisiscrowd.com/wp-content/themes/crowdtwentyone-package/dist/images/logo.svg'></crowd-icon>

### Count

Add a notification-style count to your icon using the `count` prop. Change the colors using the `--crowd-icon-count-background-color` and `--crowd-icon-count-color` CSS properties.

```
<crowd-icon count="8" name="bell-fill"></crowd-icon>
<crowd-icon count="11" name="bell-fill"></crowd-icon>
```
<crowd-icon count="8" name="bell-fill"></crowd-icon>
<crowd-icon count="11" name="bell-fill"></crowd-icon>