# Multi-toggle

A multi-select element with visual buttons to toggle.

## Usage

```
<crowd-multi-toggle>
    <crowd-multi-toggle-item value="One">Toggle Me!</crowd-multi-toggle-item>
    <crowd-multi-toggle-item value="Two" icon-position="before">Toggle Me!</crowd-multi-toggle-item>
    <crowd-multi-toggle-item  value="Three" show-icon="false">Toggle Me!</crowd-multi-toggle-item>
</crowd-multi-toggle>
```
<crowd-multi-toggle>
    <crowd-multi-toggle-item value="One">Toggle Me!</crowd-multi-toggle-item>
    <crowd-multi-toggle-item value="Two" icon-position="before">Toggle Me!</crowd-multi-toggle-item>
    <crowd-multi-toggle-item  value="Three" show-icon="false">Toggle Me!</crowd-multi-toggle-item>
</crowd-multi-toggle>

## Attributes

### `show-icon`

Use this on `<crowd-multi-toggle-item>` to show or hide the toggle icon slot. Default is `true`.

### `icon-position`

Set this on `<crowd-multi-toggle-item>` to change where the toggle icon slot appears. Use `before` to show before the button text, use `after` to show after the button text. Default is `after`.

### Input Attributes

Since `<crowd-multi-toggle>` is a form element, it inherits properties from `<crowd-input>` such as `name`, `value`, `label`, `required`, `invalid`, and `errorMessage`.

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-multi-toggle-row-gap`|Gap between toggles in the row.|<crowd-customiser property='--crowd-multi-toggle-row-gap' type='Number' unit='em' step='0.1' min='0' max='3' value='1'></crowd-customiser>|
|`--crowd-multi-toggle-off-background-color`|Toggle background color when off|<crowd-customiser property='--crowd-multi-toggle-off-background-color' value='#fff' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-on-background-color`|Toggle background color when on|<crowd-customiser property='--crowd-multi-toggle-on-background-color' value='rgba(133, 255, 102,0.3)' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-off-color`|Toggle color when off|<crowd-customiser property='--crowd-multi-toggle-off-color' value='inherit' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-on-color`|Toggle color when on|<crowd-customiser property='--crowd-multi-toggle-on-color' value='#85ff66' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-off-border-color`|Toggle border color when off|<crowd-customiser property='--crowd-multi-toggle-off-border-color' value='#000' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-on-border-color`|Toggle border color when on|<crowd-customiser property='--crowd-multi-toggle-on-border-color' value='#85ff66' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-off-focus-color`|Toggle focus color when off|<crowd-customiser property='--crowd-multi-toggle-off-focus-color' value='#000' type='Color'></crowd-customiser>|
|`--crowd-multi-toggle-on-focus-color`|Toggle focus color when on|<crowd-customiser property='--crowd-multi-toggle-on-focus-color' value='rgba(133, 255, 102,0.3)' type='Color'></crowd-customiser>|

The `<crowd-multi-toggle-item>` inherits other properties from `<crowd-button>`, for consistent UI styling.