# Progressive Enhancer

One of the flaws with Web Components is that without JavaScript, they will not load.
This tool allows you to use a standard HTML Component in your page, then progressively enahnce it to become a Web Component.

## Usage

Give your HTML element a `cc-element` attribute with the tag name of the component. Include all attributes you will need.

```
<input cc-element='crowd-input' type="text" showSuccess="true" label='Text Field' placeholder='Type something' name="Text Field" required  />
```

Include the Enahncer file in your scripts, and initialise it like so:

```
const ccEnhancer = new Enhancer()
ccEnhancer.init()
```

And voila! Your regular `<input>` will become a `<crowd-input>` before your eyes.