# Date Picker

Pick a date from a calendar.

## Example

```
<crowd-date-picker label="Pick a day"></crowd-date-picker>
```
<crowd-date-picker label="Pick a day"></crowd-date-picker>

## Attributes

Date Picker inherits a lot of standard attributes from `<crowd-input>`, like `invalid`, `required`, and `errorMessage`.

### Range

Use the `range` attribute to define a date range.

```
<crowd-date-picker range label="Pick a date range"></crowd-date-picker>
```
<crowd-date-picker range label="Pick a date range"></crowd-date-picker>

## Events

|Event|Called when|
|---|---|
|`crowdChange`|The value of the input changes|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-date-picker-calendar-padding-top`|Padding above the calendar|<crowd-customiser property='--crowd-date-picker-calendar-padding-top' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-date-time-picker-font-size`|Font size of the calendar area|<crowd-customiser property='--crowd-date-time-picker-font-size' type='Number' unit='em' value='0.7' min='0' max='3' step='0.1'></crowd-customiser>|
|`--crowd-date-picker-panel-background-color`|Background color of the picker panel|<crowd-customiser property='--crowd-date-picker-panel-background-color' value='#ffffff' type='Color'></crowd-customiser>|
|`--crowd-date-picker-panel-padding-vertical`|Padding around the calendar|<crowd-customiser property='--crowd-date-picker-panel-padding-vertical' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-date-picker-panel-padding-horizontal`|Padding around the calendar|<crowd-customiser property='--crowd-date-picker-panel-padding-horizontal' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-date-picker-calendar-nav-justify`|Justify items in the calendar top bar|<crowd-customiser property='--crowd-date-picker-calendar-nav-justify' type='Select' value='center' options='["center", "flex-start", "flex-end", "space-between", "space-around"]'></crowd-customiser>|
|`--crowd-date-picker-calendar-nav-padding-vertical`|Padding around the calendar|<crowd-customiser property='--crowd-date-picker-calendar-nav-padding-vertical' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-date-picker-calendar-nav-padding-horizontal`|Padding around the calendar|<crowd-customiser property='--crowd-date-picker-calendar-nav-padding-horizontal' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-date-picker-month-picker-width`|Width of the month picker|<crowd-customiser property='--crowd-date-picker-month-picker-width' type='Number' unit='em' value='13' min='0' max='20' step='1'></crowd-customiser>|
|`--crowd-date-picker-year-picker-width`|Width of the year picker|<crowd-customiser property='--crowd-date-picker-year-picker-width' type='Number' unit='em' value='12' min='0' max='20' step='1'></crowd-customiser>|
|`--crowd-date-picker-calendar-nav-button-hover-background-color`|Background color of the nav buttons on hover|<crowd-customiser property='--crowd-date-picker-calendar-nav-button-hover-background-color' value='#f9f9f9' type='Color'></crowd-customiser>|
|`--crowd-date-picker-day-border-width`|Border width|<crowd-customiser property='--crowd-date-picker-day-border-width' type='Number' value='1' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-date-picker-day-border-style`|Border style|<crowd-customiser property='--crowd-date-picker-day-border-style' type='Select' value='solid' options='["solid", "dashed", "dotted", "none"]'></crowd-customiser>|
|`--crowd-date-picker-day-border-color`|Input border color|<crowd-customiser property='--crowd-date-picker-day-border-color' value='#f7f7f7' type='Color'></crowd-customiser>|
|`--crowd-date-picker-day-hover-background-color`|Background color of the day squares on hover|<crowd-customiser property='--crowd-date-picker-day-hover-background-color' value='#f9f9f9' type='Color'></crowd-customiser>|
|`--crowd-date-picker-day-selected-background-color`|Background color of the selected day.|<crowd-customiser property='--crowd-date-picker-day-selected-background-color' value='#eeeeee' type='Color'></crowd-customiser>|
