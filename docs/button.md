# Button

Create consistent buttons with the button component

## Example

```
<crowd-button>Click me!</crowd-button>
```

<crowd-button>Click me!</crowd-button>

### Link Buttons

Add a `href` to link the button elsewhere. Also supports `target`.

```
<crowd-button href='https://thisiscrowd.com' target='_blank'>Click me!</crowd-button>
```

<crowd-button href='https://thisiscrowd.com' target='_blank'>Click me!</crowd-button>

### Pill

Use the `pill` attribute to make the button a pill.

<crowd-button pill>Click me!</crowd-button>


### Loading

Use the `loading` attribute to replace the content with a loading spinner.

<crowd-button loading>Click me!</crowd-button>

### Caret

Add the `caret` attribute to show a caret icon, indicating a dropdown.

<crowd-button caret>Click me!</crowd-button>

### Circle

Add the `circle` attribute to make the button round. Good for icons.

```
<crowd-button circle>
    <crowd-icon name='play'></crowd-icon>
</crowd-button>
```

<crowd-button circle>
    <crowd-icon name='play'></crowd-icon>
</crowd-button>
<crowd-button circle>
    Click me!
</crowd-button>

### Prefix & Suffix

Add content in the `prefix` and `suffix` slots to add them either side of the main button content. Good for having icons in with content.

```
<crowd-button>
    <crowd-icon name='plus' slot='prefix'></crowd-icon>
    I'm surrounded!
    <crowd-icon name='x' slot='suffix'></crowd-icon>
</crowd-button>
```

<crowd-button>
    <crowd-icon name='plus' slot='prefix'></crowd-icon>
    I'm surrounded!
    <crowd-icon name='x' slot='suffix'></crowd-icon>
</crowd-button>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-button-background-color`|Background Color|<crowd-customiser property='--crowd-button-background-color' type='Color' value='#eeeeee'></crowd-customiser>|
|`--crowd-button-padding-vertical`| Vertical padding|<crowd-customiser property='--crowd-button-padding-vertical' type='Number' unit='em' value='0.5' step='0.1' min='0' max='10'></crowd-customiser>
|`--crowd-button-padding-horizontal`|Horizontal padding|<crowd-customiser property='--crowd-button-padding-horizontal' type='Number' unit='em' value='0.5' step='0.1' min='0' max='10'></crowd-customiser>|
|`--crowd-button-color`|Text color|<crowd-customiser property='--crowd-button-color' type='Color' value=''></crowd-customiser>|
|`--crowd-button-border-width`|Border width|<crowd-customiser property='--crowd-button-border-width' type='Number' unit='px' value='2' step='1' min='0' max='16'></crowd-customiser>|
|`--crowd-button-border-style`|Border style|<crowd-customiser property='--crowd-button-border-style' type='Select' options='["solid","dashed","dotted","none"]' value='solid'></crowd-customiser>|
|`--crowd-button-border-color`|Border color|<crowd-customiser property='--crowd-button-border-color' type='Color' value='#aeaeae'></crowd-customiser>|
|`--crowd-button-border-radius`|Border radius (when not a pill)|<crowd-customiser property='--crowd-button-border-radius' type='Number' unit='px' value='2' step='1' min='0' max='16'></crowd-customiser>|
|`--crowd-button-hover-background-color`|Background color when hovered|<crowd-customiser property='--crowd-button-hover-background-color' type='Color' value='#aeaeae'></crowd-customiser>|
|`--crowd-button-hover-border-color`|Border color when hovered|<crowd-customiser property='--crowd-button-hover-border-color' type='Color' value='#aeaeae'></crowd-customiser>|
|`--crowd-button-hover-color`|Text color when hovered|<crowd-customiser property='--crowd-button-hover-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-button-text-align`|Text alignment|<crowd-customiser property='--crowd-button-text-align' type='Select' options='["left","center","right","none"]' value='center'></crowd-customiser>|
|`--crowd-button-height`|Button Height|<crowd-customiser property='--crowd-button-height' type='Number' unit='em' value='2.5' step='0.5' min='0' max='16'></crowd-customiser>|
|`--crowd-button-justify`|Content Justify (includes prefix & suffix)|<crowd-customiser property='--crowd-button-justify' type='Select' options='["center","space-between","space-around"]' value='center'></crowd-customiser>|
|`--crowd-button-gap`|Spacing between suffix, prefix, and content|<crowd-customiser property='--crowd-button-gap' type='Number' unit='em' value='0.5' step='0.5' min='0' max='16'></crowd-customiser>|
|`--crowd-button-transition-duration`|Hover transition duration|<crowd-customiser property='--crowd-button-transition-duration' type='Number' unit='s' value='0.15' step='0.5' min='0' max='10'></crowd-customiser>|
|`--crowd-button-transition-ease`|Hover transition ease|<crowd-customiser property='--crowd-button-transition-ease' type='Select' options='["ease-in-out","ease-in","ease-out","linear"]' value='ease-in-out'></crowd-customiser>|
|`--crowd-button-transition-delay`|Hover transition delay|<crowd-customiser property='--crowd-button-transition-delay' type='Number' unit='s' value='0' step='0.5' min='0' max='10'></crowd-customiser>|
|`--crowd-button-focus-width`|Width of the focus shadow|<crowd-customiser property='--crowd-button-focus-width' type='Number' unit='px' value='2' step='1' min='0' max='16'></crowd-customiser>|
|`--crowd-button-focus-color`|Color of the focus shadow|<crowd-customiser property='--crowd-button-focus-color' type='Color' value='#aeaeae'></crowd-customiser>|
|`--crowd-button-pill-border-radius`|Border radius when a pill||
|`--crowd-button-width`|Specify a width when the button is a circle|<crowd-customiser property='--crowd-button-width' type='Number' unit='em' value='2.5' step='0.5' min='0' max='16'></crowd-customiser>|