# Dialog

A modal component for showcasing content in popups

## Example

Dialogs have the `show()` method to reveal them. Use `--width` and `--height` to adjust the size.
```
<crowd-dialog style='--height:50vh;--width: 500px;' label='Itsa me!' id='demoDialog'>
    <p>Lorem Ipsum dialog text</p>
</crowd-dialog>
<crowd-button onClick='demoDialog.show()'>Show Dialog</crowd-button>
```
<crowd-dialog style='--height:50vh;--width: 500px;' label='Itsa me!' id='demoDialog'>
    <p>Lorem Ipsum dialog text</p>
</crowd-dialog>
<crowd-button onClick='demoDialog.show()'>Show Dialog</crowd-button>

## Label

The `label` attribute adds a heading to the dialog.

## Methods

|Method|Action|
|---|---|
|`show`|Open the dialog|
|`hide`|Close the dialog|
|`toggle`|Toggle open/close|

## Events

|Event|Called when|
|---|---|
|`crowdDialogShow`|Dialog is opened|
|`crowdDialogHide`|Dialog is closed|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--width`|Dialog width|
|`--height`|Dialog height|
|`--crowd-dialog-background`|Dialog background color|<crowd-customiser property='--crowd-dialog-background' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-dialog-padding-vertical`|Vertical padding|<crowd-customiser property='--crowd-dialog-padding-vertical' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser>|
|`--crowd-dialog-padding-horizontal`|Horizontal padding|<crowd-customiser property='--crowd-dialog-padding-horizontal' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser>|
|`--crowd-dialog-overlay-background`|Overlay background color|<crowd-customiser property='--crowd-dialog-overlay-background' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-dialog-spacing-vertical`|Vertical margin outside dialog|<crowd-customiser property='--crowd-dialog-spacing-vertical' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser>|
|`--crowd-dialog-spacing-horizontal`|Horizontal margin outside dialog|<crowd-customiser property='--crowd-dialog-spacing-horizontal' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser>|
|`--crowd-dialog-container-transition-duration`|Transition duration|
|`--crowd-dialog-container-transition-ease`|Transition ease|
|`--crowd-dialog-container-transition-delay`|Transition delay|
|`--crowd-dialog-border-radius`|Dialog border radius|<crowd-customiser property='--crowd-dialog-border-radius' type='Number' min='0' max='16' unit='px' value='0'></crowd-customiser>|
|`--crowd-dialog-box-shadow`|Box shadow|<crowd-customiser property='--crowd-dialog-box-shadow' value='0 1px 1px hsl(0deg 0% 0% / 0.075), 0 2px 2px hsl(0deg 0% 0% / 0.075), 0 4px 4px hsl(0deg 0% 0% / 0.075), 0 8px 8px hsl(0deg 0% 0% / 0.075), 0 16px 16px hsl(0deg 0% 0% / 0.075)' type='Text'>|
|`--crowd-dialog-z-index`|Z-index|
|`--crowd-dialog-close-background`|Close button background color|
|`--crowd-dialog-close-color`|Close button icon color|
|`--crowd-dialog-close-border-width`|Close button border width|
|`--crowd-dialog-close-border-style`|Close button border style|
|`--crowd-dialog-close-border-color`|Close button border color|
|`--crowd-dialog-close-font-size`|Close button size|
|`--crowd-dialog-close-hover-opacity`|Close button opacity on hover|