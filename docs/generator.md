# Generator

Use the customisers below to adjust the components to match your style. Copy the generated properties and use them in your CSS.

---
<div class='cols'>
<div>
<crowd-alert open>
    <crowd-icon slot='icon' name='info-circle'></crowd-icon>
    <h3>Alert</h3>
    <p>Ut varius tincidunt libero.</p>
</crowd-alert>

<crowd-badge>Lorem Ipsum</crowd-badge>
<crowd-badge pulse>Lorem Ipsum</crowd-badge>

<crowd-button>Click me!</crowd-button>

<crowd-checkbox>Tick me!</crowd-checkbox>

<crowd-dialog style='--height:50vh;--width: 500px;' label='Itsa me!' id='demoDialog'>
    <p>Lorem Ipsum dialog text</p>
</crowd-dialog>
<crowd-button onClick='demoDialog.show()'>Show Dialog</crowd-button>

<crowd-drawer style='--crowd-drawer-position-right: 60vw;' id='demoDrawer'>
    <p>Socks</p>
    <p>Pants</p>
    <p>Odds n ends</p>
</crowd-drawer>
<crowd-button onClick='demoDrawer.show()'>Open Drawer</crowd-button>

<crowd-dropdown>
    <crowd-button caret slot='trigger'>Open</crowd-button>
    Presto!
</crowd-dropdown>

<crowd-input type='text' name='First Name' placeholder='Jane Smith' label='First Name'></crowd-input>
<crowd-input type='text' password toggle name='Password' togglePassword label='Password'></crowd-input>
<crowd-input type='text' invalid errorMessage='Something is wrong' toggle name='Password' togglePassword label='Error'></crowd-input>

<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>

<crowd-progress-ring size='3em' percentage='45'></crowd-progress-ring>

<crowd-range tooltip notches numbers step='10'></crowd-range>

<crowd-select label='Select an option' placeholder='Choose something'>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>

<crowd-spinner></crowd-spinner>

<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>

<crowd-textarea placeholder='Type something' label='Bio' maxlength='50' name='bio' required></crowd-textarea>

<crowd-tooltip content='Wassup?'>
    <crowd-button>Hover me!</crowd-button>
</crowd-tooltip>
</div>
<div class='table-wrapper' style='max-height: 100vh;'>
<table>
<tbody>
<tr>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Alert Background Color</td>
    <td><crowd-customiser property='--crowd-alert-background-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
    <td>Alert Vertical Padding</td>
    <td><crowd-customiser property='--crowd-alert-padding-vertical' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Horizontal Padding</td>
<td><crowd-customiser property='--crowd-alert-padding-horizontal' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Spacing between icon and content</td>
<td><crowd-customiser property='--crowd-alert-spacing' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Top border width</td>
<td><crowd-customiser property='--crowd-alert-border-width' type='Number' unit='px' value='2' min='0' max='16' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Top border style</td>
<td><crowd-customiser property='--crowd-alert-border-style' type='Select' options='["solid", "dashed", "dotted", "none"]' value='solid'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Top border & icon color</td>
<td><crowd-customiser property='--crowd-alert-color' type='Color' value='#000'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Border radius</td>
<td><crowd-customiser property='--crowd-alert-border-radius' type='Number' unit='px' value='0' min='0' max='16' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Alert Font size</td>
<td><crowd-customiser property='--crowd-alert-font-size' type='Number' unit='em' value='1' min='0' max='10' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Box shadow around alert</td>
<td><crowd-customiser property='--crowd-alert-box-shadow' type='Text' value='0 2px 8px rgba(0, 0, 0, 0.1)'></crowd-customiser></td>
</tr>
<tr>
<td>Badge Background color</td>
<td><crowd-customiser property='--crowd-badge-color' value='#fff' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Badge Text color</td>
<td><crowd-customiser property='--crowd-badge-text-color' value='#000' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Badge Vertical padding</td>
<td><crowd-customiser property='--crowd-badge-padding-vertical' type='Number' unit='px' step='1' min='0' max='16' value='3'></crowd-customiser></td>
</tr>
<tr>
<td>Badge Horizontal Padding</td>
<td><crowd-customiser property='--crowd-badge-padding-horizontal' type='Number' unit='px' step='1' min='0' max='16' value='6'></crowd-customiser></td>
</tr>
<tr>
<td>Badge Border radius (when not a pill)</td>
<td><crowd-customiser property='--crowd-badge-border-radius' type='Number' unit='px' step='1' min='0' max='16' value='0'></crowd-customiser></td>
</tr>
<tr>
<td>Color of the Badge animated pulse</td>
<td><crowd-customiser property='--pulse-color' value='#000' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Button Background Color</td>
<td><crowd-customiser property='--crowd-button-background-color' type='Color' value='#eeeeee'></crowd-customiser></td>
</tr>
<tr>
<td>Button Vertical padding</td>
<td><crowd-customiser property='--crowd-button-padding-vertical' type='Number' unit='em' value='0.5' step='0.1' min='0' max='10'></crowd-customiser></td>
</tr>
<tr>
<td>Button Horizontal padding</td>
<td><crowd-customiser property='--crowd-button-padding-horizontal' type='Number' unit='em' value='0.5' step='0.1' min='0' max='10'></crowd-customiser></td>
</tr>
<tr>
<td>Button Text color</td>
<td><crowd-customiser property='--crowd-button-color' type='Color' value=''></crowd-customiser></td>
</tr>
<tr>
<td>Button Border width</td>
<td><crowd-customiser property='--crowd-button-border-width' type='Number' unit='px' value='2' step='1' min='0' max='16'></crowd-customiser></td>
</tr>
<tr>
<td>Button Border style</td>
<td><crowd-customiser property='--crowd-button-border-style' type='Select' options='["solid","dashed","dotted","none"]' value='solid'></crowd-customiser></td>
</tr>
<tr>
<td>Button Border color</td>
<td><crowd-customiser property='--crowd-button-border-color' type='Color' value='#aeaeae'></crowd-customiser></td>
</tr>
<tr>
<td>Button Border radius (when not a pill)</td>
<td><crowd-customiser property='--crowd-button-border-radius' type='Number' unit='px' value='2' step='1' min='0' max='16'></crowd-customiser></td>
</tr>
<tr>
<td>Button Background color when hovered</td>
<td><crowd-customiser property='--crowd-button-hover-background-color' type='Color' value='#aeaeae'></crowd-customiser></td>
</tr>
<tr>
<td>Button Border color when hovered</td>
<td><crowd-customiser property='--crowd-button-hover-border-color' type='Color' value='#aeaeae'></crowd-customiser></td>
</tr>
<tr>
<td>Button Text color when hovered</td>
<td><crowd-customiser property='--crowd-button-hover-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Button Text alignment</td>
<td><crowd-customiser property='--crowd-button-text-align' type='Select' options='["left","center","right","none"]' value='center'></crowd-customiser></td>
</tr>
<tr>
<td>Button Height</td>
<td><crowd-customiser property='--crowd-button-height' type='Number' unit='em' value='2.5' step='0.5' min='0' max='16'></crowd-customiser></td>
</tr>
<tr>
<td>Button Content Justify (includes prefix & suffix)</td>
<td><crowd-customiser property='--crowd-button-justify' type='Select' options='["center","space-between","space-around"]' value='center'></crowd-customiser></td>
</tr>
<tr>
<td>Button Spacing between suffix, prefix, and content</td>
<td><crowd-customiser property='--crowd-button-gap' type='Number' unit='em' value='0.5' step='0.5' min='0' max='16'></crowd-customiser></td>
</tr>
<tr>
<td>Button Hover transition duration</td>
<td><crowd-customiser property='--crowd-button-transition-duration' type='Number' unit='s' value='0.15' step='0.5' min='0' max='10'></crowd-customiser></td>
</tr>
<tr>
<td>Button Hover transition ease</td>
<td><crowd-customiser property='--crowd-button-transition-ease' type='Select' options='["ease-in-out","ease-in","ease-out","linear"]' value='ease-in-out'></crowd-customiser></td>
</tr>
<tr>
<td>Button Hover transition delay</td>
<td><crowd-customiser property='--crowd-button-transition-delay' type='Number' unit='s' value='0' step='0.5' min='0' max='10'></crowd-customiser></td>
</tr>
<tr>
<td>Button Width of the focus shadow</td>
<td><crowd-customiser property='--crowd-button-focus-width' type='Number' unit='px' value='2' step='1' min='0' max='16'></crowd-customiser></td>
</tr>
<tr>
<td>Button Color of the focus shadow</td>
<td><crowd-customiser property='--crowd-button-focus-color' type='Color' value='#aeaeae'></crowd-customiser></td>
</tr>
<tr>
<td>Button Border radius when a pill</td>
<td></td>
</tr>
<tr>
<td>Button Specify a width when the button is a circle</td>
<td><crowd-customiser property='--crowd-button-width' type='Number' unit='em' value='2.5' step='0.5' min='0' max='16'></crowd-customiser></td>
</tr>
<tr>
<td>Gap between label and checkbox</td>
<td><crowd-customiser property='--crowd-checkbox-spacing' type='Number' unit='em' min='0' max='3' step='0.5' value='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox border width</td>
<td><crowd-customiser property='--crowd-checkbox-border-width' type='Number' unit='px' min='0' max='5' step='1' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox border style</td>
<td><crowd-customiser property='--crowd-checkbox-border-style' type='Select' value='solid' options='["solid","dashed","dotted","none"]'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox border color</td>
<td><crowd-customiser property='--crowd-checkbox-border-color' type='Color' value='#000'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox Background Color</td>
<td><crowd-customiser property='--crowd-checkbox-background-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox Tick Color</td>
<td><crowd-customiser property='--crowd-checkbox-color' type='Color' value='#000'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox Background Color (when ticked)</td>
<td><crowd-customiser property='--crowd-checkbox-checked-background-color' type='Color' value='#000'></crowd-customiser></td>
</tr>
<tr>
<td>Checkbox Tick Color (when ticked)</td>
<td><crowd-customiser property='--crowd-checkbox-checked-color' type='Color' value='#fff'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog background color</td>
<td><crowd-customiser property='--crowd-dialog-background' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog Vertical padding</td>
<td><crowd-customiser property='--crowd-dialog-padding-vertical' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog Horizontal padding</td>
<td><crowd-customiser property='--crowd-dialog-padding-horizontal' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog Overlay background color</td>
<td><crowd-customiser property='--crowd-dialog-overlay-background' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog Vertical margin outside dialog</td>
<td><crowd-customiser property='--crowd-dialog-spacing-vertical' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog Horizontal margin outside dialog</td>
<td><crowd-customiser property='--crowd-dialog-spacing-horizontal' type='Number' min='0' max='5' unit='em' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog border radius</td>
<td><crowd-customiser property='--crowd-dialog-border-radius' type='Number' min='0' max='16' unit='px' value='0'></crowd-customiser></td>
</tr>
<tr>
<td>Dialog Box shadow</td>
<td><crowd-customiser property='--crowd-dialog-box-shadow' value='0 1px 1px hsl(0deg 0% 0% / 0.075), 0 2px 2px hsl(0deg 0% 0% / 0.075), 0 4px 4px hsl(0deg 0% 0% / 0.075), 0 8px 8px hsl(0deg 0% 0% / 0.075), 0 16px 16px hsl(0deg 0% 0% / 0.075)' type='Text'></td>
</tr>
<tr>
<td>Drawer background color</td>
<td><crowd-customiser property='--crowd-drawer-background' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Drawer top/bottom padding</td>
<td><crowd-customiser property='--crowd-drawer-padding-vertical' type='Number' unit='em' min='0' max='5' step='0.5' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Drawer left/right padding</td>
<td><crowd-customiser property='--crowd-drawer-padding-horizontal' type='Number' unit='em' min='0' max='5' step='0.5' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Drawer Overlay background</td>
<td><crowd-customiser property='--crowd-drawer-overlay-background' type='Color' value='rgba(0,0,0,0.3)'></crowd-customiser></td>
</tr>
<tr>
<td>Interior padding on Input left/right</td>
<td><crowd-customiser property='--crowd-input-padding-horizontal' type='Number' unit='em' value='1' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Font weight of the input</td>
<td><crowd-customiser property='--crowd-input-font-weight' type='Number' unit='' value='400' min='0' max='900' step='100'></crowd-customiser></td>
</tr>
<tr>
<td>Height of the input</td>
<td><crowd-customiser property='--crowd-input-height' type='Number' unit='em' value='2' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Input text color</td>
<td><crowd-customiser property='--crowd-input-color' value='#000000' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Input Label color</td>
<td><crowd-customiser property='--crowd-input-label-color' value='#000000' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Input placeholder color</td>
<td><crowd-customiser property='--crowd-input-placeholder-color' value='#000000' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Input background color</td>
<td><crowd-customiser property='--crowd-input-background' value='#ffffff' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Input Border width</td>
<td><crowd-customiser property='--crowd-input-border-width' type='Number' value='1' unit='px' min='0' max='5' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Input Border style</td>
<td><crowd-customiser property='--crowd-input-border-style' type='Select' value='solid' options='["solid", "dashed", "dotted", "none"]'></crowd-customiser></td>
</tr>
<tr>
<td>Input border color</td>
<td><crowd-customiser property='--crowd-input-border-color' value='#eeeeee' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Input Border radius</td>
<td><crowd-customiser property='--crowd-input-border-radius' type='Number' value='0' unit='px' min='0' max='16' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Font size of the input</td>
<td><crowd-customiser property='--crowd-input-font-size' type='Number' unit='rem' value='1' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Width of the Input focus ring</td>
<td><crowd-customiser property='--crowd-input-focus-width' type='Number' value='2' unit='px' min='0' max='5' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Input focus ring color</td>
<td><crowd-customiser property='--crowd-input-focus-color' value='rgba(0,0,0,0.3)' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Font size of the input</td>
<td><crowd-customiser property='--crowd-input-label-spacing' type='Number' unit='em' value='0.5' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Space above/below the input</td>
<td><crowd-customiser property='--crowd-input-wrapper-padding-vertical' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Space left/right of the input</td>
<td><crowd-customiser property='--crowd-input-wrapper-padding-horizontal' type='Number' unit='em' value='0' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Color of the Input password toggle icon</td>
<td><crowd-customiser property='--crowd-input-password-toggle-color' value='#000000' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Input Error message font size</td>
<td><crowd-customiser property='--crowd-input-error-message-font-size' type='Number' value='0.8' unit='em' min='0' max='3' step='0.1'></crowd-customiser></td>
</tr>
<tr>
<td>Color of the Input error message & outline</td>
<td><crowd-customiser property='--crowd-input-error-message-color' value='red' type='Color'></crowd-customiser></td>
</tr>
<tr>
<td>Menu Top/bottom padding</td>
<td><crowd-customiser property='--crowd-menu-padding-vertical' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Menu Left/right padding</td>
<td><crowd-customiser property='--crowd-menu-padding-horizontal' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Background color of the menu</td>
<td><crowd-customiser property='--crowd-menu-background-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Border width of the menu</td>
<td><crowd-customiser property='--crowd-menu-border-width' type='Number' value='0' unit='px' min='0' max='5' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Menu Border style</td>
<td><crowd-customiser property='--crowd-menu-border-style' type='Select' options='["solid","dashed","dotted","none"]' value='solid'></crowd-customiser></td>
</tr>
<tr>
<td>Background color of the menu</td>
<td><crowd-customiser property='--crowd-menu-border-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Menu Item Top/bottom padding</td>
<td><crowd-customiser property='--crowd-menu-item-padding-vertical' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Menu Item Left/right padding</td>
<td><crowd-customiser property='--crowd-menu-item-padding-horizontal' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Text color of the menu item</td>
<td><crowd-customiser property='--crowd-menu-item-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Background color of the menu item</td>
<td><crowd-customiser property='--crowd-menu-item-background-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Background color of the menu item on hover</td>
<td><crowd-customiser property='--crowd-menu-item-hover-background-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Text color of the menu item on hover</td>
<td><crowd-customiser property='--crowd-menu-item-hover-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Progress Ring track color</td>
<td><crowd-customiser property='--track-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Progress Ring indicator color</td>
<td><crowd-customiser property='--indicator-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Width of the range</td>
<td><crowd-customiser property='--crowd-range-width' type='Number' min='0' max='500' step='100' value='300' unit='px'></crowd-customiser></td>
</tr>

<tr>
<td>Gap between the range track and numbers</td>
<td><crowd-customiser property='--crowd-range-spacing' type='Number' min='0' max='3' unit='em' step='0.1' value='0.2'></crowd-customiser></td>
</tr>

<tr>
<td>Height of the range track</td>
<td><crowd-customiser property='--crowd-range-track-height' type='Number' value='0.5' unit='em' min='0' max='3' step='0.5'></crowd-customiser></td>
</tr>

<tr>
<td>Width of the range track border</td>
<td><crowd-customiser property='--crowd-range-track-border-width' type='Number' min='0' max='5' step='1' value='1' unit='px'></crowd-customiser></td>
</tr>

<tr>
<td>Style of the range track border</td>
<td><crowd-customiser property='--crowd-range-track-border-style' type='Select' value='solid' options='["solid","dashed","dotted","none"]'></crowd-customiser></td>
</tr>

<tr>
<td>Color of the range track border</td>
<td><crowd-customiser property='--crowd-range-track-border-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Border Radius of the range track</td>
<td><crowd-customiser property='--crowd-range-track-border-radius' type='Number' value='0' unit='px' min='0' max='5' step='1'></crowd-customiser></td>
</tr>

<tr>
<td>Background color of the range track</td>
<td><crowd-customiser property='--crowd-range-track-background-color' type='Color' value='#eeeeee'></crowd-customiser></td>
</tr>

<tr>
<td>Color of the range indicator</td>
<td><crowd-customiser property='--crowd-range-indicator-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Style of the range thumb border</td>
<td><crowd-customiser property='--crowd-range-thumb-border-style' type='Select' value='solid' options='["solid","dashed","dotted","none"]'></crowd-customiser></td>
</tr>

<tr>
<td>Color of the range thumb border</td>
<td><crowd-customiser property='--crowd-range-thumb-border-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Border Radius of the range thumb</td>
<td><crowd-customiser property='--crowd-range-thumb-border-radius' type='Number' value='50' unit='%' min='0' max='50' step='1'></crowd-customiser></td>
</tr>

<tr>
<td>Background color of the range thumb</td>
<td><crowd-customiser property='--crowd-range-thumb-background-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Height of the range notch</td>
<td><crowd-customiser property='--crowd-range-notch-height' type='Number' value='0.5' unit='em' min='0' max='3' step='0.5'></crowd-customiser></td>
</tr>

<tr>
<td>Width of the range notch</td>
<td><crowd-customiser property='--crowd-range-notch-width' type='Number' value='1' unit='px' min='0' max='3' step='1'></crowd-customiser></td>
</tr>

<tr>
<td>Color of the range notch</td>
<td><crowd-customiser property='--crowd-range-notch-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Color of the range numbers</td>
<td><crowd-customiser property='--crowd-range-number-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Max height of the Select dropdown tray</td>
<td><crowd-customiser type='Number' unit='vh' min='0' max='100' value='50' step='10' property='--crowd-select-dropdown-max-height'></crowd-customiser></td>
</tr>
<tr>
<td>Top/bottom padding of the dropdown tray</td>
<td><crowd-customiser property='--crowd-select-dropdown-padding-vertical' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Left/right padding of the dropdown tray</td>
<td><crowd-customiser property='--crowd-select-dropdown-padding-horizontal' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser></td>
</tr>
<tr>
<td>Background color of the dropdown tray</td>
<td><crowd-customiser type='Color' value='#ffffff' property='--crowd-select-dropdown-background-color'></crowd-customiser></td>
</tr>
<tr>
<td>Z-index of the dropdown tray</td>
<td><crowd-customiser property='--crowd-select-dropdown-z-index' value='999' min='0' max='1000' step='1' unit='' type='Number'></crowd-customiser></td>
</tr>
<tr>
<td>Box shadow of the dropdown tray</td>
<td><crowd-customiser property='--crowd-select-dropdown-box-shadow' value='0 2px 8px rgba(0, 0, 0, 0.1)' type='Text'></crowd-customiser></td>
</tr>
<tr>
<td>Color of the Select caret icon</td>
<td><crowd-customiser property='--crowd-select-icon-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
    <td>Select Option Text Color</td>
    <td><crowd-customiser type='Color' value='#000000' property='--crowd-option-color'></crowd-customiser></td>
</tr>
<tr>
    <td>Select Option Background Color</td>
    <td><crowd-customiser type='Color' value='#ffffff' property='--crowd-option-background-color'></crowd-customiser></td>
</tr>
<tr>
    <td>Select Option Hover Text Color</td>
    <td><crowd-customiser type='Color' value='#000000' property='--crowd-option-hover-color'></crowd-customiser></td>
</tr>
<tr>
    <td>Select Option Background Color</td>
    <td><crowd-customiser type='Color' value='rgba(0,0,0,0.1)' property='--crowd-option-hover-background-color'></crowd-customiser></td>
</tr>
<tr>
<td>Spinner: Duration of the spinning animation</td>
<td><crowd-customiser property='--crowd-spinner-animation-duration' type='Number' unit='s' min='0' max='3' value='1' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Width of the Spinner track</td>
<td><crowd-customiser property='--track-width' type='Number' unit='px' min='0' max='5' value='2' step='1'></crowd-customiser></td>
</tr>
<tr>
<td>Color of the Spinner track</td>
<td><crowd-customiser property='--track-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Color of the Spinner indicator</td>
<td><crowd-customiser property='--indicator-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Spacing between switch and the side labels</td>
<td><crowd-customiser property='--crowd-switch-spacing' type="Number" step='0.5' min='0' max='3' value='0.5' unit='em'></crowd-customiser></td>
</tr>

<tr>
<td>Spacing around the inside of the track</td>
<td><crowd-customiser property='--crowd-switch-track-spacing' type="Number" step='1' min='0' max='5' value='2' unit='px'></crowd-customiser></td>
</tr>

<tr>
<td>Border radius of the switch</td>
<td><crowd-customiser property='--crowd-switch-border-radius' type='Number' step='1' min='0' max='1000' value='999' unit='px'></crowd-customiser></td>
</tr>

<tr>
<td>Size of the switch thumb</td>
<td><crowd-customiser property='--crowd-switch-size' type='Number' step='0.5' unit='em' min='0' max='3' value='1'></crowd-customiser></td>
</tr>

<tr>
<td>Background color of the track</td>
<td><crowd-customiser property='--crowd-switch-track-background-color' type='Color' value='#eeeeee'></crowd-customiser></td>
</tr>

<tr>
<td>Background color of the thumb</td>
<td><crowd-customiser property='--crowd-switch-thumb-background-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>

<tr>
<td>Background color of the thumb when active</td>
<td><crowd-customiser property='--crowd-switch-thumb-background-color-active' type='Color' value='#000000'></crowd-customiser></td>
</tr>

<tr>
<td>Z-index of the tooltip popup</td>
<td><crowd-customiser property='--crowd-tooltip-z-index' type='Number' unit='' min='0' max='999' step='1' value='1'></crowd-customsier></td>
</tr>
<tr>
<td>Tooltip popup top/bottom padding</td>
<td><crowd-customiser property='--crowd-tooltip-padding-vertical' type='Number' min='0' max='5' step='0.5' value='0.5' unit='em'></crowd-customsier></td>
</tr>
<tr>
<td>Tooltip popup left/right padding</td>
<td><crowd-customiser property='--crowd-tooltip-padding-horizontal' type='Number' min='0' max='5' step='0.5' value='0.8' unit='em'></crowd-customsier></td>
</tr>
<tr>
<td>Background color of the tooltip</td>
<td><crowd-customiser property='--crowd-tooltip-background-color' type='Color' value='#ffffff'></crowd-customiser></td>
</tr>
<tr>
<td>Text color of the tooltip</td>
<td><crowd-customiser property='--crowd-tooltip-color' type='Color' value='#000000'></crowd-customiser></td>
</tr>
<tr>
<td>Border width of the tooltip popup</td>
<td><crowd-customiser property='--crowd-tooltip-border-width' type='Number' min='0' max='5' step='1' unit='px' value='1'></crowd-customiser></td>
</tr>
<tr>
<td>Style of the tooltip border</td>
<td><crowd-customiser property='--crowd-tooltip-border-style' type='Select' options='["solid","dashed","dotted","none"]' value='solid'></crowd-customer></td>
</tr>
<tr>
<td>Color of the tooltip border</td>
<td><crowd-customiser property='--crowd-tooltip-border-color' type='Color' value='#eeeeee'></crowd-customiser></td>
</tr>
<tr>
<td>Border radius of the tooltip popup</td>
<td><crowd-customiser property='--crowd-tooltip-border-radius' type='Number' min='0' max='5' step='1' unit='px' value='1'></crowd-customiser></td>
</tr>
</tbody>
</table>
</div>
</div>
<style>
    .cols {
        display: grid;
        grid-template-columns: repeat(auto-fit,minmax(350px,1fr));
        gap: 0.5em;
    }
</style>
<crowd-stylesheet></crowd-stylesheet>