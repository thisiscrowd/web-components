# Tooltip

A tooltip is a small box of text that appears when an element is hovered.

## Example

```
<crowd-tooltip content='Wassup?'>
    <p>Hover me!</p>
</crowd-tooltip>
```
<crowd-tooltip content='Wassup?'>
    <p style='text-align:center;'>Hover me!</p>
</crowd-tooltip>

### Show

Use the `show` attribute to toggle the tooltip manually.

```
<crowd-tooltip show content="I'm always here">
    <crowd-button>Toggle</crowd-button>
</crowd-tooltip>
```
<crowd-tooltip id='tooltipShowDemo' show content="I'm always here">
    <crowd-button id='tooltipShowToggle'>Toggle</crowd-button>
</crowd-tooltip>
<script>
    tooltipShowToggle.addEventListener('click', () => {
        if (tooltipShowDemo.show) {
            tooltipShowDemo.removeAttribute('show')
        } else {
            tooltipShowDemo.setAttribute('show',true)
        }
    }, false)
</script>

### Delay

Use the `delay` attribute to delay the tooltip showing. Use a number in milliseconds.

```
<crowd-tooltip content='Hello!' delay='1000'>
    <p>Wait a second...</p>
</crowd-tooltip>
```
<crowd-tooltip content='Hello!' delay='1000'>
    <p style='text-align:center;'>Wait a second...</p>
</crowd-tooltip>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-tooltip-z-index`|Z-index of the tooltip popup|<crowd-customiser property='--crowd-tooltip-z-index' type='Number' unit='' min='0' max='999' step='1' value='1'></crowd-customsier>|
|`--crowd-tooltip-padding-vertical`|Tooltip popup top/bottom padding|<crowd-customiser property='--crowd-tooltip-padding-vertical' type='Number' min='0' max='5' step='0.5' value='0.5' unit='em'></crowd-customsier>|
|`--crowd-tooltip-padding-horizontal`|Tooltip popup left/right padding|<crowd-customiser property='--crowd-tooltip-padding-horizontal' type='Number' min='0' max='5' step='0.5' value='0.8' unit='em'></crowd-customsier>|
|`--crowd-tooltip-background-color`|Background color of the tooltip|<crowd-customiser property='--crowd-tooltip-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-tooltip-color`|Text color of the tooltip|<crowd-customiser property='--crowd-tooltip-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-tooltip-border-width`|Border width of the tooltip popup|<crowd-customiser property='--crowd-tooltip-border-width' type='Number' min='0' max='5' step='1' unit='px' value='1'></crowd-customiser>|
|`--crowd-tooltip-border-style`|Style of the tooltip border|<crowd-customiser property='--crowd-tooltip-border-style' type='Select' options='["solid","dashed","dotted","none"]' value='solid'></crowd-customer>|
|`--crowd-tooltip-border-color`|Color of the tooltip border|<crowd-customiser property='--crowd-tooltip-border-color' type='Color' value='#eeeeee'></crowd-customiser>|
|`--crowd-tooltip-border-radius`|Border radius of the tooltip popup|<crowd-customiser property='--crowd-tooltip-border-radius' type='Number' min='0' max='5' step='1' unit='px' value='1'></crowd-customiser>|