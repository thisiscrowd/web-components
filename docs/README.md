# Crowd Web Components

This is a collection of reusable web components for Crowd projects.

## Installation

The repo is located at [https://bitbucket.org/thisiscrowd/web-components/src/master/](https://bitbucket.org/thisiscrowd/web-components/src/master/). To install, either download/copy from there, or use npm to install:

```
npm install git+ssh://git@bitbucket.org:thisiscrowd/web-components.git
```

## Usage - Blanket Use

Include `dist/js/index.js` in your project to add all components. Add them in your markup like in the demos.

## Usage - Cherry Picking

Include the individual component files as standalones in `dist/js` that you need. You will need to include the element registry code yourself.
Note that some components rely on others, so you may need to include those components too.

**Example**: 

If you used the just input element:
```
<script src='/path/dist/js/input.js'></script>
```

Then in your script somewhere:
```
customElements.define('crowd-input',Input);
```

## Usage - Customisation

All elements can be customised to an extend using CSS Custom Properties. Use the inspector to see what can change.

**Example**:

```
<crowd-input label='Input' placeholder='Type something'></crowd-input>
```
<crowd-input label='Input' placeholder='Type something'></crowd-input>

To change the border:
```
crowd-input {
    --crowd-input-border-width: 2px;
    --crowd-input-border-color: green;
}
```
<style>

    crowd-input.demo {
    --crowd-input-border-width: 2px;
    --crowd-input-border-color: green;
}
</style>
<crowd-input class='demo' label='Input' placeholder='Type something'></crowd-input>