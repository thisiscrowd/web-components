/*! For license information please see index.js.LICENSE.txt */
(()=>{var t={123:(t,e,A)=>{"use strict";A.r(e);const r=!(window.ShadyDOM&&window.ShadyDOM.inUse);let i,n;function o(t){i=(!t||!t.shimcssproperties)&&(r||Boolean(!navigator.userAgent.match(/AppleWebKit\/601|Edge\/15/)&&window.CSS&&CSS.supports&&CSS.supports("box-shadow","0 0 0 var(--foo)")))}window.ShadyCSS&&void 0!==window.ShadyCSS.cssBuild&&(n=window.ShadyCSS.cssBuild);const s=Boolean(window.ShadyCSS&&window.ShadyCSS.disableRuntime);window.ShadyCSS&&void 0!==window.ShadyCSS.nativeCss?i=window.ShadyCSS.nativeCss:window.ShadyCSS?(o(window.ShadyCSS),window.ShadyCSS=void 0):o(window.WebComponents&&window.WebComponents.flags);const a=i;class l{constructor(){this.start=0,this.end=0,this.previous=null,this.parent=null,this.rules=null,this.parsedCssText="",this.cssText="",this.atRule=!1,this.type=0,this.keyframesName="",this.selector="",this.parsedSelector=""}}function c(t){return d(function(t){let e=new l;e.start=0,e.end=t.length;let A=e;for(let r=0,i=t.length;r<i;r++)if(t[r]===p){A.rules||(A.rules=[]);let t=A,e=t.rules[t.rules.length-1]||null;A=new l,A.start=r+1,A.parent=t,A.previous=e,t.rules.push(A)}else t[r]===f&&(A.end=r+1,A=A.parent||e);return e}(t=t.replace(m.comments,"").replace(m.port,"")),t)}function d(t,e){let A=e.substring(t.start,t.end-1);if(t.parsedCssText=t.cssText=A.trim(),t.parent){let r=t.previous?t.previous.end:t.parent.start;A=e.substring(r,t.start-1),A=function(t){return t.replace(/\\([0-9a-f]{1,6})\s/gi,(function(){let t=arguments[1],e=6-t.length;for(;e--;)t="0"+t;return"\\"+t}))}(A),A=A.replace(m.multipleSpaces," "),A=A.substring(A.lastIndexOf(";")+1);let i=t.parsedSelector=t.selector=A.trim();t.atRule=0===i.indexOf(y),t.atRule?0===i.indexOf(v)?t.type=h.MEDIA_RULE:i.match(m.keyframesRule)&&(t.type=h.KEYFRAMES_RULE,t.keyframesName=t.selector.split(m.multipleSpaces).pop()):0===i.indexOf(g)?t.type=h.MIXIN_RULE:t.type=h.STYLE_RULE}let r=t.rules;if(r)for(let t,A=0,i=r.length;A<i&&(t=r[A]);A++)d(t,e);return t}function u(t,e,A=""){let r="";if(t.cssText||t.rules){let A=t.rules;if(A&&!function(t){let e=t[0];return Boolean(e)&&Boolean(e.selector)&&0===e.selector.indexOf(g)}(A))for(let t,i=0,n=A.length;i<n&&(t=A[i]);i++)r=u(t,e,r);else r=e?t.cssText:function(t){return function(t){return t.replace(m.mixinApply,"").replace(m.varApply,"")}(t=function(t){return t.replace(m.customProp,"").replace(m.mixinProp,"")}(t))}(t.cssText),r=r.trim(),r&&(r="  "+r+"\n")}return r&&(t.selector&&(A+=t.selector+" "+p+"\n"),A+=r,t.selector&&(A+=f+"\n\n")),A}const h={STYLE_RULE:1,KEYFRAMES_RULE:7,MEDIA_RULE:4,MIXIN_RULE:1e3},p="{",f="}",m={comments:/\/\*[^*]*\*+([^/*][^*]*\*+)*\//gim,port:/@import[^;]*;/gim,customProp:/(?:^[^;\-\s}]+)?--[^;{}]*?:[^{};]*?(?:[;\n]|$)/gim,mixinProp:/(?:^[^;\-\s}]+)?--[^;{}]*?:[^{};]*?{[^}]*?}(?:[;\n]|$)?/gim,mixinApply:/@apply\s*\(?[^);]*\)?\s*(?:[;\n]|$)?/gim,varApply:/[^;:]*?:[^;]*?var\([^;]*\)(?:[;\n]|$)?/gim,keyframesRule:/^@[^\s]*keyframes/,multipleSpaces:/\s+/g},g="--",v="@media",y="@",_=/(?:^|[;\s{]\s*)(--[\w-]*?)\s*:\s*(?:((?:'(?:\\'|.)*?'|"(?:\\"|.)*?"|\([^)]*?\)|[^};{])+)|\{([^}]*)\}(?:(?=[;\s}])|$))/gi,b=/(?:^|\W+)@apply\s*\(?([^);\n]*)\)?/gi,w=/@media\s(.*)/,k=new Set;function x(t){const e=t.textContent;if(!k.has(e)){k.add(e);const t=document.createElement("style");t.setAttribute("shady-unscoped",""),t.textContent=e,document.head.appendChild(t)}}function S(t){return t.hasAttribute("shady-unscoped")}function C(t,e){return t?("string"==typeof t&&(t=c(t)),e&&P(t,e),u(t,a)):""}function E(t){return!t.__cssRules&&t.textContent&&(t.__cssRules=c(t.textContent)),t.__cssRules||null}function P(t,e,A,r){if(!t)return;let i=!1,n=t.type;if(r&&n===h.MEDIA_RULE){let e=t.selector.match(w);e&&(window.matchMedia(e[1]).matches||(i=!0))}n===h.STYLE_RULE?e(t):A&&n===h.KEYFRAMES_RULE?A(t):n===h.MIXIN_RULE&&(i=!0);let o=t.rules;if(o&&!i)for(let t,i=0,n=o.length;i<n&&(t=o[i]);i++)P(t,e,A,r)}function T(t,e){let A=t.indexOf("var(");if(-1===A)return e(t,"","","");let r=function(t,e){let A=0;for(let r=e,i=t.length;r<i;r++)if("("===t[r])A++;else if(")"===t[r]&&0==--A)return r;return-1}(t,A+3),i=t.substring(A+4,r),n=t.substring(0,A),o=T(t.substring(r+1),e),s=i.indexOf(",");return-1===s?e(n,i.trim(),"",o):e(n,i.substring(0,s).trim(),i.substring(s+1).trim(),o)}window.ShadyDOM&&window.ShadyDOM.wrap;const O="css-build";function N(t){return""!==function(t){if(void 0!==n)return n;if(void 0===t.__cssBuild){const e=t.getAttribute(O);if(e)t.__cssBuild=e;else{const e=function(t){const e="template"===t.localName?t.content.firstChild:t.firstChild;if(e instanceof Comment){const t=e.textContent.trim().split(":");if(t[0]===O)return t[1]}return""}(t);""!==e&&function(t){const e="template"===t.localName?t.content.firstChild:t.firstChild;e.parentNode.removeChild(e)}(t),t.__cssBuild=e}}return t.__cssBuild||""}(t)}function $(t,e){for(let A in e)null===A?t.style.removeProperty(A):t.style.setProperty(A,e[A])}function I(t,e){const A=window.getComputedStyle(t).getPropertyValue(e);return A?A.trim():""}const M=/;\s*/m,D=/^\s*(initial)|(inherit)\s*$/,L=/\s*!important/;class B{constructor(){this._map={}}set(t,e){t=t.trim(),this._map[t]={properties:e,dependants:{}}}get(t){return t=t.trim(),this._map[t]||null}}let z=null;class R{constructor(){this._currentElement=null,this._measureElement=null,this._map=new B}detectMixin(t){return function(t){const e=b.test(t)||_.test(t);return b.lastIndex=0,_.lastIndex=0,e}(t)}gatherStyles(t){const e=function(t){const e=[],A=t.querySelectorAll("style");for(let t=0;t<A.length;t++){const i=A[t];S(i)?r||(x(i),i.parentNode.removeChild(i)):(e.push(i.textContent),i.parentNode.removeChild(i))}return e.join("").trim()}(t.content);if(e){const A=document.createElement("style");return A.textContent=e,t.content.insertBefore(A,t.content.firstChild),A}return null}transformTemplate(t,e){void 0===t._gatheredStyle&&(t._gatheredStyle=this.gatherStyles(t));const A=t._gatheredStyle;return A?this.transformStyle(A,e):null}transformStyle(t,e=""){let A=E(t);return this.transformRules(A,e),t.textContent=C(A),A}transformCustomStyle(t){let e=E(t);return P(e,(t=>{":root"===t.selector&&(t.selector="html"),this.transformRule(t)})),t.textContent=C(e),e}transformRules(t,e){this._currentElement=e,P(t,(t=>{this.transformRule(t)})),this._currentElement=null}transformRule(t){t.cssText=this.transformCssText(t.parsedCssText,t),":root"===t.selector&&(t.selector=":host > *")}transformCssText(t,e){return t=t.replace(_,((t,A,r,i)=>this._produceCssProperties(t,A,r,i,e))),this._consumeCssProperties(t,e)}_getInitialValueForProperty(t){return this._measureElement||(this._measureElement=document.createElement("meta"),this._measureElement.setAttribute("apply-shim-measure",""),this._measureElement.style.all="initial",document.head.appendChild(this._measureElement)),window.getComputedStyle(this._measureElement).getPropertyValue(t)}_fallbacksFromPreviousRules(t){let e=t;for(;e.parent;)e=e.parent;const A={};let r=!1;return P(e,(e=>{r=r||e===t,r||e.selector===t.selector&&Object.assign(A,this._cssTextToMap(e.parsedCssText))})),A}_consumeCssProperties(t,e){let A=null;for(;A=b.exec(t);){let r=A[0],i=A[1],n=A.index,o=n+r.indexOf("@apply"),s=n+r.length,a=t.slice(0,o),l=t.slice(s),c=e?this._fallbacksFromPreviousRules(e):{};Object.assign(c,this._cssTextToMap(a));let d=this._atApplyToCssProperties(i,c);t=`${a}${d}${l}`,b.lastIndex=n+d.length}return t}_atApplyToCssProperties(t,e){t=t.replace(M,"");let A=[],r=this._map.get(t);if(r||(this._map.set(t,{}),r=this._map.get(t)),r){let i,n,o;this._currentElement&&(r.dependants[this._currentElement]=!0);const s=r.properties;for(i in s)o=e&&e[i],n=[i,": var(",t,"_-_",i],o&&n.push(",",o.replace(L,"")),n.push(")"),L.test(s[i])&&n.push(" !important"),A.push(n.join(""))}return A.join("; ")}_replaceInitialOrInherit(t,e){let A=D.exec(e);return A&&(e=A[1]?this._getInitialValueForProperty(t):"apply-shim-inherit"),e}_cssTextToMap(t,e=!1){let A,r,i=t.split(";"),n={};for(let t,o,s=0;s<i.length;s++)t=i[s],t&&(o=t.split(":"),o.length>1&&(A=o[0].trim(),r=o.slice(1).join(":"),e&&(r=this._replaceInitialOrInherit(A,r)),n[A]=r));return n}_invalidateMixinEntry(t){if(z)for(let e in t.dependants)e!==this._currentElement&&z(e)}_produceCssProperties(t,e,A,r,i){if(A&&T(A,((t,e)=>{e&&this._map.get(e)&&(r=`@apply ${e};`)})),!r)return t;let n=this._consumeCssProperties(""+r,i),o=t.slice(0,t.indexOf("--")),s=this._cssTextToMap(n,!0),a=s,l=this._map.get(e),c=l&&l.properties;c?a=Object.assign(Object.create(c),s):this._map.set(e,a);let d,u,h=[],p=!1;for(d in a)u=s[d],void 0===u&&(u="initial"),c&&!(d in c)&&(p=!0),h.push(`${e}_-_${d}: ${u}`);return p&&this._invalidateMixinEntry(l),l&&(l.properties=a),A&&(o=`${t};${o}`),`${o}${h.join("; ")};`}}R.prototype.detectMixin=R.prototype.detectMixin,R.prototype.transformStyle=R.prototype.transformStyle,R.prototype.transformCustomStyle=R.prototype.transformCustomStyle,R.prototype.transformRules=R.prototype.transformRules,R.prototype.transformRule=R.prototype.transformRule,R.prototype.transformTemplate=R.prototype.transformTemplate,R.prototype._separator="_-_",Object.defineProperty(R.prototype,"invalidCallback",{get:()=>z,set(t){z=t}});const F=R,H={},U="_applyShimCurrentVersion",V="_applyShimNextVersion",q="_applyShimValidatingVersion",Y=Promise.resolve();function j(t){let e=H[t];e&&function(t){t[U]=t[U]||0,t[q]=t[q]||0,t[V]=(t[V]||0)+1}(e)}function Q(t){return t[U]===t[V]}let Z,G=null,W=window.HTMLImports&&window.HTMLImports.whenReady||null;function J(t){requestAnimationFrame((function(){W?W(t):(G||(G=new Promise((t=>{Z=t})),"complete"===document.readyState?Z():document.addEventListener("readystatechange",(()=>{"complete"===document.readyState&&Z()}))),G.then((function(){t&&t()})))}))}const K="__seenByShadyCSS",X="__shadyCSSCachedStyle";let tt=null,et=null;class At{constructor(){this.customStyles=[],this.enqueued=!1,J((()=>{window.ShadyCSS.flushCustomStyles&&window.ShadyCSS.flushCustomStyles()}))}enqueueDocumentValidation(){!this.enqueued&&et&&(this.enqueued=!0,J(et))}addCustomStyle(t){t[K]||(t[K]=!0,this.customStyles.push(t),this.enqueueDocumentValidation())}getStyleForCustomStyle(t){if(t[X])return t[X];let e;return e=t.getStyle?t.getStyle():t,e}processStyles(){const t=this.customStyles;for(let e=0;e<t.length;e++){const A=t[e];if(A[X])continue;const r=this.getStyleForCustomStyle(A);if(r){const t=r.__appliedElement||r;tt&&tt(t),A[X]=t}}return t}}At.prototype.addCustomStyle=At.prototype.addCustomStyle,At.prototype.getStyleForCustomStyle=At.prototype.getStyleForCustomStyle,At.prototype.processStyles=At.prototype.processStyles,Object.defineProperties(At.prototype,{transformCallback:{get:()=>tt,set(t){tt=t}},validateCallback:{get:()=>et,set(t){let e=!1;et||(e=!0),et=t,e&&this.enqueueDocumentValidation()}}});const rt=new F;class it{constructor(){this.customStyleInterface=null,rt.invalidCallback=j}ensure(){this.customStyleInterface||window.ShadyCSS.CustomStyleInterface&&(this.customStyleInterface=window.ShadyCSS.CustomStyleInterface,this.customStyleInterface.transformCallback=t=>{rt.transformCustomStyle(t)},this.customStyleInterface.validateCallback=()=>{requestAnimationFrame((()=>{this.customStyleInterface.enqueued&&this.flushCustomStyles()}))})}prepareTemplate(t,e){if(this.ensure(),N(t))return;H[e]=t;let A=rt.transformTemplate(t,e);t._styleAst=A}flushCustomStyles(){if(this.ensure(),!this.customStyleInterface)return;let t=this.customStyleInterface.processStyles();if(this.customStyleInterface.enqueued){for(let e=0;e<t.length;e++){let A=t[e],r=this.customStyleInterface.getStyleForCustomStyle(A);r&&rt.transformCustomStyle(r)}this.customStyleInterface.enqueued=!1}}styleSubtree(t,e){if(this.ensure(),e&&$(t,e),t.shadowRoot){this.styleElement(t);let e=t.shadowRoot.children||t.shadowRoot.childNodes;for(let t=0;t<e.length;t++)this.styleSubtree(e[t])}else{let e=t.children||t.childNodes;for(let t=0;t<e.length;t++)this.styleSubtree(e[t])}}styleElement(t){this.ensure();let{is:e}=function(t){let e=t.localName,A="",r="";return e?e.indexOf("-")>-1?A=e:(r=e,A=t.getAttribute&&t.getAttribute("is")||""):(A=t.is,r=t.extends),{is:A,typeExtension:r}}(t),A=H[e];if((!A||!N(A))&&A&&!Q(A)){(function(t){return!Q(t)&&t[q]===t[V]})(A)||(this.prepareTemplate(A,e),function(t){t[q]=t[V],t._validating||(t._validating=!0,Y.then((function(){t[U]=t[V],t._validating=!1})))}(A));let r=t.shadowRoot;if(r){let t=r.querySelector("style");t&&(t.__cssRules=A._styleAst,t.textContent=C(A._styleAst))}}}styleDocument(t){this.ensure(),this.styleSubtree(document.body,t)}}if(!window.ShadyCSS||!window.ShadyCSS.ScopingShim){const t=new it;let e=window.ShadyCSS&&window.ShadyCSS.CustomStyleInterface;window.ShadyCSS={prepareTemplate(e,A,r){t.flushCustomStyles(),t.prepareTemplate(e,A)},prepareTemplateStyles(t,e,A){window.ShadyCSS.prepareTemplate(t,e,A)},prepareTemplateDom(t,e){},styleSubtree(e,A){t.flushCustomStyles(),t.styleSubtree(e,A)},styleElement(e){t.flushCustomStyles(),t.styleElement(e)},styleDocument(e){t.flushCustomStyles(),t.styleDocument(e)},getComputedStyleValue:(t,e)=>I(t,e),flushCustomStyles(){t.flushCustomStyles()},nativeCss:a,nativeShadow:r,cssBuild:n,disableRuntime:s},e&&(window.ShadyCSS.CustomStyleInterface=e)}window.ShadyCSS.ApplyShim=rt,window.JSCompiler_renameProperty=function(t,e){return t};let nt,ot,st=/(url\()([^)]*)(\))/g,at=/(^\/[^\/])|(^#)|(^[\w-\d]*:)/;function lt(t,e){if(t&&at.test(t))return t;if("//"===t)return t;if(void 0===nt){nt=!1;try{const t=new URL("b","http://a");t.pathname="c%20d",nt="http://a/c%20d"===t.href}catch(t){}}if(e||(e=document.baseURI||window.location.href),nt)try{return new URL(t,e).href}catch(e){return t}return ot||(ot=document.implementation.createHTMLDocument("temp"),ot.base=ot.createElement("base"),ot.head.appendChild(ot.base),ot.anchor=ot.createElement("a"),ot.body.appendChild(ot.anchor)),ot.base.href=e,ot.anchor.href=t,ot.anchor.href||t}function ct(t,e){return t.replace(st,(function(t,A,r,i){return A+"'"+lt(r.replace(/["']/g,""),e)+"'"+i}))}function dt(t){return t.substring(0,t.lastIndexOf("/")+1)}const ut=!window.ShadyDOM||!window.ShadyDOM.inUse,ht=(Boolean(!window.ShadyCSS||window.ShadyCSS.nativeCss),window.customElements.polyfillWrapFlushCallback,ut&&"adoptedStyleSheets"in Document.prototype&&"replaceSync"in CSSStyleSheet.prototype&&(()=>{try{const t=new CSSStyleSheet;t.replaceSync("");const e=document.createElement("div");return e.attachShadow({mode:"open"}),e.shadowRoot.adoptedStyleSheets=[t],e.shadowRoot.adoptedStyleSheets[0]===t}catch(t){return!1}})());let pt=window.Polymer&&window.Polymer.rootPath||dt(document.baseURI||window.location.href),ft=window.Polymer&&window.Polymer.sanitizeDOMValue||void 0,mt=window.Polymer&&window.Polymer.setPassiveTouchGestures||!1,gt=window.Polymer&&window.Polymer.strictTemplatePolicy||!1,vt=window.Polymer&&window.Polymer.allowTemplateFromDomModule||!1,yt=window.Polymer&&window.Polymer.legacyOptimizations||!1,_t=window.Polymer&&window.Polymer.legacyWarnings||!1,bt=window.Polymer&&window.Polymer.syncInitialRender||!1,wt=window.Polymer&&window.Polymer.legacyUndefined||!1,kt=window.Polymer&&window.Polymer.orderedComputed||!1,xt=window.Polymer&&window.Polymer.removeNestedTemplates||!1,St=window.Polymer&&window.Polymer.fastDomIf||!1,Ct=window.Polymer&&window.Polymer.suppressTemplateNotifications||!1,Et=window.Polymer&&window.Polymer.legacyNoObservedAttributes||!1,Pt=window.Polymer&&window.Polymer.useAdoptedStyleSheetsWithBuiltCSS||!1,Tt=0;function Ot(){}Ot.prototype.__mixinApplications,Ot.prototype.__mixinSet;const Nt=function(t){let e=t.__mixinApplications;e||(e=new WeakMap,t.__mixinApplications=e);let A=Tt++;return function(r){let i=r.__mixinSet;if(i&&i[A])return r;let n=e,o=n.get(r);if(!o){o=t(r),n.set(r,o);let e=Object.create(o.__mixinSet||i||null);e[A]=!0,o.__mixinSet=e}return o}};let $t={},It={};function Mt(t,e){$t[t]=It[t.toLowerCase()]=e}function Dt(t){return $t[t]||It[t.toLowerCase()]}class Lt extends HTMLElement{static get observedAttributes(){return["id"]}static import(t,e){if(t){let A=Dt(t);return A&&e?A.querySelector(e):A}return null}attributeChangedCallback(t,e,A,r){e!==A&&this.register()}get assetpath(){if(!this.__assetpath){const t=window.HTMLImports&&HTMLImports.importForElement?HTMLImports.importForElement(this)||document:this.ownerDocument,e=lt(this.getAttribute("assetpath")||"",t.baseURI);this.__assetpath=dt(e)}return this.__assetpath}register(t){if(t=t||this.id){if(gt&&void 0!==Dt(t))throw Mt(t,null),new Error(`strictTemplatePolicy: dom-module ${t} re-registered`);this.id=t,Mt(t,this),(e=this).querySelector("style")&&console.warn("dom-module %s has style outside template",e.id)}var e}}Lt.prototype.modules=$t,customElements.define("dom-module",Lt);const Bt="shady-unscoped";function zt(t){return Lt.import(t)}function Rt(t){const e=ct((t.body?t.body:t).textContent,t.baseURI),A=document.createElement("style");return A.textContent=e,A}function Ft(t){const e=t.trim().split(/\s+/),A=[];for(let t=0;t<e.length;t++)A.push(...Ht(e[t]));return A}function Ht(t){const e=zt(t);if(!e)return console.warn("Could not find style data in module named",t),[];if(void 0===e._styles){const t=[];t.push(...Vt(e));const A=e.querySelector("template");A&&t.push(...Ut(A,e.assetpath)),e._styles=t}return e._styles}function Ut(t,e){if(!t._styles){const A=[],r=t.content.querySelectorAll("style");for(let t=0;t<r.length;t++){let i=r[t],n=i.getAttribute("include");n&&A.push(...Ft(n).filter((function(t,e,A){return A.indexOf(t)===e}))),e&&(i.textContent=ct(i.textContent,e)),A.push(i)}t._styles=A}return t._styles}function Vt(t){const e=[],A=t.querySelectorAll("link[rel=import][type~=css]");for(let t=0;t<A.length;t++){let r=A[t];if(r.import){const t=r.import,A=r.hasAttribute(Bt);if(A&&!t._unscopedStyle){const e=Rt(t);e.setAttribute(Bt,""),t._unscopedStyle=e}else t._style||(t._style=Rt(t));e.push(A?t._unscopedStyle:t._style)}}return e}function qt(t){let e=zt(t);if(e&&void 0===e._cssText){let t=function(t){let e="",A=Vt(t);for(let t=0;t<A.length;t++)e+=A[t].textContent;return e}(e),A=e.querySelector("template");A&&(t+=function(t,e){let A="";const r=Ut(t,e);for(let t=0;t<r.length;t++){let e=r[t];e.parentNode&&e.parentNode.removeChild(e),A+=e.textContent}return A}(A,e.assetpath)),e._cssText=t||null}return e||console.warn("Could not find style data in module named",t),e&&e._cssText||""}const Yt=window.ShadyDOM&&window.ShadyDOM.noPatch&&window.ShadyDOM.wrap?window.ShadyDOM.wrap:window.ShadyDOM?t=>ShadyDOM.patch(t):t=>t;function jt(t){return t.indexOf(".")>=0}function Qt(t){let e=t.indexOf(".");return-1===e?t:t.slice(0,e)}function Zt(t,e){return 0===t.indexOf(e+".")}function Gt(t,e){return 0===e.indexOf(t+".")}function Wt(t,e,A){return e+A.slice(t.length)}function Jt(t){if(Array.isArray(t)){let e=[];for(let A=0;A<t.length;A++){let r=t[A].toString().split(".");for(let t=0;t<r.length;t++)e.push(r[t])}return e.join(".")}return t}function Kt(t){return Array.isArray(t)?Jt(t).split("."):t.toString().split(".")}function Xt(t,e,A){let r=t,i=Kt(e);for(let t=0;t<i.length;t++){if(!r)return;r=r[i[t]]}return A&&(A.path=i.join(".")),r}function te(t,e,A){let r=t,i=Kt(e),n=i[i.length-1];if(i.length>1){for(let t=0;t<i.length-1;t++)if(r=r[i[t]],!r)return;r[n]=A}else r[e]=A;return i.join(".")}const ee={},Ae=/-[a-z]/g,re=/([A-Z])/g;function ie(t){return ee[t]||(ee[t]=t.indexOf("-")<0?t:t.replace(Ae,(t=>t[1].toUpperCase())))}function ne(t){return ee[t]||(ee[t]=t.replace(re,"-$1").toLowerCase())}let oe=0,se=0,ae=[],le=0,ce=!1,de=document.createTextNode("");new window.MutationObserver((function(){ce=!1;const t=ae.length;for(let e=0;e<t;e++){let t=ae[e];if(t)try{t()}catch(t){setTimeout((()=>{throw t}))}}ae.splice(0,t),se+=t})).observe(de,{characterData:!0});const ue={after:t=>({run:e=>window.setTimeout(e,t),cancel(t){window.clearTimeout(t)}}),run:(t,e)=>window.setTimeout(t,e),cancel(t){window.clearTimeout(t)}},he={run:t=>(ce||(ce=!0,de.textContent=le++),ae.push(t),oe++),cancel(t){const e=t-se;if(e>=0){if(!ae[e])throw new Error("invalid async handle: "+t);ae[e]=null}}},pe=he,fe=Nt((t=>class extends t{static createProperties(t){const e=this.prototype;for(let A in t)A in e||e._createPropertyAccessor(A)}static attributeNameForProperty(t){return t.toLowerCase()}static typeForProperty(t){}_createPropertyAccessor(t,e){this._addPropertyToAttributeMap(t),this.hasOwnProperty(JSCompiler_renameProperty("__dataHasAccessor",this))||(this.__dataHasAccessor=Object.assign({},this.__dataHasAccessor)),this.__dataHasAccessor[t]||(this.__dataHasAccessor[t]=!0,this._definePropertyAccessor(t,e))}_addPropertyToAttributeMap(t){this.hasOwnProperty(JSCompiler_renameProperty("__dataAttributes",this))||(this.__dataAttributes=Object.assign({},this.__dataAttributes));let e=this.__dataAttributes[t];return e||(e=this.constructor.attributeNameForProperty(t),this.__dataAttributes[e]=t),e}_definePropertyAccessor(t,e){Object.defineProperty(this,t,{get(){return this.__data[t]},set:e?function(){}:function(e){this._setPendingProperty(t,e,!0)&&this._invalidateProperties()}})}constructor(){super(),this.__dataEnabled=!1,this.__dataReady=!1,this.__dataInvalid=!1,this.__data={},this.__dataPending=null,this.__dataOld=null,this.__dataInstanceProps=null,this.__dataCounter=0,this.__serializing=!1,this._initializeProperties()}ready(){this.__dataReady=!0,this._flushProperties()}_initializeProperties(){for(let t in this.__dataHasAccessor)this.hasOwnProperty(t)&&(this.__dataInstanceProps=this.__dataInstanceProps||{},this.__dataInstanceProps[t]=this[t],delete this[t])}_initializeInstanceProperties(t){Object.assign(this,t)}_setProperty(t,e){this._setPendingProperty(t,e)&&this._invalidateProperties()}_getProperty(t){return this.__data[t]}_setPendingProperty(t,e,A){let r=this.__data[t],i=this._shouldPropertyChange(t,e,r);return i&&(this.__dataPending||(this.__dataPending={},this.__dataOld={}),this.__dataOld&&!(t in this.__dataOld)&&(this.__dataOld[t]=r),this.__data[t]=e,this.__dataPending[t]=e),i}_isPropertyPending(t){return!(!this.__dataPending||!this.__dataPending.hasOwnProperty(t))}_invalidateProperties(){!this.__dataInvalid&&this.__dataReady&&(this.__dataInvalid=!0,pe.run((()=>{this.__dataInvalid&&(this.__dataInvalid=!1,this._flushProperties())})))}_enableProperties(){this.__dataEnabled||(this.__dataEnabled=!0,this.__dataInstanceProps&&(this._initializeInstanceProperties(this.__dataInstanceProps),this.__dataInstanceProps=null),this.ready())}_flushProperties(){this.__dataCounter++;const t=this.__data,e=this.__dataPending,A=this.__dataOld;this._shouldPropertiesChange(t,e,A)&&(this.__dataPending=null,this.__dataOld=null,this._propertiesChanged(t,e,A)),this.__dataCounter--}_shouldPropertiesChange(t,e,A){return Boolean(e)}_propertiesChanged(t,e,A){}_shouldPropertyChange(t,e,A){return A!==e&&(A==A||e==e)}attributeChangedCallback(t,e,A,r){e!==A&&this._attributeToProperty(t,A),super.attributeChangedCallback&&super.attributeChangedCallback(t,e,A,r)}_attributeToProperty(t,e,A){if(!this.__serializing){const r=this.__dataAttributes,i=r&&r[t]||t;this[i]=this._deserializeValue(e,A||this.constructor.typeForProperty(i))}}_propertyToAttribute(t,e,A){this.__serializing=!0,A=arguments.length<3?this[t]:A,this._valueToNodeAttribute(this,A,e||this.constructor.attributeNameForProperty(t)),this.__serializing=!1}_valueToNodeAttribute(t,e,A){const r=this._serializeValue(e);"class"!==A&&"name"!==A&&"slot"!==A||(t=Yt(t)),void 0===r?t.removeAttribute(A):t.setAttribute(A,r)}_serializeValue(t){return"boolean"==typeof t?t?"":void 0:null!=t?t.toString():void 0}_deserializeValue(t,e){switch(e){case Boolean:return null!==t;case Number:return Number(t);default:return t}}})),me={};let ge=HTMLElement.prototype;for(;ge;){let t=Object.getOwnPropertyNames(ge);for(let e=0;e<t.length;e++)me[t[e]]=!0;ge=Object.getPrototypeOf(ge)}const ve=Nt((t=>{const e=fe(t);return class extends e{static createPropertiesForAttributes(){let t=this.observedAttributes;for(let e=0;e<t.length;e++)this.prototype._createPropertyAccessor(ie(t[e]))}static attributeNameForProperty(t){return ne(t)}_initializeProperties(){this.__dataProto&&(this._initializeProtoProperties(this.__dataProto),this.__dataProto=null),super._initializeProperties()}_initializeProtoProperties(t){for(let e in t)this._setProperty(e,t[e])}_ensureAttribute(t,e){this.hasAttribute(t)||this._valueToNodeAttribute(this,e,t)}_serializeValue(t){if("object"==typeof t){if(t instanceof Date)return t.toString();if(t)try{return JSON.stringify(t)}catch(t){return""}}return super._serializeValue(t)}_deserializeValue(t,e){let A;switch(e){case Object:try{A=JSON.parse(t)}catch(e){A=t}break;case Array:try{A=JSON.parse(t)}catch(e){A=null,console.warn(`Polymer::Attributes: couldn't decode Array as JSON: ${t}`)}break;case Date:A=isNaN(t)?String(t):Number(t),A=new Date(A);break;default:A=super._deserializeValue(t,e)}return A}_definePropertyAccessor(t,e){!function(t,e){if(!me[e]){let A=t[e];void 0!==A&&(t.__data?t._setPendingProperty(e,A):(t.__dataProto?t.hasOwnProperty(JSCompiler_renameProperty("__dataProto",t))||(t.__dataProto=Object.create(t.__dataProto)):t.__dataProto={},t.__dataProto[e]=A))}}(this,t),super._definePropertyAccessor(t,e)}_hasAccessor(t){return this.__dataHasAccessor&&this.__dataHasAccessor[t]}_isPropertyPending(t){return Boolean(this.__dataPending&&t in this.__dataPending)}}})),ye={"dom-if":!0,"dom-repeat":!0};let _e=!1,be=!1;function we(t){let e=t.getAttribute("is");if(e&&ye[e]){let A=t;for(A.removeAttribute("is"),t=A.ownerDocument.createElement(e),A.parentNode.replaceChild(t,A),t.appendChild(A);A.attributes.length;)t.setAttribute(A.attributes[0].name,A.attributes[0].value),A.removeAttribute(A.attributes[0].name)}return t}function ke(t,e){let A=e.parentInfo&&ke(t,e.parentInfo);if(!A)return t;for(let t=A.firstChild,r=0;t;t=t.nextSibling)if(e.parentIndex===r++)return t}function xe(t,e,A,r){r.id&&(e[r.id]=A)}function Se(t,e,A){if(A.events&&A.events.length)for(let r,i=0,n=A.events;i<n.length&&(r=n[i]);i++)t._addMethodEventListenerToNode(e,r.name,r.value,t)}function Ce(t,e,A,r){A.templateInfo&&(e._templateInfo=A.templateInfo,e._parentTemplateInfo=r)}const Ee=Nt((t=>class extends t{static _parseTemplate(t,e){if(!t._templateInfo){let A=t._templateInfo={};A.nodeInfoList=[],A.nestedTemplate=Boolean(e),A.stripWhiteSpace=e&&e.stripWhiteSpace||t.hasAttribute("strip-whitespace"),this._parseTemplateContent(t,A,{parent:null})}return t._templateInfo}static _parseTemplateContent(t,e,A){return this._parseTemplateNode(t.content,e,A)}static _parseTemplateNode(t,e,A){let r=!1,i=t;return"template"!=i.localName||i.hasAttribute("preserve-content")?"slot"===i.localName&&(e.hasInsertionPoint=!0):r=this._parseTemplateNestedTemplate(i,e,A)||r,function(t){(function(){if(!_e){_e=!0;const t=document.createElement("textarea");t.placeholder="a",be=t.placeholder===t.textContent}return be})()&&"textarea"===t.localName&&t.placeholder&&t.placeholder===t.textContent&&(t.textContent=null)}(i),i.firstChild&&this._parseTemplateChildNodes(i,e,A),i.hasAttributes&&i.hasAttributes()&&(r=this._parseTemplateNodeAttributes(i,e,A)||r),r||A.noted}static _parseTemplateChildNodes(t,e,A){if("script"!==t.localName&&"style"!==t.localName)for(let r,i=t.firstChild,n=0;i;i=r){if("template"==i.localName&&(i=we(i)),r=i.nextSibling,i.nodeType===Node.TEXT_NODE){let A=r;for(;A&&A.nodeType===Node.TEXT_NODE;)i.textContent+=A.textContent,r=A.nextSibling,t.removeChild(A),A=r;if(e.stripWhiteSpace&&!i.textContent.trim()){t.removeChild(i);continue}}let o={parentIndex:n,parentInfo:A};this._parseTemplateNode(i,e,o)&&(o.infoIndex=e.nodeInfoList.push(o)-1),i.parentNode&&n++}}static _parseTemplateNestedTemplate(t,e,A){let r=t,i=this._parseTemplate(r,e);return(i.content=r.content.ownerDocument.createDocumentFragment()).appendChild(r.content),A.templateInfo=i,!0}static _parseTemplateNodeAttributes(t,e,A){let r=!1,i=Array.from(t.attributes);for(let n,o=i.length-1;n=i[o];o--)r=this._parseTemplateNodeAttribute(t,e,A,n.name,n.value)||r;return r}static _parseTemplateNodeAttribute(t,e,A,r,i){return"on-"===r.slice(0,3)?(t.removeAttribute(r),A.events=A.events||[],A.events.push({name:r.slice(3),value:i}),!0):"id"===r&&(A.id=i,!0)}static _contentForTemplate(t){let e=t._templateInfo;return e&&e.content||t.content}_stampTemplate(t,e){t&&!t.content&&window.HTMLTemplateElement&&HTMLTemplateElement.decorate&&HTMLTemplateElement.decorate(t);let A=(e=e||this.constructor._parseTemplate(t)).nodeInfoList,r=e.content||t.content,i=document.importNode(r,!0);i.__noInsertionPoint=!e.hasInsertionPoint;let n=i.nodeList=new Array(A.length);i.$={};for(let t,r=0,o=A.length;r<o&&(t=A[r]);r++){let A=n[r]=ke(i,t);xe(0,i.$,A,t),Ce(0,A,t,e),Se(this,A,t)}return i=i,i}_addMethodEventListenerToNode(t,e,A,r){let i=function(t,e,A){return t=t._methodHost||t,function(e){t[A]?t[A](e,e.detail):console.warn("listener method `"+A+"` not defined")}}(r=r||t,0,A);return this._addEventListenerToNode(t,e,i),i}_addEventListenerToNode(t,e,A){t.addEventListener(e,A)}_removeEventListenerFromNode(t,e,A){t.removeEventListener(e,A)}}));let Pe=0;const Te=[],Oe={COMPUTE:"__computeEffects",REFLECT:"__reflectEffects",NOTIFY:"__notifyEffects",PROPAGATE:"__propagateEffects",OBSERVE:"__observeEffects",READ_ONLY:"__readOnly"},Ne=/[A-Z]/;function $e(t,e,A){let r=t[e];if(r){if(!t.hasOwnProperty(e)&&(r=t[e]=Object.create(t[e]),A))for(let t in r){let e=r[t],A=r[t]=Array(e.length);for(let t=0;t<e.length;t++)A[t]=e[t]}}else r=t[e]={};return r}function Ie(t,e,A,r,i,n){if(e){let o=!1;const s=Pe++;for(let a in A){let l=e[i?Qt(a):a];if(l)for(let e,c=0,d=l.length;c<d&&(e=l[c]);c++)e.info&&e.info.lastRun===s||i&&!De(a,e.trigger)||(e.info&&(e.info.lastRun=s),e.fn(t,a,A,r,e.info,i,n),o=!0)}return o}return!1}function Me(t,e,A,r,i,n,o,s){let a=!1,l=e[o?Qt(r):r];if(l)for(let e,c=0,d=l.length;c<d&&(e=l[c]);c++)e.info&&e.info.lastRun===A||o&&!De(r,e.trigger)||(e.info&&(e.info.lastRun=A),e.fn(t,r,i,n,e.info,o,s),a=!0);return a}function De(t,e){if(e){let A=e.name;return A==t||!(!e.structured||!Zt(A,t))||!(!e.wildcard||!Gt(A,t))}return!0}function Le(t,e,A,r,i){let n="string"==typeof i.method?t[i.method]:i.method,o=i.property;n?n.call(t,t.__data[o],r[o]):i.dynamicFn||console.warn("observer method `"+i.method+"` not defined")}function Be(t,e,A){let r=Qt(e);return r!==e&&(ze(t,ne(r)+"-changed",A[e],e),!0)}function ze(t,e,A,r){let i={value:A,queueProperty:!0};r&&(i.path=r),Yt(t).dispatchEvent(new CustomEvent(e,{detail:i}))}function Re(t,e,A,r,i,n){let o=(n?Qt(e):e)!=e?e:null,s=o?Xt(t,o):t.__data[e];o&&void 0===s&&(s=A[e]),ze(t,i.eventName,s,o)}function Fe(t,e,A,r,i){let n=t.__data[e];ft&&(n=ft(n,i.attrName,"attribute",t)),t._propertyToAttribute(e,i.attrName,n)}const He=(t,e,A)=>{let r=0,i=e.length-1,n=-1;for(;r<=i;){const o=r+i>>1,s=A.get(e[o].methodInfo)-A.get(t.methodInfo);if(s<0)r=o+1;else{if(!(s>0)){n=o;break}i=o-1}}n<0&&(n=i+1),e.splice(n,0,t)},Ue=(t,e,A,r,i)=>{const n=e[i?Qt(t):t];if(n)for(let e=0;e<n.length;e++){const o=n[e];o.info.lastRun===Pe||i&&!De(t,o.trigger)||(o.info.lastRun=Pe,He(o.info,A,r))}};function Ve(t,e,A,r,i){let n=We(t,e,A,0,i);if(n===Te)return!1;let o=i.methodInfo;return t.__dataHasAccessor&&t.__dataHasAccessor[o]?t._setPendingProperty(o,n,!0):(t[o]=n,!1)}function qe(t,e,A,r,i,n,o){A.bindings=A.bindings||[];let s={kind:r,target:i,parts:n,literal:o,isCompound:1!==n.length};if(A.bindings.push(s),function(t){return Boolean(t.target)&&"attribute"!=t.kind&&"text"!=t.kind&&!t.isCompound&&"{"===t.parts[0].mode}(s)){let{event:t,negate:e}=s.parts[0];s.listenerEvent=t||ne(i)+"-changed",s.listenerNegate=e}let a=e.nodeInfoList.length;for(let A=0;A<s.parts.length;A++){let r=s.parts[A];r.compoundIndex=A,Ye(t,e,s,r,a)}}function Ye(t,e,A,r,i){if(!r.literal)if("attribute"===A.kind&&"-"===A.target[0])console.warn("Cannot set attribute "+A.target+' because "-" is not a valid attribute starting character');else{let n=r.dependencies,o={index:i,binding:A,part:r,evaluator:t};for(let A=0;A<n.length;A++){let r=n[A];"string"==typeof r&&(r=eA(r),r.wildcard=!0),t._addTemplatePropertyEffect(e,r.rootProperty,{fn:je,info:o,trigger:r})}}}function je(t,e,A,r,i,n,o){let s=o[i.index],a=i.binding,l=i.part;if(n&&l.source&&e.length>l.source.length&&"property"==a.kind&&!a.isCompound&&s.__isPropertyEffectsClient&&s.__dataHasAccessor&&s.__dataHasAccessor[a.target]){let r=A[e];e=Wt(l.source,a.target,e),s._setPendingPropertyOrPath(e,r,!1,!0)&&t._enqueueClient(s)}else{let o=i.evaluator._evaluateBinding(t,l,e,A,r,n);o!==Te&&function(t,e,A,r,i){if(i=function(t,e,A,r){if(A.isCompound){let i=t.__dataCompoundStorage[A.target];i[r.compoundIndex]=e,e=i.join("")}return"attribute"!==A.kind&&("textContent"!==A.target&&("value"!==A.target||"input"!==t.localName&&"textarea"!==t.localName)||(e=null==e?"":e)),e}(e,i,A,r),ft&&(i=ft(i,A.target,A.kind,e)),"attribute"==A.kind)t._valueToNodeAttribute(e,i,A.target);else{let r=A.target;e.__isPropertyEffectsClient&&e.__dataHasAccessor&&e.__dataHasAccessor[r]?e[Oe.READ_ONLY]&&e[Oe.READ_ONLY][r]||e._setPendingProperty(r,i)&&t._enqueueClient(e):t._setUnmanagedPropertyToNode(e,r,i)}}(t,s,a,l,o)}}function Qe(t,e){if(e.isCompound){let A=t.__dataCompoundStorage||(t.__dataCompoundStorage={}),r=e.parts,i=new Array(r.length);for(let t=0;t<r.length;t++)i[t]=r[t].literal;let n=e.target;A[n]=i,e.literal&&"property"==e.kind&&("className"===n&&(t=Yt(t)),t[n]=e.literal)}}function Ze(t,e,A){if(A.listenerEvent){let r=A.parts[0];t.addEventListener(A.listenerEvent,(function(t){!function(t,e,A,r,i){let n,o=t.detail,s=o&&o.path;s?(r=Wt(A,r,s),n=o&&o.value):n=t.currentTarget[A],n=i?!n:n,e[Oe.READ_ONLY]&&e[Oe.READ_ONLY][r]||!e._setPendingPropertyOrPath(r,n,!0,Boolean(s))||o&&o.queueProperty||e._invalidateProperties()}(t,e,A.target,r.source,r.negate)}))}}function Ge(t,e,A,r,i,n){n=e.static||n&&("object"!=typeof n||n[e.methodName]);let o={methodName:e.methodName,args:e.args,methodInfo:i,dynamicFn:n};for(let i,n=0;n<e.args.length&&(i=e.args[n]);n++)i.literal||t._addPropertyEffect(i.rootProperty,A,{fn:r,info:o,trigger:i});return n&&t._addPropertyEffect(e.methodName,A,{fn:r,info:o}),o}function We(t,e,A,r,i){let n=t._methodHost||t,o=n[i.methodName];if(o){let r=t._marshalArgs(i.args,e,A);return r===Te?Te:o.apply(n,r)}i.dynamicFn||console.warn("method `"+i.methodName+"` not defined")}const Je=[],Ke=new RegExp("(\\[\\[|{{)\\s*(?:(!)\\s*)?((?:[a-zA-Z_$][\\w.:$\\-*]*)\\s*(?:\\(\\s*(?:(?:(?:((?:[a-zA-Z_$][\\w.:$\\-*]*)|(?:[-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)|(?:(?:'(?:[^'\\\\]|\\\\.)*')|(?:\"(?:[^\"\\\\]|\\\\.)*\")))\\s*)(?:,\\s*(?:((?:[a-zA-Z_$][\\w.:$\\-*]*)|(?:[-+]?[0-9]*\\.?[0-9]+(?:[eE][-+]?[0-9]+)?)|(?:(?:'(?:[^'\\\\]|\\\\.)*')|(?:\"(?:[^\"\\\\]|\\\\.)*\")))\\s*))*)?)\\)\\s*)?)(?:]]|}})","g");function Xe(t){let e="";for(let A=0;A<t.length;A++)e+=t[A].literal||"";return e}function tA(t){let e=t.match(/([^\s]+?)\(([\s\S]*)\)/);if(e){let t={methodName:e[1],static:!0,args:Je};return e[2].trim()?function(t,e){return e.args=t.map((function(t){let A=eA(t);return A.literal||(e.static=!1),A}),this),e}(e[2].replace(/\\,/g,"&comma;").split(","),t):t}return null}function eA(t){let e=t.trim().replace(/&comma;/g,",").replace(/\\(.)/g,"$1"),A={name:e,value:"",literal:!1},r=e[0];switch("-"===r&&(r=e[1]),r>="0"&&r<="9"&&(r="#"),r){case"'":case'"':A.value=e.slice(1,-1),A.literal=!0;break;case"#":A.value=Number(e),A.literal=!0}return A.literal||(A.rootProperty=Qt(e),A.structured=jt(e),A.structured&&(A.wildcard=".*"==e.slice(-2),A.wildcard&&(A.name=e.slice(0,-2)))),A}function AA(t,e,A){let r=Xt(t,A);return void 0===r&&(r=e[A]),r}function rA(t,e,A,r){const i={indexSplices:r};wt&&!t._overrideLegacyUndefined&&(e.splices=i),t.notifyPath(A+".splices",i),t.notifyPath(A+".length",e.length),wt&&!t._overrideLegacyUndefined&&(i.indexSplices=[])}function iA(t,e,A,r,i,n){rA(t,e,A,[{index:r,addedCount:i,removed:n,object:e,type:"splice"}])}const nA=Nt((t=>{const e=Ee(ve(t));return class extends e{constructor(){super(),this.__isPropertyEffectsClient=!0,this.__dataClientsReady,this.__dataPendingClients,this.__dataToNotify,this.__dataLinkedPaths,this.__dataHasPaths,this.__dataCompoundStorage,this.__dataHost,this.__dataTemp,this.__dataClientsInitialized,this.__data,this.__dataPending,this.__dataOld,this.__computeEffects,this.__computeInfo,this.__reflectEffects,this.__notifyEffects,this.__propagateEffects,this.__observeEffects,this.__readOnly,this.__templateInfo,this._overrideLegacyUndefined}get PROPERTY_EFFECT_TYPES(){return Oe}_initializeProperties(){super._initializeProperties(),this._registerHost(),this.__dataClientsReady=!1,this.__dataPendingClients=null,this.__dataToNotify=null,this.__dataLinkedPaths=null,this.__dataHasPaths=!1,this.__dataCompoundStorage=this.__dataCompoundStorage||null,this.__dataHost=this.__dataHost||null,this.__dataTemp={},this.__dataClientsInitialized=!1}_registerHost(){if(oA.length){let t=oA[oA.length-1];t._enqueueClient(this),this.__dataHost=t}}_initializeProtoProperties(t){this.__data=Object.create(t),this.__dataPending=Object.create(t),this.__dataOld={}}_initializeInstanceProperties(t){let e=this[Oe.READ_ONLY];for(let A in t)e&&e[A]||(this.__dataPending=this.__dataPending||{},this.__dataOld=this.__dataOld||{},this.__data[A]=this.__dataPending[A]=t[A])}_addPropertyEffect(t,e,A){this._createPropertyAccessor(t,e==Oe.READ_ONLY);let r=$e(this,e,!0)[t];r||(r=this[e][t]=[]),r.push(A)}_removePropertyEffect(t,e,A){let r=$e(this,e,!0)[t],i=r.indexOf(A);i>=0&&r.splice(i,1)}_hasPropertyEffect(t,e){let A=this[e];return Boolean(A&&A[t])}_hasReadOnlyEffect(t){return this._hasPropertyEffect(t,Oe.READ_ONLY)}_hasNotifyEffect(t){return this._hasPropertyEffect(t,Oe.NOTIFY)}_hasReflectEffect(t){return this._hasPropertyEffect(t,Oe.REFLECT)}_hasComputedEffect(t){return this._hasPropertyEffect(t,Oe.COMPUTE)}_setPendingPropertyOrPath(t,e,A,r){if(r||Qt(Array.isArray(t)?t[0]:t)!==t){if(!r){let A=Xt(this,t);if(!(t=te(this,t,e))||!super._shouldPropertyChange(t,e,A))return!1}if(this.__dataHasPaths=!0,this._setPendingProperty(t,e,A))return function(t,e,A){let r=t.__dataLinkedPaths;if(r){let i;for(let n in r){let o=r[n];Gt(n,e)?(i=Wt(n,o,e),t._setPendingPropertyOrPath(i,A,!0,!0)):Gt(o,e)&&(i=Wt(o,n,e),t._setPendingPropertyOrPath(i,A,!0,!0))}}}(this,t,e),!0}else{if(this.__dataHasAccessor&&this.__dataHasAccessor[t])return this._setPendingProperty(t,e,A);this[t]=e}return!1}_setUnmanagedPropertyToNode(t,e,A){A===t[e]&&"object"!=typeof A||("className"===e&&(t=Yt(t)),t[e]=A)}_setPendingProperty(t,e,A){let r=this.__dataHasPaths&&jt(t),i=r?this.__dataTemp:this.__data;return!!this._shouldPropertyChange(t,e,i[t])&&(this.__dataPending||(this.__dataPending={},this.__dataOld={}),t in this.__dataOld||(this.__dataOld[t]=this.__data[t]),r?this.__dataTemp[t]=e:this.__data[t]=e,this.__dataPending[t]=e,(r||this[Oe.NOTIFY]&&this[Oe.NOTIFY][t])&&(this.__dataToNotify=this.__dataToNotify||{},this.__dataToNotify[t]=A),!0)}_setProperty(t,e){this._setPendingProperty(t,e,!0)&&this._invalidateProperties()}_invalidateProperties(){this.__dataReady&&this._flushProperties()}_enqueueClient(t){this.__dataPendingClients=this.__dataPendingClients||[],t!==this&&this.__dataPendingClients.push(t)}_flushClients(){this.__dataClientsReady?this.__enableOrFlushClients():(this.__dataClientsReady=!0,this._readyClients(),this.__dataReady=!0)}__enableOrFlushClients(){let t=this.__dataPendingClients;if(t){this.__dataPendingClients=null;for(let e=0;e<t.length;e++){let A=t[e];A.__dataEnabled?A.__dataPending&&A._flushProperties():A._enableProperties()}}}_readyClients(){this.__enableOrFlushClients()}setProperties(t,e){for(let A in t)!e&&this[Oe.READ_ONLY]&&this[Oe.READ_ONLY][A]||this._setPendingPropertyOrPath(A,t[A],!0);this._invalidateProperties()}ready(){this._flushProperties(),this.__dataClientsReady||this._flushClients(),this.__dataPending&&this._flushProperties()}_propertiesChanged(t,e,A){let r,i=this.__dataHasPaths;this.__dataHasPaths=!1,function(t,e,A,r){let i=t[Oe.COMPUTE];if(i)if(kt){Pe++;const n=function(t){let e=t.constructor.__orderedComputedDeps;if(!e){e=new Map;const A=t[Oe.COMPUTE];let r,{counts:i,ready:n,total:o}=function(t){const e=t.__computeInfo,A={},r=t[Oe.COMPUTE],i=[];let n=0;for(let t in e){const r=e[t];n+=A[t]=r.args.filter((t=>!t.literal)).length+(r.dynamicFn?1:0)}for(let t in r)e[t]||i.push(t);return{counts:A,ready:i,total:n}}(t);for(;r=n.shift();){e.set(r,e.size);const t=A[r];t&&t.forEach((t=>{const e=t.info.methodInfo;--o,0==--i[e]&&n.push(e)}))}if(0!==o){const e=t;console.warn(`Computed graph for ${e.localName} incomplete; circular?`)}t.constructor.__orderedComputedDeps=e}return e}(t),o=[];for(let t in e)Ue(t,i,o,n,r);let s;for(;s=o.shift();)Ve(t,"",e,0,s)&&Ue(s.methodInfo,i,o,n,r);Object.assign(A,t.__dataOld),Object.assign(e,t.__dataPending),t.__dataPending=null}else{let n=e;for(;Ie(t,i,n,A,r);)Object.assign(A,t.__dataOld),Object.assign(e,t.__dataPending),n=t.__dataPending,t.__dataPending=null}}(this,e,A,i),r=this.__dataToNotify,this.__dataToNotify=null,this._propagatePropertyChanges(e,A,i),this._flushClients(),Ie(this,this[Oe.REFLECT],e,A,i),Ie(this,this[Oe.OBSERVE],e,A,i),r&&function(t,e,A,r,i){let n,o,s=t[Oe.NOTIFY],a=Pe++;for(let o in e)e[o]&&(s&&Me(t,s,a,o,A,r,i)||i&&Be(t,o,A))&&(n=!0);n&&(o=t.__dataHost)&&o._invalidateProperties&&o._invalidateProperties()}(this,r,e,A,i),1==this.__dataCounter&&(this.__dataTemp={})}_propagatePropertyChanges(t,e,A){this[Oe.PROPAGATE]&&Ie(this,this[Oe.PROPAGATE],t,e,A),this.__templateInfo&&this._runEffectsForTemplate(this.__templateInfo,t,e,A)}_runEffectsForTemplate(t,e,A,r){const i=(e,r)=>{Ie(this,t.propertyEffects,e,A,r,t.nodeList);for(let i=t.firstChild;i;i=i.nextSibling)this._runEffectsForTemplate(i,e,A,r)};t.runEffects?t.runEffects(i,e,r):i(e,r)}linkPaths(t,e){t=Jt(t),e=Jt(e),this.__dataLinkedPaths=this.__dataLinkedPaths||{},this.__dataLinkedPaths[t]=e}unlinkPaths(t){t=Jt(t),this.__dataLinkedPaths&&delete this.__dataLinkedPaths[t]}notifySplices(t,e){let A={path:""};rA(this,Xt(this,t,A),A.path,e)}get(t,e){return Xt(e||this,t)}set(t,e,A){A?te(A,t,e):this[Oe.READ_ONLY]&&this[Oe.READ_ONLY][t]||this._setPendingPropertyOrPath(t,e,!0)&&this._invalidateProperties()}push(t,...e){let A={path:""},r=Xt(this,t,A),i=r.length,n=r.push(...e);return e.length&&iA(this,r,A.path,i,e.length,[]),n}pop(t){let e={path:""},A=Xt(this,t,e),r=Boolean(A.length),i=A.pop();return r&&iA(this,A,e.path,A.length,0,[i]),i}splice(t,e,A,...r){let i,n={path:""},o=Xt(this,t,n);return e<0?e=o.length-Math.floor(-e):e&&(e=Math.floor(e)),i=2===arguments.length?o.splice(e):o.splice(e,A,...r),(r.length||i.length)&&iA(this,o,n.path,e,r.length,i),i}shift(t){let e={path:""},A=Xt(this,t,e),r=Boolean(A.length),i=A.shift();return r&&iA(this,A,e.path,0,0,[i]),i}unshift(t,...e){let A={path:""},r=Xt(this,t,A),i=r.unshift(...e);return e.length&&iA(this,r,A.path,0,e.length,[]),i}notifyPath(t,e){let A;if(1==arguments.length){let r={path:""};e=Xt(this,t,r),A=r.path}else A=Array.isArray(t)?Jt(t):t;this._setPendingPropertyOrPath(A,e,!0,!0)&&this._invalidateProperties()}_createReadOnlyProperty(t,e){var A;this._addPropertyEffect(t,Oe.READ_ONLY),e&&(this["_set"+(A=t,A[0].toUpperCase()+A.substring(1))]=function(e){this._setProperty(t,e)})}_createPropertyObserver(t,e,A){let r={property:t,method:e,dynamicFn:Boolean(A)};this._addPropertyEffect(t,Oe.OBSERVE,{fn:Le,info:r,trigger:{name:t}}),A&&this._addPropertyEffect(e,Oe.OBSERVE,{fn:Le,info:r,trigger:{name:e}})}_createMethodObserver(t,e){let A=tA(t);if(!A)throw new Error("Malformed observer expression '"+t+"'");Ge(this,A,Oe.OBSERVE,We,null,e)}_createNotifyingProperty(t){this._addPropertyEffect(t,Oe.NOTIFY,{fn:Re,info:{eventName:ne(t)+"-changed",property:t}})}_createReflectedProperty(t){let e=this.constructor.attributeNameForProperty(t);"-"===e[0]?console.warn("Property "+t+" cannot be reflected to attribute "+e+' because "-" is not a valid starting attribute name. Use a lowercase first letter for the property instead.'):this._addPropertyEffect(t,Oe.REFLECT,{fn:Fe,info:{attrName:e}})}_createComputedProperty(t,e,A){let r=tA(e);if(!r)throw new Error("Malformed computed expression '"+e+"'");const i=Ge(this,r,Oe.COMPUTE,Ve,t,A);$e(this,"__computeInfo")[t]=i}_marshalArgs(t,e,A){const r=this.__data,i=[];for(let n=0,o=t.length;n<o;n++){let{name:o,structured:s,wildcard:a,value:l,literal:c}=t[n];if(!c)if(a){const t=Gt(o,e),i=AA(r,A,t?e:o);l={path:t?e:o,value:i,base:t?Xt(r,o):i}}else l=s?AA(r,A,o):r[o];if(wt&&!this._overrideLegacyUndefined&&void 0===l&&t.length>1)return Te;i[n]=l}return i}static addPropertyEffect(t,e,A){this.prototype._addPropertyEffect(t,e,A)}static createPropertyObserver(t,e,A){this.prototype._createPropertyObserver(t,e,A)}static createMethodObserver(t,e){this.prototype._createMethodObserver(t,e)}static createNotifyingProperty(t){this.prototype._createNotifyingProperty(t)}static createReadOnlyProperty(t,e){this.prototype._createReadOnlyProperty(t,e)}static createReflectedProperty(t){this.prototype._createReflectedProperty(t)}static createComputedProperty(t,e,A){this.prototype._createComputedProperty(t,e,A)}static bindTemplate(t){return this.prototype._bindTemplate(t)}_bindTemplate(t,e){let A=this.constructor._parseTemplate(t),r=this.__preBoundTemplateInfo==A;if(!r)for(let t in A.propertyEffects)this._createPropertyAccessor(t);if(e)if(A=Object.create(A),A.wasPreBound=r,this.__templateInfo){const e=t._parentTemplateInfo||this.__templateInfo,r=e.lastChild;A.parent=e,e.lastChild=A,A.previousSibling=r,r?r.nextSibling=A:e.firstChild=A}else this.__templateInfo=A;else this.__preBoundTemplateInfo=A;return A}static _addTemplatePropertyEffect(t,e,A){(t.hostProps=t.hostProps||{})[e]=!0;let r=t.propertyEffects=t.propertyEffects||{};(r[e]=r[e]||[]).push(A)}_stampTemplate(t,e){e=e||this._bindTemplate(t,!0),oA.push(this);let A=super._stampTemplate(t,e);if(oA.pop(),e.nodeList=A.nodeList,!e.wasPreBound){let t=e.childNodes=[];for(let e=A.firstChild;e;e=e.nextSibling)t.push(e)}return A.templateInfo=e,function(t,e){let{nodeList:A,nodeInfoList:r}=e;if(r.length)for(let e=0;e<r.length;e++){let i=r[e],n=A[e],o=i.bindings;if(o)for(let e=0;e<o.length;e++){let A=o[e];Qe(n,A),Ze(n,t,A)}n.__dataHost=t}}(this,e),this.__dataClientsReady&&(this._runEffectsForTemplate(e,this.__data,null,!1),this._flushClients()),A}_removeBoundDom(t){const e=t.templateInfo,{previousSibling:A,nextSibling:r,parent:i}=e;A?A.nextSibling=r:i&&(i.firstChild=r),r?r.previousSibling=A:i&&(i.lastChild=A),e.nextSibling=e.previousSibling=null;let n=e.childNodes;for(let t=0;t<n.length;t++){let e=n[t];Yt(Yt(e).parentNode).removeChild(e)}}static _parseTemplateNode(t,A,r){let i=e._parseTemplateNode.call(this,t,A,r);if(t.nodeType===Node.TEXT_NODE){let e=this._parseBindings(t.textContent,A);e&&(t.textContent=Xe(e)||" ",qe(this,A,r,"text","textContent",e),i=!0)}return i}static _parseTemplateNodeAttribute(t,A,r,i,n){let o=this._parseBindings(n,A);if(o){let e=i,n="property";Ne.test(i)?n="attribute":"$"==i[i.length-1]&&(i=i.slice(0,-1),n="attribute");let s=Xe(o);return s&&"attribute"==n&&("class"==i&&t.hasAttribute("class")&&(s+=" "+t.getAttribute(i)),t.setAttribute(i,s)),"attribute"==n&&"disable-upgrade$"==e&&t.setAttribute(i,""),"input"===t.localName&&"value"===e&&t.setAttribute(e,""),t.removeAttribute(e),"property"===n&&(i=ie(i)),qe(this,A,r,n,i,o,s),!0}return e._parseTemplateNodeAttribute.call(this,t,A,r,i,n)}static _parseTemplateNestedTemplate(t,A,r){let i=e._parseTemplateNestedTemplate.call(this,t,A,r);const n=t.parentNode,o=r.templateInfo,s="dom-if"===n.localName,a="dom-repeat"===n.localName;xt&&(s||a)&&(n.removeChild(t),(r=r.parentInfo).templateInfo=o,r.noted=!0,i=!1);let l=o.hostProps;if(St&&s)l&&(A.hostProps=Object.assign(A.hostProps||{},l),xt||(r.parentInfo.noted=!0));else{let t="{";for(let e in l)qe(this,A,r,"property","_host_"+e,[{mode:t,source:e,dependencies:[e],hostProp:!0}])}return i}static _parseBindings(t,e){let A,r=[],i=0;for(;null!==(A=Ke.exec(t));){A.index>i&&r.push({literal:t.slice(i,A.index)});let n=A[1][0],o=Boolean(A[2]),s=A[3].trim(),a=!1,l="",c=-1;"{"==n&&(c=s.indexOf("::"))>0&&(l=s.substring(c+2),s=s.substring(0,c),a=!0);let d=tA(s),u=[];if(d){let{args:t,methodName:A}=d;for(let e=0;e<t.length;e++){let A=t[e];A.literal||u.push(A)}let r=e.dynamicFns;(r&&r[A]||d.static)&&(u.push(A),d.dynamicFn=!0)}else u.push(s);r.push({source:s,mode:n,negate:o,customEvent:a,signature:d,dependencies:u,event:l}),i=Ke.lastIndex}if(i&&i<t.length){let e=t.substring(i);e&&r.push({literal:e})}return r.length?r:null}static _evaluateBinding(t,e,A,r,i,n){let o;return o=e.signature?We(t,A,r,0,e.signature):A!=e.source?Xt(t,e.source):n&&jt(A)?Xt(t,A):t.__data[A],e.negate&&(o=!o),o}}})),oA=[],sA=[];function aA(t){sA.push(t)}const lA=Nt((t=>{const e=fe(t);function A(t){const e=Object.getPrototypeOf(t);return e.prototype instanceof i?e:null}function r(t){if(!t.hasOwnProperty(JSCompiler_renameProperty("__ownProperties",t))){let e=null;if(t.hasOwnProperty(JSCompiler_renameProperty("properties",t))){const A=t.properties;A&&(e=function(t){const e={};for(let A in t){const r=t[A];e[A]="function"==typeof r?{type:r}:r}return e}(A))}t.__ownProperties=e}return t.__ownProperties}class i extends e{static get observedAttributes(){if(!this.hasOwnProperty(JSCompiler_renameProperty("__observedAttributes",this))){aA(this.prototype);const t=this._properties;this.__observedAttributes=t?Object.keys(t).map((t=>this.prototype._addPropertyToAttributeMap(t))):[]}return this.__observedAttributes}static finalize(){if(!this.hasOwnProperty(JSCompiler_renameProperty("__finalized",this))){const t=A(this);t&&t.finalize(),this.__finalized=!0,this._finalizeClass()}}static _finalizeClass(){const t=r(this);t&&this.createProperties(t)}static get _properties(){if(!this.hasOwnProperty(JSCompiler_renameProperty("__properties",this))){const t=A(this);this.__properties=Object.assign({},t&&t._properties,r(this))}return this.__properties}static typeForProperty(t){const e=this._properties[t];return e&&e.type}_initializeProperties(){this.constructor.finalize(),super._initializeProperties()}connectedCallback(){super.connectedCallback&&super.connectedCallback(),this._enableProperties()}disconnectedCallback(){super.disconnectedCallback&&super.disconnectedCallback()}}return i})),cA=window.ShadyCSS&&window.ShadyCSS.cssBuild,dA=Nt((t=>{const e=lA(nA(t));function A(t,e,A,r){A.computed&&(A.readOnly=!0),A.computed&&(t._hasReadOnlyEffect(e)?console.warn(`Cannot redefine computed property '${e}'.`):t._createComputedProperty(e,A.computed,r)),A.readOnly&&!t._hasReadOnlyEffect(e)?t._createReadOnlyProperty(e,!A.computed):!1===A.readOnly&&t._hasReadOnlyEffect(e)&&console.warn(`Cannot make readOnly property '${e}' non-readOnly.`),A.reflectToAttribute&&!t._hasReflectEffect(e)?t._createReflectedProperty(e):!1===A.reflectToAttribute&&t._hasReflectEffect(e)&&console.warn(`Cannot make reflected property '${e}' non-reflected.`),A.notify&&!t._hasNotifyEffect(e)?t._createNotifyingProperty(e):!1===A.notify&&t._hasNotifyEffect(e)&&console.warn(`Cannot make notify property '${e}' non-notify.`),A.observer&&t._createPropertyObserver(e,A.observer,r[A.observer]),t._addPropertyToAttributeMap(e)}return class extends e{static get polymerElementVersion(){return"3.4.1"}static _finalizeClass(){e._finalizeClass.call(this);const t=((A=this).hasOwnProperty(JSCompiler_renameProperty("__ownObservers",A))||(A.__ownObservers=A.hasOwnProperty(JSCompiler_renameProperty("observers",A))?A.observers:null),A.__ownObservers);var A;t&&this.createObservers(t,this._properties),this._prepareTemplate()}static _prepareTemplate(){let t=this.template;t&&("string"==typeof t?(console.error("template getter must return HTMLTemplateElement"),t=null):yt||(t=t.cloneNode(!0))),this.prototype._template=t}static createProperties(t){for(let e in t)A(this.prototype,e,t[e],t)}static createObservers(t,e){const A=this.prototype;for(let r=0;r<t.length;r++)A._createMethodObserver(t[r],e)}static get template(){if(!this.hasOwnProperty(JSCompiler_renameProperty("_template",this))){const t=this.prototype.hasOwnProperty(JSCompiler_renameProperty("_template",this.prototype))?this.prototype._template:void 0;this._template=void 0!==t?t:this.hasOwnProperty(JSCompiler_renameProperty("is",this))&&function(t){let e=null;if(t&&(!gt||vt)&&(e=Lt.import(t,"template"),gt&&!e))throw new Error(`strictTemplatePolicy: expecting dom-module or null template for ${t}`);return e}(this.is)||Object.getPrototypeOf(this.prototype).constructor.template}return this._template}static set template(t){this._template=t}static get importPath(){if(!this.hasOwnProperty(JSCompiler_renameProperty("_importPath",this))){const t=this.importMeta;if(t)this._importPath=dt(t.url);else{const t=Lt.import(this.is);this._importPath=t&&t.assetpath||Object.getPrototypeOf(this.prototype).constructor.importPath}}return this._importPath}constructor(){super(),this._template,this._importPath,this.rootPath,this.importPath,this.root,this.$}_initializeProperties(){this.constructor.finalize(),this.constructor._finalizeTemplate(this.localName),super._initializeProperties(),this.rootPath=pt,this.importPath=this.constructor.importPath;let t=function(t){if(!t.hasOwnProperty(JSCompiler_renameProperty("__propertyDefaults",t))){t.__propertyDefaults=null;let e=t._properties;for(let A in e){let r=e[A];"value"in r&&(t.__propertyDefaults=t.__propertyDefaults||{},t.__propertyDefaults[A]=r)}}return t.__propertyDefaults}(this.constructor);if(t)for(let e in t){let A=t[e];if(this._canApplyPropertyDefault(e)){let t="function"==typeof A.value?A.value.call(this):A.value;this._hasAccessor(e)?this._setPendingProperty(e,t,!0):this[e]=t}}}_canApplyPropertyDefault(t){return!this.hasOwnProperty(t)}static _processStyleText(t,e){return ct(t,e)}static _finalizeTemplate(t){const e=this.prototype._template;if(e&&!e.__polymerFinalized){e.__polymerFinalized=!0;const A=this.importPath;(function(t,e,A,r){if(!cA){const i=e.content.querySelectorAll("style"),n=Ut(e),o=function(t){let e=zt(t);return e?Vt(e):[]}(A),s=e.content.firstElementChild;for(let A=0;A<o.length;A++){let i=o[A];i.textContent=t._processStyleText(i.textContent,r),e.content.insertBefore(i,s)}let a=0;for(let e=0;e<n.length;e++){let A=n[e],o=i[a];o!==A?(A=A.cloneNode(!0),o.parentNode.insertBefore(A,o)):a++,A.textContent=t._processStyleText(A.textContent,r)}}if(window.ShadyCSS&&window.ShadyCSS.prepareTemplate(e,A),Pt&&cA&&ht){const A=e.content.querySelectorAll("style");if(A){let e="";Array.from(A).forEach((t=>{e+=t.textContent,t.parentNode.removeChild(t)})),t._styleSheet=new CSSStyleSheet,t._styleSheet.replaceSync(e)}}})(this,e,t,A?lt(A):""),this.prototype._bindTemplate(e)}}connectedCallback(){window.ShadyCSS&&this._template&&window.ShadyCSS.styleElement(this),super.connectedCallback()}ready(){this._template&&(this.root=this._stampTemplate(this._template),this.$=this.root.$),super.ready()}_readyClients(){this._template&&(this.root=this._attachDom(this.root)),super._readyClients()}_attachDom(t){const e=Yt(this);if(e.attachShadow)return t?(e.shadowRoot||(e.attachShadow({mode:"open",shadyUpgradeFragment:t}),e.shadowRoot.appendChild(t),this.constructor._styleSheet&&(e.shadowRoot.adoptedStyleSheets=[this.constructor._styleSheet])),bt&&window.ShadyDOM&&window.ShadyDOM.flushInitial(e.shadowRoot),e.shadowRoot):null;throw new Error("ShadowDOM not available. PolymerElement can create dom as children instead of in ShadowDOM by setting `this.root = this;` before `ready`.")}updateStyles(t){window.ShadyCSS&&window.ShadyCSS.styleSubtree(this,t)}resolveUrl(t,e){return!e&&this.importPath&&(e=lt(this.importPath)),lt(t,e)}static _parseTemplateContent(t,A,r){return A.dynamicFns=A.dynamicFns||this._properties,e._parseTemplateContent.call(this,t,A,r)}static _addTemplatePropertyEffect(t,A,r){return!_t||A in this._properties||r.info.part.signature&&r.info.part.signature.static||r.info.part.hostProp||t.nestedTemplate||console.warn(`Property '${A}' used in template but not declared in 'properties'; attribute will not be observed.`),e._addTemplatePropertyEffect.call(this,t,A,r)}}}));class uA{constructor(){this._asyncModule=null,this._callback=null,this._timer=null}setConfig(t,e){this._asyncModule=t,this._callback=e,this._timer=this._asyncModule.run((()=>{this._timer=null,hA.delete(this),this._callback()}))}cancel(){this.isActive()&&(this._cancelAsync(),hA.delete(this))}_cancelAsync(){this.isActive()&&(this._asyncModule.cancel(this._timer),this._timer=null)}flush(){this.isActive()&&(this.cancel(),this._callback())}isActive(){return null!=this._timer}static debounce(t,e,A){return t instanceof uA?t._cancelAsync():t=new uA,t.setConfig(e,A),t}}let hA=new Set;const pA=function(t){hA.add(t)},fA=function(){const t=Boolean(hA.size);return hA.forEach((t=>{try{t.flush()}catch(t){setTimeout((()=>{throw t}))}})),t};let mA="string"==typeof document.head.style.touchAction,gA="__polymerGestures",vA="__polymerGesturesHandled",yA="__polymerGesturesTouchAction",_A=["mousedown","mousemove","mouseup","click"],bA=[0,1,4,2],wA=function(){try{return 1===new MouseEvent("test",{buttons:1}).buttons}catch(t){return!1}}();function kA(t){return _A.indexOf(t)>-1}let xA=!1;function SA(t){if(!kA(t)&&"touchend"!==t)return mA&&xA&&mt?{passive:!0}:void 0}!function(){try{let t=Object.defineProperty({},"passive",{get(){xA=!0}});window.addEventListener("test",null,t),window.removeEventListener("test",null,t)}catch(t){}}();let CA=navigator.userAgent.match(/iP(?:[oa]d|hone)|Android/);const EA=[],PA={button:!0,input:!0,keygen:!0,meter:!0,output:!0,textarea:!0,progress:!0,select:!0},TA={button:!0,command:!0,fieldset:!0,input:!0,keygen:!0,optgroup:!0,option:!0,select:!0,textarea:!0};function OA(t){let e=Array.prototype.slice.call(t.labels||[]);if(!e.length){e=[];let A=t.getRootNode();if(t.id){let r=A.querySelectorAll(`label[for = ${t.id}]`);for(let t=0;t<r.length;t++)e.push(r[t])}}return e}let NA=function(t){let e=t.sourceCapabilities;var A;if((!e||e.firesTouchEvents)&&(t[vA]={skip:!0},"click"===t.type)){let e=!1,r=BA(t);for(let t=0;t<r.length;t++){if(r[t].nodeType===Node.ELEMENT_NODE)if("label"===r[t].localName)EA.push(r[t]);else if(A=r[t],PA[A.localName]){let A=OA(r[t]);for(let t=0;t<A.length;t++)e=e||EA.indexOf(A[t])>-1}if(r[t]===MA.mouse.target)return}if(e)return;t.preventDefault(),t.stopPropagation()}};function $A(t){let e=CA?["click"]:_A;for(let A,r=0;r<e.length;r++)A=e[r],t?(EA.length=0,document.addEventListener(A,NA,!0)):document.removeEventListener(A,NA,!0)}function IA(t){let e=t.type;if(!kA(e))return!1;if("mousemove"===e){let e=void 0===t.buttons?1:t.buttons;return t instanceof window.MouseEvent&&!wA&&(e=bA[t.which]||0),Boolean(1&e)}return 0===(void 0===t.button?0:t.button)}let MA={mouse:{target:null,mouseIgnoreJob:null},touch:{x:0,y:0,id:-1,scrollDecided:!1}};function DA(t,e,A){t.movefn=e,t.upfn=A,document.addEventListener("mousemove",e),document.addEventListener("mouseup",A)}function LA(t){document.removeEventListener("mousemove",t.movefn),document.removeEventListener("mouseup",t.upfn),t.movefn=null,t.upfn=null}document.addEventListener("touchend",(function(t){MA.mouse.mouseIgnoreJob||$A(!0),MA.mouse.target=BA(t)[0],MA.mouse.mouseIgnoreJob=uA.debounce(MA.mouse.mouseIgnoreJob,ue.after(2500),(function(){$A(),MA.mouse.target=null,MA.mouse.mouseIgnoreJob=null}))}),!!xA&&{passive:!0});const BA=window.ShadyDOM&&window.ShadyDOM.noPatch?window.ShadyDOM.composedPath:t=>t.composedPath&&t.composedPath()||[],zA={},RA=[];function FA(t){const e=BA(t);return e.length>0?e[0]:t.target}function HA(t){let e,A=t.type,r=t.currentTarget[gA];if(!r)return;let i=r[A];if(i){if(!t[vA]&&(t[vA]={},"touch"===A.slice(0,5))){let e=(t=t).changedTouches[0];if("touchstart"===A&&1===t.touches.length&&(MA.touch.id=e.identifier),MA.touch.id!==e.identifier)return;mA||"touchstart"!==A&&"touchmove"!==A||function(t){let e=t.changedTouches[0],A=t.type;if("touchstart"===A)MA.touch.x=e.clientX,MA.touch.y=e.clientY,MA.touch.scrollDecided=!1;else if("touchmove"===A){if(MA.touch.scrollDecided)return;MA.touch.scrollDecided=!0;let A=function(t){let e="auto",A=BA(t);for(let t,r=0;r<A.length;r++)if(t=A[r],t[yA]){e=t[yA];break}return e}(t),r=!1,i=Math.abs(MA.touch.x-e.clientX),n=Math.abs(MA.touch.y-e.clientY);t.cancelable&&("none"===A?r=!0:"pan-x"===A?r=n>i:"pan-y"===A&&(r=i>n)),r?t.preventDefault():YA("track")}}(t)}if(e=t[vA],!e.skip){for(let A,r=0;r<RA.length;r++)A=RA[r],i[A.name]&&!e[A.name]&&A.flow&&A.flow.start.indexOf(t.type)>-1&&A.reset&&A.reset();for(let r,n=0;n<RA.length;n++)r=RA[n],i[r.name]&&!e[r.name]&&(e[r.name]=!0,r[A](t))}}}function UA(t){RA.push(t);for(let e=0;e<t.emits.length;e++)zA[t.emits[e]]=t}function VA(t,e){mA&&t instanceof HTMLElement&&he.run((()=>{t.style.touchAction=e})),t[yA]=e}function qA(t,e,A){let r=new Event(e,{bubbles:!0,cancelable:!0,composed:!0});if(r.detail=A,Yt(t).dispatchEvent(r),r.defaultPrevented){let t=A.preventer||A.sourceEvent;t&&t.preventDefault&&t.preventDefault()}}function YA(t){let e=function(t){for(let e,A=0;A<RA.length;A++){e=RA[A];for(let A,r=0;r<e.emits.length;r++)if(A=e.emits[r],A===t)return e}return null}(t);e.info&&(e.info.prevent=!0)}function jA(t,e,A,r){e&&qA(e,t,{x:A.clientX,y:A.clientY,sourceEvent:A,preventer:r,prevent:function(t){return YA(t)}})}function QA(t,e,A){if(t.prevent)return!1;if(t.started)return!0;let r=Math.abs(t.x-e),i=Math.abs(t.y-A);return r>=5||i>=5}function ZA(t,e,A){if(!e)return;let r,i=t.moves[t.moves.length-2],n=t.moves[t.moves.length-1],o=n.x-t.x,s=n.y-t.y,a=0;i&&(r=n.x-i.x,a=n.y-i.y),qA(e,"track",{state:t.state,x:A.clientX,y:A.clientY,dx:o,dy:s,ddx:r,ddy:a,sourceEvent:A,hover:function(){return function(t,e){let A=document.elementFromPoint(t,e),r=A;for(;r&&r.shadowRoot&&!window.ShadyDOM;){let i=r;if(r=r.shadowRoot.elementFromPoint(t,e),i===r)break;r&&(A=r)}return A}(A.clientX,A.clientY)}})}function GA(t,e,A){let r=Math.abs(e.clientX-t.x),i=Math.abs(e.clientY-t.y),n=FA(A||e);!n||TA[n.localName]&&n.hasAttribute("disabled")||(isNaN(r)||isNaN(i)||r<=25&&i<=25||function(t){if("click"===t.type){if(0===t.detail)return!0;let e=FA(t);if(!e.nodeType||e.nodeType!==Node.ELEMENT_NODE)return!0;let A=e.getBoundingClientRect(),r=t.pageX,i=t.pageY;return!(r>=A.left&&r<=A.right&&i>=A.top&&i<=A.bottom)}return!1}(e))&&(t.prevent||qA(n,"tap",{x:e.clientX,y:e.clientY,sourceEvent:e,preventer:A}))}UA({name:"downup",deps:["mousedown","touchstart","touchend"],flow:{start:["mousedown","touchstart"],end:["mouseup","touchend"]},emits:["down","up"],info:{movefn:null,upfn:null},reset:function(){LA(this.info)},mousedown:function(t){if(!IA(t))return;let e=FA(t),A=this;DA(this.info,(function(t){IA(t)||(jA("up",e,t),LA(A.info))}),(function(t){IA(t)&&jA("up",e,t),LA(A.info)})),jA("down",e,t)},touchstart:function(t){jA("down",FA(t),t.changedTouches[0],t)},touchend:function(t){jA("up",FA(t),t.changedTouches[0],t)}}),UA({name:"track",touchAction:"none",deps:["mousedown","touchstart","touchmove","touchend"],flow:{start:["mousedown","touchstart"],end:["mouseup","touchend"]},emits:["track"],info:{x:0,y:0,state:"start",started:!1,moves:[],addMove:function(t){this.moves.length>2&&this.moves.shift(),this.moves.push(t)},movefn:null,upfn:null,prevent:!1},reset:function(){this.info.state="start",this.info.started=!1,this.info.moves=[],this.info.x=0,this.info.y=0,this.info.prevent=!1,LA(this.info)},mousedown:function(t){if(!IA(t))return;let e=FA(t),A=this,r=function(t){let r=t.clientX,i=t.clientY;QA(A.info,r,i)&&(A.info.state=A.info.started?"mouseup"===t.type?"end":"track":"start","start"===A.info.state&&YA("tap"),A.info.addMove({x:r,y:i}),IA(t)||(A.info.state="end",LA(A.info)),e&&ZA(A.info,e,t),A.info.started=!0)};DA(this.info,r,(function(t){A.info.started&&r(t),LA(A.info)})),this.info.x=t.clientX,this.info.y=t.clientY},touchstart:function(t){let e=t.changedTouches[0];this.info.x=e.clientX,this.info.y=e.clientY},touchmove:function(t){let e=FA(t),A=t.changedTouches[0],r=A.clientX,i=A.clientY;QA(this.info,r,i)&&("start"===this.info.state&&YA("tap"),this.info.addMove({x:r,y:i}),ZA(this.info,e,A),this.info.state="track",this.info.started=!0)},touchend:function(t){let e=FA(t),A=t.changedTouches[0];this.info.started&&(this.info.state="end",this.info.addMove({x:A.clientX,y:A.clientY}),ZA(this.info,e,A))}}),UA({name:"tap",deps:["mousedown","click","touchstart","touchend"],flow:{start:["mousedown","touchstart"],end:["click","touchend"]},emits:["tap"],info:{x:NaN,y:NaN,prevent:!1},reset:function(){this.info.x=NaN,this.info.y=NaN,this.info.prevent=!1},mousedown:function(t){IA(t)&&(this.info.x=t.clientX,this.info.y=t.clientY)},click:function(t){IA(t)&&GA(this.info,t)},touchstart:function(t){const e=t.changedTouches[0];this.info.x=e.clientX,this.info.y=e.clientY},touchend:function(t){GA(this.info,t.changedTouches[0],t)}});const WA=Nt((t=>class extends t{_addEventListenerToNode(t,e,A){(function(t,e,A){return!!zA[e]&&(function(t,e,A){let r=zA[e],i=r.deps,n=r.name,o=t[gA];o||(t[gA]=o={});for(let e,A,r=0;r<i.length;r++)e=i[r],CA&&kA(e)&&"click"!==e||(A=o[e],A||(o[e]=A={_count:0}),0===A._count&&t.addEventListener(e,HA,SA(e)),A[n]=(A[n]||0)+1,A._count=(A._count||0)+1);t.addEventListener(e,A),r.touchAction&&VA(t,r.touchAction)}(t,e,A),!0)})(t,e,A)||super._addEventListenerToNode(t,e,A)}_removeEventListenerFromNode(t,e,A){(function(t,e,A){return!!zA[e]&&(function(t,e,A){let r=zA[e],i=r.deps,n=r.name,o=t[gA];if(o)for(let e,A,r=0;r<i.length;r++)e=i[r],A=o[e],A&&A[n]&&(A[n]=(A[n]||1)-1,A._count=(A._count||1)-1,0===A._count&&t.removeEventListener(e,HA,SA(e)));t.removeEventListener(e,A)}(t,e,A),!0)})(t,e,A)||super._removeEventListenerFromNode(t,e,A)}})),JA=/:host\(:dir\((ltr|rtl)\)\)/g,KA=/([\s\w-#\.\[\]\*]*):dir\((ltr|rtl)\)/g,XA=/:dir\((?:ltr|rtl)\)/,tr=Boolean(window.ShadyDOM&&window.ShadyDOM.inUse),er=[];let Ar=null,rr="";function ir(){rr=document.documentElement.getAttribute("dir")}function nr(t){t.__autoDirOptOut||t.setAttribute("dir",rr)}function or(){ir(),rr=document.documentElement.getAttribute("dir");for(let t=0;t<er.length;t++)nr(er[t])}const sr=Nt((t=>{tr||Ar||(ir(),Ar=new MutationObserver(or),Ar.observe(document.documentElement,{attributes:!0,attributeFilter:["dir"]}));const e=ve(t);class A extends e{static _processStyleText(t,A){return t=e._processStyleText.call(this,t,A),!tr&&XA.test(t)&&(t=this._replaceDirInCssText(t),this.__activateDir=!0),t}static _replaceDirInCssText(t){let e=t;return e=e.replace(JA,':host([dir="$1"])'),e=e.replace(KA,':host([dir="$2"]) $1'),e}constructor(){super(),this.__autoDirOptOut=!1}ready(){super.ready(),this.__autoDirOptOut=this.hasAttribute("dir")}connectedCallback(){e.prototype.connectedCallback&&super.connectedCallback(),this.constructor.__activateDir&&(Ar&&Ar.takeRecords().length&&or(),er.push(this),nr(this))}disconnectedCallback(){if(e.prototype.disconnectedCallback&&super.disconnectedCallback(),this.constructor.__activateDir){const t=er.indexOf(this);t>-1&&er.splice(t,1)}}}return A.__activateDir=!1,A}));function ar(){document.body.removeAttribute("unresolved")}function lr(t,e,A){return{index:t,removed:e,addedCount:A}}function cr(t,e){return function(t,e,A,r,i,n){let o,s=0,a=0,l=Math.min(A-e,n-i);if(0==e&&0==i&&(s=function(t,e,A){for(let r=0;r<A;r++)if(!dr(t[r],e[r]))return r;return A}(t,r,l)),A==t.length&&n==r.length&&(a=function(t,e,A){let r=t.length,i=e.length,n=0;for(;n<A&&dr(t[--r],e[--i]);)n++;return n}(t,r,l-s)),i+=s,n-=a,(A-=a)-(e+=s)==0&&n-i==0)return[];if(e==A){for(o=lr(e,[],0);i<n;)o.removed.push(r[i++]);return[o]}if(i==n)return[lr(e,[],A-e)];let c=function(t){let e=t.length-1,A=t[0].length-1,r=t[e][A],i=[];for(;e>0||A>0;){if(0==e){i.push(2),A--;continue}if(0==A){i.push(3),e--;continue}let n,o=t[e-1][A-1],s=t[e-1][A],a=t[e][A-1];n=s<a?s<o?s:o:a<o?a:o,n==o?(o==r?i.push(0):(i.push(1),r=o),e--,A--):n==s?(i.push(3),e--,r=s):(i.push(2),A--,r=a)}return i.reverse(),i}(function(t,e,A,r,i,n){let o=n-i+1,s=A-e+1,a=new Array(o);for(let t=0;t<o;t++)a[t]=new Array(s),a[t][0]=t;for(let t=0;t<s;t++)a[0][t]=t;for(let A=1;A<o;A++)for(let n=1;n<s;n++)if(dr(t[e+n-1],r[i+A-1]))a[A][n]=a[A-1][n-1];else{let t=a[A-1][n]+1,e=a[A][n-1]+1;a[A][n]=t<e?t:e}return a}(t,e,A,r,i,n));o=void 0;let d=[],u=e,h=i;for(let t=0;t<c.length;t++)switch(c[t]){case 0:o&&(d.push(o),o=void 0),u++,h++;break;case 1:o||(o=lr(u,[],0)),o.addedCount++,u++,o.removed.push(r[h]),h++;break;case 2:o||(o=lr(u,[],0)),o.addedCount++,u++;break;case 3:o||(o=lr(u,[],0)),o.removed.push(r[h]),h++}return o&&d.push(o),d}(t,0,t.length,e,0,e.length)}function dr(t,e){return t===e}function ur(t){return"slot"===t.localName}"interactive"===document.readyState||"complete"===document.readyState?ar():window.addEventListener("DOMContentLoaded",ar);let hr=class{static getFlattenedNodes(t){const e=Yt(t);return ur(t)?(t=t,e.assignedNodes({flatten:!0})):Array.from(e.childNodes).map((t=>ur(t)?Yt(t=t).assignedNodes({flatten:!0}):[t])).reduce(((t,e)=>t.concat(e)),[])}constructor(t,e){this._shadyChildrenObserver=null,this._nativeChildrenObserver=null,this._connected=!1,this._target=t,this.callback=e,this._effectiveNodes=[],this._observer=null,this._scheduled=!1,this._boundSchedule=()=>{this._schedule()},this.connect(),this._schedule()}connect(){ur(this._target)?this._listenSlots([this._target]):Yt(this._target).children&&(this._listenSlots(Yt(this._target).children),window.ShadyDOM?this._shadyChildrenObserver=window.ShadyDOM.observeChildren(this._target,(t=>{this._processMutations(t)})):(this._nativeChildrenObserver=new MutationObserver((t=>{this._processMutations(t)})),this._nativeChildrenObserver.observe(this._target,{childList:!0}))),this._connected=!0}disconnect(){ur(this._target)?this._unlistenSlots([this._target]):Yt(this._target).children&&(this._unlistenSlots(Yt(this._target).children),window.ShadyDOM&&this._shadyChildrenObserver?(window.ShadyDOM.unobserveChildren(this._shadyChildrenObserver),this._shadyChildrenObserver=null):this._nativeChildrenObserver&&(this._nativeChildrenObserver.disconnect(),this._nativeChildrenObserver=null)),this._connected=!1}_schedule(){this._scheduled||(this._scheduled=!0,he.run((()=>this.flush())))}_processMutations(t){this._processSlotMutations(t),this.flush()}_processSlotMutations(t){if(t)for(let e=0;e<t.length;e++){let A=t[e];A.addedNodes&&this._listenSlots(A.addedNodes),A.removedNodes&&this._unlistenSlots(A.removedNodes)}}flush(){if(!this._connected)return!1;window.ShadyDOM&&ShadyDOM.flush(),this._nativeChildrenObserver?this._processSlotMutations(this._nativeChildrenObserver.takeRecords()):this._shadyChildrenObserver&&this._processSlotMutations(this._shadyChildrenObserver.takeRecords()),this._scheduled=!1;let t={target:this._target,addedNodes:[],removedNodes:[]},e=this.constructor.getFlattenedNodes(this._target),A=cr(e,this._effectiveNodes);for(let e,r=0;r<A.length&&(e=A[r]);r++)for(let A,r=0;r<e.removed.length&&(A=e.removed[r]);r++)t.removedNodes.push(A);for(let r,i=0;i<A.length&&(r=A[i]);i++)for(let A=r.index;A<r.index+r.addedCount;A++)t.addedNodes.push(e[A]);this._effectiveNodes=e;let r=!1;return(t.addedNodes.length||t.removedNodes.length)&&(r=!0,this.callback.call(this._target,t)),r}_listenSlots(t){for(let e=0;e<t.length;e++){let A=t[e];ur(A)&&A.addEventListener("slotchange",this._boundSchedule)}}_unlistenSlots(t){for(let e=0;e<t.length;e++){let A=t[e];ur(A)&&A.removeEventListener("slotchange",this._boundSchedule)}}};const pr=function(){let t,e;do{t=window.ShadyDOM&&ShadyDOM.flush(),window.ShadyCSS&&window.ShadyCSS.ScopingShim&&window.ShadyCSS.ScopingShim.flush(),e=fA()}while(t||e)},fr=Element.prototype,mr=fr.matches||fr.matchesSelector||fr.mozMatchesSelector||fr.msMatchesSelector||fr.oMatchesSelector||fr.webkitMatchesSelector,gr=function(t,e){return mr.call(t,e)};class vr{constructor(t){window.ShadyDOM&&window.ShadyDOM.inUse&&window.ShadyDOM.patch(t),this.node=t}observeNodes(t){return new hr(this.node,t)}unobserveNodes(t){t.disconnect()}notifyObserver(){}deepContains(t){if(Yt(this.node).contains(t))return!0;let e=t,A=t.ownerDocument;for(;e&&e!==A&&e!==this.node;)e=Yt(e).parentNode||Yt(e).host;return e===this.node}getOwnerRoot(){return Yt(this.node).getRootNode()}getDistributedNodes(){return"slot"===this.node.localName?Yt(this.node).assignedNodes({flatten:!0}):[]}getDestinationInsertionPoints(){let t=[],e=Yt(this.node).assignedSlot;for(;e;)t.push(e),e=Yt(e).assignedSlot;return t}importNode(t,e){let A=this.node instanceof Document?this.node:this.node.ownerDocument;return Yt(A).importNode(t,e)}getEffectiveChildNodes(){return hr.getFlattenedNodes(this.node)}queryDistributedElements(t){let e=this.getEffectiveChildNodes(),A=[];for(let r,i=0,n=e.length;i<n&&(r=e[i]);i++)r.nodeType===Node.ELEMENT_NODE&&gr(r,t)&&A.push(r);return A}get activeElement(){let t=this.node;return void 0!==t._activeElement?t._activeElement:t.activeElement}}function yr(t,e){for(let A=0;A<e.length;A++){let r=e[A];Object.defineProperty(t,r,{get:function(){return this.node[r]},configurable:!0})}}class _r{constructor(t){this.event=t}get rootTarget(){return this.path[0]}get localTarget(){return this.event.target}get path(){return this.event.composedPath()}}vr.prototype.cloneNode,vr.prototype.appendChild,vr.prototype.insertBefore,vr.prototype.removeChild,vr.prototype.replaceChild,vr.prototype.setAttribute,vr.prototype.removeAttribute,vr.prototype.querySelector,vr.prototype.querySelectorAll,vr.prototype.parentNode,vr.prototype.firstChild,vr.prototype.lastChild,vr.prototype.nextSibling,vr.prototype.previousSibling,vr.prototype.firstElementChild,vr.prototype.lastElementChild,vr.prototype.nextElementSibling,vr.prototype.previousElementSibling,vr.prototype.childNodes,vr.prototype.children,vr.prototype.classList,vr.prototype.textContent,vr.prototype.innerHTML;let br=vr;if(window.ShadyDOM&&window.ShadyDOM.inUse&&window.ShadyDOM.noPatch&&window.ShadyDOM.Wrapper){class t extends window.ShadyDOM.Wrapper{}Object.getOwnPropertyNames(vr.prototype).forEach((e=>{"activeElement"!=e&&(t.prototype[e]=vr.prototype[e])})),yr(t.prototype,["classList"]),br=t,Object.defineProperties(_r.prototype,{localTarget:{get(){const t=this.event.currentTarget,e=t&&wr(t).getOwnerRoot(),A=this.path;for(let t=0;t<A.length;t++){const r=A[t];if(wr(r).getOwnerRoot()===e)return r}},configurable:!0},path:{get(){return window.ShadyDOM.composedPath(this.event)},configurable:!0}})}else!function(t,e){for(let A=0;A<e.length;A++){let r=e[A];t[r]=function(){return this.node[r].apply(this.node,arguments)}}}(vr.prototype,["cloneNode","appendChild","insertBefore","removeChild","replaceChild","setAttribute","removeAttribute","querySelector","querySelectorAll"]),yr(vr.prototype,["parentNode","firstChild","lastChild","nextSibling","previousSibling","firstElementChild","lastElementChild","nextElementSibling","previousElementSibling","childNodes","children","classList"]),function(t,e){for(let A=0;A<e.length;A++){let r=e[A];Object.defineProperty(t,r,{get:function(){return this.node[r]},set:function(t){this.node[r]=t},configurable:!0})}}(vr.prototype,["textContent","innerHTML","className"]);const wr=function(t){if((t=t||document)instanceof br)return t;if(t instanceof _r)return t;let e=t.__domApi;return e||(e=t instanceof Event?new _r(t):new br(t),t.__domApi=e),e},kr=window.ShadyDOM,xr=window.ShadyCSS;function Sr(t,e){return Yt(t).getRootNode()===e}const Cr="disable-upgrade",Er=t=>{for(;t;){const e=Object.getOwnPropertyDescriptor(t,"observedAttributes");if(e)return e.get;t=Object.getPrototypeOf(t.prototype).constructor}return()=>[]},Pr=(Nt((t=>{const e=dA(t);let A=Er(e);return class extends e{constructor(){super(),this.__isUpgradeDisabled}static get observedAttributes(){return A.call(this).concat(Cr)}_initializeProperties(){this.hasAttribute(Cr)?this.__isUpgradeDisabled=!0:super._initializeProperties()}_enableProperties(){this.__isUpgradeDisabled||super._enableProperties()}_canApplyPropertyDefault(t){return super._canApplyPropertyDefault(t)&&!(this.__isUpgradeDisabled&&this._isPropertyPending(t))}attributeChangedCallback(t,e,A,r){t==Cr?this.__isUpgradeDisabled&&null==A&&(super._initializeProperties(),this.__isUpgradeDisabled=!1,Yt(this).isConnected&&super.connectedCallback()):super.attributeChangedCallback(t,e,A,r)}connectedCallback(){this.__isUpgradeDisabled||super.connectedCallback()}disconnectedCallback(){this.__isUpgradeDisabled||super.disconnectedCallback()}}})),"disable-upgrade");let Tr=window.ShadyCSS;const Or=Nt((t=>{const e=WA(dA(t)),A=cA?e:sr(e),r=Er(A),i={x:"pan-x",y:"pan-y",none:"none",all:"auto"};class n extends A{constructor(){super(),this.isAttached,this.__boundListeners,this._debouncers,this.__isUpgradeDisabled,this.__needsAttributesAtConnected,this._legacyForceObservedAttributes}static get importMeta(){return this.prototype.importMeta}created(){}__attributeReaction(t,e,A){(this.__dataAttributes&&this.__dataAttributes[t]||t===Pr)&&this.attributeChangedCallback(t,e,A,null)}setAttribute(t,e){if(Et&&!this._legacyForceObservedAttributes){const A=this.getAttribute(t);super.setAttribute(t,e),this.__attributeReaction(t,A,String(e))}else super.setAttribute(t,e)}removeAttribute(t){if(Et&&!this._legacyForceObservedAttributes){const e=this.getAttribute(t);super.removeAttribute(t),this.__attributeReaction(t,e,null)}else super.removeAttribute(t)}static get observedAttributes(){return Et&&!this.prototype._legacyForceObservedAttributes?(this.hasOwnProperty(JSCompiler_renameProperty("__observedAttributes",this))||(this.__observedAttributes=[],aA(this.prototype)),this.__observedAttributes):r.call(this).concat(Pr)}_enableProperties(){this.__isUpgradeDisabled||super._enableProperties()}_canApplyPropertyDefault(t){return super._canApplyPropertyDefault(t)&&!(this.__isUpgradeDisabled&&this._isPropertyPending(t))}connectedCallback(){this.__needsAttributesAtConnected&&this._takeAttributes(),this.__isUpgradeDisabled||(super.connectedCallback(),this.isAttached=!0,this.attached())}attached(){}disconnectedCallback(){this.__isUpgradeDisabled||(super.disconnectedCallback(),this.isAttached=!1,this.detached())}detached(){}attributeChangedCallback(t,e,A,r){e!==A&&(t==Pr?this.__isUpgradeDisabled&&null==A&&(this._initializeProperties(),this.__isUpgradeDisabled=!1,Yt(this).isConnected&&this.connectedCallback()):(super.attributeChangedCallback(t,e,A,r),this.attributeChanged(t,e,A)))}attributeChanged(t,e,A){}_initializeProperties(){if(yt&&this.hasAttribute(Pr))this.__isUpgradeDisabled=!0;else{let t=Object.getPrototypeOf(this);t.hasOwnProperty(JSCompiler_renameProperty("__hasRegisterFinished",t))||(this._registered(),t.__hasRegisterFinished=!0),super._initializeProperties(),this.root=this,this.created(),Et&&!this._legacyForceObservedAttributes&&(this.hasAttributes()?this._takeAttributes():this.parentNode||(this.__needsAttributesAtConnected=!0)),this._applyListeners()}}_takeAttributes(){const t=this.attributes;for(let e=0,A=t.length;e<A;e++){const A=t[e];this.__attributeReaction(A.name,null,A.value)}}_registered(){}ready(){this._ensureAttributes(),super.ready()}_ensureAttributes(){}_applyListeners(){}serialize(t){return this._serializeValue(t)}deserialize(t,e){return this._deserializeValue(t,e)}reflectPropertyToAttribute(t,e,A){this._propertyToAttribute(t,e,A)}serializeValueToAttribute(t,e,A){this._valueToNodeAttribute(A||this,t,e)}extend(t,e){if(!t||!e)return t||e;let A=Object.getOwnPropertyNames(e);for(let r,i=0;i<A.length&&(r=A[i]);i++){let A=Object.getOwnPropertyDescriptor(e,r);A&&Object.defineProperty(t,r,A)}return t}mixin(t,e){for(let A in e)t[A]=e[A];return t}chainObject(t,e){return t&&e&&t!==e&&(t.__proto__=e),t}instanceTemplate(t){let e=this.constructor._contentForTemplate(t);return document.importNode(e,!0)}fire(t,e,A){A=A||{},e=null==e?{}:e;let r=new Event(t,{bubbles:void 0===A.bubbles||A.bubbles,cancelable:Boolean(A.cancelable),composed:void 0===A.composed||A.composed});r.detail=e;let i=A.node||this;return Yt(i).dispatchEvent(r),r}listen(t,e,A){t=t||this;let r=this.__boundListeners||(this.__boundListeners=new WeakMap),i=r.get(t);i||(i={},r.set(t,i));let n=e+A;i[n]||(i[n]=this._addMethodEventListenerToNode(t,e,A,this))}unlisten(t,e,A){t=t||this;let r=this.__boundListeners&&this.__boundListeners.get(t),i=e+A,n=r&&r[i];n&&(this._removeEventListenerFromNode(t,e,n),r[i]=null)}setScrollDirection(t,e){VA(e||this,i[t]||"auto")}$$(t){return this.root.querySelector(t)}get domHost(){let t=Yt(this).getRootNode();return t instanceof DocumentFragment?t.host:t}distributeContent(){const t=wr(this);window.ShadyDOM&&t.shadowRoot&&ShadyDOM.flush()}getEffectiveChildNodes(){return wr(this).getEffectiveChildNodes()}queryDistributedElements(t){return wr(this).queryDistributedElements(t)}getEffectiveChildren(){return this.getEffectiveChildNodes().filter((function(t){return t.nodeType===Node.ELEMENT_NODE}))}getEffectiveTextContent(){let t=this.getEffectiveChildNodes(),e=[];for(let A,r=0;A=t[r];r++)A.nodeType!==Node.COMMENT_NODE&&e.push(A.textContent);return e.join("")}queryEffectiveChildren(t){let e=this.queryDistributedElements(t);return e&&e[0]}queryAllEffectiveChildren(t){return this.queryDistributedElements(t)}getContentChildNodes(t){let e=this.root.querySelector(t||"slot");return e?wr(e).getDistributedNodes():[]}getContentChildren(t){return this.getContentChildNodes(t).filter((function(t){return t.nodeType===Node.ELEMENT_NODE}))}isLightDescendant(t){const e=this;return e!==t&&Yt(e).contains(t)&&Yt(e).getRootNode()===Yt(t).getRootNode()}isLocalDescendant(t){return this.root===Yt(t).getRootNode()}scopeSubtree(t,e=!1){return function(t,e=!1){if(!kr||!xr)return null;if(!kr.handlesDynamicScoping)return null;const A=xr.ScopingShim;if(!A)return null;const r=A.scopeForNode(t),i=Yt(t).getRootNode(),n=t=>{if(!Sr(t,i))return;const e=Array.from(kr.nativeMethods.querySelectorAll.call(t,"*"));e.push(t);for(let t=0;t<e.length;t++){const n=e[t];if(!Sr(n,i))continue;const o=A.currentScopeForNode(n);o!==r&&(""!==o&&A.unscopeNode(n,o),A.scopeNode(n,r))}};if(n(t),e){const e=new MutationObserver((t=>{for(let e=0;e<t.length;e++){const A=t[e];for(let t=0;t<A.addedNodes.length;t++){const e=A.addedNodes[t];e.nodeType===Node.ELEMENT_NODE&&n(e)}}}));return e.observe(t,{childList:!0,subtree:!0}),e}return null}(t,e)}getComputedStyleValue(t){return Tr.getComputedStyleValue(this,t)}debounce(t,e,A){return this._debouncers=this._debouncers||{},this._debouncers[t]=uA.debounce(this._debouncers[t],A>0?ue.after(A):he,e.bind(this))}isDebouncerActive(t){this._debouncers=this._debouncers||{};let e=this._debouncers[t];return!(!e||!e.isActive())}flushDebouncer(t){this._debouncers=this._debouncers||{};let e=this._debouncers[t];e&&e.flush()}cancelDebouncer(t){this._debouncers=this._debouncers||{};let e=this._debouncers[t];e&&e.cancel()}async(t,e){return e>0?ue.run(t.bind(this),e):~he.run(t.bind(this))}cancelAsync(t){t<0?he.cancel(~t):ue.cancel(t)}create(t,e){let A=document.createElement(t);if(e)if(A.setProperties)A.setProperties(e);else for(let t in e)A[t]=e[t];return A}elementMatches(t,e){return gr(e||this,t)}toggleAttribute(t,e){let A=this;return 3===arguments.length&&(A=arguments[2]),1==arguments.length&&(e=!A.hasAttribute(t)),e?(Yt(A).setAttribute(t,""),!0):(Yt(A).removeAttribute(t),!1)}toggleClass(t,e,A){A=A||this,1==arguments.length&&(e=!A.classList.contains(t)),e?A.classList.add(t):A.classList.remove(t)}transform(t,e){(e=e||this).style.webkitTransform=t,e.style.transform=t}translate3d(t,e,A,r){r=r||this,this.transform("translate3d("+t+","+e+","+A+")",r)}arrayDelete(t,e){let A;if(Array.isArray(t)){if(A=t.indexOf(e),A>=0)return t.splice(A,1)}else if(A=Xt(this,t).indexOf(e),A>=0)return this.splice(t,A,1);return null}_logger(t,e){switch(Array.isArray(e)&&1===e.length&&Array.isArray(e[0])&&(e=e[0]),t){case"log":case"warn":case"error":console[t](...e)}}_log(...t){this._logger("log",t)}_warn(...t){this._logger("warn",t)}_error(...t){this._logger("error",t)}_logf(t,...e){return["[%s::%s]",this.is,t,...e]}}return n.prototype.is="",n})),Nr={attached:!0,detached:!0,ready:!0,created:!0,beforeRegister:!0,registered:!0,attributeChanged:!0,listeners:!0,hostAttributes:!0},$r={attached:!0,detached:!0,ready:!0,created:!0,beforeRegister:!0,registered:!0,attributeChanged:!0,behaviors:!0,_noAccessors:!0},Ir=Object.assign({listeners:!0,hostAttributes:!0,properties:!0,observers:!0},$r);function Mr(t,e,A,r){!function(t,e,A){const r=t._noAccessors,i=Object.getOwnPropertyNames(t);for(let n=0;n<i.length;n++){let o=i[n];if(!(o in A))if(r)e[o]=t[o];else{let A=Object.getOwnPropertyDescriptor(t,o);A&&(A.configurable=!0,Object.defineProperty(e,o,A))}}}(e,t,r);for(let t in Nr)e[t]&&(A[t]=A[t]||[],A[t].push(e[t]))}function Dr(t,e,A){e=e||[];for(let r=t.length-1;r>=0;r--){let i=t[r];i?Array.isArray(i)?Dr(i,e):e.indexOf(i)<0&&(!A||A.indexOf(i)<0)&&e.unshift(i):console.warn("behavior is null, check for missing or 404 import")}return e}function Lr(t,e){for(const A in e){const r=t[A],i=e[A];t[A]=!("value"in i)&&r&&"value"in r?Object.assign({value:r.value},i):i}}const Br=Or(HTMLElement);function zr(t,e,A){let r;const i={};class n extends e{static _finalizeClass(){if(this.hasOwnProperty(JSCompiler_renameProperty("generatedFrom",this))){if(r)for(let t,e=0;e<r.length;e++)t=r[e],t.properties&&this.createProperties(t.properties),t.observers&&this.createObservers(t.observers,t.properties);t.properties&&this.createProperties(t.properties),t.observers&&this.createObservers(t.observers,t.properties),this._prepareTemplate()}else e._finalizeClass.call(this)}static get properties(){const e={};if(r)for(let t=0;t<r.length;t++)Lr(e,r[t].properties);return Lr(e,t.properties),e}static get observers(){let e=[];if(r)for(let t,A=0;A<r.length;A++)t=r[A],t.observers&&(e=e.concat(t.observers));return t.observers&&(e=e.concat(t.observers)),e}created(){super.created();const t=i.created;if(t)for(let e=0;e<t.length;e++)t[e].call(this)}_registered(){const t=n.prototype;if(!t.hasOwnProperty(JSCompiler_renameProperty("__hasRegisterFinished",t))){t.__hasRegisterFinished=!0,super._registered(),yt&&o(t);const e=Object.getPrototypeOf(this);let A=i.beforeRegister;if(A)for(let t=0;t<A.length;t++)A[t].call(e);if(A=i.registered,A)for(let t=0;t<A.length;t++)A[t].call(e)}}_applyListeners(){super._applyListeners();const t=i.listeners;if(t)for(let e=0;e<t.length;e++){const A=t[e];if(A)for(let t in A)this._addMethodEventListenerToNode(this,t,A[t])}}_ensureAttributes(){const t=i.hostAttributes;if(t)for(let e=t.length-1;e>=0;e--){const A=t[e];for(let t in A)this._ensureAttribute(t,A[t])}super._ensureAttributes()}ready(){super.ready();let t=i.ready;if(t)for(let e=0;e<t.length;e++)t[e].call(this)}attached(){super.attached();let t=i.attached;if(t)for(let e=0;e<t.length;e++)t[e].call(this)}detached(){super.detached();let t=i.detached;if(t)for(let e=0;e<t.length;e++)t[e].call(this)}attributeChanged(t,e,A){super.attributeChanged();let r=i.attributeChanged;if(r)for(let i=0;i<r.length;i++)r[i].call(this,t,e,A)}}if(A){Array.isArray(A)||(A=[A]);let t=e.prototype.behaviors;r=Dr(A,null,t),n.prototype.behaviors=t?t.concat(A):r}const o=e=>{r&&function(t,e,A){for(let r=0;r<e.length;r++)Mr(t,e[r],A,Ir)}(e,r,i),Mr(e,t,i,$r)};return yt||o(n.prototype),n.generatedFrom=t,n}const Rr=function(t){let e;return e="function"==typeof t?t:Rr.Class(t),t._legacyForceObservedAttributes&&(e.prototype._legacyForceObservedAttributes=t._legacyForceObservedAttributes),customElements.define(e.is,e),e};function Fr(t,e,A,r,i){let n;i&&(n="object"==typeof A&&null!==A,n&&(r=t.__dataTemp[e]));let o=r!==A&&(r==r||A==A);return n&&o&&(t.__dataTemp[e]=A),o}Rr.Class=function(t,e){t||console.warn("Polymer.Class requires `info` argument");let A=e?e(Br):Br;return A=zr(t,A,t.behaviors),A.is=A.prototype.is=t.is,A};const Hr=Nt((t=>class extends t{_shouldPropertyChange(t,e,A){return Fr(this,t,e,A,!0)}})),Ur=Nt((t=>class extends t{static get properties(){return{mutableData:Boolean}}_shouldPropertyChange(t,e,A){return Fr(this,t,e,A,this.mutableData)}}));Hr._mutablePropertyChange=Fr;let Vr=null;function qr(){return Vr}qr.prototype=Object.create(HTMLTemplateElement.prototype,{constructor:{value:qr,writable:!0}});const Yr=nA(qr),jr=Hr(Yr),Qr=nA(class{});function Zr(t,e){for(let A=0;A<e.length;A++){let r=e[A];if(Boolean(t)!=Boolean(r.__hideTemplateChildren__))if(r.nodeType===Node.TEXT_NODE)t?(r.__polymerTextContent__=r.textContent,r.textContent=""):r.textContent=r.__polymerTextContent__;else if("slot"===r.localName)if(t)r.__polymerReplaced__=document.createComment("hidden-slot"),Yt(Yt(r).parentNode).replaceChild(r.__polymerReplaced__,r);else{const t=r.__polymerReplaced__;t&&Yt(Yt(t).parentNode).replaceChild(r,t)}else r.style&&(t?(r.__polymerDisplay__=r.style.display,r.style.display="none"):r.style.display=r.__polymerDisplay__);r.__hideTemplateChildren__=t,r._showHideChildren&&r._showHideChildren(t)}}class Gr extends Qr{constructor(t){super(),this._configureProperties(t),this.root=this._stampTemplate(this.__dataHost);let e=[];this.children=e;for(let t=this.root.firstChild;t;t=t.nextSibling)e.push(t),t.__templatizeInstance=this;this.__templatizeOwner&&this.__templatizeOwner.__hideTemplateChildren__&&this._showHideChildren(!0);let A=this.__templatizeOptions;(t&&A.instanceProps||!A.instanceProps)&&this._enableProperties()}_configureProperties(t){if(this.__templatizeOptions.forwardHostProp)for(let t in this.__hostProps)this._setPendingProperty(t,this.__dataHost["_host_"+t]);for(let e in t)this._setPendingProperty(e,t[e])}forwardHostProp(t,e){this._setPendingPropertyOrPath(t,e,!1,!0)&&this.__dataHost._enqueueClient(this)}_addEventListenerToNode(t,e,A){if(this._methodHost&&this.__templatizeOptions.parentModel)this._methodHost._addEventListenerToNode(t,e,(t=>{t.model=this,A(t)}));else{let r=this.__dataHost.__dataHost;r&&r._addEventListenerToNode(t,e,A)}}_showHideChildren(t){Zr(t,this.children)}_setUnmanagedPropertyToNode(t,e,A){t.__hideTemplateChildren__&&t.nodeType==Node.TEXT_NODE&&"textContent"==e?t.__polymerTextContent__=A:super._setUnmanagedPropertyToNode(t,e,A)}get parentModel(){let t=this.__parentModel;if(!t){let e;t=this;do{t=t.__dataHost.__dataHost}while((e=t.__templatizeOptions)&&!e.parentModel);this.__parentModel=t}return t}dispatchEvent(t){return!0}}Gr.prototype.__dataHost,Gr.prototype.__templatizeOptions,Gr.prototype._methodHost,Gr.prototype.__templatizeOwner,Gr.prototype.__hostProps;const Wr=Hr(Gr);function Jr(t){let e=t.__dataHost;return e&&e._methodHost||e}function Kr(t,e){return function(t,A,r){e.call(t.__templatizeOwner,A.substring("_host_".length),r[A])}}function Xr(t,e){return function(t,A,r){e.call(t.__templatizeOwner,t,A,r[A])}}function ti(t,e,A){if(gt&&!Jr(t))throw new Error("strictTemplatePolicy: template owner not trusted");if(A=A||{},t.__templatizeOwner)throw new Error("A <template> can only be templatized once");t.__templatizeOwner=e;let r=(e?e.constructor:Gr)._parseTemplate(t),i=r.templatizeInstanceClass;i||(i=function(t,e,A){let r=A.mutableData?Wr:Gr;ti.mixin&&(r=ti.mixin(r));let i=class extends r{};return i.prototype.__templatizeOptions=A,i.prototype._bindTemplate(t),function(t,e,A,r){let i=A.hostProps||{};for(let e in r.instanceProps){delete i[e];let A=r.notifyInstanceProp;A&&t.prototype._addPropertyEffect(e,t.prototype.PROPERTY_EFFECT_TYPES.NOTIFY,{fn:Xr(0,A)})}if(r.forwardHostProp&&e.__dataHost)for(let e in i)A.hasHostProps||(A.hasHostProps=!0),t.prototype._addPropertyEffect(e,t.prototype.PROPERTY_EFFECT_TYPES.NOTIFY,{fn:function(t,e,A){t.__dataHost._setPendingPropertyOrPath("_host_"+e,A[e],!0,!0)}})}(i,t,e,A),i}(t,r,A),r.templatizeInstanceClass=i);const n=Jr(t);!function(t,e,A,r){let i=A.forwardHostProp;if(i&&e.hasHostProps){const s="template"==t.localName;let a=e.templatizeTemplateClass;if(!a){if(s){let t=A.mutableData?jr:Yr;class r extends t{}a=e.templatizeTemplateClass=r}else{const A=t.constructor;class r extends A{}a=e.templatizeTemplateClass=r}let n=e.hostProps;for(let t in n)a.prototype._addPropertyEffect("_host_"+t,a.prototype.PROPERTY_EFFECT_TYPES.PROPAGATE,{fn:Kr(0,i)}),a.prototype._createNotifyingProperty("_host_"+t);_t&&r&&function(t,e,A){const r=A.constructor._properties,{propertyEffects:i}=t,{instanceProps:n}=e;for(let t in i)if(!(r[t]||n&&n[t])){const e=i[t];for(let A=0;A<e.length;A++){const{part:r}=e[A].info;if(!r.signature||!r.signature.static){console.warn(`Property '${t}' used in template but not declared in 'properties'; attribute will not be observed.`);break}}}}(e,A,r)}if(t.__dataProto&&Object.assign(t.__data,t.__dataProto),s)o=a,Vr=n=t,Object.setPrototypeOf(n,o.prototype),new o,Vr=null,t.__dataTemp={},t.__dataPending=null,t.__dataOld=null,t._enableProperties();else{Object.setPrototypeOf(t,a.prototype);const A=e.hostProps;for(let e in A)if(e="_host_"+e,e in t){const A=t[e];delete t[e],t.__data[e]=A}}}var n,o}(t,r,A,n);let o=class extends i{};return o.prototype._methodHost=n,o.prototype.__dataHost=t,o.prototype.__templatizeOwner=e,o.prototype.__hostProps=r.hostProps,o=o,o}let ei=!1;function Ai(){if(yt&&!ut){if(!ei){ei=!0;const t=document.createElement("style");t.textContent="dom-bind,dom-if,dom-repeat{display:none;}",document.head.appendChild(t)}return!0}return!1}const ri=WA(Ur(nA(HTMLElement)));customElements.define("dom-bind",class extends ri{static get observedAttributes(){return["mutable-data"]}constructor(){if(super(),gt)throw new Error("strictTemplatePolicy: dom-bind not allowed");this.root=null,this.$=null,this.__children=null}attributeChangedCallback(t,e,A,r){this.mutableData=!0}connectedCallback(){Ai()||(this.style.display="none"),this.render()}disconnectedCallback(){this.__removeChildren()}__insertChildren(){Yt(Yt(this).parentNode).insertBefore(this.root,this)}__removeChildren(){if(this.__children)for(let t=0;t<this.__children.length;t++)this.root.appendChild(this.__children[t])}render(){let t;if(!this.__children){if(t=t||this.querySelector("template"),!t){let e=new MutationObserver((()=>{if(t=this.querySelector("template"),!t)throw new Error("dom-bind requires a <template> child");e.disconnect(),this.render()}));return void e.observe(this,{childList:!0})}this.root=this._stampTemplate(t),this.$=this.root.$,this.__children=[];for(let t=this.root.firstChild;t;t=t.nextSibling)this.__children[this.__children.length]=t;this._enableProperties()}this.__insertChildren(),this.dispatchEvent(new CustomEvent("dom-change",{bubbles:!0,composed:!0}))}});class ii{constructor(t){this.value=t.toString()}toString(){return this.value}}const ni=dA(HTMLElement),oi=Ur(ni);class si extends oi{static get is(){return"dom-repeat"}static get template(){return null}static get properties(){return{items:{type:Array},as:{type:String,value:"item"},indexAs:{type:String,value:"index"},itemsIndexAs:{type:String,value:"itemsIndex"},sort:{type:Function,observer:"__sortChanged"},filter:{type:Function,observer:"__filterChanged"},observe:{type:String,observer:"__observeChanged"},delay:Number,renderedItemCount:{type:Number,notify:!Ct,readOnly:!0},initialCount:{type:Number},targetFramerate:{type:Number,value:20},_targetFrameTime:{type:Number,computed:"__computeFrameTime(targetFramerate)"},notifyDomChange:{type:Boolean},reuseChunkedInstances:{type:Boolean}}}static get observers(){return["__itemsChanged(items.*)"]}constructor(){super(),this.__instances=[],this.__renderDebouncer=null,this.__itemsIdxToInstIdx={},this.__chunkCount=null,this.__renderStartTime=null,this.__itemsArrayChanged=!1,this.__shouldMeasureChunk=!1,this.__shouldContinueChunking=!1,this.__chunkingId=0,this.__sortFn=null,this.__filterFn=null,this.__observePaths=null,this.__ctor=null,this.__isDetached=!0,this.template=null,this._templateInfo}disconnectedCallback(){super.disconnectedCallback(),this.__isDetached=!0;for(let t=0;t<this.__instances.length;t++)this.__detachInstance(t)}connectedCallback(){if(super.connectedCallback(),Ai()||(this.style.display="none"),this.__isDetached){this.__isDetached=!1;let t=Yt(Yt(this).parentNode);for(let e=0;e<this.__instances.length;e++)this.__attachInstance(e,t)}}__ensureTemplatized(){if(!this.__ctor){const t=this;let e=this.template=t._templateInfo?t:this.querySelector("template");if(!e){let t=new MutationObserver((()=>{if(!this.querySelector("template"))throw new Error("dom-repeat requires a <template> child");t.disconnect(),this.__render()}));return t.observe(this,{childList:!0}),!1}let A={};A[this.as]=!0,A[this.indexAs]=!0,A[this.itemsIndexAs]=!0,this.__ctor=ti(e,this,{mutableData:this.mutableData,parentModel:!0,instanceProps:A,forwardHostProp:function(t,e){let A=this.__instances;for(let r,i=0;i<A.length&&(r=A[i]);i++)r.forwardHostProp(t,e)},notifyInstanceProp:function(t,e,A){if((r=this.as)===(i=e)||Zt(r,i)||Gt(r,i)){let r=t[this.itemsIndexAs];e==this.as&&(this.items[r]=A);let i=Wt(this.as,`${JSCompiler_renameProperty("items",this)}.${r}`,e);this.notifyPath(i,A)}var r,i}})}return!0}__getMethodHost(){return this.__dataHost._methodHost||this.__dataHost}__functionFromPropertyValue(t){if("string"==typeof t){let e=t,A=this.__getMethodHost();return function(){return A[e].apply(A,arguments)}}return t}__sortChanged(t){this.__sortFn=this.__functionFromPropertyValue(t),this.items&&this.__debounceRender(this.__render)}__filterChanged(t){this.__filterFn=this.__functionFromPropertyValue(t),this.items&&this.__debounceRender(this.__render)}__computeFrameTime(t){return Math.ceil(1e3/t)}__observeChanged(){this.__observePaths=this.observe&&this.observe.replace(".*",".").split(" ")}__handleObservedPaths(t){if(this.__sortFn||this.__filterFn)if(t){if(this.__observePaths){let e=this.__observePaths;for(let A=0;A<e.length;A++)0===t.indexOf(e[A])&&this.__debounceRender(this.__render,this.delay)}}else this.__debounceRender(this.__render,this.delay)}__itemsChanged(t){this.items&&!Array.isArray(this.items)&&console.warn("dom-repeat expected array for `items`, found",this.items),this.__handleItemPath(t.path,t.value)||("items"===t.path&&(this.__itemsArrayChanged=!0),this.__debounceRender(this.__render))}__debounceRender(t,e=0){this.__renderDebouncer=uA.debounce(this.__renderDebouncer,e>0?ue.after(e):he,t.bind(this)),pA(this.__renderDebouncer)}render(){this.__debounceRender(this.__render),pr()}__render(){if(!this.__ensureTemplatized())return;let t=this.items||[];const e=this.__sortAndFilterItems(t),A=this.__calculateLimit(e.length);this.__updateInstances(t,A,e),this.initialCount&&(this.__shouldMeasureChunk||this.__shouldContinueChunking)&&(cancelAnimationFrame(this.__chunkingId),this.__chunkingId=requestAnimationFrame((()=>this.__continueChunking()))),this._setRenderedItemCount(this.__instances.length),Ct&&!this.notifyDomChange||this.dispatchEvent(new CustomEvent("dom-change",{bubbles:!0,composed:!0}))}__sortAndFilterItems(t){let e=new Array(t.length);for(let A=0;A<t.length;A++)e[A]=A;return this.__filterFn&&(e=e.filter(((e,A,r)=>this.__filterFn(t[e],A,r)))),this.__sortFn&&e.sort(((e,A)=>this.__sortFn(t[e],t[A]))),e}__calculateLimit(t){let e=t;const A=this.__instances.length;if(this.initialCount){let r;!this.__chunkCount||this.__itemsArrayChanged&&!this.reuseChunkedInstances?(e=Math.min(t,this.initialCount),r=Math.max(e-A,0),this.__chunkCount=r||1):(r=Math.min(Math.max(t-A,0),this.__chunkCount),e=Math.min(A+r,t)),this.__shouldMeasureChunk=r===this.__chunkCount,this.__shouldContinueChunking=e<t,this.__renderStartTime=performance.now()}return this.__itemsArrayChanged=!1,e}__continueChunking(){if(this.__shouldMeasureChunk){const t=performance.now()-this.__renderStartTime,e=this._targetFrameTime/t;this.__chunkCount=Math.round(this.__chunkCount*e)||1}this.__shouldContinueChunking&&this.__debounceRender(this.__render)}__updateInstances(t,e,A){const r=this.__itemsIdxToInstIdx={};let i;for(i=0;i<e;i++){let e=this.__instances[i],n=A[i],o=t[n];r[n]=i,e?(e._setPendingProperty(this.as,o),e._setPendingProperty(this.indexAs,i),e._setPendingProperty(this.itemsIndexAs,n),e._flushProperties()):this.__insertInstance(o,i,n)}for(let t=this.__instances.length-1;t>=i;t--)this.__detachAndRemoveInstance(t)}__detachInstance(t){let e=this.__instances[t];const A=Yt(e.root);for(let t=0;t<e.children.length;t++){let r=e.children[t];A.appendChild(r)}return e}__attachInstance(t,e){let A=this.__instances[t];e.insertBefore(A.root,this)}__detachAndRemoveInstance(t){this.__detachInstance(t),this.__instances.splice(t,1)}__stampInstance(t,e,A){let r={};return r[this.as]=t,r[this.indexAs]=e,r[this.itemsIndexAs]=A,new this.__ctor(r)}__insertInstance(t,e,A){const r=this.__stampInstance(t,e,A);let i=this.__instances[e+1],n=i?i.children[0]:this;return Yt(Yt(this).parentNode).insertBefore(r.root,n),this.__instances[e]=r,r}_showHideChildren(t){for(let e=0;e<this.__instances.length;e++)this.__instances[e]._showHideChildren(t)}__handleItemPath(t,e){let A=t.slice(6),r=A.indexOf("."),i=r<0?A:A.substring(0,r);if(i==parseInt(i,10)){let t=r<0?"":A.substring(r+1);this.__handleObservedPaths(t);let n=this.__itemsIdxToInstIdx[i],o=this.__instances[n];if(o){let A=this.as+(t?"."+t:"");o._setPendingPropertyOrPath(A,e,!1,!0),o._flushProperties()}return!0}}itemForElement(t){let e=this.modelForElement(t);return e&&e[this.as]}indexForElement(t){let e=this.modelForElement(t);return e&&e[this.indexAs]}modelForElement(t){return function(t,e){let A;for(;e;)if(A=e.__dataHost?e:e.__templatizeInstance){if(A.__dataHost==t)return A;e=A.__dataHost}else e=Yt(e).parentNode;return null}(this.template,t)}}customElements.define(si.is,si);class ai extends ni{static get is(){return"dom-if"}static get template(){return null}static get properties(){return{if:{type:Boolean,observer:"__debounceRender"},restamp:{type:Boolean,observer:"__debounceRender"},notifyDomChange:{type:Boolean}}}constructor(){super(),this.__renderDebouncer=null,this._lastIf=!1,this.__hideTemplateChildren__=!1,this.__template,this._templateInfo}__debounceRender(){this.__renderDebouncer=uA.debounce(this.__renderDebouncer,he,(()=>this.__render())),pA(this.__renderDebouncer)}disconnectedCallback(){super.disconnectedCallback();const t=Yt(this).parentNode;t&&(t.nodeType!=Node.DOCUMENT_FRAGMENT_NODE||Yt(t).host)||this.__teardownInstance()}connectedCallback(){super.connectedCallback(),Ai()||(this.style.display="none"),this.if&&this.__debounceRender()}__ensureTemplate(){if(!this.__template){const t=this;let e=t._templateInfo?t:Yt(t).querySelector("template");if(!e){let t=new MutationObserver((()=>{if(!Yt(this).querySelector("template"))throw new Error("dom-if requires a <template> child");t.disconnect(),this.__render()}));return t.observe(this,{childList:!0}),!1}this.__template=e}return!0}__ensureInstance(){let t=Yt(this).parentNode;if(this.__hasInstance()){let e=this.__getInstanceNodes();if(e&&e.length&&Yt(this).previousSibling!==e[e.length-1])for(let A,r=0;r<e.length&&(A=e[r]);r++)Yt(t).insertBefore(A,this)}else{if(!t)return!1;if(!this.__ensureTemplate())return!1;this.__createAndInsertInstance(t)}return!0}render(){pr()}__render(){if(this.if){if(!this.__ensureInstance())return}else this.restamp&&this.__teardownInstance();this._showHideChildren(),Ct&&!this.notifyDomChange||this.if==this._lastIf||(this.dispatchEvent(new CustomEvent("dom-change",{bubbles:!0,composed:!0})),this._lastIf=this.if)}__hasInstance(){}__getInstanceNodes(){}__createAndInsertInstance(t){}__teardownInstance(){}_showHideChildren(){}}const li=St?class extends ai{constructor(){super(),this.__instance=null,this.__syncInfo=null}__hasInstance(){return Boolean(this.__instance)}__getInstanceNodes(){return this.__instance.templateInfo.childNodes}__createAndInsertInstance(t){const e=this.__dataHost||this;if(gt&&!this.__dataHost)throw new Error("strictTemplatePolicy: template owner not trusted");const A=e._bindTemplate(this.__template,!0);A.runEffects=(t,e,A)=>{let r=this.__syncInfo;if(this.if)r&&(this.__syncInfo=null,this._showHideChildren(),e=Object.assign(r.changedProps,e)),t(e,A);else if(this.__instance)if(r||(r=this.__syncInfo={runEffects:t,changedProps:{}}),A)for(const t in e){const e=Qt(t);r.changedProps[e]=this.__dataHost[e]}else Object.assign(r.changedProps,e)},this.__instance=e._stampTemplate(this.__template,A),Yt(t).insertBefore(this.__instance,this)}__syncHostProperties(){const t=this.__syncInfo;t&&(this.__syncInfo=null,t.runEffects(t.changedProps,!1))}__teardownInstance(){const t=this.__dataHost||this;this.__instance&&(t._removeBoundDom(this.__instance),this.__instance=null,this.__syncInfo=null)}_showHideChildren(){const t=this.__hideTemplateChildren__||!this.if;this.__instance&&Boolean(this.__instance.__hidden)!==t&&(this.__instance.__hidden=t,Zr(t,this.__instance.templateInfo.childNodes)),t||this.__syncHostProperties()}}:class extends ai{constructor(){super(),this.__ctor=null,this.__instance=null,this.__invalidProps=null}__hasInstance(){return Boolean(this.__instance)}__getInstanceNodes(){return this.__instance.children}__createAndInsertInstance(t){this.__ctor||(this.__ctor=ti(this.__template,this,{mutableData:!0,forwardHostProp:function(t,e){this.__instance&&(this.if?this.__instance.forwardHostProp(t,e):(this.__invalidProps=this.__invalidProps||Object.create(null),this.__invalidProps[Qt(t)]=!0))}})),this.__instance=new this.__ctor,Yt(t).insertBefore(this.__instance.root,this)}__teardownInstance(){if(this.__instance){let t=this.__instance.children;if(t&&t.length){let e=Yt(t[0]).parentNode;if(e){e=Yt(e);for(let A,r=0;r<t.length&&(A=t[r]);r++)e.removeChild(A)}}this.__invalidProps=null,this.__instance=null}}__syncHostProperties(){let t=this.__invalidProps;if(t){this.__invalidProps=null;for(let e in t)this.__instance._setPendingProperty(e,this.__dataHost[e]);this.__instance._flushProperties()}}_showHideChildren(){const t=this.__hideTemplateChildren__||!this.if;this.__instance&&Boolean(this.__instance.__hidden)!==t&&(this.__instance.__hidden=t,this.__instance._showHideChildren(t)),t||this.__syncHostProperties()}};customElements.define(li.is,li);let ci=Nt((t=>{let e=dA(t);return class extends e{static get properties(){return{items:{type:Array},multi:{type:Boolean,value:!1},selected:{type:Object,notify:!0},selectedItem:{type:Object,notify:!0},toggle:{type:Boolean,value:!1}}}static get observers(){return["__updateSelection(multi, items.*)"]}constructor(){super(),this.__lastItems=null,this.__lastMulti=null,this.__selectedMap=null}__updateSelection(t,e){let A=e.path;if(A==JSCompiler_renameProperty("items",this)){let A=e.base||[],r=this.__lastItems;if(t!==this.__lastMulti&&this.clearSelection(),r){let t=cr(A,r);this.__applySplices(t)}this.__lastItems=A,this.__lastMulti=t}else if(e.path==`${JSCompiler_renameProperty("items",this)}.splices`)this.__applySplices(e.value.indexSplices);else{let t=A.slice(`${JSCompiler_renameProperty("items",this)}.`.length),e=parseInt(t,10);t.indexOf(".")<0&&t==e&&this.__deselectChangedIdx(e)}}__applySplices(t){let e=this.__selectedMap;for(let A=0;A<t.length;A++){let r=t[A];e.forEach(((t,A)=>{t<r.index||(t>=r.index+r.removed.length?e.set(A,t+r.addedCount-r.removed.length):e.set(A,-1))}));for(let t=0;t<r.addedCount;t++){let A=r.index+t;e.has(this.items[A])&&e.set(this.items[A],A)}}this.__updateLinks();let A=0;e.forEach(((t,r)=>{t<0?(this.multi?this.splice(JSCompiler_renameProperty("selected",this),A,1):this.selected=this.selectedItem=null,e.delete(r)):A++}))}__updateLinks(){if(this.__dataLinkedPaths={},this.multi){let t=0;this.__selectedMap.forEach((e=>{e>=0&&this.linkPaths(`${JSCompiler_renameProperty("items",this)}.${e}`,`${JSCompiler_renameProperty("selected",this)}.${t++}`)}))}else this.__selectedMap.forEach((t=>{this.linkPaths(JSCompiler_renameProperty("selected",this),`${JSCompiler_renameProperty("items",this)}.${t}`),this.linkPaths(JSCompiler_renameProperty("selectedItem",this),`${JSCompiler_renameProperty("items",this)}.${t}`)}))}clearSelection(){this.__dataLinkedPaths={},this.__selectedMap=new Map,this.selected=this.multi?[]:null,this.selectedItem=null}isSelected(t){return this.__selectedMap.has(t)}isIndexSelected(t){return this.isSelected(this.items[t])}__deselectChangedIdx(t){let e=this.__selectedIndexForItemIndex(t);if(e>=0){let t=0;this.__selectedMap.forEach(((A,r)=>{e==t++&&this.deselect(r)}))}}__selectedIndexForItemIndex(t){let e=this.__dataLinkedPaths[`${JSCompiler_renameProperty("items",this)}.${t}`];if(e)return parseInt(e.slice(`${JSCompiler_renameProperty("selected",this)}.`.length),10)}deselect(t){let e=this.__selectedMap.get(t);if(e>=0){let A;this.__selectedMap.delete(t),this.multi&&(A=this.__selectedIndexForItemIndex(e)),this.__updateLinks(),this.multi?this.splice(JSCompiler_renameProperty("selected",this),A,1):this.selected=this.selectedItem=null}}deselectIndex(t){this.deselect(this.items[t])}select(t){this.selectIndex(this.items.indexOf(t))}selectIndex(t){let e=this.items[t];this.isSelected(e)?this.toggle&&this.deselectIndex(t):(this.multi||this.__selectedMap.clear(),this.__selectedMap.set(e,t),this.__updateLinks(),this.multi?this.push(JSCompiler_renameProperty("selected",this),e):this.selected=this.selectedItem=e)}}}))(ni);class di extends ci{static get is(){return"array-selector"}static get template(){return null}}customElements.define(di.is,di);const ui=new At;window.ShadyCSS||(window.ShadyCSS={prepareTemplate(t,e,A){},prepareTemplateDom(t,e){},prepareTemplateStyles(t,e,A){},styleSubtree(t,e){ui.processStyles(),$(t,e)},styleElement(t){ui.processStyles()},styleDocument(t){ui.processStyles(),$(document.body,t)},getComputedStyleValue:(t,e)=>I(t,e),flushCustomStyles(){},nativeCss:a,nativeShadow:r,cssBuild:n,disableRuntime:s}),window.ShadyCSS.CustomStyleInterface=ui;const hi="include",pi=window.ShadyCSS.CustomStyleInterface;class fi extends HTMLElement{constructor(){super(),this._style=null,pi.addCustomStyle(this)}getStyle(){if(this._style)return this._style;const t=this.querySelector("style");if(!t)return null;this._style=t;const e=t.getAttribute(hi);return e&&(t.removeAttribute(hi),t.textContent=function(t){let e=t.trim().split(/\s+/),A="";for(let t=0;t<e.length;t++)A+=qt(e[t]);return A}(e)+t.textContent),this.ownerDocument!==window.document&&window.document.head.appendChild(this),this._style}}let mi;window.customElements.define("custom-style",fi),mi=Hr._mutablePropertyChange,Boolean;const gi=Or(HTMLElement).prototype,vi={properties:{libraryLoaded:{type:Boolean,value:!1,notify:!0,readOnly:!0},libraryErrorMessage:{type:String,value:null,notify:!0,readOnly:!0}},observers:["_libraryUrlChanged(libraryUrl)"],_libraryUrlChanged:function(t){this._isReady&&this.libraryUrl&&this._loadLibrary()},_libraryLoadCallback:function(t,e){t?(gi._warn("Library load failed:",t.message),this._setLibraryErrorMessage(t.message)):(this._setLibraryErrorMessage(null),this._setLibraryLoaded(!0),this.notifyEvent&&this.fire(this.notifyEvent,e,{composed:!0}))},_loadLibrary:function(){yi.require(this.libraryUrl,this._libraryLoadCallback.bind(this),this.callbackName)},ready:function(){this._isReady=!0,this.libraryUrl&&this._loadLibrary()}};var yi={apiMap:{},require:function(t,e,A){var r=this.nameFromUrl(t);this.apiMap[r]||(this.apiMap[r]=new _i(r,t,A)),this.apiMap[r].requestNotify(e)},nameFromUrl:function(t){return t.replace(/[\:\/\%\?\&\.\=\-\,]/g,"_")+"_api"}},_i=function(t,e,A){if(this.notifiers=[],!A){if(!(e.indexOf(this.callbackMacro)>=0))return void(this.error=new Error("IronJsonpLibraryBehavior a %%callback%% parameter is required in libraryUrl"));A=t+"_loaded",e=e.replace(this.callbackMacro,A)}this.callbackName=A,window[this.callbackName]=this.success.bind(this),this.addScript(e)};_i.prototype={callbackMacro:"%%callback%%",loaded:!1,addScript:function(t){var e=document.createElement("script");e.src=t,e.onerror=this.handleError.bind(this);var A=document.querySelector("script")||document.body;A.parentNode.insertBefore(e,A),this.script=e},removeScript:function(){this.script.parentNode&&this.script.parentNode.removeChild(this.script),this.script=null},handleError:function(t){this.error=new Error("Library failed to load"),this.notifyAll(),this.cleanup()},success:function(){this.loaded=!0,this.result=Array.prototype.slice.call(arguments),this.notifyAll(),this.cleanup()},cleanup:function(){delete window[this.callbackName]},notifyAll:function(){this.notifiers.forEach(function(t){t(this.error,this.result)}.bind(this)),this.notifiers=[]},requestNotify:function(t){this.loaded||this.error?t(this.error,this.result):this.notifiers.push(t)}},Rr({is:"iron-jsonp-library",behaviors:[vi],properties:{libraryUrl:String,callbackName:String,notifyEvent:String}}),Rr({is:"google-youtube-api",_template:null,behaviors:[vi],properties:{libraryUrl:{type:String,value:"https://www.youtube.com/iframe_api"},notifyEvent:{type:String,value:"api-load"},callbackName:{type:String,value:"onYouTubeIframeAPIReady"}},get api(){return window.YT}}),Rr({is:"iron-localstorage",properties:{name:{type:String,value:""},value:{type:Object,notify:!0},useRaw:{type:Boolean,value:!1},autoSaveDisabled:{type:Boolean,value:!1},errorMessage:{type:String,notify:!0},_loaded:{type:Boolean,value:!1}},observers:["_debounceReload(name,useRaw)","_trySaveValue(autoSaveDisabled)","_trySaveValue(value.*)"],ready:function(){this._boundHandleStorage=this._handleStorage.bind(this)},attached:function(){window.addEventListener("storage",this._boundHandleStorage)},detached:function(){window.removeEventListener("storage",this._boundHandleStorage)},_handleStorage:function(t){t.key==this.name&&this._load(!0)},_trySaveValue:function(){void 0===this.autoSaveDisabled||this._doNotSave||this._loaded&&!this.autoSaveDisabled&&this.debounce("save",this.save)},_debounceReload:function(){void 0!==this.name&&void 0!==this.useRaw&&this.debounce("reload",this.reload)},reload:function(){this._loaded=!1,this._load()},_load:function(t){try{var e=window.localStorage.getItem(this.name)}catch(t){this.errorMessage=t.message,this._error("Could not save to localStorage.  Try enabling cookies for this page.",t)}if(null===e)this._loaded=!0,this._doNotSave=!0,this.value=null,this._doNotSave=!1,this.fire("iron-localstorage-load-empty",{externalChange:t},{composed:!0});else{if(!this.useRaw)try{e=JSON.parse(e)}catch(t){this.errorMessage="Could not parse local storage value",gi._error("could not parse local storage value",e),e=null}this._loaded=!0,this._doNotSave=!0,this.value=e,this._doNotSave=!1,this.fire("iron-localstorage-load",{externalChange:t},{composed:!0})}},save:function(){var t=this.useRaw?this.value:JSON.stringify(this.value);try{null===this.value||void 0===this.value?window.localStorage.removeItem(this.name):window.localStorage.setItem(this.name,t)}catch(t){this.errorMessage=t.message,gi._error("Could not save to localStorage. Incognito mode may be blocking this action",t)}}}),Rr({_template:(function(t,...e){const A=document.createElement("template");return A.innerHTML=e.reduce(((e,A,r)=>e+function(t){if(t instanceof HTMLTemplateElement)return t.innerHTML;if(t instanceof ii)return function(t){if(t instanceof ii)return t.value;throw new Error(`non-literal value passed to Polymer's htmlLiteral function: ${t}`)}(t);throw new Error(`non-template value passed to Polymer's html function: ${t}`)}(A)+t[r+1]),t[0]),A})`
    <style>
      :host {
        display: block;
      }

      :host([fluid]) {
        width: 100%;
        max-width: 100%;
        position: relative;
      }

      :host([fluid]) iframe,
      :host([fluid]) #thumbnail {
        vertical-align: bottom;
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
      }

      iframe {
        @apply --google-youtube-iframe;
      }

      #container {
        max-width: 100%;
        max-height: 100%;
        @apply --google-youtube-container;
      }

      #thumbnail {
        width: 100%;
        height: 100%;
        cursor: pointer;
        @apply --google-youtube-thumbnail;
      }
    </style>
    <div id="container" style\$="{{_computeContainerStyle(width, height)}}">
      <template is="dom-if" if="{{thumbnail}}">
        <img id="thumbnail" src\$="{{thumbnail}}" title="YouTube video thumbnail." alt="YouTube video thumbnail." on-tap="_handleThumbnailTap">
      </template>

      <template is="dom-if" if="{{!thumbnail}}">
        <template is="dom-if" if="[[shouldLoadApi]]">
          <google-youtube-api on-api-load="_apiLoad"></google-youtube-api>
        </template>
      </template>

      <!-- Use this._playsupportedLocalStorage as the value, since this.playsupported is set to
           true as soon as initial playback has started, and we don't want that cached. -->
      <iron-localstorage name="google-youtube-playsupported" value="{{_playsupportedLocalStorage}}" on-iron-localstorage-load="_useExistingPlaySupportedValue" on-iron-localstorage-load-empty="_determinePlaySupported">
      </iron-localstorage>

      <div id="player"></div>
    </div>
`,is:"google-youtube",properties:{videoId:{type:String,value:"",observer:"_videoIdChanged"},list:{type:String,value:""},listType:String,shouldLoadApi:{type:Boolean,computed:"_computeShouldLoadApi(list, videoId)"},playsupported:{type:Boolean,value:null,notify:!0},autoplay:{type:Number,value:0},playbackstarted:{type:Boolean,value:!1,notify:!0},height:{type:String,value:"270px"},width:{type:String,value:"480px"},state:{type:Number,value:-1,notify:!0},currenttime:{type:Number,value:0,notify:!0},duration:{type:Number,value:1,notify:!0},currenttimeformatted:{type:String,value:"0:00",notify:!0},durationformatted:{type:String,value:"0:00",notify:!0},fractionloaded:{type:Number,value:0,notify:!0},chromeless:{type:Boolean,value:!1},thumbnail:{type:String,value:""},fluid:{type:Boolean,value:!1},volume:{type:Number,value:100,notify:!0},playbackrate:{type:Number,value:1,notify:!0},playbackquality:{type:String,value:"",notify:!0},statsUpdateInterval:{type:Number,value:1e3}},_computeContainerStyle:function(t,e){return"width:"+t+"; height:"+e},_computeShouldLoadApi:function(t,e){return Boolean(t||e)},_useExistingPlaySupportedValue:function(){this.playsupported=this._playsupportedLocalStorage},_determinePlaySupported:function(){if(null==this.playsupported){var t,e=document.createElement("video");if("play"in e){e.id="playtest",e.style.position="absolute",e.style.top="-9999px",e.style.left="-9999px";var A=document.createElement("source");A.src="data:video/mp4;base64,AAAAFGZ0eXBNU05WAAACAE1TTlYAAAOUbW9vdgAAAGxtdmhkAAAAAM9ghv7PYIb+AAACWAAACu8AAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAnh0cmFrAAAAXHRraGQAAAAHz2CG/s9ghv4AAAABAAAAAAAACu8AAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAFAAAAA4AAAAAAHgbWRpYQAAACBtZGhkAAAAAM9ghv7PYIb+AAALuAAANq8AAAAAAAAAIWhkbHIAAAAAbWhscnZpZGVBVlMgAAAAAAABAB4AAAABl21pbmYAAAAUdm1oZAAAAAAAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAVdzdGJsAAAAp3N0c2QAAAAAAAAAAQAAAJdhdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAFAAOABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAAEmNvbHJuY2xjAAEAAQABAAAAL2F2Y0MBTUAz/+EAGGdNQDOadCk/LgIgAAADACAAAAMA0eMGVAEABGjuPIAAAAAYc3R0cwAAAAAAAAABAAAADgAAA+gAAAAUc3RzcwAAAAAAAAABAAAAAQAAABxzdHNjAAAAAAAAAAEAAAABAAAADgAAAAEAAABMc3RzegAAAAAAAAAAAAAADgAAAE8AAAAOAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA4AAAAOAAAAFHN0Y28AAAAAAAAAAQAAA7AAAAA0dXVpZFVTTVQh0k/Ou4hpXPrJx0AAAAAcTVREVAABABIAAAAKVcQAAAAAAAEAAAAAAAAAqHV1aWRVU01UIdJPzruIaVz6ycdAAAAAkE1URFQABAAMAAAAC1XEAAACHAAeAAAABBXHAAEAQQBWAFMAIABNAGUAZABpAGEAAAAqAAAAASoOAAEAZABlAHQAZQBjAHQAXwBhAHUAdABvAHAAbABhAHkAAAAyAAAAA1XEAAEAMgAwADAANQBtAGUALwAwADcALwAwADYAMAA2ACAAMwA6ADUAOgAwAAABA21kYXQAAAAYZ01AM5p0KT8uAiAAAAMAIAAAAwDR4wZUAAAABGjuPIAAAAAnZYiAIAAR//eBLT+oL1eA2Nlb/edvwWZflzEVLlhlXtJvSAEGRA3ZAAAACkGaAQCyJ/8AFBAAAAAJQZoCATP/AOmBAAAACUGaAwGz/wDpgAAAAAlBmgQCM/8A6YEAAAAJQZoFArP/AOmBAAAACUGaBgMz/wDpgQAAAAlBmgcDs/8A6YEAAAAJQZoIBDP/AOmAAAAACUGaCQSz/wDpgAAAAAlBmgoFM/8A6YEAAAAJQZoLBbP/AOmAAAAACkGaDAYyJ/8AFBAAAAAKQZoNBrIv/4cMeQ==",e.appendChild(A);var r=document.createElement("source");r.src="data:video/webm;base64,GkXfo49CgoR3ZWJtQoeBAUKFgQEYU4BnAQAAAAAAF60RTZt0vE27jFOrhBVJqWZTrIIQA027jFOrhBZUrmtTrIIQbE27jFOrhBFNm3RTrIIXmU27jFOrhBxTu2tTrIIWs+xPvwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUmpZuQq17GDD0JATYCjbGliZWJtbCB2MC43LjcgKyBsaWJtYXRyb3NrYSB2MC44LjFXQY9BVlNNYXRyb3NrYUZpbGVEiYRFnEAARGGIBc2Lz1QNtgBzpJCy3XZ0KNuKNZS4+fDpFxzUFlSua9iu1teBAXPFhL4G+bmDgQG5gQGIgQFVqoEAnIEAbeeBASMxT4Q/gAAAVe6BAIaFVl9WUDiqgQEj44OEE95DVSK1nIN1bmTgkbCBULqBPJqBAFSwgVBUuoE87EQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9DtnVB4eeBAKC4obaBAAAAkAMAnQEqUAA8AABHCIWFiIWEiAICAAamYnoOC6cfJa8f5Zvda4D+/7YOf//nNefQYACgnKGWgQFNANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQKbANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQPoANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQU1ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQaDANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQfQANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQkdANEBAAEQEBRgAGFgv9AAIiGAAPuC/rOgnKGWgQprANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQu4ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ0FANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ5TANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQ+gANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRDtANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRI7ANEBAAEQEAAYABhYL/QACIhgAPuC/rIcU7trQOC7jLOBALeH94EB8YIUzLuNs4IBTbeH94EB8YIUzLuNs4ICm7eH94EB8YIUzLuNs4ID6LeH94EB8YIUzLuNs4IFNbeH94EB8YIUzLuNs4IGg7eH94EB8YIUzLuNs4IH0LeH94EB8YIUzLuNs4IJHbeH94EB8YIUzLuNs4IKa7eH94EB8YIUzLuNs4ILuLeH94EB8YIUzLuNs4INBbeH94EB8YIUzLuNs4IOU7eH94EB8YIUzLuNs4IPoLeH94EB8YIUzLuNs4IQ7beH94EB8YIUzLuNs4ISO7eH94EB8YIUzBFNm3SPTbuMU6uEH0O2dVOsghTM",e.appendChild(r),document.body.appendChild(e),this.async((function(){e.onplaying=function(A){clearTimeout(t),this.playsupported=A&&"playing"===A.type||0!==e.currentTime,this._playsupportedLocalStorage=this.playsupported,e.onplaying=null,document.body.removeChild(e)}.bind(this),t=setTimeout(e.onplaying,500),e.play()}))}else this.playsupported=!1,this._playsupportedLocalStorage=!1}},ready:function(){if(this.hasAttribute("fluid")){var t=parseInt(this.height,10)/parseInt(this.width,10);isNaN(t)&&(t=9/16),t*=100,this.width="100%",this.height="auto",this.$.container.style["padding-top"]=t+"%"}},detached:function(){this._player&&this._player.destroy()},play:function(){this._player&&this._player.playVideo&&this.playsupported&&this._player.playVideo()},setVolume:function(t){this._player&&this._player.setVolume&&this._player.setVolume(t)},mute:function(){this._player&&this._player.mute&&this._player.mute()},unMute:function(){this._player&&this._player.unMute&&this._player.unMute()},pause:function(){this._player&&this._player.pauseVideo&&this._player.pauseVideo()},seekTo:function(t){this._player&&this._player.seekTo&&(this._player.seekTo(t,!0),this.async((function(){this._updatePlaybackStats()}),100))},setPlaybackRate:function(t){this._player&&this._player.setPlaybackRate&&this._player.setPlaybackRate(t)},setPlaybackQuality:function(t){this._player&&this._player.setPlaybackQuality&&this._player.setPlaybackQuality(t)},_videoIdChanged:function(){this.videoId&&(this.currenttime=0,this.currenttimeformatted=this._toHHMMSS(0),this.fractionloaded=0,this.duration=1,this.durationformatted=this._toHHMMSS(0),this._player&&this._player.cueVideoById?this.playsupported&&this.attributes.autoplay&&"1"==this.attributes.autoplay.value?this._player.loadVideoById(this.videoId):this._player.cueVideoById(this.videoId):this._pendingVideoId=this.videoId)},_player:null,__updatePlaybackStatsInterval:null,_pendingVideoId:"",_apiLoad:function(){var t={playsinline:1,controls:2,autohide:1,autoplay:this.autoplay};this.chromeless&&(t.controls=0,t.modestbranding=1,t.showinfo=0,t.iv_load_policy=3,t.rel=0);for(var e=0;e<this.attributes.length;e++){var A=this.attributes[e];t[A.nodeName]=A.value}this._player=new YT.Player(this.$.player,{videoId:this.videoId,width:"100%",height:"100%",playerVars:t,events:{onReady:function(t){this._pendingVideoId&&this._pendingVideoId!=this.videoId&&(this._player.cueVideoById(this._pendingVideoId),this._pendingVideoId=""),this.fire("google-youtube-ready",t)}.bind(this),onStateChange:function(t){this.state=t.data,1==this.state?(this.playbackstarted=!0,this.playsupported=!0,this.duration=this._player.getDuration(),this.durationformatted=this._toHHMMSS(this.duration),this.__updatePlaybackStatsInterval||(this.__updatePlaybackStatsInterval=setInterval(this._updatePlaybackStats.bind(this),this.statsUpdateInterval))):this.__updatePlaybackStatsInterval&&(clearInterval(this.__updatePlaybackStatsInterval),this.__updatePlaybackStatsInterval=null),this.fire("google-youtube-state-change",t)}.bind(this),onPlaybackQualityChange:function(t){this.playbackquality=t.data}.bind(this),onPlaybackRateChange:function(t){this.playbackrate=t.data}.bind(this),onError:function(t){this.state=0,this.fire("google-youtube-error",t)}.bind(this)}})},_updatePlaybackStats:function(){this.currenttime=this.statsUpdateInterval>=1e3?Math.round(this._player.getCurrentTime()):this._player.getCurrentTime(),this.currenttimeformatted=this._toHHMMSS(this.currenttime),this.fractionloaded=this._player.getVideoLoadedFraction(),this.volume=this._player.getVolume()},_toHHMMSS:function(t){var e=Math.floor(t/3600);t-=3600*e;var A=Math.floor(t/60),r=Math.round(t-60*A),i="";return e>0&&(i+=e+":",A<10&&(A="0"+A)),r<10&&(r="0"+r),i+A+":"+r},_handleThumbnailTap:function(){this.autoplay=1,this.thumbnail=""}})},490:(t,e)=>{"use strict";function A(t,e){for(var A=0;A<e.length;A++){var r=e[A];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(t,r.key,r)}}function r(t,e,r){return e&&A(t.prototype,e),r&&A(t,r),t}function i(){return i=Object.assign||function(t){for(var e=1;e<arguments.length;e++){var A=arguments[e];for(var r in A)Object.prototype.hasOwnProperty.call(A,r)&&(t[r]=A[r])}return t},i.apply(this,arguments)}function n(t,e){t.prototype=Object.create(e.prototype),t.prototype.constructor=t,s(t,e)}function o(t){return o=Object.setPrototypeOf?Object.getPrototypeOf:function(t){return t.__proto__||Object.getPrototypeOf(t)},o(t)}function s(t,e){return s=Object.setPrototypeOf||function(t,e){return t.__proto__=e,t},s(t,e)}function a(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],(function(){}))),!0}catch(t){return!1}}function l(t,e,A){return l=a()?Reflect.construct:function(t,e,A){var r=[null];r.push.apply(r,e);var i=new(Function.bind.apply(t,r));return A&&s(i,A.prototype),i},l.apply(null,arguments)}function c(t){var e="function"==typeof Map?new Map:void 0;return c=function(t){if(null===t||(A=t,-1===Function.toString.call(A).indexOf("[native code]")))return t;var A;if("function"!=typeof t)throw new TypeError("Super expression must either be null or a function");if(void 0!==e){if(e.has(t))return e.get(t);e.set(t,r)}function r(){return l(t,arguments,o(this).constructor)}return r.prototype=Object.create(t.prototype,{constructor:{value:r,enumerable:!1,writable:!0,configurable:!0}}),s(r,t)},c(t)}function d(t,e){if(null==t)return{};var A,r,i={},n=Object.keys(t);for(r=0;r<n.length;r++)A=n[r],e.indexOf(A)>=0||(i[A]=t[A]);return i}function u(t,e){(null==e||e>t.length)&&(e=t.length);for(var A=0,r=new Array(e);A<e;A++)r[A]=t[A];return r}function h(t,e){var A="undefined"!=typeof Symbol&&t[Symbol.iterator]||t["@@iterator"];if(A)return(A=A.call(t)).next.bind(A);if(Array.isArray(t)||(A=function(t,e){if(t){if("string"==typeof t)return u(t,e);var A=Object.prototype.toString.call(t).slice(8,-1);return"Object"===A&&t.constructor&&(A=t.constructor.name),"Map"===A||"Set"===A?Array.from(t):"Arguments"===A||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(A)?u(t,e):void 0}}(t))||e&&t&&"number"==typeof t.length){A&&(t=A);var r=0;return function(){return r>=t.length?{done:!0}:{done:!1,value:t[r++]}}}throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}Object.defineProperty(e,"__esModule",{value:!0});var p=function(t){function e(){return t.apply(this,arguments)||this}return n(e,t),e}(c(Error)),f=function(t){function e(e){return t.call(this,"Invalid DateTime: "+e.toMessage())||this}return n(e,t),e}(p),m=function(t){function e(e){return t.call(this,"Invalid Interval: "+e.toMessage())||this}return n(e,t),e}(p),g=function(t){function e(e){return t.call(this,"Invalid Duration: "+e.toMessage())||this}return n(e,t),e}(p),v=function(t){function e(){return t.apply(this,arguments)||this}return n(e,t),e}(p),y=function(t){function e(e){return t.call(this,"Invalid unit "+e)||this}return n(e,t),e}(p),_=function(t){function e(){return t.apply(this,arguments)||this}return n(e,t),e}(p),b=function(t){function e(){return t.call(this,"Zone is an abstract class")||this}return n(e,t),e}(p),w="numeric",k="short",x="long",S={year:w,month:w,day:w},C={year:w,month:k,day:w},E={year:w,month:k,day:w,weekday:k},P={year:w,month:x,day:w},T={year:w,month:x,day:w,weekday:x},O={hour:w,minute:w},N={hour:w,minute:w,second:w},$={hour:w,minute:w,second:w,timeZoneName:k},I={hour:w,minute:w,second:w,timeZoneName:x},M={hour:w,minute:w,hourCycle:"h23"},D={hour:w,minute:w,second:w,hourCycle:"h23"},L={hour:w,minute:w,second:w,hourCycle:"h23",timeZoneName:k},B={hour:w,minute:w,second:w,hourCycle:"h23",timeZoneName:x},z={year:w,month:w,day:w,hour:w,minute:w},R={year:w,month:w,day:w,hour:w,minute:w,second:w},F={year:w,month:k,day:w,hour:w,minute:w},H={year:w,month:k,day:w,hour:w,minute:w,second:w},U={year:w,month:k,day:w,weekday:k,hour:w,minute:w},V={year:w,month:x,day:w,hour:w,minute:w,timeZoneName:k},q={year:w,month:x,day:w,hour:w,minute:w,second:w,timeZoneName:k},Y={year:w,month:x,day:w,weekday:x,hour:w,minute:w,timeZoneName:x},j={year:w,month:x,day:w,weekday:x,hour:w,minute:w,second:w,timeZoneName:x};function Q(t){return void 0===t}function Z(t){return"number"==typeof t}function G(t){return"number"==typeof t&&t%1==0}function W(){try{return"undefined"!=typeof Intl&&!!Intl.RelativeTimeFormat}catch(t){return!1}}function J(t,e,A){if(0!==t.length)return t.reduce((function(t,r){var i=[e(r),r];return t&&A(t[0],i[0])===t[0]?t:i}),null)[1]}function K(t,e){return Object.prototype.hasOwnProperty.call(t,e)}function X(t,e,A){return G(t)&&t>=e&&t<=A}function tt(t,e){return void 0===e&&(e=2),t<0?"-"+(""+-t).padStart(e,"0"):(""+t).padStart(e,"0")}function et(t){return Q(t)||null===t||""===t?void 0:parseInt(t,10)}function At(t){return Q(t)||null===t||""===t?void 0:parseFloat(t)}function rt(t){if(!Q(t)&&null!==t&&""!==t){var e=1e3*parseFloat("0."+t);return Math.floor(e)}}function it(t,e,A){void 0===A&&(A=!1);var r=Math.pow(10,e);return(A?Math.trunc:Math.round)(t*r)/r}function nt(t){return t%4==0&&(t%100!=0||t%400==0)}function ot(t){return nt(t)?366:365}function st(t,e){var A,r=(A=e-1)-12*Math.floor(A/12)+1;return 2===r?nt(t+(e-r)/12)?29:28:[31,null,31,30,31,30,31,31,30,31,30,31][r-1]}function at(t){var e=Date.UTC(t.year,t.month-1,t.day,t.hour,t.minute,t.second,t.millisecond);return t.year<100&&t.year>=0&&(e=new Date(e)).setUTCFullYear(e.getUTCFullYear()-1900),+e}function lt(t){var e=(t+Math.floor(t/4)-Math.floor(t/100)+Math.floor(t/400))%7,A=t-1,r=(A+Math.floor(A/4)-Math.floor(A/100)+Math.floor(A/400))%7;return 4===e||3===r?53:52}function ct(t){return t>99?t:t>60?1900+t:2e3+t}function dt(t,e,A,r){void 0===r&&(r=null);var n=new Date(t),o={hourCycle:"h23",year:"numeric",month:"2-digit",day:"2-digit",hour:"2-digit",minute:"2-digit"};r&&(o.timeZone=r);var s=i({timeZoneName:e},o),a=new Intl.DateTimeFormat(A,s).formatToParts(n).find((function(t){return"timezonename"===t.type.toLowerCase()}));return a?a.value:null}function ut(t,e){var A=parseInt(t,10);Number.isNaN(A)&&(A=0);var r=parseInt(e,10)||0;return 60*A+(A<0||Object.is(A,-0)?-r:r)}function ht(t){var e=Number(t);if("boolean"==typeof t||""===t||Number.isNaN(e))throw new _("Invalid unit value "+t);return e}function pt(t,e){var A={};for(var r in t)if(K(t,r)){var i=t[r];if(null==i)continue;A[e(r)]=ht(i)}return A}function ft(t,e){var A=Math.trunc(Math.abs(t/60)),r=Math.trunc(Math.abs(t%60)),i=t>=0?"+":"-";switch(e){case"short":return""+i+tt(A,2)+":"+tt(r,2);case"narrow":return""+i+A+(r>0?":"+r:"");case"techie":return""+i+tt(A,2)+tt(r,2);default:throw new RangeError("Value format "+e+" is out of range for property format")}}function mt(t){return function(t,e){return["hour","minute","second","millisecond"].reduce((function(e,A){return e[A]=t[A],e}),{})}(t)}var gt=/[A-Za-z_+-]{1,256}(:?\/[A-Za-z0-9_+-]{1,256}(\/[A-Za-z0-9_+-]{1,256})?)?/,vt=["January","February","March","April","May","June","July","August","September","October","November","December"],yt=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],_t=["J","F","M","A","M","J","J","A","S","O","N","D"];function bt(t){switch(t){case"narrow":return[].concat(_t);case"short":return[].concat(yt);case"long":return[].concat(vt);case"numeric":return["1","2","3","4","5","6","7","8","9","10","11","12"];case"2-digit":return["01","02","03","04","05","06","07","08","09","10","11","12"];default:return null}}var wt=["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],kt=["Mon","Tue","Wed","Thu","Fri","Sat","Sun"],xt=["M","T","W","T","F","S","S"];function St(t){switch(t){case"narrow":return[].concat(xt);case"short":return[].concat(kt);case"long":return[].concat(wt);case"numeric":return["1","2","3","4","5","6","7"];default:return null}}var Ct=["AM","PM"],Et=["Before Christ","Anno Domini"],Pt=["BC","AD"],Tt=["B","A"];function Ot(t){switch(t){case"narrow":return[].concat(Tt);case"short":return[].concat(Pt);case"long":return[].concat(Et);default:return null}}function Nt(t,e){for(var A,r="",i=h(t);!(A=i()).done;){var n=A.value;n.literal?r+=n.val:r+=e(n.val)}return r}var $t={D:S,DD:C,DDD:P,DDDD:T,t:O,tt:N,ttt:$,tttt:I,T:M,TT:D,TTT:L,TTTT:B,f:z,ff:F,fff:V,ffff:Y,F:R,FF:H,FFF:q,FFFF:j},It=function(){function t(t,e){this.opts=e,this.loc=t,this.systemLoc=null}t.create=function(e,A){return void 0===A&&(A={}),new t(e,A)},t.parseFormat=function(t){for(var e=null,A="",r=!1,i=[],n=0;n<t.length;n++){var o=t.charAt(n);"'"===o?(A.length>0&&i.push({literal:r,val:A}),e=null,A="",r=!r):r||o===e?A+=o:(A.length>0&&i.push({literal:!1,val:A}),A=o,e=o)}return A.length>0&&i.push({literal:r,val:A}),i},t.macroTokenToFormatOpts=function(t){return $t[t]};var e=t.prototype;return e.formatWithSystemDefault=function(t,e){return null===this.systemLoc&&(this.systemLoc=this.loc.redefaultToSystem()),this.systemLoc.dtFormatter(t,i({},this.opts,e)).format()},e.formatDateTime=function(t,e){return void 0===e&&(e={}),this.loc.dtFormatter(t,i({},this.opts,e)).format()},e.formatDateTimeParts=function(t,e){return void 0===e&&(e={}),this.loc.dtFormatter(t,i({},this.opts,e)).formatToParts()},e.resolvedOptions=function(t,e){return void 0===e&&(e={}),this.loc.dtFormatter(t,i({},this.opts,e)).resolvedOptions()},e.num=function(t,e){if(void 0===e&&(e=0),this.opts.forceSimple)return tt(t,e);var A=i({},this.opts);return e>0&&(A.padTo=e),this.loc.numberFormatter(A).format(t)},e.formatDateTimeFromString=function(e,A){var r=this,i="en"===this.loc.listingMode(),n=this.loc.outputCalendar&&"gregory"!==this.loc.outputCalendar,o=function(t,A){return r.loc.extract(e,t,A)},s=function(t){return e.isOffsetFixed&&0===e.offset&&t.allowZ?"Z":e.isValid?e.zone.formatOffset(e.ts,t.format):""},a=function(t,A){return i?function(t,e){return bt(e)[t.month-1]}(e,t):o(A?{month:t}:{month:t,day:"numeric"},"month")},l=function(t,A){return i?function(t,e){return St(e)[t.weekday-1]}(e,t):o(A?{weekday:t}:{weekday:t,month:"long",day:"numeric"},"weekday")},c=function(t){return i?function(t,e){return Ot(e)[t.year<0?0:1]}(e,t):o({era:t},"era")};return Nt(t.parseFormat(A),(function(A){switch(A){case"S":return r.num(e.millisecond);case"u":case"SSS":return r.num(e.millisecond,3);case"s":return r.num(e.second);case"ss":return r.num(e.second,2);case"uu":return r.num(Math.floor(e.millisecond/10),2);case"uuu":return r.num(Math.floor(e.millisecond/100));case"m":return r.num(e.minute);case"mm":return r.num(e.minute,2);case"h":return r.num(e.hour%12==0?12:e.hour%12);case"hh":return r.num(e.hour%12==0?12:e.hour%12,2);case"H":return r.num(e.hour);case"HH":return r.num(e.hour,2);case"Z":return s({format:"narrow",allowZ:r.opts.allowZ});case"ZZ":return s({format:"short",allowZ:r.opts.allowZ});case"ZZZ":return s({format:"techie",allowZ:r.opts.allowZ});case"ZZZZ":return e.zone.offsetName(e.ts,{format:"short",locale:r.loc.locale});case"ZZZZZ":return e.zone.offsetName(e.ts,{format:"long",locale:r.loc.locale});case"z":return e.zoneName;case"a":return i?function(t){return Ct[t.hour<12?0:1]}(e):o({hour:"numeric",hourCycle:"h12"},"dayperiod");case"d":return n?o({day:"numeric"},"day"):r.num(e.day);case"dd":return n?o({day:"2-digit"},"day"):r.num(e.day,2);case"c":case"E":return r.num(e.weekday);case"ccc":return l("short",!0);case"cccc":return l("long",!0);case"ccccc":return l("narrow",!0);case"EEE":return l("short",!1);case"EEEE":return l("long",!1);case"EEEEE":return l("narrow",!1);case"L":return n?o({month:"numeric",day:"numeric"},"month"):r.num(e.month);case"LL":return n?o({month:"2-digit",day:"numeric"},"month"):r.num(e.month,2);case"LLL":return a("short",!0);case"LLLL":return a("long",!0);case"LLLLL":return a("narrow",!0);case"M":return n?o({month:"numeric"},"month"):r.num(e.month);case"MM":return n?o({month:"2-digit"},"month"):r.num(e.month,2);case"MMM":return a("short",!1);case"MMMM":return a("long",!1);case"MMMMM":return a("narrow",!1);case"y":return n?o({year:"numeric"},"year"):r.num(e.year);case"yy":return n?o({year:"2-digit"},"year"):r.num(e.year.toString().slice(-2),2);case"yyyy":return n?o({year:"numeric"},"year"):r.num(e.year,4);case"yyyyyy":return n?o({year:"numeric"},"year"):r.num(e.year,6);case"G":return c("short");case"GG":return c("long");case"GGGGG":return c("narrow");case"kk":return r.num(e.weekYear.toString().slice(-2),2);case"kkkk":return r.num(e.weekYear,4);case"W":return r.num(e.weekNumber);case"WW":return r.num(e.weekNumber,2);case"o":return r.num(e.ordinal);case"ooo":return r.num(e.ordinal,3);case"q":return r.num(e.quarter);case"qq":return r.num(e.quarter,2);case"X":return r.num(Math.floor(e.ts/1e3));case"x":return r.num(e.ts);default:return function(A){var i=t.macroTokenToFormatOpts(A);return i?r.formatWithSystemDefault(e,i):A}(A)}}))},e.formatDurationFromString=function(e,A){var r,i=this,n=function(t){switch(t[0]){case"S":return"millisecond";case"s":return"second";case"m":return"minute";case"h":return"hour";case"d":return"day";case"M":return"month";case"y":return"year";default:return null}},o=t.parseFormat(A),s=o.reduce((function(t,e){var A=e.literal,r=e.val;return A?t:t.concat(r)}),[]),a=e.shiftTo.apply(e,s.map(n).filter((function(t){return t})));return Nt(o,(r=a,function(t){var e=n(t);return e?i.num(r.get(e),t.length):t}))},t}(),Mt=function(){function t(t,e){this.reason=t,this.explanation=e}return t.prototype.toMessage=function(){return this.explanation?this.reason+": "+this.explanation:this.reason},t}(),Dt=function(){function t(){}var e=t.prototype;return e.offsetName=function(t,e){throw new b},e.formatOffset=function(t,e){throw new b},e.offset=function(t){throw new b},e.equals=function(t){throw new b},r(t,[{key:"type",get:function(){throw new b}},{key:"name",get:function(){throw new b}},{key:"isUniversal",get:function(){throw new b}},{key:"isValid",get:function(){throw new b}}]),t}(),Lt=null,Bt=function(t){function e(){return t.apply(this,arguments)||this}n(e,t);var A=e.prototype;return A.offsetName=function(t,e){return dt(t,e.format,e.locale)},A.formatOffset=function(t,e){return ft(this.offset(t),e)},A.offset=function(t){return-new Date(t).getTimezoneOffset()},A.equals=function(t){return"system"===t.type},r(e,[{key:"type",get:function(){return"system"}},{key:"name",get:function(){return(new Intl.DateTimeFormat).resolvedOptions().timeZone}},{key:"isUniversal",get:function(){return!1}},{key:"isValid",get:function(){return!0}}],[{key:"instance",get:function(){return null===Lt&&(Lt=new e),Lt}}]),e}(Dt),zt=RegExp("^"+gt.source+"$"),Rt={},Ft={year:0,month:1,day:2,hour:3,minute:4,second:5},Ht={},Ut=function(t){function e(A){var r;return(r=t.call(this)||this).zoneName=A,r.valid=e.isValidZone(A),r}n(e,t),e.create=function(t){return Ht[t]||(Ht[t]=new e(t)),Ht[t]},e.resetCache=function(){Ht={},Rt={}},e.isValidSpecifier=function(t){return!(!t||!t.match(zt))},e.isValidZone=function(t){if(!t)return!1;try{return new Intl.DateTimeFormat("en-US",{timeZone:t}).format(),!0}catch(t){return!1}};var A=e.prototype;return A.offsetName=function(t,e){return dt(t,e.format,e.locale,this.name)},A.formatOffset=function(t,e){return ft(this.offset(t),e)},A.offset=function(t){var e=new Date(t);if(isNaN(e))return NaN;var A,r=(A=this.name,Rt[A]||(Rt[A]=new Intl.DateTimeFormat("en-US",{hour12:!1,timeZone:A,year:"numeric",month:"2-digit",day:"2-digit",hour:"2-digit",minute:"2-digit",second:"2-digit"})),Rt[A]),i=r.formatToParts?function(t,e){for(var A=t.formatToParts(e),r=[],i=0;i<A.length;i++){var n=A[i],o=n.type,s=n.value,a=Ft[o];Q(a)||(r[a]=parseInt(s,10))}return r}(r,e):function(t,e){var A=t.format(e).replace(/\u200E/g,""),r=/(\d+)\/(\d+)\/(\d+),? (\d+):(\d+):(\d+)/.exec(A),i=r[1],n=r[2];return[r[3],i,n,r[4],r[5],r[6]]}(r,e),n=i[0],o=i[1],s=i[2],a=i[3],l=+e,c=l%1e3;return(at({year:n,month:o,day:s,hour:24===a?0:a,minute:i[4],second:i[5],millisecond:0})-(l-=c>=0?c:1e3+c))/6e4},A.equals=function(t){return"iana"===t.type&&t.name===this.name},r(e,[{key:"type",get:function(){return"iana"}},{key:"name",get:function(){return this.zoneName}},{key:"isUniversal",get:function(){return!1}},{key:"isValid",get:function(){return this.valid}}]),e}(Dt),Vt=null,qt=function(t){function e(e){var A;return(A=t.call(this)||this).fixed=e,A}n(e,t),e.instance=function(t){return 0===t?e.utcInstance:new e(t)},e.parseSpecifier=function(t){if(t){var A=t.match(/^utc(?:([+-]\d{1,2})(?::(\d{2}))?)?$/i);if(A)return new e(ut(A[1],A[2]))}return null};var A=e.prototype;return A.offsetName=function(){return this.name},A.formatOffset=function(t,e){return ft(this.fixed,e)},A.offset=function(){return this.fixed},A.equals=function(t){return"fixed"===t.type&&t.fixed===this.fixed},r(e,[{key:"type",get:function(){return"fixed"}},{key:"name",get:function(){return 0===this.fixed?"UTC":"UTC"+ft(this.fixed,"narrow")}},{key:"isUniversal",get:function(){return!0}},{key:"isValid",get:function(){return!0}}],[{key:"utcInstance",get:function(){return null===Vt&&(Vt=new e(0)),Vt}}]),e}(Dt),Yt=function(t){function e(e){var A;return(A=t.call(this)||this).zoneName=e,A}n(e,t);var A=e.prototype;return A.offsetName=function(){return null},A.formatOffset=function(){return""},A.offset=function(){return NaN},A.equals=function(){return!1},r(e,[{key:"type",get:function(){return"invalid"}},{key:"name",get:function(){return this.zoneName}},{key:"isUniversal",get:function(){return!1}},{key:"isValid",get:function(){return!1}}]),e}(Dt);function jt(t,e){if(Q(t)||null===t)return e;if(t instanceof Dt)return t;if("string"==typeof t){var A=t.toLowerCase();return"local"===A||"system"===A?e:"utc"===A||"gmt"===A?qt.utcInstance:Ut.isValidSpecifier(A)?Ut.create(t):qt.parseSpecifier(A)||new Yt(t)}return Z(t)?qt.instance(t):"object"==typeof t&&t.offset&&"number"==typeof t.offset?t:new Yt(t)}var Qt,Zt=function(){return Date.now()},Gt="system",Wt=null,Jt=null,Kt=null,Xt=function(){function t(){}return t.resetCaches=function(){ue.resetCache(),Ut.resetCache()},r(t,null,[{key:"now",get:function(){return Zt},set:function(t){Zt=t}},{key:"defaultZone",get:function(){return jt(Gt,Bt.instance)},set:function(t){Gt=t}},{key:"defaultLocale",get:function(){return Wt},set:function(t){Wt=t}},{key:"defaultNumberingSystem",get:function(){return Jt},set:function(t){Jt=t}},{key:"defaultOutputCalendar",get:function(){return Kt},set:function(t){Kt=t}},{key:"throwOnInvalid",get:function(){return Qt},set:function(t){Qt=t}}]),t}(),te=["base"],ee=["padTo","floor"],Ae={},re={};function ie(t,e){void 0===e&&(e={});var A=JSON.stringify([t,e]),r=re[A];return r||(r=new Intl.DateTimeFormat(t,e),re[A]=r),r}var ne={},oe={},se=null;function ae(t,e,A,r,i){var n=t.listingMode(A);return"error"===n?null:"en"===n?r(e):i(e)}var le=function(){function t(t,e,A){this.padTo=A.padTo||0,this.floor=A.floor||!1,A.padTo,A.floor;var r=d(A,ee);if(!e||Object.keys(r).length>0){var n=i({useGrouping:!1},A);A.padTo>0&&(n.minimumIntegerDigits=A.padTo),this.inf=function(t,e){void 0===e&&(e={});var A=JSON.stringify([t,e]),r=ne[A];return r||(r=new Intl.NumberFormat(t,e),ne[A]=r),r}(t,n)}}return t.prototype.format=function(t){if(this.inf){var e=this.floor?Math.floor(t):t;return this.inf.format(e)}return tt(this.floor?Math.floor(t):it(t,3),this.padTo)},t}(),ce=function(){function t(t,e,A){var r;if(this.opts=A,t.zone.isUniversal){var n=t.offset/60*-1,o=n>=0?"Etc/GMT+"+n:"Etc/GMT"+n;0!==t.offset&&Ut.create(o).valid?(r=o,this.dt=t):(r="UTC",A.timeZoneName?this.dt=t:this.dt=0===t.offset?t:cr.fromMillis(t.ts+60*t.offset*1e3))}else"system"===t.zone.type?this.dt=t:(this.dt=t,r=t.zone.name);var s=i({},this.opts);r&&(s.timeZone=r),this.dtf=ie(e,s)}var e=t.prototype;return e.format=function(){return this.dtf.format(this.dt.toJSDate())},e.formatToParts=function(){return this.dtf.formatToParts(this.dt.toJSDate())},e.resolvedOptions=function(){return this.dtf.resolvedOptions()},t}(),de=function(){function t(t,e,A){this.opts=i({style:"long"},A),!e&&W()&&(this.rtf=function(t,e){void 0===e&&(e={});var A=e;A.base;var r=d(A,te),i=JSON.stringify([t,r]),n=oe[i];return n||(n=new Intl.RelativeTimeFormat(t,e),oe[i]=n),n}(t,A))}var e=t.prototype;return e.format=function(t,e){return this.rtf?this.rtf.format(t,e):function(t,e,A,r){void 0===A&&(A="always"),void 0===r&&(r=!1);var i={years:["year","yr."],quarters:["quarter","qtr."],months:["month","mo."],weeks:["week","wk."],days:["day","day","days"],hours:["hour","hr."],minutes:["minute","min."],seconds:["second","sec."]},n=-1===["hours","minutes","seconds"].indexOf(t);if("auto"===A&&n){var o="days"===t;switch(e){case 1:return o?"tomorrow":"next "+i[t][0];case-1:return o?"yesterday":"last "+i[t][0];case 0:return o?"today":"this "+i[t][0]}}var s=Object.is(e,-0)||e<0,a=Math.abs(e),l=1===a,c=i[t],d=r?l?c[1]:c[2]||c[1]:l?i[t][0]:t;return s?a+" "+d+" ago":"in "+a+" "+d}(e,t,this.opts.numeric,"long"!==this.opts.style)},e.formatToParts=function(t,e){return this.rtf?this.rtf.formatToParts(t,e):[]},t}(),ue=function(){function t(t,e,A,r){var i=function(t){var e=t.indexOf("-u-");if(-1===e)return[t];var A,r=t.substring(0,e);try{A=ie(t).resolvedOptions()}catch(t){A=ie(r).resolvedOptions()}var i=A;return[r,i.numberingSystem,i.calendar]}(t),n=i[0],o=i[1],s=i[2];this.locale=n,this.numberingSystem=e||o||null,this.outputCalendar=A||s||null,this.intl=function(t,e,A){return A||e?(t+="-u",A&&(t+="-ca-"+A),e&&(t+="-nu-"+e),t):t}(this.locale,this.numberingSystem,this.outputCalendar),this.weekdaysCache={format:{},standalone:{}},this.monthsCache={format:{},standalone:{}},this.meridiemCache=null,this.eraCache={},this.specifiedLocale=r,this.fastNumbersCached=null}t.fromOpts=function(e){return t.create(e.locale,e.numberingSystem,e.outputCalendar,e.defaultToEN)},t.create=function(e,A,r,i){void 0===i&&(i=!1);var n=e||Xt.defaultLocale;return new t(n||(i?"en-US":se||(se=(new Intl.DateTimeFormat).resolvedOptions().locale)),A||Xt.defaultNumberingSystem,r||Xt.defaultOutputCalendar,n)},t.resetCache=function(){se=null,re={},ne={},oe={}},t.fromObject=function(e){var A=void 0===e?{}:e,r=A.locale,i=A.numberingSystem,n=A.outputCalendar;return t.create(r,i,n)};var e=t.prototype;return e.listingMode=function(){var t=this.isEnglish(),e=!(null!==this.numberingSystem&&"latn"!==this.numberingSystem||null!==this.outputCalendar&&"gregory"!==this.outputCalendar);return t&&e?"en":"intl"},e.clone=function(e){return e&&0!==Object.getOwnPropertyNames(e).length?t.create(e.locale||this.specifiedLocale,e.numberingSystem||this.numberingSystem,e.outputCalendar||this.outputCalendar,e.defaultToEN||!1):this},e.redefaultToEN=function(t){return void 0===t&&(t={}),this.clone(i({},t,{defaultToEN:!0}))},e.redefaultToSystem=function(t){return void 0===t&&(t={}),this.clone(i({},t,{defaultToEN:!1}))},e.months=function(t,e,A){var r=this;return void 0===e&&(e=!1),void 0===A&&(A=!0),ae(this,t,A,bt,(function(){var A=e?{month:t,day:"numeric"}:{month:t},i=e?"format":"standalone";return r.monthsCache[i][t]||(r.monthsCache[i][t]=function(t){for(var e=[],A=1;A<=12;A++){var r=cr.utc(2016,A,1);e.push(t(r))}return e}((function(t){return r.extract(t,A,"month")}))),r.monthsCache[i][t]}))},e.weekdays=function(t,e,A){var r=this;return void 0===e&&(e=!1),void 0===A&&(A=!0),ae(this,t,A,St,(function(){var A=e?{weekday:t,year:"numeric",month:"long",day:"numeric"}:{weekday:t},i=e?"format":"standalone";return r.weekdaysCache[i][t]||(r.weekdaysCache[i][t]=function(t){for(var e=[],A=1;A<=7;A++){var r=cr.utc(2016,11,13+A);e.push(t(r))}return e}((function(t){return r.extract(t,A,"weekday")}))),r.weekdaysCache[i][t]}))},e.meridiems=function(t){var e=this;return void 0===t&&(t=!0),ae(this,void 0,t,(function(){return Ct}),(function(){if(!e.meridiemCache){var t={hour:"numeric",hourCycle:"h12"};e.meridiemCache=[cr.utc(2016,11,13,9),cr.utc(2016,11,13,19)].map((function(A){return e.extract(A,t,"dayperiod")}))}return e.meridiemCache}))},e.eras=function(t,e){var A=this;return void 0===e&&(e=!0),ae(this,t,e,Ot,(function(){var e={era:t};return A.eraCache[t]||(A.eraCache[t]=[cr.utc(-40,1,1),cr.utc(2017,1,1)].map((function(t){return A.extract(t,e,"era")}))),A.eraCache[t]}))},e.extract=function(t,e,A){var r=this.dtFormatter(t,e).formatToParts().find((function(t){return t.type.toLowerCase()===A}));return r?r.value:null},e.numberFormatter=function(t){return void 0===t&&(t={}),new le(this.intl,t.forceSimple||this.fastNumbers,t)},e.dtFormatter=function(t,e){return void 0===e&&(e={}),new ce(t,this.intl,e)},e.relFormatter=function(t){return void 0===t&&(t={}),new de(this.intl,this.isEnglish(),t)},e.listFormatter=function(t){return void 0===t&&(t={}),function(t,e){void 0===e&&(e={});var A=JSON.stringify([t,e]),r=Ae[A];return r||(r=new Intl.ListFormat(t,e),Ae[A]=r),r}(this.intl,t)},e.isEnglish=function(){return"en"===this.locale||"en-us"===this.locale.toLowerCase()||new Intl.DateTimeFormat(this.intl).resolvedOptions().locale.startsWith("en-us")},e.equals=function(t){return this.locale===t.locale&&this.numberingSystem===t.numberingSystem&&this.outputCalendar===t.outputCalendar},r(t,[{key:"fastNumbers",get:function(){var t;return null==this.fastNumbersCached&&(this.fastNumbersCached=(!(t=this).numberingSystem||"latn"===t.numberingSystem)&&("latn"===t.numberingSystem||!t.locale||t.locale.startsWith("en")||"latn"===new Intl.DateTimeFormat(t.intl).resolvedOptions().numberingSystem)),this.fastNumbersCached}}]),t}();function he(){for(var t=arguments.length,e=new Array(t),A=0;A<t;A++)e[A]=arguments[A];var r=e.reduce((function(t,e){return t+e.source}),"");return RegExp("^"+r+"$")}function pe(){for(var t=arguments.length,e=new Array(t),A=0;A<t;A++)e[A]=arguments[A];return function(t){return e.reduce((function(e,A){var r=e[0],n=e[1],o=e[2],s=A(t,o),a=s[0],l=s[1],c=s[2];return[i({},r,a),n||l,c]}),[{},null,1]).slice(0,2)}}function fe(t){if(null==t)return[null,null];for(var e=arguments.length,A=new Array(e>1?e-1:0),r=1;r<e;r++)A[r-1]=arguments[r];for(var i=0,n=A;i<n.length;i++){var o=n[i],s=o[0],a=o[1],l=s.exec(t);if(l)return a(l)}return[null,null]}function me(){for(var t=arguments.length,e=new Array(t),A=0;A<t;A++)e[A]=arguments[A];return function(t,A){var r,i={};for(r=0;r<e.length;r++)i[e[r]]=et(t[A+r]);return[i,null,A+r]}}var ge=/(?:(Z)|([+-]\d\d)(?::?(\d\d))?)/,ve=/(\d\d)(?::?(\d\d)(?::?(\d\d)(?:[.,](\d{1,30}))?)?)?/,ye=RegExp(""+ve.source+ge.source+"?"),_e=RegExp("(?:T"+ye.source+")?"),be=me("weekYear","weekNumber","weekDay"),we=me("year","ordinal"),ke=RegExp(ve.source+" ?(?:"+ge.source+"|("+gt.source+"))?"),xe=RegExp("(?: "+ke.source+")?");function Se(t,e,A){var r=t[e];return Q(r)?A:et(r)}function Ce(t,e){return[{year:Se(t,e),month:Se(t,e+1,1),day:Se(t,e+2,1)},null,e+3]}function Ee(t,e){return[{hours:Se(t,e,0),minutes:Se(t,e+1,0),seconds:Se(t,e+2,0),milliseconds:rt(t[e+3])},null,e+4]}function Pe(t,e){var A=!t[e]&&!t[e+1],r=ut(t[e+1],t[e+2]);return[{},A?null:qt.instance(r),e+3]}function Te(t,e){return[{},t[e]?Ut.create(t[e]):null,e+1]}var Oe=RegExp("^T?"+ve.source+"$"),Ne=/^-?P(?:(?:(-?\d{1,9}(?:\.\d{1,9})?)Y)?(?:(-?\d{1,9}(?:\.\d{1,9})?)M)?(?:(-?\d{1,9}(?:\.\d{1,9})?)W)?(?:(-?\d{1,9}(?:\.\d{1,9})?)D)?(?:T(?:(-?\d{1,9}(?:\.\d{1,9})?)H)?(?:(-?\d{1,9}(?:\.\d{1,9})?)M)?(?:(-?\d{1,20})(?:[.,](-?\d{1,9}))?S)?)?)$/;function $e(t){var e=t[0],A=t[1],r=t[2],i=t[3],n=t[4],o=t[5],s=t[6],a=t[7],l=t[8],c="-"===e[0],d=a&&"-"===a[0],u=function(t,e){return void 0===e&&(e=!1),void 0!==t&&(e||t&&c)?-t:t};return[{years:u(At(A)),months:u(At(r)),weeks:u(At(i)),days:u(At(n)),hours:u(At(o)),minutes:u(At(s)),seconds:u(At(a),"-0"===a),milliseconds:u(rt(l),d)}]}var Ie={GMT:0,EDT:-240,EST:-300,CDT:-300,CST:-360,MDT:-360,MST:-420,PDT:-420,PST:-480};function Me(t,e,A,r,i,n,o){var s={year:2===e.length?ct(et(e)):et(e),month:yt.indexOf(A)+1,day:et(r),hour:et(i),minute:et(n)};return o&&(s.second=et(o)),t&&(s.weekday=t.length>3?wt.indexOf(t)+1:kt.indexOf(t)+1),s}var De=/^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|(?:([+-]\d\d)(\d\d)))$/;function Le(t){var e,A=t[1],r=t[2],i=t[3],n=t[4],o=t[5],s=t[6],a=t[7],l=t[8],c=t[9],d=t[10],u=t[11],h=Me(A,n,i,r,o,s,a);return e=l?Ie[l]:c?0:ut(d,u),[h,new qt(e)]}var Be=/^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), (\d\d) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) (\d{4}) (\d\d):(\d\d):(\d\d) GMT$/,ze=/^(Monday|Tuesday|Wedsday|Thursday|Friday|Saturday|Sunday), (\d\d)-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\d\d) (\d\d):(\d\d):(\d\d) GMT$/,Re=/^(Mon|Tue|Wed|Thu|Fri|Sat|Sun) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ( \d|\d\d) (\d\d):(\d\d):(\d\d) (\d{4})$/;function Fe(t){var e=t[1],A=t[2],r=t[3];return[Me(e,t[4],r,A,t[5],t[6],t[7]),qt.utcInstance]}function He(t){var e=t[1],A=t[2],r=t[3],i=t[4],n=t[5],o=t[6];return[Me(e,t[7],A,r,i,n,o),qt.utcInstance]}var Ue=he(/([+-]\d{6}|\d{4})(?:-?(\d\d)(?:-?(\d\d))?)?/,_e),Ve=he(/(\d{4})-?W(\d\d)(?:-?(\d))?/,_e),qe=he(/(\d{4})-?(\d{3})/,_e),Ye=he(ye),je=pe(Ce,Ee,Pe),Qe=pe(be,Ee,Pe),Ze=pe(we,Ee,Pe),Ge=pe(Ee,Pe),We=pe(Ee),Je=he(/(\d{4})-(\d\d)-(\d\d)/,xe),Ke=he(ke),Xe=pe(Ce,Ee,Pe,Te),tA=pe(Ee,Pe,Te),eA={weeks:{days:7,hours:168,minutes:10080,seconds:604800,milliseconds:6048e5},days:{hours:24,minutes:1440,seconds:86400,milliseconds:864e5},hours:{minutes:60,seconds:3600,milliseconds:36e5},minutes:{seconds:60,milliseconds:6e4},seconds:{milliseconds:1e3}},AA=i({years:{quarters:4,months:12,weeks:52,days:365,hours:8760,minutes:525600,seconds:31536e3,milliseconds:31536e6},quarters:{months:3,weeks:13,days:91,hours:2184,minutes:131040,seconds:7862400,milliseconds:78624e5},months:{weeks:4,days:30,hours:720,minutes:43200,seconds:2592e3,milliseconds:2592e6}},eA),rA=i({years:{quarters:4,months:12,weeks:52.1775,days:365.2425,hours:8765.82,minutes:525949.2,seconds:525949.2*60,milliseconds:525949.2*60*1e3},quarters:{months:3,weeks:13.044375,days:91.310625,hours:2191.455,minutes:131487.3,seconds:525949.2*60/4,milliseconds:7889237999.999999},months:{weeks:4.3481250000000005,days:30.436875,hours:730.485,minutes:43829.1,seconds:2629746,milliseconds:2629746e3}},eA),iA=["years","quarters","months","weeks","days","hours","minutes","seconds","milliseconds"],nA=iA.slice(0).reverse();function oA(t,e,A){void 0===A&&(A=!1);var r={values:A?e.values:i({},t.values,e.values||{}),loc:t.loc.clone(e.loc),conversionAccuracy:e.conversionAccuracy||t.conversionAccuracy};return new aA(r)}function sA(t,e,A,r,i){var n=t[i][A],o=e[A]/n,s=Math.sign(o)!==Math.sign(r[i])&&0!==r[i]&&Math.abs(o)<=1?function(t){return t<0?Math.floor(t):Math.ceil(t)}(o):Math.trunc(o);r[i]+=s,e[A]-=s*n}var aA=function(){function t(t){var e="longterm"===t.conversionAccuracy||!1;this.values=t.values,this.loc=t.loc||ue.create(),this.conversionAccuracy=e?"longterm":"casual",this.invalid=t.invalid||null,this.matrix=e?rA:AA,this.isLuxonDuration=!0}t.fromMillis=function(e,A){return t.fromObject({milliseconds:e},A)},t.fromObject=function(e,A){if(void 0===A&&(A={}),null==e||"object"!=typeof e)throw new _("Duration.fromObject: argument expected to be an object, got "+(null===e?"null":typeof e));return new t({values:pt(e,t.normalizeUnit),loc:ue.fromObject(A),conversionAccuracy:A.conversionAccuracy})},t.fromDurationLike=function(e){if(Z(e))return t.fromMillis(e);if(t.isDuration(e))return e;if("object"==typeof e)return t.fromObject(e);throw new _("Unknown duration argument "+e+" of type "+typeof e)},t.fromISO=function(e,A){var r=function(t){return fe(t,[Ne,$e])}(e),i=r[0];return i?t.fromObject(i,A):t.invalid("unparsable",'the input "'+e+"\" can't be parsed as ISO 8601")},t.fromISOTime=function(e,A){var r=function(t){return fe(t,[Oe,We])}(e),i=r[0];return i?t.fromObject(i,A):t.invalid("unparsable",'the input "'+e+"\" can't be parsed as ISO 8601")},t.invalid=function(e,A){if(void 0===A&&(A=null),!e)throw new _("need to specify a reason the Duration is invalid");var r=e instanceof Mt?e:new Mt(e,A);if(Xt.throwOnInvalid)throw new g(r);return new t({invalid:r})},t.normalizeUnit=function(t){var e={year:"years",years:"years",quarter:"quarters",quarters:"quarters",month:"months",months:"months",week:"weeks",weeks:"weeks",day:"days",days:"days",hour:"hours",hours:"hours",minute:"minutes",minutes:"minutes",second:"seconds",seconds:"seconds",millisecond:"milliseconds",milliseconds:"milliseconds"}[t?t.toLowerCase():t];if(!e)throw new y(t);return e},t.isDuration=function(t){return t&&t.isLuxonDuration||!1};var e=t.prototype;return e.toFormat=function(t,e){void 0===e&&(e={});var A=i({},e,{floor:!1!==e.round&&!1!==e.floor});return this.isValid?It.create(this.loc,A).formatDurationFromString(this,t):"Invalid Duration"},e.toHuman=function(t){var e=this;void 0===t&&(t={});var A=iA.map((function(A){var r=e.values[A];return Q(r)?null:e.loc.numberFormatter(i({style:"unit",unitDisplay:"long"},t,{unit:A.slice(0,-1)})).format(r)})).filter((function(t){return t}));return this.loc.listFormatter(i({type:"conjunction",style:t.listStyle||"narrow"},t)).format(A)},e.toObject=function(){return this.isValid?i({},this.values):{}},e.toISO=function(){if(!this.isValid)return null;var t="P";return 0!==this.years&&(t+=this.years+"Y"),0===this.months&&0===this.quarters||(t+=this.months+3*this.quarters+"M"),0!==this.weeks&&(t+=this.weeks+"W"),0!==this.days&&(t+=this.days+"D"),0===this.hours&&0===this.minutes&&0===this.seconds&&0===this.milliseconds||(t+="T"),0!==this.hours&&(t+=this.hours+"H"),0!==this.minutes&&(t+=this.minutes+"M"),0===this.seconds&&0===this.milliseconds||(t+=it(this.seconds+this.milliseconds/1e3,3)+"S"),"P"===t&&(t+="T0S"),t},e.toISOTime=function(t){if(void 0===t&&(t={}),!this.isValid)return null;var e=this.toMillis();if(e<0||e>=864e5)return null;t=i({suppressMilliseconds:!1,suppressSeconds:!1,includePrefix:!1,format:"extended"},t);var A=this.shiftTo("hours","minutes","seconds","milliseconds"),r="basic"===t.format?"hhmm":"hh:mm";t.suppressSeconds&&0===A.seconds&&0===A.milliseconds||(r+="basic"===t.format?"ss":":ss",t.suppressMilliseconds&&0===A.milliseconds||(r+=".SSS"));var n=A.toFormat(r);return t.includePrefix&&(n="T"+n),n},e.toJSON=function(){return this.toISO()},e.toString=function(){return this.toISO()},e.toMillis=function(){return this.as("milliseconds")},e.valueOf=function(){return this.toMillis()},e.plus=function(e){if(!this.isValid)return this;for(var A,r=t.fromDurationLike(e),i={},n=h(iA);!(A=n()).done;){var o=A.value;(K(r.values,o)||K(this.values,o))&&(i[o]=r.get(o)+this.get(o))}return oA(this,{values:i},!0)},e.minus=function(e){if(!this.isValid)return this;var A=t.fromDurationLike(e);return this.plus(A.negate())},e.mapUnits=function(t){if(!this.isValid)return this;for(var e={},A=0,r=Object.keys(this.values);A<r.length;A++){var i=r[A];e[i]=ht(t(this.values[i],i))}return oA(this,{values:e},!0)},e.get=function(e){return this[t.normalizeUnit(e)]},e.set=function(e){return this.isValid?oA(this,{values:i({},this.values,pt(e,t.normalizeUnit))}):this},e.reconfigure=function(t){var e=void 0===t?{}:t,A=e.locale,r=e.numberingSystem,i=e.conversionAccuracy,n={loc:this.loc.clone({locale:A,numberingSystem:r})};return i&&(n.conversionAccuracy=i),oA(this,n)},e.as=function(t){return this.isValid?this.shiftTo(t).get(t):NaN},e.normalize=function(){if(!this.isValid)return this;var t=this.toObject();return function(t,e){nA.reduce((function(A,r){return Q(e[r])?A:(A&&sA(t,e,A,e,r),r)}),null)}(this.matrix,t),oA(this,{values:t},!0)},e.shiftTo=function(){for(var e=arguments.length,A=new Array(e),r=0;r<e;r++)A[r]=arguments[r];if(!this.isValid)return this;if(0===A.length)return this;A=A.map((function(e){return t.normalizeUnit(e)}));for(var i,n,o={},s={},a=this.toObject(),l=h(iA);!(n=l()).done;){var c=n.value;if(A.indexOf(c)>=0){i=c;var d=0;for(var u in s)d+=this.matrix[u][c]*s[u],s[u]=0;Z(a[c])&&(d+=a[c]);var p=Math.trunc(d);for(var f in o[c]=p,s[c]=(1e3*d-1e3*p)/1e3,a)iA.indexOf(f)>iA.indexOf(c)&&sA(this.matrix,a,f,o,c)}else Z(a[c])&&(s[c]=a[c])}for(var m in s)0!==s[m]&&(o[i]+=m===i?s[m]:s[m]/this.matrix[i][m]);return oA(this,{values:o},!0).normalize()},e.negate=function(){if(!this.isValid)return this;for(var t={},e=0,A=Object.keys(this.values);e<A.length;e++){var r=A[e];t[r]=-this.values[r]}return oA(this,{values:t},!0)},e.equals=function(t){if(!this.isValid||!t.isValid)return!1;if(!this.loc.equals(t.loc))return!1;for(var e,A=h(iA);!(e=A()).done;){var r=e.value;if(i=this.values[r],n=t.values[r],!(void 0===i||0===i?void 0===n||0===n:i===n))return!1}var i,n;return!0},r(t,[{key:"locale",get:function(){return this.isValid?this.loc.locale:null}},{key:"numberingSystem",get:function(){return this.isValid?this.loc.numberingSystem:null}},{key:"years",get:function(){return this.isValid?this.values.years||0:NaN}},{key:"quarters",get:function(){return this.isValid?this.values.quarters||0:NaN}},{key:"months",get:function(){return this.isValid?this.values.months||0:NaN}},{key:"weeks",get:function(){return this.isValid?this.values.weeks||0:NaN}},{key:"days",get:function(){return this.isValid?this.values.days||0:NaN}},{key:"hours",get:function(){return this.isValid?this.values.hours||0:NaN}},{key:"minutes",get:function(){return this.isValid?this.values.minutes||0:NaN}},{key:"seconds",get:function(){return this.isValid?this.values.seconds||0:NaN}},{key:"milliseconds",get:function(){return this.isValid?this.values.milliseconds||0:NaN}},{key:"isValid",get:function(){return null===this.invalid}},{key:"invalidReason",get:function(){return this.invalid?this.invalid.reason:null}},{key:"invalidExplanation",get:function(){return this.invalid?this.invalid.explanation:null}}]),t}(),lA="Invalid Interval";function cA(t,e){return t&&t.isValid?e&&e.isValid?e<t?dA.invalid("end before start","The end of an interval must be after its start, but you had start="+t.toISO()+" and end="+e.toISO()):null:dA.invalid("missing or invalid end"):dA.invalid("missing or invalid start")}var dA=function(){function t(t){this.s=t.start,this.e=t.end,this.invalid=t.invalid||null,this.isLuxonInterval=!0}t.invalid=function(e,A){if(void 0===A&&(A=null),!e)throw new _("need to specify a reason the Interval is invalid");var r=e instanceof Mt?e:new Mt(e,A);if(Xt.throwOnInvalid)throw new m(r);return new t({invalid:r})},t.fromDateTimes=function(e,A){var r=dr(e),i=dr(A),n=cA(r,i);return null==n?new t({start:r,end:i}):n},t.after=function(e,A){var r=aA.fromDurationLike(A),i=dr(e);return t.fromDateTimes(i,i.plus(r))},t.before=function(e,A){var r=aA.fromDurationLike(A),i=dr(e);return t.fromDateTimes(i.minus(r),i)},t.fromISO=function(e,A){var r=(e||"").split("/",2),i=r[0],n=r[1];if(i&&n){var o,s,a,l;try{s=(o=cr.fromISO(i,A)).isValid}catch(n){s=!1}try{l=(a=cr.fromISO(n,A)).isValid}catch(n){l=!1}if(s&&l)return t.fromDateTimes(o,a);if(s){var c=aA.fromISO(n,A);if(c.isValid)return t.after(o,c)}else if(l){var d=aA.fromISO(i,A);if(d.isValid)return t.before(a,d)}}return t.invalid("unparsable",'the input "'+e+"\" can't be parsed as ISO 8601")},t.isInterval=function(t){return t&&t.isLuxonInterval||!1};var e=t.prototype;return e.length=function(t){return void 0===t&&(t="milliseconds"),this.isValid?this.toDuration.apply(this,[t]).get(t):NaN},e.count=function(t){if(void 0===t&&(t="milliseconds"),!this.isValid)return NaN;var e=this.start.startOf(t),A=this.end.startOf(t);return Math.floor(A.diff(e,t).get(t))+1},e.hasSame=function(t){return!!this.isValid&&(this.isEmpty()||this.e.minus(1).hasSame(this.s,t))},e.isEmpty=function(){return this.s.valueOf()===this.e.valueOf()},e.isAfter=function(t){return!!this.isValid&&this.s>t},e.isBefore=function(t){return!!this.isValid&&this.e<=t},e.contains=function(t){return!!this.isValid&&this.s<=t&&this.e>t},e.set=function(e){var A=void 0===e?{}:e,r=A.start,i=A.end;return this.isValid?t.fromDateTimes(r||this.s,i||this.e):this},e.splitAt=function(){var e=this;if(!this.isValid)return[];for(var A=arguments.length,r=new Array(A),i=0;i<A;i++)r[i]=arguments[i];for(var n=r.map(dr).filter((function(t){return e.contains(t)})).sort(),o=[],s=this.s,a=0;s<this.e;){var l=n[a]||this.e,c=+l>+this.e?this.e:l;o.push(t.fromDateTimes(s,c)),s=c,a+=1}return o},e.splitBy=function(e){var A=aA.fromDurationLike(e);if(!this.isValid||!A.isValid||0===A.as("milliseconds"))return[];for(var r,i=this.s,n=1,o=[];i<this.e;){var s=this.start.plus(A.mapUnits((function(t){return t*n})));r=+s>+this.e?this.e:s,o.push(t.fromDateTimes(i,r)),i=r,n+=1}return o},e.divideEqually=function(t){return this.isValid?this.splitBy(this.length()/t).slice(0,t):[]},e.overlaps=function(t){return this.e>t.s&&this.s<t.e},e.abutsStart=function(t){return!!this.isValid&&+this.e==+t.s},e.abutsEnd=function(t){return!!this.isValid&&+t.e==+this.s},e.engulfs=function(t){return!!this.isValid&&this.s<=t.s&&this.e>=t.e},e.equals=function(t){return!(!this.isValid||!t.isValid)&&this.s.equals(t.s)&&this.e.equals(t.e)},e.intersection=function(e){if(!this.isValid)return this;var A=this.s>e.s?this.s:e.s,r=this.e<e.e?this.e:e.e;return A>=r?null:t.fromDateTimes(A,r)},e.union=function(e){if(!this.isValid)return this;var A=this.s<e.s?this.s:e.s,r=this.e>e.e?this.e:e.e;return t.fromDateTimes(A,r)},t.merge=function(t){var e=t.sort((function(t,e){return t.s-e.s})).reduce((function(t,e){var A=t[0],r=t[1];return r?r.overlaps(e)||r.abutsStart(e)?[A,r.union(e)]:[A.concat([r]),e]:[A,e]}),[[],null]),A=e[0],r=e[1];return r&&A.push(r),A},t.xor=function(e){for(var A,r,i=null,n=0,o=[],s=e.map((function(t){return[{time:t.s,type:"s"},{time:t.e,type:"e"}]})),a=h((A=Array.prototype).concat.apply(A,s).sort((function(t,e){return t.time-e.time})));!(r=a()).done;){var l=r.value;1===(n+="s"===l.type?1:-1)?i=l.time:(i&&+i!=+l.time&&o.push(t.fromDateTimes(i,l.time)),i=null)}return t.merge(o)},e.difference=function(){for(var e=this,A=arguments.length,r=new Array(A),i=0;i<A;i++)r[i]=arguments[i];return t.xor([this].concat(r)).map((function(t){return e.intersection(t)})).filter((function(t){return t&&!t.isEmpty()}))},e.toString=function(){return this.isValid?"["+this.s.toISO()+" – "+this.e.toISO()+")":lA},e.toISO=function(t){return this.isValid?this.s.toISO(t)+"/"+this.e.toISO(t):lA},e.toISODate=function(){return this.isValid?this.s.toISODate()+"/"+this.e.toISODate():lA},e.toISOTime=function(t){return this.isValid?this.s.toISOTime(t)+"/"+this.e.toISOTime(t):lA},e.toFormat=function(t,e){var A=(void 0===e?{}:e).separator,r=void 0===A?" – ":A;return this.isValid?""+this.s.toFormat(t)+r+this.e.toFormat(t):lA},e.toDuration=function(t,e){return this.isValid?this.e.diff(this.s,t,e):aA.invalid(this.invalidReason)},e.mapEndpoints=function(e){return t.fromDateTimes(e(this.s),e(this.e))},r(t,[{key:"start",get:function(){return this.isValid?this.s:null}},{key:"end",get:function(){return this.isValid?this.e:null}},{key:"isValid",get:function(){return null===this.invalidReason}},{key:"invalidReason",get:function(){return this.invalid?this.invalid.reason:null}},{key:"invalidExplanation",get:function(){return this.invalid?this.invalid.explanation:null}}]),t}(),uA=function(){function t(){}return t.hasDST=function(t){void 0===t&&(t=Xt.defaultZone);var e=cr.now().setZone(t).set({month:12});return!t.isUniversal&&e.offset!==e.set({month:6}).offset},t.isValidIANAZone=function(t){return Ut.isValidSpecifier(t)&&Ut.isValidZone(t)},t.normalizeZone=function(t){return jt(t,Xt.defaultZone)},t.months=function(t,e){void 0===t&&(t="long");var A=void 0===e?{}:e,r=A.locale,i=void 0===r?null:r,n=A.numberingSystem,o=void 0===n?null:n,s=A.locObj,a=void 0===s?null:s,l=A.outputCalendar,c=void 0===l?"gregory":l;return(a||ue.create(i,o,c)).months(t)},t.monthsFormat=function(t,e){void 0===t&&(t="long");var A=void 0===e?{}:e,r=A.locale,i=void 0===r?null:r,n=A.numberingSystem,o=void 0===n?null:n,s=A.locObj,a=void 0===s?null:s,l=A.outputCalendar,c=void 0===l?"gregory":l;return(a||ue.create(i,o,c)).months(t,!0)},t.weekdays=function(t,e){void 0===t&&(t="long");var A=void 0===e?{}:e,r=A.locale,i=void 0===r?null:r,n=A.numberingSystem,o=void 0===n?null:n,s=A.locObj;return((void 0===s?null:s)||ue.create(i,o,null)).weekdays(t)},t.weekdaysFormat=function(t,e){void 0===t&&(t="long");var A=void 0===e?{}:e,r=A.locale,i=void 0===r?null:r,n=A.numberingSystem,o=void 0===n?null:n,s=A.locObj;return((void 0===s?null:s)||ue.create(i,o,null)).weekdays(t,!0)},t.meridiems=function(t){var e=(void 0===t?{}:t).locale,A=void 0===e?null:e;return ue.create(A).meridiems()},t.eras=function(t,e){void 0===t&&(t="short");var A=(void 0===e?{}:e).locale,r=void 0===A?null:A;return ue.create(r,null,"gregory").eras(t)},t.features=function(){return{relative:W()}},t}();function hA(t,e){var A=function(t){return t.toUTC(0,{keepLocalTime:!0}).startOf("day").valueOf()},r=A(e)-A(t);return Math.floor(aA.fromMillis(r).as("days"))}var pA={arab:"[٠-٩]",arabext:"[۰-۹]",bali:"[᭐-᭙]",beng:"[০-৯]",deva:"[०-९]",fullwide:"[０-９]",gujr:"[૦-૯]",hanidec:"[〇|一|二|三|四|五|六|七|八|九]",khmr:"[០-៩]",knda:"[೦-೯]",laoo:"[໐-໙]",limb:"[᥆-᥏]",mlym:"[൦-൯]",mong:"[᠐-᠙]",mymr:"[၀-၉]",orya:"[୦-୯]",tamldec:"[௦-௯]",telu:"[౦-౯]",thai:"[๐-๙]",tibt:"[༠-༩]",latn:"\\d"},fA={arab:[1632,1641],arabext:[1776,1785],bali:[6992,7001],beng:[2534,2543],deva:[2406,2415],fullwide:[65296,65303],gujr:[2790,2799],khmr:[6112,6121],knda:[3302,3311],laoo:[3792,3801],limb:[6470,6479],mlym:[3430,3439],mong:[6160,6169],mymr:[4160,4169],orya:[2918,2927],tamldec:[3046,3055],telu:[3174,3183],thai:[3664,3673],tibt:[3872,3881]},mA=pA.hanidec.replace(/[\[|\]]/g,"").split("");function gA(t,e){var A=t.numberingSystem;return void 0===e&&(e=""),new RegExp(""+pA[A||"latn"]+e)}function vA(t,e){return void 0===e&&(e=function(t){return t}),{regex:t,deser:function(t){var A=t[0];return e(function(t){var e=parseInt(t,10);if(isNaN(e)){e="";for(var A=0;A<t.length;A++){var r=t.charCodeAt(A);if(-1!==t[A].search(pA.hanidec))e+=mA.indexOf(t[A]);else for(var i in fA){var n=fA[i],o=n[0],s=n[1];r>=o&&r<=s&&(e+=r-o)}}return parseInt(e,10)}return e}(A))}}}var yA="( |"+String.fromCharCode(160)+")",_A=new RegExp(yA,"g");function bA(t){return t.replace(/\./g,"\\.?").replace(_A,yA)}function wA(t){return t.replace(/\./g,"").replace(_A," ").toLowerCase()}function kA(t,e){return null===t?null:{regex:RegExp(t.map(bA).join("|")),deser:function(A){var r=A[0];return t.findIndex((function(t){return wA(r)===wA(t)}))+e}}}function xA(t,e){return{regex:t,deser:function(t){return ut(t[1],t[2])},groups:e}}function SA(t){return{regex:t,deser:function(t){return t[0]}}}var CA={year:{"2-digit":"yy",numeric:"yyyyy"},month:{numeric:"M","2-digit":"MM",short:"MMM",long:"MMMM"},day:{numeric:"d","2-digit":"dd"},weekday:{short:"EEE",long:"EEEE"},dayperiod:"a",dayPeriod:"a",hour:{numeric:"h","2-digit":"hh"},minute:{numeric:"m","2-digit":"mm"},second:{numeric:"s","2-digit":"ss"}},EA=null;function PA(t,e,A){var r=function(t,e){var A;return(A=Array.prototype).concat.apply(A,t.map((function(t){return function(t,e){if(t.literal)return t;var A=It.macroTokenToFormatOpts(t.val);if(!A)return t;var r=It.create(e,A).formatDateTimeParts((EA||(EA=cr.fromMillis(1555555555555)),EA)).map((function(t){return function(t,e,A){var r=t.type,i=t.value;if("literal"===r)return{literal:!0,val:i};var n=A[r],o=CA[r];return"object"==typeof o&&(o=o[n]),o?{literal:!1,val:o}:void 0}(t,0,A)}));return r.includes(void 0)?t:r}(t,e)})))}(It.parseFormat(A),t),i=r.map((function(e){return A=e,i=gA(r=t),n=gA(r,"{2}"),o=gA(r,"{3}"),s=gA(r,"{4}"),a=gA(r,"{6}"),l=gA(r,"{1,2}"),c=gA(r,"{1,3}"),d=gA(r,"{1,6}"),u=gA(r,"{1,9}"),h=gA(r,"{2,4}"),p=gA(r,"{4,6}"),f=function(t){return{regex:RegExp((e=t.val,e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&"))),deser:function(t){return t[0]},literal:!0};var e},m=function(t){if(A.literal)return f(t);switch(t.val){case"G":return kA(r.eras("short",!1),0);case"GG":return kA(r.eras("long",!1),0);case"y":return vA(d);case"yy":case"kk":return vA(h,ct);case"yyyy":case"kkkk":return vA(s);case"yyyyy":return vA(p);case"yyyyyy":return vA(a);case"M":case"L":case"d":case"H":case"h":case"m":case"q":case"s":case"W":return vA(l);case"MM":case"LL":case"dd":case"HH":case"hh":case"mm":case"qq":case"ss":case"WW":return vA(n);case"MMM":return kA(r.months("short",!0,!1),1);case"MMMM":return kA(r.months("long",!0,!1),1);case"LLL":return kA(r.months("short",!1,!1),1);case"LLLL":return kA(r.months("long",!1,!1),1);case"o":case"S":return vA(c);case"ooo":case"SSS":return vA(o);case"u":return SA(u);case"uu":return SA(l);case"uuu":case"E":case"c":return vA(i);case"a":return kA(r.meridiems(),0);case"EEE":return kA(r.weekdays("short",!1,!1),1);case"EEEE":return kA(r.weekdays("long",!1,!1),1);case"ccc":return kA(r.weekdays("short",!0,!1),1);case"cccc":return kA(r.weekdays("long",!0,!1),1);case"Z":case"ZZ":return xA(new RegExp("([+-]"+l.source+")(?::("+n.source+"))?"),2);case"ZZZ":return xA(new RegExp("([+-]"+l.source+")("+n.source+")?"),2);case"z":return SA(/[a-z_+-/]{1,256}?/i);default:return f(t)}}(A)||{invalidReason:"missing Intl.DateTimeFormat.formatToParts support"},m.token=A,m;var A,r,i,n,o,s,a,l,c,d,u,h,p,f,m})),n=i.find((function(t){return t.invalidReason}));if(n)return{input:e,tokens:r,invalidReason:n.invalidReason};var o=function(t){return["^"+t.map((function(t){return t.regex})).reduce((function(t,e){return t+"("+e.source+")"}),"")+"$",t]}(i),s=o[0],a=o[1],l=RegExp(s,"i"),c=function(t,e,A){var r=t.match(e);if(r){var i={},n=1;for(var o in A)if(K(A,o)){var s=A[o],a=s.groups?s.groups+1:1;!s.literal&&s.token&&(i[s.token.val[0]]=s.deser(r.slice(n,n+a))),n+=a}return[r,i]}return[r,{}]}(e,l,a),d=c[0],u=c[1],h=u?function(t){var e,A=null;return Q(t.z)||(A=Ut.create(t.z)),Q(t.Z)||(A||(A=new qt(t.Z)),e=t.Z),Q(t.q)||(t.M=3*(t.q-1)+1),Q(t.h)||(t.h<12&&1===t.a?t.h+=12:12===t.h&&0===t.a&&(t.h=0)),0===t.G&&t.y&&(t.y=-t.y),Q(t.u)||(t.S=rt(t.u)),[Object.keys(t).reduce((function(e,A){var r=function(t){switch(t){case"S":return"millisecond";case"s":return"second";case"m":return"minute";case"h":case"H":return"hour";case"d":return"day";case"o":return"ordinal";case"L":case"M":return"month";case"y":return"year";case"E":case"c":return"weekday";case"W":return"weekNumber";case"k":return"weekYear";case"q":return"quarter";default:return null}}(A);return r&&(e[r]=t[A]),e}),{}),A,e]}(u):[null,null,void 0],p=h[0],f=h[1],m=h[2];if(K(u,"a")&&K(u,"H"))throw new v("Can't include meridiem when specifying 24-hour format");return{input:e,tokens:r,regex:l,rawMatches:d,matches:u,result:p,zone:f,specificOffset:m}}var TA=[0,31,59,90,120,151,181,212,243,273,304,334],OA=[0,31,60,91,121,152,182,213,244,274,305,335];function NA(t,e){return new Mt("unit out of range","you specified "+e+" (of type "+typeof e+") as a "+t+", which is invalid")}function $A(t,e,A){var r=new Date(Date.UTC(t,e-1,A)).getUTCDay();return 0===r?7:r}function IA(t,e,A){return A+(nt(t)?OA:TA)[e-1]}function MA(t,e){var A=nt(t)?OA:TA,r=A.findIndex((function(t){return t<e}));return{month:r+1,day:e-A[r]}}function DA(t){var e,A=t.year,r=t.month,n=t.day,o=IA(A,r,n),s=$A(A,r,n),a=Math.floor((o-s+10)/7);return a<1?a=lt(e=A-1):a>lt(A)?(e=A+1,a=1):e=A,i({weekYear:e,weekNumber:a,weekday:s},mt(t))}function LA(t){var e,A=t.weekYear,r=t.weekNumber,n=t.weekday,o=$A(A,1,4),s=ot(A),a=7*r+n-o-3;a<1?a+=ot(e=A-1):a>s?(e=A+1,a-=ot(A)):e=A;var l=MA(e,a);return i({year:e,month:l.month,day:l.day},mt(t))}function BA(t){var e=t.year;return i({year:e,ordinal:IA(e,t.month,t.day)},mt(t))}function zA(t){var e=t.year,A=MA(e,t.ordinal);return i({year:e,month:A.month,day:A.day},mt(t))}function RA(t){var e=G(t.year),A=X(t.month,1,12),r=X(t.day,1,st(t.year,t.month));return e?A?!r&&NA("day",t.day):NA("month",t.month):NA("year",t.year)}function FA(t){var e=t.hour,A=t.minute,r=t.second,i=t.millisecond,n=X(e,0,23)||24===e&&0===A&&0===r&&0===i,o=X(A,0,59),s=X(r,0,59),a=X(i,0,999);return n?o?s?!a&&NA("millisecond",i):NA("second",r):NA("minute",A):NA("hour",e)}var HA="Invalid DateTime",UA=864e13;function VA(t){return new Mt("unsupported zone",'the zone "'+t.name+'" is not supported')}function qA(t){return null===t.weekData&&(t.weekData=DA(t.c)),t.weekData}function YA(t,e){var A={ts:t.ts,zone:t.zone,c:t.c,o:t.o,loc:t.loc,invalid:t.invalid};return new cr(i({},A,e,{old:A}))}function jA(t,e,A){var r=t-60*e*1e3,i=A.offset(r);if(e===i)return[r,e];r-=60*(i-e)*1e3;var n=A.offset(r);return i===n?[r,i]:[t-60*Math.min(i,n)*1e3,Math.max(i,n)]}function QA(t,e){var A=new Date(t+=60*e*1e3);return{year:A.getUTCFullYear(),month:A.getUTCMonth()+1,day:A.getUTCDate(),hour:A.getUTCHours(),minute:A.getUTCMinutes(),second:A.getUTCSeconds(),millisecond:A.getUTCMilliseconds()}}function ZA(t,e,A){return jA(at(t),e,A)}function GA(t,e){var A=t.o,r=t.c.year+Math.trunc(e.years),n=t.c.month+Math.trunc(e.months)+3*Math.trunc(e.quarters),o=i({},t.c,{year:r,month:n,day:Math.min(t.c.day,st(r,n))+Math.trunc(e.days)+7*Math.trunc(e.weeks)}),s=aA.fromObject({years:e.years-Math.trunc(e.years),quarters:e.quarters-Math.trunc(e.quarters),months:e.months-Math.trunc(e.months),weeks:e.weeks-Math.trunc(e.weeks),days:e.days-Math.trunc(e.days),hours:e.hours,minutes:e.minutes,seconds:e.seconds,milliseconds:e.milliseconds}).as("milliseconds"),a=jA(at(o),A,t.zone),l=a[0],c=a[1];return 0!==s&&(l+=s,c=t.zone.offset(l)),{ts:l,o:c}}function WA(t,e,A,r,n,o){var s=A.setZone,a=A.zone;if(t&&0!==Object.keys(t).length){var l=e||a,c=cr.fromObject(t,i({},A,{zone:l,specificOffset:o}));return s?c:c.setZone(a)}return cr.invalid(new Mt("unparsable",'the input "'+n+"\" can't be parsed as "+r))}function JA(t,e,A){return void 0===A&&(A=!0),t.isValid?It.create(ue.create("en-US"),{allowZ:A,forceSimple:!0}).formatDateTimeFromString(t,e):null}function KA(t,e){var A=t.c.year>9999||t.c.year<0,r="";return A&&t.c.year>=0&&(r+="+"),r+=tt(t.c.year,A?6:4),e?(r+="-",r+=tt(t.c.month),r+="-",r+=tt(t.c.day)):(r+=tt(t.c.month),r+=tt(t.c.day)),r}function XA(t,e,A,r,i){var n=tt(t.c.hour);return e?(n+=":",n+=tt(t.c.minute),0===t.c.second&&A||(n+=":")):n+=tt(t.c.minute),0===t.c.second&&A||(n+=tt(t.c.second),0===t.c.millisecond&&r||(n+=".",n+=tt(t.c.millisecond,3))),i&&(t.isOffsetFixed&&0===t.offset?n+="Z":t.o<0?(n+="-",n+=tt(Math.trunc(-t.o/60)),n+=":",n+=tt(Math.trunc(-t.o%60))):(n+="+",n+=tt(Math.trunc(t.o/60)),n+=":",n+=tt(Math.trunc(t.o%60)))),n}var tr={month:1,day:1,hour:0,minute:0,second:0,millisecond:0},er={weekNumber:1,weekday:1,hour:0,minute:0,second:0,millisecond:0},Ar={ordinal:1,hour:0,minute:0,second:0,millisecond:0},rr=["year","month","day","hour","minute","second","millisecond"],ir=["weekYear","weekNumber","weekday","hour","minute","second","millisecond"],nr=["year","ordinal","hour","minute","second","millisecond"];function or(t){var e={year:"year",years:"year",month:"month",months:"month",day:"day",days:"day",hour:"hour",hours:"hour",minute:"minute",minutes:"minute",quarter:"quarter",quarters:"quarter",second:"second",seconds:"second",millisecond:"millisecond",milliseconds:"millisecond",weekday:"weekday",weekdays:"weekday",weeknumber:"weekNumber",weeksnumber:"weekNumber",weeknumbers:"weekNumber",weekyear:"weekYear",weekyears:"weekYear",ordinal:"ordinal"}[t.toLowerCase()];if(!e)throw new y(t);return e}function sr(t,e){var A,r,i=jt(e.zone,Xt.defaultZone),n=ue.fromObject(e),o=Xt.now();if(Q(t.year))A=o;else{for(var s,a=h(rr);!(s=a()).done;){var l=s.value;Q(t[l])&&(t[l]=tr[l])}var c=RA(t)||FA(t);if(c)return cr.invalid(c);var d=ZA(t,i.offset(o),i);A=d[0],r=d[1]}return new cr({ts:A,zone:i,loc:n,o:r})}function ar(t,e,A){var r=!!Q(A.round)||A.round,i=function(t,i){return t=it(t,r||A.calendary?0:2,!0),e.loc.clone(A).relFormatter(A).format(t,i)},n=function(r){return A.calendary?e.hasSame(t,r)?0:e.startOf(r).diff(t.startOf(r),r).get(r):e.diff(t,r).get(r)};if(A.unit)return i(n(A.unit),A.unit);for(var o,s=h(A.units);!(o=s()).done;){var a=o.value,l=n(a);if(Math.abs(l)>=1)return i(l,a)}return i(t>e?-0:0,A.units[A.units.length-1])}function lr(t){var e,A={};return t.length>0&&"object"==typeof t[t.length-1]?(A=t[t.length-1],e=Array.from(t).slice(0,t.length-1)):e=Array.from(t),[A,e]}var cr=function(){function t(t){var e=t.zone||Xt.defaultZone,A=t.invalid||(Number.isNaN(t.ts)?new Mt("invalid input"):null)||(e.isValid?null:VA(e));this.ts=Q(t.ts)?Xt.now():t.ts;var r=null,i=null;if(!A)if(t.old&&t.old.ts===this.ts&&t.old.zone.equals(e)){var n=[t.old.c,t.old.o];r=n[0],i=n[1]}else{var o=e.offset(this.ts);r=QA(this.ts,o),r=(A=Number.isNaN(r.year)?new Mt("invalid input"):null)?null:r,i=A?null:o}this._zone=e,this.loc=t.loc||ue.create(),this.invalid=A,this.weekData=null,this.c=r,this.o=i,this.isLuxonDateTime=!0}t.now=function(){return new t({})},t.local=function(){var t=lr(arguments),e=t[0],A=t[1],r=A[0],i=A[1],n=A[2],o=A[3],s=A[4],a=A[5],l=A[6];return sr({year:r,month:i,day:n,hour:o,minute:s,second:a,millisecond:l},e)},t.utc=function(){var t=lr(arguments),e=t[0],A=t[1],r=A[0],i=A[1],n=A[2],o=A[3],s=A[4],a=A[5],l=A[6];return e.zone=qt.utcInstance,sr({year:r,month:i,day:n,hour:o,minute:s,second:a,millisecond:l},e)},t.fromJSDate=function(e,A){void 0===A&&(A={});var r,i=(r=e,"[object Date]"===Object.prototype.toString.call(r)?e.valueOf():NaN);if(Number.isNaN(i))return t.invalid("invalid input");var n=jt(A.zone,Xt.defaultZone);return n.isValid?new t({ts:i,zone:n,loc:ue.fromObject(A)}):t.invalid(VA(n))},t.fromMillis=function(e,A){if(void 0===A&&(A={}),Z(e))return e<-UA||e>UA?t.invalid("Timestamp out of range"):new t({ts:e,zone:jt(A.zone,Xt.defaultZone),loc:ue.fromObject(A)});throw new _("fromMillis requires a numerical input, but received a "+typeof e+" with value "+e)},t.fromSeconds=function(e,A){if(void 0===A&&(A={}),Z(e))return new t({ts:1e3*e,zone:jt(A.zone,Xt.defaultZone),loc:ue.fromObject(A)});throw new _("fromSeconds requires a numerical input")},t.fromObject=function(e,A){void 0===A&&(A={}),e=e||{};var r=jt(A.zone,Xt.defaultZone);if(!r.isValid)return t.invalid(VA(r));var i=Xt.now(),n=Q(A.specificOffset)?r.offset(i):A.specificOffset,o=pt(e,or),s=!Q(o.ordinal),a=!Q(o.year),l=!Q(o.month)||!Q(o.day),c=a||l,d=o.weekYear||o.weekNumber,u=ue.fromObject(A);if((c||s)&&d)throw new v("Can't mix weekYear/weekNumber units with year/month/day or ordinals");if(l&&s)throw new v("Can't mix ordinal dates with month/day");var p,f,m=d||o.weekday&&!c,g=QA(i,n);m?(p=ir,f=er,g=DA(g)):s?(p=nr,f=Ar,g=BA(g)):(p=rr,f=tr);for(var y,_=!1,b=h(p);!(y=b()).done;){var w=y.value;Q(o[w])?o[w]=_?f[w]:g[w]:_=!0}var k=m?function(t){var e=G(t.weekYear),A=X(t.weekNumber,1,lt(t.weekYear)),r=X(t.weekday,1,7);return e?A?!r&&NA("weekday",t.weekday):NA("week",t.week):NA("weekYear",t.weekYear)}(o):s?function(t){var e=G(t.year),A=X(t.ordinal,1,ot(t.year));return e?!A&&NA("ordinal",t.ordinal):NA("year",t.year)}(o):RA(o),x=k||FA(o);if(x)return t.invalid(x);var S=ZA(m?LA(o):s?zA(o):o,n,r),C=new t({ts:S[0],zone:r,o:S[1],loc:u});return o.weekday&&c&&e.weekday!==C.weekday?t.invalid("mismatched weekday","you can't specify both a weekday of "+o.weekday+" and a date of "+C.toISO()):C},t.fromISO=function(t,e){void 0===e&&(e={});var A=function(t){return fe(t,[Ue,je],[Ve,Qe],[qe,Ze],[Ye,Ge])}(t);return WA(A[0],A[1],e,"ISO 8601",t)},t.fromRFC2822=function(t,e){void 0===e&&(e={});var A=function(t){return fe(function(t){return t.replace(/\([^)]*\)|[\n\t]/g," ").replace(/(\s\s+)/g," ").trim()}(t),[De,Le])}(t);return WA(A[0],A[1],e,"RFC 2822",t)},t.fromHTTP=function(t,e){void 0===e&&(e={});var A=function(t){return fe(t,[Be,Fe],[ze,Fe],[Re,He])}(t);return WA(A[0],A[1],e,"HTTP",e)},t.fromFormat=function(e,A,r){if(void 0===r&&(r={}),Q(e)||Q(A))throw new _("fromFormat requires an input string and a format");var i=r,n=i.locale,o=void 0===n?null:n,s=i.numberingSystem,a=void 0===s?null:s,l=function(t,e,A){var r=PA(t,e,A);return[r.result,r.zone,r.specificOffset,r.invalidReason]}(ue.fromOpts({locale:o,numberingSystem:a,defaultToEN:!0}),e,A),c=l[0],d=l[1],u=l[2],h=l[3];return h?t.invalid(h):WA(c,d,r,"format "+A,e,u)},t.fromString=function(e,A,r){return void 0===r&&(r={}),t.fromFormat(e,A,r)},t.fromSQL=function(t,e){void 0===e&&(e={});var A=function(t){return fe(t,[Je,Xe],[Ke,tA])}(t);return WA(A[0],A[1],e,"SQL",t)},t.invalid=function(e,A){if(void 0===A&&(A=null),!e)throw new _("need to specify a reason the DateTime is invalid");var r=e instanceof Mt?e:new Mt(e,A);if(Xt.throwOnInvalid)throw new f(r);return new t({invalid:r})},t.isDateTime=function(t){return t&&t.isLuxonDateTime||!1};var e=t.prototype;return e.get=function(t){return this[t]},e.resolvedLocaleOptions=function(t){void 0===t&&(t={});var e=It.create(this.loc.clone(t),t).resolvedOptions(this);return{locale:e.locale,numberingSystem:e.numberingSystem,outputCalendar:e.calendar}},e.toUTC=function(t,e){return void 0===t&&(t=0),void 0===e&&(e={}),this.setZone(qt.instance(t),e)},e.toLocal=function(){return this.setZone(Xt.defaultZone)},e.setZone=function(e,A){var r=void 0===A?{}:A,i=r.keepLocalTime,n=void 0!==i&&i,o=r.keepCalendarTime,s=void 0!==o&&o;if((e=jt(e,Xt.defaultZone)).equals(this.zone))return this;if(e.isValid){var a=this.ts;if(n||s){var l=e.offset(this.ts);a=ZA(this.toObject(),l,e)[0]}return YA(this,{ts:a,zone:e})}return t.invalid(VA(e))},e.reconfigure=function(t){var e=void 0===t?{}:t,A=e.locale,r=e.numberingSystem,i=e.outputCalendar;return YA(this,{loc:this.loc.clone({locale:A,numberingSystem:r,outputCalendar:i})})},e.setLocale=function(t){return this.reconfigure({locale:t})},e.set=function(t){if(!this.isValid)return this;var e,A=pt(t,or),r=!Q(A.weekYear)||!Q(A.weekNumber)||!Q(A.weekday),n=!Q(A.ordinal),o=!Q(A.year),s=!Q(A.month)||!Q(A.day),a=o||s,l=A.weekYear||A.weekNumber;if((a||n)&&l)throw new v("Can't mix weekYear/weekNumber units with year/month/day or ordinals");if(s&&n)throw new v("Can't mix ordinal dates with month/day");r?e=LA(i({},DA(this.c),A)):Q(A.ordinal)?(e=i({},this.toObject(),A),Q(A.day)&&(e.day=Math.min(st(e.year,e.month),e.day))):e=zA(i({},BA(this.c),A));var c=ZA(e,this.o,this.zone);return YA(this,{ts:c[0],o:c[1]})},e.plus=function(t){return this.isValid?YA(this,GA(this,aA.fromDurationLike(t))):this},e.minus=function(t){return this.isValid?YA(this,GA(this,aA.fromDurationLike(t).negate())):this},e.startOf=function(t){if(!this.isValid)return this;var e={},A=aA.normalizeUnit(t);switch(A){case"years":e.month=1;case"quarters":case"months":e.day=1;case"weeks":case"days":e.hour=0;case"hours":e.minute=0;case"minutes":e.second=0;case"seconds":e.millisecond=0}if("weeks"===A&&(e.weekday=1),"quarters"===A){var r=Math.ceil(this.month/3);e.month=3*(r-1)+1}return this.set(e)},e.endOf=function(t){var e;return this.isValid?this.plus((e={},e[t]=1,e)).startOf(t).minus(1):this},e.toFormat=function(t,e){return void 0===e&&(e={}),this.isValid?It.create(this.loc.redefaultToEN(e)).formatDateTimeFromString(this,t):HA},e.toLocaleString=function(t,e){return void 0===t&&(t=S),void 0===e&&(e={}),this.isValid?It.create(this.loc.clone(e),t).formatDateTime(this):HA},e.toLocaleParts=function(t){return void 0===t&&(t={}),this.isValid?It.create(this.loc.clone(t),t).formatDateTimeParts(this):[]},e.toISO=function(t){var e=void 0===t?{}:t,A=e.format,r=void 0===A?"extended":A,i=e.suppressSeconds,n=void 0!==i&&i,o=e.suppressMilliseconds,s=void 0!==o&&o,a=e.includeOffset,l=void 0===a||a;if(!this.isValid)return null;var c="extended"===r,d=KA(this,c);return(d+="T")+XA(this,c,n,s,l)},e.toISODate=function(t){var e=(void 0===t?{}:t).format,A=void 0===e?"extended":e;return this.isValid?KA(this,"extended"===A):null},e.toISOWeekDate=function(){return JA(this,"kkkk-'W'WW-c")},e.toISOTime=function(t){var e=void 0===t?{}:t,A=e.suppressMilliseconds,r=void 0!==A&&A,i=e.suppressSeconds,n=void 0!==i&&i,o=e.includeOffset,s=void 0===o||o,a=e.includePrefix,l=void 0!==a&&a,c=e.format,d=void 0===c?"extended":c;return this.isValid?(l?"T":"")+XA(this,"extended"===d,n,r,s):null},e.toRFC2822=function(){return JA(this,"EEE, dd LLL yyyy HH:mm:ss ZZZ",!1)},e.toHTTP=function(){return JA(this.toUTC(),"EEE, dd LLL yyyy HH:mm:ss 'GMT'")},e.toSQLDate=function(){return this.isValid?KA(this,!0):null},e.toSQLTime=function(t){var e=void 0===t?{}:t,A=e.includeOffset,r=void 0===A||A,i=e.includeZone,n=void 0!==i&&i,o="HH:mm:ss.SSS";return(n||r)&&(o+=" ",n?o+="z":r&&(o+="ZZ")),JA(this,o,!0)},e.toSQL=function(t){return void 0===t&&(t={}),this.isValid?this.toSQLDate()+" "+this.toSQLTime(t):null},e.toString=function(){return this.isValid?this.toISO():HA},e.valueOf=function(){return this.toMillis()},e.toMillis=function(){return this.isValid?this.ts:NaN},e.toSeconds=function(){return this.isValid?this.ts/1e3:NaN},e.toJSON=function(){return this.toISO()},e.toBSON=function(){return this.toJSDate()},e.toObject=function(t){if(void 0===t&&(t={}),!this.isValid)return{};var e=i({},this.c);return t.includeConfig&&(e.outputCalendar=this.outputCalendar,e.numberingSystem=this.loc.numberingSystem,e.locale=this.loc.locale),e},e.toJSDate=function(){return new Date(this.isValid?this.ts:NaN)},e.diff=function(t,e,A){if(void 0===e&&(e="milliseconds"),void 0===A&&(A={}),!this.isValid||!t.isValid)return aA.invalid("created by diffing an invalid DateTime");var r,n=i({locale:this.locale,numberingSystem:this.numberingSystem},A),o=(r=e,Array.isArray(r)?r:[r]).map(aA.normalizeUnit),s=t.valueOf()>this.valueOf(),a=function(t,e,A,r){var i,n=function(t,e,A){for(var r,i,n={},o=0,s=[["years",function(t,e){return e.year-t.year}],["quarters",function(t,e){return e.quarter-t.quarter}],["months",function(t,e){return e.month-t.month+12*(e.year-t.year)}],["weeks",function(t,e){var A=hA(t,e);return(A-A%7)/7}],["days",hA]];o<s.length;o++){var a=s[o],l=a[0],c=a[1];if(A.indexOf(l)>=0){var d;r=l;var u,h=c(t,e);(i=t.plus(((d={})[l]=h,d)))>e?(t=t.plus(((u={})[l]=h-1,u)),h-=1):t=i,n[l]=h}}return[t,n,i,r]}(t,e,A),o=n[0],s=n[1],a=n[2],l=n[3],c=e-o,d=A.filter((function(t){return["hours","minutes","seconds","milliseconds"].indexOf(t)>=0}));0===d.length&&(a<e&&(a=o.plus(((i={})[l]=1,i))),a!==o&&(s[l]=(s[l]||0)+c/(a-o)));var u,h=aA.fromObject(s,r);return d.length>0?(u=aA.fromMillis(c,r)).shiftTo.apply(u,d).plus(h):h}(s?this:t,s?t:this,o,n);return s?a.negate():a},e.diffNow=function(e,A){return void 0===e&&(e="milliseconds"),void 0===A&&(A={}),this.diff(t.now(),e,A)},e.until=function(t){return this.isValid?dA.fromDateTimes(this,t):this},e.hasSame=function(t,e){if(!this.isValid)return!1;var A=t.valueOf(),r=this.setZone(t.zone,{keepLocalTime:!0});return r.startOf(e)<=A&&A<=r.endOf(e)},e.equals=function(t){return this.isValid&&t.isValid&&this.valueOf()===t.valueOf()&&this.zone.equals(t.zone)&&this.loc.equals(t.loc)},e.toRelative=function(e){if(void 0===e&&(e={}),!this.isValid)return null;var A=e.base||t.fromObject({},{zone:this.zone}),r=e.padding?this<A?-e.padding:e.padding:0,n=["years","months","days","hours","minutes","seconds"],o=e.unit;return Array.isArray(e.unit)&&(n=e.unit,o=void 0),ar(A,this.plus(r),i({},e,{numeric:"always",units:n,unit:o}))},e.toRelativeCalendar=function(e){return void 0===e&&(e={}),this.isValid?ar(e.base||t.fromObject({},{zone:this.zone}),this,i({},e,{numeric:"auto",units:["years","months","days"],calendary:!0})):null},t.min=function(){for(var e=arguments.length,A=new Array(e),r=0;r<e;r++)A[r]=arguments[r];if(!A.every(t.isDateTime))throw new _("min requires all arguments be DateTimes");return J(A,(function(t){return t.valueOf()}),Math.min)},t.max=function(){for(var e=arguments.length,A=new Array(e),r=0;r<e;r++)A[r]=arguments[r];if(!A.every(t.isDateTime))throw new _("max requires all arguments be DateTimes");return J(A,(function(t){return t.valueOf()}),Math.max)},t.fromFormatExplain=function(t,e,A){void 0===A&&(A={});var r=A,i=r.locale,n=void 0===i?null:i,o=r.numberingSystem,s=void 0===o?null:o;return PA(ue.fromOpts({locale:n,numberingSystem:s,defaultToEN:!0}),t,e)},t.fromStringExplain=function(e,A,r){return void 0===r&&(r={}),t.fromFormatExplain(e,A,r)},r(t,[{key:"isValid",get:function(){return null===this.invalid}},{key:"invalidReason",get:function(){return this.invalid?this.invalid.reason:null}},{key:"invalidExplanation",get:function(){return this.invalid?this.invalid.explanation:null}},{key:"locale",get:function(){return this.isValid?this.loc.locale:null}},{key:"numberingSystem",get:function(){return this.isValid?this.loc.numberingSystem:null}},{key:"outputCalendar",get:function(){return this.isValid?this.loc.outputCalendar:null}},{key:"zone",get:function(){return this._zone}},{key:"zoneName",get:function(){return this.isValid?this.zone.name:null}},{key:"year",get:function(){return this.isValid?this.c.year:NaN}},{key:"quarter",get:function(){return this.isValid?Math.ceil(this.c.month/3):NaN}},{key:"month",get:function(){return this.isValid?this.c.month:NaN}},{key:"day",get:function(){return this.isValid?this.c.day:NaN}},{key:"hour",get:function(){return this.isValid?this.c.hour:NaN}},{key:"minute",get:function(){return this.isValid?this.c.minute:NaN}},{key:"second",get:function(){return this.isValid?this.c.second:NaN}},{key:"millisecond",get:function(){return this.isValid?this.c.millisecond:NaN}},{key:"weekYear",get:function(){return this.isValid?qA(this).weekYear:NaN}},{key:"weekNumber",get:function(){return this.isValid?qA(this).weekNumber:NaN}},{key:"weekday",get:function(){return this.isValid?qA(this).weekday:NaN}},{key:"ordinal",get:function(){return this.isValid?BA(this.c).ordinal:NaN}},{key:"monthShort",get:function(){return this.isValid?uA.months("short",{locObj:this.loc})[this.month-1]:null}},{key:"monthLong",get:function(){return this.isValid?uA.months("long",{locObj:this.loc})[this.month-1]:null}},{key:"weekdayShort",get:function(){return this.isValid?uA.weekdays("short",{locObj:this.loc})[this.weekday-1]:null}},{key:"weekdayLong",get:function(){return this.isValid?uA.weekdays("long",{locObj:this.loc})[this.weekday-1]:null}},{key:"offset",get:function(){return this.isValid?+this.o:NaN}},{key:"offsetNameShort",get:function(){return this.isValid?this.zone.offsetName(this.ts,{format:"short",locale:this.locale}):null}},{key:"offsetNameLong",get:function(){return this.isValid?this.zone.offsetName(this.ts,{format:"long",locale:this.locale}):null}},{key:"isOffsetFixed",get:function(){return this.isValid?this.zone.isUniversal:null}},{key:"isInDST",get:function(){return!this.isOffsetFixed&&(this.offset>this.set({month:1}).offset||this.offset>this.set({month:5}).offset)}},{key:"isInLeapYear",get:function(){return nt(this.year)}},{key:"daysInMonth",get:function(){return st(this.year,this.month)}},{key:"daysInYear",get:function(){return this.isValid?ot(this.year):NaN}},{key:"weeksInWeekYear",get:function(){return this.isValid?lt(this.weekYear):NaN}}],[{key:"DATE_SHORT",get:function(){return S}},{key:"DATE_MED",get:function(){return C}},{key:"DATE_MED_WITH_WEEKDAY",get:function(){return E}},{key:"DATE_FULL",get:function(){return P}},{key:"DATE_HUGE",get:function(){return T}},{key:"TIME_SIMPLE",get:function(){return O}},{key:"TIME_WITH_SECONDS",get:function(){return N}},{key:"TIME_WITH_SHORT_OFFSET",get:function(){return $}},{key:"TIME_WITH_LONG_OFFSET",get:function(){return I}},{key:"TIME_24_SIMPLE",get:function(){return M}},{key:"TIME_24_WITH_SECONDS",get:function(){return D}},{key:"TIME_24_WITH_SHORT_OFFSET",get:function(){return L}},{key:"TIME_24_WITH_LONG_OFFSET",get:function(){return B}},{key:"DATETIME_SHORT",get:function(){return z}},{key:"DATETIME_SHORT_WITH_SECONDS",get:function(){return R}},{key:"DATETIME_MED",get:function(){return F}},{key:"DATETIME_MED_WITH_SECONDS",get:function(){return H}},{key:"DATETIME_MED_WITH_WEEKDAY",get:function(){return U}},{key:"DATETIME_FULL",get:function(){return V}},{key:"DATETIME_FULL_WITH_SECONDS",get:function(){return q}},{key:"DATETIME_HUGE",get:function(){return Y}},{key:"DATETIME_HUGE_WITH_SECONDS",get:function(){return j}}]),t}();function dr(t){if(cr.isDateTime(t))return t;if(t&&t.valueOf&&Z(t.valueOf()))return cr.fromJSDate(t);if(t&&"object"==typeof t)return cr.fromObject(t);throw new _("Unknown datetime argument: "+t+", of type "+typeof t)}e.DateTime=cr,e.Duration=aA,e.FixedOffsetZone=qt,e.IANAZone=Ut,e.Info=uA,e.Interval=dA,e.InvalidZone=Yt,e.Settings=Xt,e.SystemZone=Bt,e.VERSION="2.3.0",e.Zone=Dt},621:(t,e,A)=>{var r;!function(i){var n=/^\s+/,o=/\s+$/,s=0,a=i.round,l=i.min,c=i.max,d=i.random;function u(t,e){if(e=e||{},(t=t||"")instanceof u)return t;if(!(this instanceof u))return new u(t,e);var A=function(t){var e,A,r,s={r:0,g:0,b:0},a=1,d=null,u=null,h=null,p=!1,f=!1;return"string"==typeof t&&(t=function(t){t=t.replace(n,"").replace(o,"").toLowerCase();var e,A=!1;if(O[t])t=O[t],A=!0;else if("transparent"==t)return{r:0,g:0,b:0,a:0,format:"name"};return(e=V.rgb.exec(t))?{r:e[1],g:e[2],b:e[3]}:(e=V.rgba.exec(t))?{r:e[1],g:e[2],b:e[3],a:e[4]}:(e=V.hsl.exec(t))?{h:e[1],s:e[2],l:e[3]}:(e=V.hsla.exec(t))?{h:e[1],s:e[2],l:e[3],a:e[4]}:(e=V.hsv.exec(t))?{h:e[1],s:e[2],v:e[3]}:(e=V.hsva.exec(t))?{h:e[1],s:e[2],v:e[3],a:e[4]}:(e=V.hex8.exec(t))?{r:D(e[1]),g:D(e[2]),b:D(e[3]),a:R(e[4]),format:A?"name":"hex8"}:(e=V.hex6.exec(t))?{r:D(e[1]),g:D(e[2]),b:D(e[3]),format:A?"name":"hex"}:(e=V.hex4.exec(t))?{r:D(e[1]+""+e[1]),g:D(e[2]+""+e[2]),b:D(e[3]+""+e[3]),a:R(e[4]+""+e[4]),format:A?"name":"hex8"}:!!(e=V.hex3.exec(t))&&{r:D(e[1]+""+e[1]),g:D(e[2]+""+e[2]),b:D(e[3]+""+e[3]),format:A?"name":"hex"}}(t)),"object"==typeof t&&(q(t.r)&&q(t.g)&&q(t.b)?(e=t.r,A=t.g,r=t.b,s={r:255*I(e,255),g:255*I(A,255),b:255*I(r,255)},p=!0,f="%"===String(t.r).substr(-1)?"prgb":"rgb"):q(t.h)&&q(t.s)&&q(t.v)?(d=B(t.s),u=B(t.v),s=function(t,e,A){t=6*I(t,360),e=I(e,100),A=I(A,100);var r=i.floor(t),n=t-r,o=A*(1-e),s=A*(1-n*e),a=A*(1-(1-n)*e),l=r%6;return{r:255*[A,s,o,o,a,A][l],g:255*[a,A,A,s,o,o][l],b:255*[o,o,a,A,A,s][l]}}(t.h,d,u),p=!0,f="hsv"):q(t.h)&&q(t.s)&&q(t.l)&&(d=B(t.s),h=B(t.l),s=function(t,e,A){var r,i,n;function o(t,e,A){return A<0&&(A+=1),A>1&&(A-=1),A<1/6?t+6*(e-t)*A:A<.5?e:A<2/3?t+(e-t)*(2/3-A)*6:t}if(t=I(t,360),e=I(e,100),A=I(A,100),0===e)r=i=n=A;else{var s=A<.5?A*(1+e):A+e-A*e,a=2*A-s;r=o(a,s,t+1/3),i=o(a,s,t),n=o(a,s,t-1/3)}return{r:255*r,g:255*i,b:255*n}}(t.h,d,h),p=!0,f="hsl"),t.hasOwnProperty("a")&&(a=t.a)),a=$(a),{ok:p,format:t.format||f,r:l(255,c(s.r,0)),g:l(255,c(s.g,0)),b:l(255,c(s.b,0)),a}}(t);this._originalInput=t,this._r=A.r,this._g=A.g,this._b=A.b,this._a=A.a,this._roundA=a(100*this._a)/100,this._format=e.format||A.format,this._gradientType=e.gradientType,this._r<1&&(this._r=a(this._r)),this._g<1&&(this._g=a(this._g)),this._b<1&&(this._b=a(this._b)),this._ok=A.ok,this._tc_id=s++}function h(t,e,A){t=I(t,255),e=I(e,255),A=I(A,255);var r,i,n=c(t,e,A),o=l(t,e,A),s=(n+o)/2;if(n==o)r=i=0;else{var a=n-o;switch(i=s>.5?a/(2-n-o):a/(n+o),n){case t:r=(e-A)/a+(e<A?6:0);break;case e:r=(A-t)/a+2;break;case A:r=(t-e)/a+4}r/=6}return{h:r,s:i,l:s}}function p(t,e,A){t=I(t,255),e=I(e,255),A=I(A,255);var r,i,n=c(t,e,A),o=l(t,e,A),s=n,a=n-o;if(i=0===n?0:a/n,n==o)r=0;else{switch(n){case t:r=(e-A)/a+(e<A?6:0);break;case e:r=(A-t)/a+2;break;case A:r=(t-e)/a+4}r/=6}return{h:r,s:i,v:s}}function f(t,e,A,r){var i=[L(a(t).toString(16)),L(a(e).toString(16)),L(a(A).toString(16))];return r&&i[0].charAt(0)==i[0].charAt(1)&&i[1].charAt(0)==i[1].charAt(1)&&i[2].charAt(0)==i[2].charAt(1)?i[0].charAt(0)+i[1].charAt(0)+i[2].charAt(0):i.join("")}function m(t,e,A,r){return[L(z(r)),L(a(t).toString(16)),L(a(e).toString(16)),L(a(A).toString(16))].join("")}function g(t,e){e=0===e?0:e||10;var A=u(t).toHsl();return A.s-=e/100,A.s=M(A.s),u(A)}function v(t,e){e=0===e?0:e||10;var A=u(t).toHsl();return A.s+=e/100,A.s=M(A.s),u(A)}function y(t){return u(t).desaturate(100)}function _(t,e){e=0===e?0:e||10;var A=u(t).toHsl();return A.l+=e/100,A.l=M(A.l),u(A)}function b(t,e){e=0===e?0:e||10;var A=u(t).toRgb();return A.r=c(0,l(255,A.r-a(-e/100*255))),A.g=c(0,l(255,A.g-a(-e/100*255))),A.b=c(0,l(255,A.b-a(-e/100*255))),u(A)}function w(t,e){e=0===e?0:e||10;var A=u(t).toHsl();return A.l-=e/100,A.l=M(A.l),u(A)}function k(t,e){var A=u(t).toHsl(),r=(A.h+e)%360;return A.h=r<0?360+r:r,u(A)}function x(t){var e=u(t).toHsl();return e.h=(e.h+180)%360,u(e)}function S(t){var e=u(t).toHsl(),A=e.h;return[u(t),u({h:(A+120)%360,s:e.s,l:e.l}),u({h:(A+240)%360,s:e.s,l:e.l})]}function C(t){var e=u(t).toHsl(),A=e.h;return[u(t),u({h:(A+90)%360,s:e.s,l:e.l}),u({h:(A+180)%360,s:e.s,l:e.l}),u({h:(A+270)%360,s:e.s,l:e.l})]}function E(t){var e=u(t).toHsl(),A=e.h;return[u(t),u({h:(A+72)%360,s:e.s,l:e.l}),u({h:(A+216)%360,s:e.s,l:e.l})]}function P(t,e,A){e=e||6,A=A||30;var r=u(t).toHsl(),i=360/A,n=[u(t)];for(r.h=(r.h-(i*e>>1)+720)%360;--e;)r.h=(r.h+i)%360,n.push(u(r));return n}function T(t,e){e=e||6;for(var A=u(t).toHsv(),r=A.h,i=A.s,n=A.v,o=[],s=1/e;e--;)o.push(u({h:r,s:i,v:n})),n=(n+s)%1;return o}u.prototype={isDark:function(){return this.getBrightness()<128},isLight:function(){return!this.isDark()},isValid:function(){return this._ok},getOriginalInput:function(){return this._originalInput},getFormat:function(){return this._format},getAlpha:function(){return this._a},getBrightness:function(){var t=this.toRgb();return(299*t.r+587*t.g+114*t.b)/1e3},getLuminance:function(){var t,e,A,r=this.toRgb();return t=r.r/255,e=r.g/255,A=r.b/255,.2126*(t<=.03928?t/12.92:i.pow((t+.055)/1.055,2.4))+.7152*(e<=.03928?e/12.92:i.pow((e+.055)/1.055,2.4))+.0722*(A<=.03928?A/12.92:i.pow((A+.055)/1.055,2.4))},setAlpha:function(t){return this._a=$(t),this._roundA=a(100*this._a)/100,this},toHsv:function(){var t=p(this._r,this._g,this._b);return{h:360*t.h,s:t.s,v:t.v,a:this._a}},toHsvString:function(){var t=p(this._r,this._g,this._b),e=a(360*t.h),A=a(100*t.s),r=a(100*t.v);return 1==this._a?"hsv("+e+", "+A+"%, "+r+"%)":"hsva("+e+", "+A+"%, "+r+"%, "+this._roundA+")"},toHsl:function(){var t=h(this._r,this._g,this._b);return{h:360*t.h,s:t.s,l:t.l,a:this._a}},toHslString:function(){var t=h(this._r,this._g,this._b),e=a(360*t.h),A=a(100*t.s),r=a(100*t.l);return 1==this._a?"hsl("+e+", "+A+"%, "+r+"%)":"hsla("+e+", "+A+"%, "+r+"%, "+this._roundA+")"},toHex:function(t){return f(this._r,this._g,this._b,t)},toHexString:function(t){return"#"+this.toHex(t)},toHex8:function(t){return function(t,e,A,r,i){var n=[L(a(t).toString(16)),L(a(e).toString(16)),L(a(A).toString(16)),L(z(r))];return i&&n[0].charAt(0)==n[0].charAt(1)&&n[1].charAt(0)==n[1].charAt(1)&&n[2].charAt(0)==n[2].charAt(1)&&n[3].charAt(0)==n[3].charAt(1)?n[0].charAt(0)+n[1].charAt(0)+n[2].charAt(0)+n[3].charAt(0):n.join("")}(this._r,this._g,this._b,this._a,t)},toHex8String:function(t){return"#"+this.toHex8(t)},toRgb:function(){return{r:a(this._r),g:a(this._g),b:a(this._b),a:this._a}},toRgbString:function(){return 1==this._a?"rgb("+a(this._r)+", "+a(this._g)+", "+a(this._b)+")":"rgba("+a(this._r)+", "+a(this._g)+", "+a(this._b)+", "+this._roundA+")"},toPercentageRgb:function(){return{r:a(100*I(this._r,255))+"%",g:a(100*I(this._g,255))+"%",b:a(100*I(this._b,255))+"%",a:this._a}},toPercentageRgbString:function(){return 1==this._a?"rgb("+a(100*I(this._r,255))+"%, "+a(100*I(this._g,255))+"%, "+a(100*I(this._b,255))+"%)":"rgba("+a(100*I(this._r,255))+"%, "+a(100*I(this._g,255))+"%, "+a(100*I(this._b,255))+"%, "+this._roundA+")"},toName:function(){return 0===this._a?"transparent":!(this._a<1)&&(N[f(this._r,this._g,this._b,!0)]||!1)},toFilter:function(t){var e="#"+m(this._r,this._g,this._b,this._a),A=e,r=this._gradientType?"GradientType = 1, ":"";if(t){var i=u(t);A="#"+m(i._r,i._g,i._b,i._a)}return"progid:DXImageTransform.Microsoft.gradient("+r+"startColorstr="+e+",endColorstr="+A+")"},toString:function(t){var e=!!t;t=t||this._format;var A=!1,r=this._a<1&&this._a>=0;return e||!r||"hex"!==t&&"hex6"!==t&&"hex3"!==t&&"hex4"!==t&&"hex8"!==t&&"name"!==t?("rgb"===t&&(A=this.toRgbString()),"prgb"===t&&(A=this.toPercentageRgbString()),"hex"!==t&&"hex6"!==t||(A=this.toHexString()),"hex3"===t&&(A=this.toHexString(!0)),"hex4"===t&&(A=this.toHex8String(!0)),"hex8"===t&&(A=this.toHex8String()),"name"===t&&(A=this.toName()),"hsl"===t&&(A=this.toHslString()),"hsv"===t&&(A=this.toHsvString()),A||this.toHexString()):"name"===t&&0===this._a?this.toName():this.toRgbString()},clone:function(){return u(this.toString())},_applyModification:function(t,e){var A=t.apply(null,[this].concat([].slice.call(e)));return this._r=A._r,this._g=A._g,this._b=A._b,this.setAlpha(A._a),this},lighten:function(){return this._applyModification(_,arguments)},brighten:function(){return this._applyModification(b,arguments)},darken:function(){return this._applyModification(w,arguments)},desaturate:function(){return this._applyModification(g,arguments)},saturate:function(){return this._applyModification(v,arguments)},greyscale:function(){return this._applyModification(y,arguments)},spin:function(){return this._applyModification(k,arguments)},_applyCombination:function(t,e){return t.apply(null,[this].concat([].slice.call(e)))},analogous:function(){return this._applyCombination(P,arguments)},complement:function(){return this._applyCombination(x,arguments)},monochromatic:function(){return this._applyCombination(T,arguments)},splitcomplement:function(){return this._applyCombination(E,arguments)},triad:function(){return this._applyCombination(S,arguments)},tetrad:function(){return this._applyCombination(C,arguments)}},u.fromRatio=function(t,e){if("object"==typeof t){var A={};for(var r in t)t.hasOwnProperty(r)&&(A[r]="a"===r?t[r]:B(t[r]));t=A}return u(t,e)},u.equals=function(t,e){return!(!t||!e)&&u(t).toRgbString()==u(e).toRgbString()},u.random=function(){return u.fromRatio({r:d(),g:d(),b:d()})},u.mix=function(t,e,A){A=0===A?0:A||50;var r=u(t).toRgb(),i=u(e).toRgb(),n=A/100;return u({r:(i.r-r.r)*n+r.r,g:(i.g-r.g)*n+r.g,b:(i.b-r.b)*n+r.b,a:(i.a-r.a)*n+r.a})},u.readability=function(t,e){var A=u(t),r=u(e);return(i.max(A.getLuminance(),r.getLuminance())+.05)/(i.min(A.getLuminance(),r.getLuminance())+.05)},u.isReadable=function(t,e,A){var r,i,n,o,s,a=u.readability(t,e);switch(i=!1,(n=A,"AA"!==(o=((n=n||{level:"AA",size:"small"}).level||"AA").toUpperCase())&&"AAA"!==o&&(o="AA"),"small"!==(s=(n.size||"small").toLowerCase())&&"large"!==s&&(s="small"),r={level:o,size:s}).level+r.size){case"AAsmall":case"AAAlarge":i=a>=4.5;break;case"AAlarge":i=a>=3;break;case"AAAsmall":i=a>=7}return i},u.mostReadable=function(t,e,A){var r,i,n,o,s=null,a=0;i=(A=A||{}).includeFallbackColors,n=A.level,o=A.size;for(var l=0;l<e.length;l++)(r=u.readability(t,e[l]))>a&&(a=r,s=u(e[l]));return u.isReadable(t,s,{level:n,size:o})||!i?s:(A.includeFallbackColors=!1,u.mostReadable(t,["#fff","#000"],A))};var O=u.names={aliceblue:"f0f8ff",antiquewhite:"faebd7",aqua:"0ff",aquamarine:"7fffd4",azure:"f0ffff",beige:"f5f5dc",bisque:"ffe4c4",black:"000",blanchedalmond:"ffebcd",blue:"00f",blueviolet:"8a2be2",brown:"a52a2a",burlywood:"deb887",burntsienna:"ea7e5d",cadetblue:"5f9ea0",chartreuse:"7fff00",chocolate:"d2691e",coral:"ff7f50",cornflowerblue:"6495ed",cornsilk:"fff8dc",crimson:"dc143c",cyan:"0ff",darkblue:"00008b",darkcyan:"008b8b",darkgoldenrod:"b8860b",darkgray:"a9a9a9",darkgreen:"006400",darkgrey:"a9a9a9",darkkhaki:"bdb76b",darkmagenta:"8b008b",darkolivegreen:"556b2f",darkorange:"ff8c00",darkorchid:"9932cc",darkred:"8b0000",darksalmon:"e9967a",darkseagreen:"8fbc8f",darkslateblue:"483d8b",darkslategray:"2f4f4f",darkslategrey:"2f4f4f",darkturquoise:"00ced1",darkviolet:"9400d3",deeppink:"ff1493",deepskyblue:"00bfff",dimgray:"696969",dimgrey:"696969",dodgerblue:"1e90ff",firebrick:"b22222",floralwhite:"fffaf0",forestgreen:"228b22",fuchsia:"f0f",gainsboro:"dcdcdc",ghostwhite:"f8f8ff",gold:"ffd700",goldenrod:"daa520",gray:"808080",green:"008000",greenyellow:"adff2f",grey:"808080",honeydew:"f0fff0",hotpink:"ff69b4",indianred:"cd5c5c",indigo:"4b0082",ivory:"fffff0",khaki:"f0e68c",lavender:"e6e6fa",lavenderblush:"fff0f5",lawngreen:"7cfc00",lemonchiffon:"fffacd",lightblue:"add8e6",lightcoral:"f08080",lightcyan:"e0ffff",lightgoldenrodyellow:"fafad2",lightgray:"d3d3d3",lightgreen:"90ee90",lightgrey:"d3d3d3",lightpink:"ffb6c1",lightsalmon:"ffa07a",lightseagreen:"20b2aa",lightskyblue:"87cefa",lightslategray:"789",lightslategrey:"789",lightsteelblue:"b0c4de",lightyellow:"ffffe0",lime:"0f0",limegreen:"32cd32",linen:"faf0e6",magenta:"f0f",maroon:"800000",mediumaquamarine:"66cdaa",mediumblue:"0000cd",mediumorchid:"ba55d3",mediumpurple:"9370db",mediumseagreen:"3cb371",mediumslateblue:"7b68ee",mediumspringgreen:"00fa9a",mediumturquoise:"48d1cc",mediumvioletred:"c71585",midnightblue:"191970",mintcream:"f5fffa",mistyrose:"ffe4e1",moccasin:"ffe4b5",navajowhite:"ffdead",navy:"000080",oldlace:"fdf5e6",olive:"808000",olivedrab:"6b8e23",orange:"ffa500",orangered:"ff4500",orchid:"da70d6",palegoldenrod:"eee8aa",palegreen:"98fb98",paleturquoise:"afeeee",palevioletred:"db7093",papayawhip:"ffefd5",peachpuff:"ffdab9",peru:"cd853f",pink:"ffc0cb",plum:"dda0dd",powderblue:"b0e0e6",purple:"800080",rebeccapurple:"663399",red:"f00",rosybrown:"bc8f8f",royalblue:"4169e1",saddlebrown:"8b4513",salmon:"fa8072",sandybrown:"f4a460",seagreen:"2e8b57",seashell:"fff5ee",sienna:"a0522d",silver:"c0c0c0",skyblue:"87ceeb",slateblue:"6a5acd",slategray:"708090",slategrey:"708090",snow:"fffafa",springgreen:"00ff7f",steelblue:"4682b4",tan:"d2b48c",teal:"008080",thistle:"d8bfd8",tomato:"ff6347",turquoise:"40e0d0",violet:"ee82ee",wheat:"f5deb3",white:"fff",whitesmoke:"f5f5f5",yellow:"ff0",yellowgreen:"9acd32"},N=u.hexNames=function(t){var e={};for(var A in t)t.hasOwnProperty(A)&&(e[t[A]]=A);return e}(O);function $(t){return t=parseFloat(t),(isNaN(t)||t<0||t>1)&&(t=1),t}function I(t,e){(function(t){return"string"==typeof t&&-1!=t.indexOf(".")&&1===parseFloat(t)})(t)&&(t="100%");var A=function(t){return"string"==typeof t&&-1!=t.indexOf("%")}(t);return t=l(e,c(0,parseFloat(t))),A&&(t=parseInt(t*e,10)/100),i.abs(t-e)<1e-6?1:t%e/parseFloat(e)}function M(t){return l(1,c(0,t))}function D(t){return parseInt(t,16)}function L(t){return 1==t.length?"0"+t:""+t}function B(t){return t<=1&&(t=100*t+"%"),t}function z(t){return i.round(255*parseFloat(t)).toString(16)}function R(t){return D(t)/255}var F,H,U,V=(H="[\\s|\\(]+("+(F="(?:[-\\+]?\\d*\\.\\d+%?)|(?:[-\\+]?\\d+%?)")+")[,|\\s]+("+F+")[,|\\s]+("+F+")\\s*\\)?",U="[\\s|\\(]+("+F+")[,|\\s]+("+F+")[,|\\s]+("+F+")[,|\\s]+("+F+")\\s*\\)?",{CSS_UNIT:new RegExp(F),rgb:new RegExp("rgb"+H),rgba:new RegExp("rgba"+U),hsl:new RegExp("hsl"+H),hsla:new RegExp("hsla"+U),hsv:new RegExp("hsv"+H),hsva:new RegExp("hsva"+U),hex3:/^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,hex6:/^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/,hex4:/^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,hex8:/^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/});function q(t){return!!V.CSS_UNIT.exec(t)}t.exports?t.exports=u:void 0===(r=function(){return u}.call(e,A,e,t))||(t.exports=r)}(Math)}},e={};function A(r){var i=e[r];if(void 0!==i)return i.exports;var n=e[r]={exports:{}};return t[r](n,n.exports,A),n.exports}A.r=t=>{"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},(()=>{"use strict";const t=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,e=Symbol(),r=new Map;class i{constructor(t,A){if(this._$cssResult$=!0,A!==e)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let e=r.get(this.cssText);return t&&void 0===e&&(r.set(this.cssText,e=new CSSStyleSheet),e.replaceSync(this.cssText)),e}toString(){return this.cssText}}const n=t=>new i("string"==typeof t?t:t+"",e),o=(t,...A)=>{const r=1===t.length?t[0]:A.reduce(((e,A,r)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(A)+t[r+1]),t[0]);return new i(r,e)},s=t?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const A of t.cssRules)e+=A.cssText;return n(e)})(t):t;var a,l;const c={toAttribute(t,e){switch(e){case Boolean:t=t?"":null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let A=t;switch(e){case Boolean:A=null!==t;break;case Number:A=null===t?null:Number(t);break;case Object:case Array:try{A=JSON.parse(t)}catch(t){A=null}}return A}},d=(t,e)=>e!==t&&(e==e||t==t),u={attribute:!0,type:String,converter:c,reflect:!1,hasChanged:d};class h extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,A)=>{const r=this._$Eh(A,e);void 0!==r&&(this._$Eu.set(r,A),t.push(r))})),t}static createProperty(t,e=u){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const A="symbol"==typeof t?Symbol():"__"+t,r=this.getPropertyDescriptor(t,A,e);void 0!==r&&Object.defineProperty(this.prototype,t,r)}}static getPropertyDescriptor(t,e,A){return{get(){return this[e]},set(r){const i=this[t];this[e]=r,this.requestUpdate(t,i,A)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||u}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const A of e)this.createProperty(A,t[A])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const A=new Set(t.flat(1/0).reverse());for(const t of A)e.unshift(s(t))}else void 0!==t&&e.push(s(t));return e}static _$Eh(t,e){const A=e.attribute;return!1===A?void 0:"string"==typeof A?A:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ev=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Ep(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,A;(null!==(e=this._$Em)&&void 0!==e?e:this._$Em=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(A=t.hostConnected)||void 0===A||A.call(t))}removeController(t){var e;null===(e=this._$Em)||void 0===e||e.splice(this._$Em.indexOf(t)>>>0,1)}_$Ep(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var e;const A=null!==(e=this.shadowRoot)&&void 0!==e?e:this.attachShadow(this.constructor.shadowRootOptions);return((e,A)=>{t?e.adoptedStyleSheets=A.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):A.forEach((t=>{const A=document.createElement("style"),r=window.litNonce;void 0!==r&&A.setAttribute("nonce",r),A.textContent=t.cssText,e.appendChild(A)}))})(A,this.constructor.elementStyles),A}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,A){this._$AK(t,A)}_$Eg(t,e,A=u){var r,i;const n=this.constructor._$Eh(t,A);if(void 0!==n&&!0===A.reflect){const o=(null!==(i=null===(r=A.converter)||void 0===r?void 0:r.toAttribute)&&void 0!==i?i:c.toAttribute)(e,A.type);this._$Ei=t,null==o?this.removeAttribute(n):this.setAttribute(n,o),this._$Ei=null}}_$AK(t,e){var A,r,i;const n=this.constructor,o=n._$Eu.get(t);if(void 0!==o&&this._$Ei!==o){const t=n.getPropertyOptions(o),s=t.converter,a=null!==(i=null!==(r=null===(A=s)||void 0===A?void 0:A.fromAttribute)&&void 0!==r?r:"function"==typeof s?s:null)&&void 0!==i?i:c.fromAttribute;this._$Ei=o,this[o]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,A){let r=!0;void 0!==t&&(((A=A||this.constructor.getPropertyOptions(t)).hasChanged||d)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===A.reflect&&this._$Ei!==t&&(void 0===this._$ES&&(this._$ES=new Map),this._$ES.set(t,A))):r=!1),!this.isUpdatePending&&r&&(this._$Ev=this._$EC())}async _$EC(){this.isUpdatePending=!0;try{await this._$Ev}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const A=this._$AL;try{e=this.shouldUpdate(A),e?(this.willUpdate(A),null===(t=this._$Em)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(A)):this._$ET()}catch(t){throw e=!1,this._$ET(),t}e&&this._$AE(A)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Em)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$ET(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ev}shouldUpdate(t){return!0}update(t){void 0!==this._$ES&&(this._$ES.forEach(((t,e)=>this._$Eg(e,this[e],t))),this._$ES=void 0),this._$ET()}updated(t){}firstUpdated(t){}}var p,f;h.finalized=!0,h.elementProperties=new Map,h.elementStyles=[],h.shadowRootOptions={mode:"open"},null===(a=globalThis.reactiveElementPolyfillSupport)||void 0===a||a.call(globalThis,{ReactiveElement:h}),(null!==(l=globalThis.reactiveElementVersions)&&void 0!==l?l:globalThis.reactiveElementVersions=[]).push("1.0.0");const m=globalThis.trustedTypes,g=m?m.createPolicy("lit-html",{createHTML:t=>t}):void 0,v=`lit$${(Math.random()+"").slice(9)}$`,y="?"+v,_=`<${y}>`,b=document,w=(t="")=>b.createComment(t),k=t=>null===t||"object"!=typeof t&&"function"!=typeof t,x=Array.isArray,S=t=>{var e;return x(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])},C=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,E=/-->/g,P=/>/g,T=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,O=/'/g,N=/"/g,$=/^(?:script|style|textarea)$/i,I=t=>(e,...A)=>({_$litType$:t,strings:e,values:A}),M=I(1),D=(I(2),Symbol.for("lit-noChange")),L=Symbol.for("lit-nothing"),B=new WeakMap,z=b.createTreeWalker(b,129,null,!1),R=(t,e)=>{const A=t.length-1,r=[];let i,n=2===e?"<svg>":"",o=C;for(let e=0;e<A;e++){const A=t[e];let s,a,l=-1,c=0;for(;c<A.length&&(o.lastIndex=c,a=o.exec(A),null!==a);)c=o.lastIndex,o===C?"!--"===a[1]?o=E:void 0!==a[1]?o=P:void 0!==a[2]?($.test(a[2])&&(i=RegExp("</"+a[2],"g")),o=T):void 0!==a[3]&&(o=T):o===T?">"===a[0]?(o=null!=i?i:C,l=-1):void 0===a[1]?l=-2:(l=o.lastIndex-a[2].length,s=a[1],o=void 0===a[3]?T:'"'===a[3]?N:O):o===N||o===O?o=T:o===E||o===P?o=C:(o=T,i=void 0);const d=o===T&&t[e+1].startsWith("/>")?" ":"";n+=o===C?A+_:l>=0?(r.push(s),A.slice(0,l)+"$lit$"+A.slice(l)+v+d):A+v+(-2===l?(r.push(void 0),e):d)}const s=n+(t[A]||"<?>")+(2===e?"</svg>":"");return[void 0!==g?g.createHTML(s):s,r]};class F{constructor({strings:t,_$litType$:e},A){let r;this.parts=[];let i=0,n=0;const o=t.length-1,s=this.parts,[a,l]=R(t,e);if(this.el=F.createElement(a,A),z.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(r=z.nextNode())&&s.length<o;){if(1===r.nodeType){if(r.hasAttributes()){const t=[];for(const e of r.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(v)){const A=l[n++];if(t.push(e),void 0!==A){const t=r.getAttribute(A.toLowerCase()+"$lit$").split(v),e=/([.?@])?(.*)/.exec(A);s.push({type:1,index:i,name:e[2],strings:t,ctor:"."===e[1]?Y:"?"===e[1]?j:"@"===e[1]?Q:q})}else s.push({type:6,index:i})}for(const e of t)r.removeAttribute(e)}if($.test(r.tagName)){const t=r.textContent.split(v),e=t.length-1;if(e>0){r.textContent=m?m.emptyScript:"";for(let A=0;A<e;A++)r.append(t[A],w()),z.nextNode(),s.push({type:2,index:++i});r.append(t[e],w())}}}else if(8===r.nodeType)if(r.data===y)s.push({type:2,index:i});else{let t=-1;for(;-1!==(t=r.data.indexOf(v,t+1));)s.push({type:7,index:i}),t+=v.length-1}i++}}static createElement(t,e){const A=b.createElement("template");return A.innerHTML=t,A}}function H(t,e,A=t,r){var i,n,o,s;if(e===D)return e;let a=void 0!==r?null===(i=A._$Cl)||void 0===i?void 0:i[r]:A._$Cu;const l=k(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==l&&(null===(n=null==a?void 0:a._$AO)||void 0===n||n.call(a,!1),void 0===l?a=void 0:(a=new l(t),a._$AT(t,A,r)),void 0!==r?(null!==(o=(s=A)._$Cl)&&void 0!==o?o:s._$Cl=[])[r]=a:A._$Cu=a),void 0!==a&&(e=H(t,a._$AS(t,e.values),a,r)),e}class U{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:A},parts:r}=this._$AD,i=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:b).importNode(A,!0);z.currentNode=i;let n=z.nextNode(),o=0,s=0,a=r[0];for(;void 0!==a;){if(o===a.index){let e;2===a.type?e=new V(n,n.nextSibling,this,t):1===a.type?e=new a.ctor(n,a.name,a.strings,this,t):6===a.type&&(e=new Z(n,this,t)),this.v.push(e),a=r[++s]}o!==(null==a?void 0:a.index)&&(n=z.nextNode(),o++)}return i}m(t){let e=0;for(const A of this.v)void 0!==A&&(void 0!==A.strings?(A._$AI(t,A,e),e+=A.strings.length-2):A._$AI(t[e])),e++}}class V{constructor(t,e,A,r){var i;this.type=2,this._$AH=L,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=A,this.options=r,this._$Cg=null===(i=null==r?void 0:r.isConnected)||void 0===i||i}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=H(this,t,e),k(t)?t===L||null==t||""===t?(this._$AH!==L&&this._$AR(),this._$AH=L):t!==this._$AH&&t!==D&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.S(t):S(t)?this.M(t):this.$(t)}A(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}S(t){this._$AH!==t&&(this._$AR(),this._$AH=this.A(t))}$(t){this._$AH!==L&&k(this._$AH)?this._$AA.nextSibling.data=t:this.S(b.createTextNode(t)),this._$AH=t}T(t){var e;const{values:A,_$litType$:r}=t,i="number"==typeof r?this._$AC(t):(void 0===r.el&&(r.el=F.createElement(r.h,this.options)),r);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===i)this._$AH.m(A);else{const t=new U(i,this),e=t.p(this.options);t.m(A),this.S(e),this._$AH=t}}_$AC(t){let e=B.get(t.strings);return void 0===e&&B.set(t.strings,e=new F(t)),e}M(t){x(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let A,r=0;for(const i of t)r===e.length?e.push(A=new V(this.A(w()),this.A(w()),this,this.options)):A=e[r],A._$AI(i),r++;r<e.length&&(this._$AR(A&&A._$AB.nextSibling,r),e.length=r)}_$AR(t=this._$AA.nextSibling,e){var A;for(null===(A=this._$AP)||void 0===A||A.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class q{constructor(t,e,A,r,i){this.type=1,this._$AH=L,this._$AN=void 0,this.element=t,this.name=e,this._$AM=r,this.options=i,A.length>2||""!==A[0]||""!==A[1]?(this._$AH=Array(A.length-1).fill(new String),this.strings=A):this._$AH=L}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,A,r){const i=this.strings;let n=!1;if(void 0===i)t=H(this,t,e,0),n=!k(t)||t!==this._$AH&&t!==D,n&&(this._$AH=t);else{const r=t;let o,s;for(t=i[0],o=0;o<i.length-1;o++)s=H(this,r[A+o],e,o),s===D&&(s=this._$AH[o]),n||(n=!k(s)||s!==this._$AH[o]),s===L?t=L:t!==L&&(t+=(null!=s?s:"")+i[o+1]),this._$AH[o]=s}n&&!r&&this.k(t)}k(t){t===L?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class Y extends q{constructor(){super(...arguments),this.type=3}k(t){this.element[this.name]=t===L?void 0:t}}class j extends q{constructor(){super(...arguments),this.type=4}k(t){t&&t!==L?this.element.setAttribute(this.name,""):this.element.removeAttribute(this.name)}}class Q extends q{constructor(t,e,A,r,i){super(t,e,A,r,i),this.type=5}_$AI(t,e=this){var A;if((t=null!==(A=H(this,t,e,0))&&void 0!==A?A:L)===D)return;const r=this._$AH,i=t===L&&r!==L||t.capture!==r.capture||t.once!==r.once||t.passive!==r.passive,n=t!==L&&(r===L||i);i&&this.element.removeEventListener(this.name,this,r),n&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,A;"function"==typeof this._$AH?this._$AH.call(null!==(A=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==A?A:this.element,t):this._$AH.handleEvent(t)}}class Z{constructor(t,e,A){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=A}get _$AU(){return this._$AM._$AU}_$AI(t){H(this,t)}}const G={P:"$lit$",V:v,L:y,I:1,N:R,R:U,D:S,j:H,H:V,O:q,F:j,B:Q,W:Y,Z};var W,J,K;null===(p=globalThis.litHtmlPolyfillSupport)||void 0===p||p.call(globalThis,F,V),(null!==(f=globalThis.litHtmlVersions)&&void 0!==f?f:globalThis.litHtmlVersions=[]).push("2.0.0");class X extends h{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const A=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=A.firstChild),A}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=((t,e,A)=>{var r,i;const n=null!==(r=null==A?void 0:A.renderBefore)&&void 0!==r?r:e;let o=n._$litPart$;if(void 0===o){const t=null!==(i=null==A?void 0:A.renderBefore)&&void 0!==i?i:null;n._$litPart$=o=new V(e.insertBefore(w(),t),t,void 0,null!=A?A:{})}return o._$AI(t),o})(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return D}}X.finalized=!0,X._$litElement$=!0,null===(W=globalThis.litElementHydrateSupport)||void 0===W||W.call(globalThis,{LitElement:X}),null===(J=globalThis.litElementPolyfillSupport)||void 0===J||J.call(globalThis,{LitElement:X}),(null!==(K=globalThis.litElementVersions)&&void 0!==K?K:globalThis.litElementVersions=[]).push("3.0.0");class tt extends X{static properties={multiple:{type:Boolean}};static styles=[o`
            :host {
                display: block;
            }
            .container {
                border: var(--crowd-accordion-border-width, 1px) var(--crowd-accordion-border-style, solid) var(--crowd-accordion-border-color, #ddd);
                border-radius: var(--crowd-accordion-border-radius, 4px);
            }
        `];connectedCallback(){super.connectedCallback();let t=[].slice.call(this.querySelectorAll("crowd-accordion-item"));t&&t.forEach((e=>{e.addEventListener("crowdOpen",(()=>{t.forEach((t=>{t===e||this.multiple||t.close()}))}),!1)}))}render(){return M`
            <div class='container' part='container'>
                <slot></slot>
            </div>
        `}}class et extends X{static properties={open:{type:Boolean,reflect:!0}};static styles=[o`
            :host {
                display: block;
            }
            * {
                margin: 0;
                padding: 0;
            }
            *,*::before, *::after {
                box-sizing: border-box;
            }
            .tray {
                max-height: 0px;
                transition-property: max-height;
                transition-duration: var(--accordion-item-transition-duration, 0.15s);
                transition-delay: var(--accordion-item-transition-delay, 0s);
                transition-timing-function: var(--accordion-item-ease, ease-in-out);
                overflow-y: scroll;
                padding: 0 var(--crowd-accordion-item-tray-padding-horizontal,1em);
                background-color: var(--crowd-accordion-tray-background-color, #fff);
                pointer-events: none;
            }
            :host([open]) .tray {
                max-height: 900vh;
                padding: var(--crowd-accordion-item-tray-padding-vertical,0.5em) var(--crowd-accordion-item-tray-padding-horizontal,1em);
                pointer-events: all;
            }
            h3 {
                display: flex;
                flex-flow: row nowrap;
                justify-content: space-between;
                align-items: center;
                cursor: pointer;
                padding: var(--crowd-accordion-item-title-padding-vertical,1em) var(--crowd-accordion-item-title-padding-horizontal,1em);
                color: var(--crowd-accordion-item-title-color, #000);
                background-color: var(--crowd-accordion-item-title-background-color, #fff);
                font-weight: var(--crowd-accordion-item-title-weight, 400);
                font-size: var(--crowd-accordion-item-title-font-size, 1.15em);
                font-family: var(--crowd-accordion-title-font-family, inherit);
            }
            @media (hover: hover) {
                h3:hover {
                    color: var(--crowd-accordion-item-title-hover-color, #000);
                    background-color: var(--crowd-accordion-item-title-hover-background-color, #eee);
                }
            }
            input {
                opacity: 0;
                position: absolute;
                -webkit-appearance: none;
            }
            .container:focus-within h3 {
                color: var(--crowd-accordion-item-title-hover-color, #000);
                background-color: var(--crowd-accordion-item-title-hover-background-color, #eee);
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }

            :host([open]) h3 {
                color: var(--crowd-accordion-item-title-open-color, #000);
                background-color: var(--crowd-accordion-item-title-open-background-color, #eee);
                border-bottom: var(--crowd-accordion-border-width, 1px) var(--crowd-accordion-border-style, solid) var(--crowd-accordion-border-color, #ddd);
            }

            h3 crowd-icon {
                transition-property: transform;
                transition-duration: var(--accordion-item-transition-duration, 0.15s);
                transition-delay: var(--accordion-item-transition-delay, 0s);
                transition-timing-function: var(--accordion-item-ease, ease-in-out);
            }
            :host([open]) h3 crowd-icon {
                transform: rotate(180deg);
            }
            :host(:first-child) .container,:host(:first-child) .container h3 {
                border-top-left-radius: var(--crowd-accordion-border-radius, 4px);
                border-top-right-radius: var(--crowd-accordion-border-radius, 4px);
            }
            :host(:last-child) .container,:host(:last-child) .container .tray, :host(:last-child:not([open])) .container h3 {
                border-bottom-left-radius: var(--crowd-accordion-border-radius, 4px);
                border-bottom-right-radius: var(--crowd-accordion-border-radius, 4px);
            }
            :host(:not(:first-child)) .container {
                border-top: var(--crowd-accordion-border-width, 1px) var(--crowd-accordion-border-style, solid) var(--crowd-accordion-border-color, #ddd);
            }
            :host(:nth-child(2):last-child) .container {
                border-top: none;
            }
        `];show(){this.open=!0;const t=new CustomEvent("crowdOpen");this.dispatchEvent(t)}close(){this.open=!1;const t=new CustomEvent("crowdClose");this.dispatchEvent(t)}toggle(){let t;this.open=!this.open,t=this.open?new CustomEvent("crowdOpen"):new CustomEvent("crowdClose"),this.dispatchEvent(t)}_focus(){this.renderRoot.querySelector("input").focus()}_keyDown(t){switch(t.key){case" ":case"Enter":this.toggle();break;case"Tab":break;default:t.preventDefault()}}render(){return M`
            <div class='container' part='container'>
                <input inputmode='none' @keydown='${t=>this._keyDown(t)}' type='text' />
                <h3 @click='${()=>{this._focus(),this.toggle()}}'>${this.title}<crowd-icon name='chevron-down'></crowd-icon></h3>
                <div class='tray' part='tray'>
                    <slot></slot>
                </div>
            </div>
        `}}class At extends X{static properties={open:{type:Boolean,reflect:!0},closable:{type:Boolean},duration:{type:Number},toast:{type:Boolean},title:{type:String}};static styles=[o`
            :host {
                display: block;
            }
            :host, :host * {
                box-sizing: inherit;
            }
            .alert {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                background-color: var(--crowd-alert-background-color, white);
                padding: var(--crowd-alert-padding-vertical, 1em) var(--crowd-alert-padding-horizontal,1em);
                gap: var(--crowd-alert-spacing, 1em);
                color: inherit;
                border: 1px solid var(--crowd-alert-outline-color,#eee);
                border-top: var(--crowd-alert-border-width, 3px) var(--crowd-alert-border-style, solid) var(--crowd-alert-border-color,black);
                animation: close 0.2s forwards;
                border-radius: var(--crowd-alert-border-radius,2px);
                font-size: var(--crowd-alert-font-size, inherit);
                pointer-events: var(--crowd-alert-pointer-events, all);
            }
            .alert > div {
                flex: 1 1 auto;
            }
            slot[name='icon'] {
                color: var(--crowd-alert-color, black);
            }

            :host([open]) .alert {
                animation: open 0.2s forwards;
            }
            :host([toast]) .alert {
                box-shadow: var(
                    --crowd-alert-box-shadow,
                    0 2px 8px rgba(0, 0, 0, 0.1)
                );
                animation: none;
            }
            [popover] {
                margin: var(--alert-toast-position-top, 1em) var(--alert-toast-position-right, 1em) var(--alert-toast-position-bottom, auto) var(--alert-toast-position-left, auto);
                padding: 0;
                border: none;
            }

            .close {
                margin-left: auto;
            }

            h3 {
                display: flex;
                gap: 0.5em;
                align-items: center;
                margin: 0;
            }

            @keyframes open {
                0% {
                    height: 0px;
                    transform: scale(0);
                    opacity: 0;
                    padding: 0;
                    border-width: 0px;
                }
                1% {
                    height: auto;
                    transform: scale(0.5);
                    padding: 0;
                    border-width: 0px;
                    opacity: 0;
                }
                100% {
                    transform: scale(1);
                    opacity: 1;
                }
            }

            @keyframes close {
                0% {
                    height: auto;
                    transform: scale(1);
                    opacity: 1;
                }
                99% {
                    height: auto;
                    transform: scale(0.5);
                    opacity: 0;
                    padding: 0;
                    border-width: 0px;
                }
                100% {
                    height: 0px;
                    transform: scale(0);
                    opacity: 0;
                    padding: 0;
                    border-width: 0px;
                }
            }
        `];connectedCallback(){super.connectedCallback(),this.duration&&setTimeout((()=>this.hide()),this.duration),this.toast&&(console.log(this.alert),this.toastEl.hidePopover())}get toastEl(){return console.log("getting"),this.renderRoot.querySelector("[popover]")}show(){this.toast?this.toastEl.showPopover():(this.open=!0,this.duration&&setTimeout((()=>this.hide()),this.duration))}hide(){this.open=!1,this.toast&&this.toastEl.hidePopover()}constructor(){super(),this.toast=!1}render(){let t="";this.closable&&(t=M`
                <crowd-icon-button class='close' name='x' @click='${()=>this.hide()}'></crowd-icon-button>
            `);const e=M`
            <article class='alert' part='alert'>
                <div>
                    <h3>
                        <slot name='icon'></slot>
                        ${this.title}
                    </h3>
                    <slot></slot>
                </div>
                ${t}
            </article>
        `;return this.toast?M`<div popover>
                ${e}
            </div>`:M`
            ${e}
        `}}class rt extends X{static properties={pill:{type:Boolean},pulse:{type:Boolean},type:{type:String}};static styles=[o`
            :host {
                display: inline-grid;
                place-items: center;
            }

            :host([type='danger']) {
                --crowd-badge-color: #f92f06;
            }

            :host([type='warning']) {
                --crowd-badge-color: #ffd000;
            }

            :host([type='success']) {
                --crowd-badge-color: #65d672;
            }

            .badge {
                color: var(--crowd-badge-text-color, inherit);
                background-color: var(--crowd-badge-color);
                display: inline-flex;
                align-items: center;
                justify-content: center;
                font-size: inherit;
                font-weight: inherit;
                line-height: 1;
                white-space: nowrap;
                padding: var(--crowd-badge-padding-vertical,3px) var(--crowd-badge-padding-horizontal, 6px);
                user-select: none;
                cursor: inherit;
            }

            :host([pill]) .badge {
                border-radius: 999px;
            }

            :host([pulse]) .badge {
                animation: 1.5s ease 0s infinite normal none running pulse;
            }

            .badge {
                border-radius: var(--crowd-badge-border-radius, 0px);
            }

            @keyframes pulse {
                0% {
                    box-shadow: 0 0 0 0 var(--pulse-color, var(--crowd-badge-color));
                }
                70% {
                    box-shadow: 0 0 0 0.5rem transparent;
                }
                100% {
                    box-shadow: 0 0 0 0 transparent;
                }
            }
        `];render(){return M`
            <span class='badge' part='badge'>
                <slot></slot>
            </span>
        `}}class it extends X{static properties={href:{type:String},pill:{type:Boolean},loading:{type:Boolean},disabled:{type:Boolean},caret:{type:Boolean},target:{type:String},circle:{type:Boolean}};static styles=o`
        :host {
            display: inline-block;
            width: auto;
            cursor: pointer;
            line-height: 1;
            height:min-content;
        }
        button,a {
            -webkit-appearance: none;
            background-color: var(--crowd-button-background-color, #eeeeee);
            padding: var(--crowd-button-padding-vertical,0.5em) var(--crowd-button-padding-horizontal, 1em);
            color: var(--crowd-button-color, inherit);
            border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-button-border-color, #aeaeae);
            border-radius: var(--crowd-button-border-radius, 3px);
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            text-align: var(--crowd-button-text-align, center);
            display: inline-flex;
            width: 100%;
            height: var(--crowd-button-height, 2.5em);
            flex-flow: row nowrap;
            justify-content: var(--crowd-button-justify,center);
            align-items: center;
            gap: var(--crowd-button-gap, 0.5em);
            cursor: pointer;
            margin: 0;
            text-decoration: none;
            text-transform: inherit;
            box-sizing: border-box;
            transition-property: background-color, border-color, color;
            transition-duration: var(--crowd-button-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-button-transition-ease, ease-in-out);
            transition-delay: var(--crowd-button-transition-delay, 0s);
            position:relative;
            text-transform: var(--crowd-button-text-transform, inherit);
        }
        button:focus-visible, button:active, a:focus-visible,a:active {
            outline: none;
        }
        button:focus-visible,button:active, a:focus-visible,a:active {
            box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-button-focus-color, rgba(0,0,0,0.3));
        }
        :host([pill]) button, :host([pill]) a {
            border-radius: var(--crowd-button-pill-border-radius, 999px);
        }
        @media (hover: hover) {
            button:hover, a:hover {
                background-color: var(--crowd-button-hover-background-color, #aeaeae);
                border-color: var(--crowd-button-hover-border-color, #aeaeae);
                color: var(--crowd-button-hover-color, #fff);
            }
        }
        :host([disabled]) {
            opacity: 0.5;
            pointer-events: none;
        }
        :host([circle]) button, :host([circle]) a {
            aspect-ratio: 1/1;
            height: var(--crowd-button-width, auto);
            width: var(--crowd-button-width, auto);
            border-radius: 50%;
        }
        slot[name='prefix'] svg,
        slot[name='suffix'] svg {
            height: 1.5em;
        }
        .prefix,.suffix {
            display: inline-grid;
            place-items:center;
        }
        .label {
            display: flex;
            flex-flow: row nowrap;
            justfy-content: flex-start;
            align-items:center;
        }
        :host([loading]) button, :host([loading]) a {
            display: inline-grid;
            place-items: center;
        }
        crowd-spinner {
            position: absolute;
            top:50%;
            left:50%;
            transform: translate(-50%,-50%);
        }
        .loading {
            display: inline-flex;
            flex-flow: row nowrap;
            justify-content: var(--crowd-button-justify,center);
            align-items: center;
            gap: var(--crowd-button-gap, 0.5em);
            opacity: 0;
        }
    `;constructor(){super()}render(){let t="";this.caret&&(t=M`
                <crowd-icon name='chevron-down'></crowd-icon>
            `);let e=M`
            <span class='prefix' part='prefix'>
                <slot name='prefix'></slot>
            </span>
            <span class='label' part='label'>
                <slot></slot>
            </span>
            <span class='suffix' part='suffix'>
                <slot name='suffix'>${t}</slot>
            </span>
        `;this.loading&&(e=M`<crowd-spinner></crowd-spinner><span class='loading'>${e}</span>`);let A=M`
            <button part='button'>
                ${e}
            </button>
        `;return this.href&&(A=M`
                <a part='button' href='${this.href}' target='${this.target}'>
                    ${e}
                </a>
            `),M`
            ${A}
        `}}class nt{constructor(t){this.element=t,this.form=t.closest("form"),this.bindHandleFormData=this.handleFormData.bind(this),this.form&&this.element.name&&this.form.addEventListener("formdata",this.bindHandleFormData,!1),this.bindHandleFormSubmit=this.handleFormSubmit.bind(this),this.form&&this.form.addEventListener("submit",this.bindHandleFormSubmit,!1)}handleFormSubmit(t){const e=this.element.disabled;this.element.validate&&this.element.validate(),this.form&&!this.form.noValidate&&!e&&this.element.invalid&&(t.preventDefault(),t.stopImmediatePropagation())}handleFormData(t){for(var e of(null==this.element.value?t.formData.append(this.element.name,null):Array.isArray(this.element.value)?this.element.value.forEach((e=>{t.formData.append(this.element.name,e.toString())})):(console.log(this.element.name,this.element.value.toString()),t.formData.append(this.element.name,this.element.value.toString())),t.formData.entries()))console.log(e[0]+", "+e[1])}}class ot extends X{static properties={name:{type:String},value:{type:String},checked:{type:Boolean,reflect:!0},required:{type:Boolean},errorMessage:{type:String},invalid:{type:Boolean,reflect:!0}};static styles=o`
        :host {
            display: block;
            position:relative;
        }
        input {
            -webkit-apperance: none;
            height: 0px;
            width: 0px;
            opacity: 0;
            position: absolute;
            margin: 0;
        }
        label {
            color: var(--crowd-input-color, inherit);
            font-family: inherit;
            font-weight: inherit;
            font-size: inherit;
            cursor: pointer;
            display: inline-flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: var(--crowd-checkbox-align, center);
            gap: var(--crowd-checkbox-spacing,0.5em);
        }
        .box {
            border: var(--crowd-checkbox-border-width,1px) var(--crowd-checkbox-border-style,solid) var(--crowd-checkbox-border-color,black);
            min-width: calc(1.2em - (2 * var(--crowd-checkbox-border-width,1px)));
            min-height: calc(1.2em - (2 * var(--crowd-checkbox-border-height,1px)));
            max-width: calc(1.2em - (2 * var(--crowd-checkbox-border-width,1px)));
            max-height: calc(1.2em - (2 * var(--crowd-checkbox-border-height,1px)));
            background-color: var(--crowd-checkbox-background-color, transparent);
            color: var(--crowd-checkbox-color, black);
            line-height: 1;
            display: grid;
            place-items: center;
            font-size: 1em;
            position: relative;
            transition-property: color, background-color;
            transition-duration: var(--crowd-checkbox-transition-duration,0.15s);
            transition-timing-function: var(--crowd-checkbox-transition-ease, ease-in-out);
            transition-delay: var(--crowd-checkbox-transition-delay,0s);
        }
        .container {
            max-width: 100%;
        }
        .box svg {
            position:absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        input:focus-visible + label .box {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        .error {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        :host([checked]) .box {
            background-color: var(--crowd-checkbox-checked-background-color, black);
            color: var(--crowd-checkbox-checked-color, white);
        }
        :host([invalid]) input {
            outline: 1px solid var(--crowd-input-error-message-color, red);
        }
    `;constructor(){super(),this.checked=!1,this.invalid=!1}connectedCallback(){super.connectedCallback(),this.id="checkbox-"+Date.now(),new nt(this)}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onChange(t){this.checked=t.currentTarget.checked,this.invalid=!1,this.checked?this.value=t.currentTarget.value:this.value=null,this._dispatchChange()}validate(){this.required&&(this.checked||(this.invalid=!0))}render(){let t="";this.checked&&(t=M`
                <slot name='check-icon'>
                    <svg viewBox="0 0 16 16"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"><g stroke="currentColor" stroke-width="2"><g transform="translate(3.428571, 3.428571)"><path d="M0,5.71428571 L3.42857143,9.14285714"></path><path d="M9.14285714,0 L3.42857143,9.14285714"></path></g></g></g></svg>
                </slot>
            `);let e="";return this.invalid&&(e=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `),M`
            <div class='container' part='container'>
                <input @change='${this._onChange}' type='checkbox' id='${this.id}' name='${this.name}' value='${this.value}' required='${this.required}' />
                <label part='label' for='${this.id}'>
                    <div class='box' part='box'>
                        ${t}
                    </div>
                    <slot></slot>
                </label>
                ${e}
            </div>
        `}}const st=A(621),at=(t=0,e=20)=>+(Math.round(t+`e${e}`)+`e-${e}`);class lt extends X{static properties={value:{type:String,reflect:!0},name:{type:String},label:{type:String},_unit:{type:String},_hue:{type:Number},_sat:{type:Number},_v:{type:Number},_alpha:{type:Number},_recentColors:{type:Array}};static styles=[o`
            :host {
                display: inline-block;
            }
            :host,:host * {
                box-sizing: border-box;
                margin: 0;
                padding: 0;
            }
            .pallete {
                background: linear-gradient(to top, hsla(0,0%,0%,calc(var(--a))), transparent), linear-gradient(to left, hsla(calc(var(--h)),100%,50%,calc(var(--a))),hsla(0,0%,100%,calc(var(--a)))),linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 ),linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 );
                background-position: 0 0, 0 0,0 0,5px 5px;
                background-size: 100% 100%, 100% 100%, 10px 10px, 10px 10px;
                user-select: none;
                cursor: crosshair;
                min-width: 150px;
                min-height: 150px;
                position:relative;
            }
            .hue-range,.alpha-range {
                -webkit-appearance: none;
                display: block;
                border-radius: 999px;
                width: 100%;
            }
            input[type="range"]::-webkit-slider-thumb {
                -webkit-appearance: none;
                position: relative;
                width: 10px;
                height: 10px;
                transform: scale(1.2);
                border-radius: 50%;
                box-shadow: 0 0 10px rgb(0 0 0 / 10%);
                background: #fff;
                transition: .2s cubic-bezier(.12, .4, .29, 1.46);
            }
            .hue-range {
                background: linear-gradient(to right, red, yellow, lime, cyan, blue, magenta, red);
            }
            .alpha-range {
                background: linear-gradient(to right, hsla(calc(var(--h)),100%,50%,0), hsla(calc(var(--h)),100%,50%,1)),linear-gradient( 
                    45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 ),linear-gradient( 
                    45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 );
                        background-position: 0 0,0 0,5px 5px;
                        background-size: 100% 100%,10px 10px,10px 10px;
            }
            .indicator {
                position: relative;
            }
            .indicator::after,.indicator::before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
            }
            .indicator::after {
                background: var(--background);
            }
            .indicator::before {
                background: linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 ),linear-gradient( 45deg, #ddd 25%,transparent 0,transparent 75%,#ddd 0 );
                background-position: 0 0,5px 5px;
                background-size: 10px 10px, 10px 10px;
            }
            .container {
                display: flex;
                flex-flow: column;
                align-items: stretch;
                gap: var(--crowd-color-picker-spacing,0.2em);
            }
            .recent-colors {
                display: flex;
                gap: 5px;
            }
            .recent-colors button {
                -webkit-appearance: none;
                border: 1px solid #eee;
                padding: 0;
            }
            .pallete-indicator {
                display: inline-block;
                position: absolute;
                left: var(--s);
                top: calc(100% - var(--v));
                width: 5px;
                height: 5px;
                transform: translate(-50%,-50%);
                border-radius: 50%;
                background-color: white;
                border: 1px solid #eee;
            }
        `];_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_setValue(){let t=st(`hsva(${this._hue},${at(this._sat,2)}%,${at(this._v,2)}%,${this._alpha}%)`);t.setAlpha(this._alpha/100),this.value=t.toHslString();let e=JSON.parse(localStorage.getItem("recentColors"));e||(e=[]),e.indexOf(this.value)<0&&e.push(this.value),localStorage.setItem("recentColors",JSON.stringify(e)),this._dispatchChange()}_chooseColor(t){let e=t.currentTarget.getBoundingClientRect().width,A=t.currentTarget.getBoundingClientRect().height,r=t.offsetX/e*100,i=100*(1-t.offsetY/A);this._sat=r,this._v=i,this._setValue()}constructor(){super(),this._hue=0,this._sat=0,this._v=0,this._alpha=100,this._recentColors=[]}_loop(){this._recentColors=JSON.parse(localStorage.getItem("recentColors")),requestAnimationFrame((()=>this._loop()))}connectedCallback(){super.connectedCallback(),localStorage.getItem("recentColors")||localStorage.setItem("recentColors","[]"),this._loop()}_redraw(){let t=st(this.value).toHsv();this._hue=t.h,this._sat=100*t.s,this._v=100*t.v,this._alpha=100*t.a,this._dispatchChange()}render(){let t="";return this._recentColors&&(t=M`
                <div class='recent-colors'>
                    ${this._recentColors.slice(-8).reverse().map((t=>M`<button @click='${()=>{this.value=t,this._redraw()}}' style='background-color: ${t};width:1em;height: 1em;'></button>`))}
                </div>
            `),M`
            <crowd-dropdown style='--h: ${this._hue};--s:${this._sat}%;--v: ${this._v}%;--a: ${this._alpha}%;'>
                <crowd-button slot='trigger' style='--crowd-button-background-color:var(--crowd-color-picker-background-color,#eee);--crowd-button-gap:0px;--crowd-button-padding-vertical:10px;--crowd-button-padding-horizontal: 10px;'>
                    <div style='width: 1em;height: 1em;--background: ${this.value};' class='indicator'></div>
                </crowd-button>
                <div part='container' class='container'>
                    <div @mouseup='${t=>this._chooseColor(t)}' part='pallete' class='pallete'>
                        <span class='pallete-indicator'></span>
                    </div>
                    <input class='hue-range' type='range' min='0' max='360' step='1' value='${this._hue}' @change='${t=>{this._hue=t.currentTarget.value,this._setValue()}}' />
                    <input class='alpha-range' type='range' min='0' max='100' step='1' value='${this._alpha}' @change='${t=>{this._alpha=t.currentTarget.value,this._setValue()}}' />
                    ${t}
                    <small>${this.value}</small>
                </div>
            </crowd-dropdown>
        `}}class ct extends X{static properties={show:{type:Boolean,reflect:!0}};toggle(){this.show=!this.show}open(){this.show=!0,this.querySelector("crowd-menu")&&this.querySelector("crowd-menu").querySelector("crowd-menu-item")&&this.querySelector("crowd-menu").querySelector("crowd-menu-item").triggerFocus()}close(){this.show=!1}static styles=[o`
            :host {
                display: contents;
                --mouse-y: 0px;
                --mouse-x: 0px;
            }
            * {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
                transition-duration: var(--context-menu-transition-duration, 0.15s);
                transition-property: none;
            }
            @media (prefers-reduced-motion: reduced) {
                * {
                    transition-duration: none;
                }
            }
            .context {
                position: fixed;
                top: calc(var(--mouse-y) + 0.5em);
                left: calc(var(--mouse-x) + 0.5em);
                opacity: 0;
                transform: scale(0);
                transition-property: opacity, transform;
                background-color: var(--context-background-color, white);
                padding: var(--context-padding-vertical,1em) var(--context-padding-horizontal,1em);
                box-shadow: var(--context-padding-box-shadow, 0px 10px 15px -3px rgba(0,0,0,0.1));
                z-index: var(--context-z-index,999);
                width: var(--context-width,250px);
            }
            :host([show]) .context {
                opacity: 1;
                transform: scale(1);
            }
            .overlay {
                position: fixed;
                inset: 0;
                pointer-events: none;
                z-index: calc(var(--context-z-index,999) - 1);
            }
            :host([show]) .overlay {
                pointer-events: all;
            }
        `];connectedCallback(){super.connectedCallback(),window.addEventListener("mousemove",(t=>{this.show||(this.style.setProperty("--mouse-x",t.clientX+"px"),this.style.setProperty("--mouse-y",t.clientY+"px"))})),document.addEventListener("contextmenu",(t=>{t.preventDefault(),this.open()}),!1)}render(){return M`
            <div class='overlay' @click='${()=>this.close()}'></div>
            <div class='context'>
                <slot></slot>
            </div>
        `}}class dt extends X{static properties={property:{type:String},type:{type:String},min:{type:Number},max:{type:Number},value:{type:String,reflect:!0},unit:{type:String},step:{type:Number},options:{type:Array}};static styles=[o`
            :host {
                display: inline-block;
            }
        `];_onChange(t){let e;this.value=t.target.value,e="Number"==this.type?this.value+this.unit:this.value,document.documentElement.style.setProperty(this.property,e);const A=new CustomEvent("crowdChange");this.dispatchEvent(A)}constructor(){super()}render(){if(!this.property)throw'"property" attribute is not defined.';if(!this.type)throw'"type" attribute is not defined.';if("Number"===this.type&&null===this.unit)throw'"unit" attribute is not defined.';let t="";return"Number"==this.type?t=M`
                <!-- <crowd-range @crowdChange='${t=>this._onChange(t)}' step='${this.step}' tooltip min='${this.min}' max='${this.max}' value='${this.value}' ></crowd-range> -->
                <crowd-number @crowdChange='${t=>this._onChange(t)}' placement='after' unit="${this.unit}" value='${this.value}' step='${this.step}' min='${this.min}' max='${this.max}'></crowd-number>
            `:"Color"==this.type?t=M`
                <crowd-color-picker @crowdChange='${t=>this._onChange(t)}' value='${this.value}'></crowd-color-picker>
            `:"Select"==this.type?t=M`
                <crowd-select @crowdChange='${t=>this._onChange(t)}' value='${this.value}'>
                    ${this.options.map((t=>M`<crowd-option value='${t}'>${t}</crowd-option>`))}
                </crowd-select>
            `:"Text"==this.type&&(t=M`
                <crowd-input @crowdChange='${t=>this._onChange(t)}' type='text' value='${this.value}'></crowd-input>
            `),M`
            ${t}
        `}}const{DateTime:ut}=A(490);class ht extends X{static properties={value:{type:String,reflect:!0},open:{type:Boolean,reflect:!0},month:{type:Number},year:{type:Number},selected:{type:Object},label:{type:String},required:{type:Boolean},errorMessage:{type:String},invalid:{type:Boolean,reflect:!0},format:{type:String,reflect:!0},range:{type:Boolean},dateRange:{type:Array},rangeState:{type:Number},weekFormat:{type:String}};static styles=[o`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .container {
                position: relative;
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            label.label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            .calendar {
                display: grid;
                grid-template-columns: repeat(7,1fr);
                padding-top: var(--crowd-date-picker-calendar-padding-top,1em);
            }
            .panel {
                position: absolute;
                top: 100%;
                left: 0;
                width:max-content;
                transform-origin: top center;
                transform: scaleY(0);
                transition-property: transform;
                transition-duration: var(--crowd-date-picker-transition-duration, 0.15s);
                transition-delay: var(--crowd-date-picker-transition-delay, 0s);
                transition-timing-function: var(--crowd-date-picker-transition-ease, ease-in-out);
            }
            :host([open]) .panel {
                transform: scaleY(1);
            }
            .panel {
                font-size: var(--crowd-date-time-picker-font-size,0.7rem);
                --crowd-input-font-size: var(--crowd-date-time-picker-font-size,0.7rem);
                --crowd-input-padding-vertical: 0px;
                background-color: var(--crowd-date-picker-panel-background-color, #fff);
                padding: var(--crowd-date-picker-panel-padding-vertical, 1em) var(--crowd-date-picker-panel-padding-horizontal, 1em);
                box-shadow: var(
                    --crowd-date-picker-panel-box-shadow,
                    0 2px 8px rgba(0, 0, 0, 0.1)
                );
                z-index: var(--crowd-date-picker-z-index, 999);
            }
            .calendar-nav {
                display: flex;
                justify-content: var(--crowd-date-picker-calendar-nav-justify, center);
                align-items: center;
                padding: var(--crowd-date-picker-calendar-nav-padding-vertical, 0) var(--crowd-date-picker-calendar-nav-padding-horizontal, 1em);
            }
            crowd-select::part(toggle) {
                font-size: 0.5em;
            }
            .month-picker {
                width: var(--crowd-date-picker-month-picker-width,13ch);
            }
            .year-picker {
                width: var(--crowd-date-picker-year-picker-width,12ch);
            }
            .calendar-nav crowd-button {
                margin: 0 auto;
                -webkit-appearance: none;
                cursor: pointer;
                --crowd-button-width: 1.5em;
                --crowd-button-padding-vertical: 0px;
                --crowd-button-padding-horizontal: 0px;
                --crowd-button-background-color: transparent;
                --crowd-button-hover-background-color: var(--crowd-date-picker-calendar-nav-button-hover-background-color, #f9f9f9);
                --crowd-button-border-width: 0px;
                --crowd-button-color: var(--crowd-menu-item-hover-color,inherit);
                --crowd-button-hover-color: var(--crowd-menu-item-hover-color,inherit);
            }
            .calendar-nav .active {
                text-decoration: underline;
            }
            .calendar-nav .today-button {
                margin-right: 0;
            }

            .weekday,.day {
                aspect-ratio: 1/1;
                padding: 5px;
            }

            .weekday {
                font-size: 0.5em;
            }

            .day {
                cursor: pointer;
                transition-property: background-color;
                transition-duration: var(--crowd-date-picker-transition-duration, 0.15s);
                transition-timing-function: var(--crowd-date-picker-transition-ease, ease-in-out);
                transition-delay: var(--crowd-date-picker-transition-delay, 0s);
                border: var(--crowd-date-picker-day-border-width,1px) var(--crowd-date-picker-day-border-style,solid) var(--crowd-date-picker-day-border-color,#f7f7f7);
            }

            @media (hover: hover) {
                .day:hover {
                    background-color: var(--crowd-date-picker-day-hover-background-color,#f9f9f9);
                }
            }
            .day:focus-visible {
                background-color: var(--crowd-date-picker-day-hover-background-color,#f9f9f9);
            }

            .day.today {
                text-decoration: underline;
            }
            .day.ranged.from ~ * {
                background-color: var(--crowd-date-picker-day-hover-background-color,#f9f9f9);
            }
            .day.ranged.from.to ~ *,.day.ranged.to ~ * {
                background-color: transparent;
            }

            .day.ranged,.day.ranged.from,.day.ranged.to, .day.active, .day:active {
                background-color: var(--crowd-date-picker-day-selected-background-color, #eee);
            }
            .action-buttons {
                margin-top: 0.5em;
                display: flex;
                gap: 1em;
                justify-content: space-between;
            }
        `];_generateCalendar(){let t=new Date(this.year,this.month+1,0).getDate(),e=[];for(let A=0;A<t;A++)e[A]=ut.fromObject({day:A+1,month:this.month+1,year:this.year});return e}_prevMonth(){0==this.month?(this.month=11,this.year-=1):this.month-=1}_nextMonth(){11==this.month?(this.month=0,this.year+=1):this.month+=1}_selectDay(t){this.selected=t,this.month=t.month-1,this.year=t.year,this.range&&(this.dateRange[this.rangeState]=this.selected)}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onInput(t){this.invalid=!1,this._dispatchChange()}_setValue(){if(this.range)this.value=this.dateRange[0].toFormat(this.format)+" - "+this.dateRange[1].toFormat(this.format);else{let t=this.selected;this.value=t.toFormat(this.format)}this._dispatchChange()}_onKeydown(t){"Tab"!=t.key&&t.preventDefault()}_showPicker(){this.open=!0}_hidePicker(){this.open=!1}_swapRange(t){this.rangeState=t,this.selected=this.dateRange[t]}_isSelected(t){return t.hasSame(this.selected,"day")&&t.hasSame(this.selected,"month")&&t.hasSame(this.selected,"year")}_isRanged(t){if(!this.range)return!1;let e=t.hasSame(this.dateRange[0],"day")&&t.hasSame(this.dateRange[0],"month")&&t.hasSame(this.dateRange[0],"year"),A=t.hasSame(this.dateRange[1],"day")&&t.hasSame(this.dateRange[1],"month")&&t.hasSame(this.dateRange[1],"year");return e||A}_dayRangeState(t){let e=t.hasSame(this.dateRange[0],"day")&&t.hasSame(this.dateRange[0],"month")&&t.hasSame(this.dateRange[0],"year"),A=t.hasSame(this.dateRange[1],"day")&&t.hasSame(this.dateRange[1],"month")&&t.hasSame(this.dateRange[1],"year");return e&&A?"from to":e?"from":A?"to":void 0}connectedCallback(){super.connectedCallback(),new nt(this)}constructor(){super(),this.year=(new Date).getFullYear(),this.month=(new Date).getMonth(),this.weekdays=new Array(7).fill(0),this.monthList=new Array(12).fill(0);let t=ut.now();this.selected=t,this.format="dd/MM/y",this.dateRange=[t,t],this.rangeState=0,this._setValue(),this.weekFormat="cccc"}render(){let t="";this.invalid&&(t=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let e="";return this.label&&(e=M`
                <label class='label' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `),M`
            <div class='container' @focusin='${this._showPicker}' @focusout='${this._hidePicker}'>
                ${e}
                <div class='input'>
                    <crowd-input part='input' @keydown='${this._onKeydown}' @crowdChange='${this._onInput}' type='text' value='${this.value}'></crowd-input>
                </div>
                <div class='panel' part='panel' tabindex='0'>
                    <div class='calendar-nav'>
                        <crowd-button circle @click='${this._prevMonth}'>
                            <slot name='prev-icon'>
                                <crowd-icon name='chevron-left'></crowd-icon>
                            </slot>
                        </crowd-button>
                        <p>
                            <crowd-select class='month-picker' @crowdChange='${t=>this.month=parseInt(t.currentTarget.value)}' value='${this.month}'>
                                ${this.monthList.map(((t,e)=>{let A=ut.fromObject({month:e+1});return M`<crowd-option value='${e}'>${A.monthLong}</crowd-option>`}))}
                            </crowd-select>
                            <crowd-select class='year-picker' @crowdChange='${t=>this.year=parseInt(t.currentTarget.value)}' value='${this.year}'>
                                ${new Array(5).fill(0).map(((t,e)=>M`
                                        <crowd-option value='${this.year-5+e}'>${this.year-5+e}</crowd-option>
                                    `))}
                                ${new Array(5).fill(0).map(((t,e)=>M`
                                        <crowd-option value='${this.year+e}'>${this.year+e}</crowd-option>
                                    `))}
                            </crowd-select>
                        </p>
                        <crowd-button circle @click='${this._nextMonth}'>
                            <slot name='next-icon'>
                                <crowd-icon name='chevron-right'></crowd-icon>
                            </slot>
                        </crowd-button>
                        <crowd-button class='today-button' circle @click='${()=>this._selectDay(ut.now())}'>
                            <slot name='today-icon'>
                                <crowd-icon name='calendar-event'></crowd-icon>
                            </slot>
                        </crowd-button>
                    </div>
                    ${this.range?M`
                        <div class='calendar-nav'>
                            <crowd-button @click='${()=>this._swapRange(0)}' class='${0==this.rangeState?"active":""}'>FROM</crowd-button>
                            <crowd-button @click='${()=>this._swapRange(1)}' class='${1==this.rangeState?"active":""}'>TO</crowd-button>
                        </div>
                    `:""}
                    <div class='calendar'>
                        ${this.weekdays.map(((t,e)=>{let A=ut.fromObject({weekday:e+1});return M`<div class='weekday'>${A.toFormat(this.weekFormat)}</div>`}))}
                        ${new Array(this._generateCalendar()[0].weekday-1).fill(0).map(((t,e)=>M`<div class='day' @click='${this._prevMonth}'></div>`))}
                        ${this._generateCalendar().map(((t,e)=>M`
                                <div tabindex='0' @click='${()=>this._selectDay(t)}' class='day ${this._isRanged(t)?`ranged ${this._dayRangeState(t)}`:""} ${this._isSelected(t)?"active":""} ${t.hasSame(ut.now(),"day")?"today":""}' part='day'>
                                    ${t.day}
                                </div>
                            `))}
                        ${new Array(7-this._generateCalendar()[this._generateCalendar().length-1].weekday).fill(0).map(((t,e)=>M`<div class='day' @click='${this._nextMonth}'></div>`))}
                    </div>
                    <div class='action-buttons'>
                        <crowd-button @click='${this._hidePicker}'>CANCEL</crowd-button>
                        <crowd-button @click='${()=>{this._setValue(),this._hidePicker()}}'>OK</crowd-button>
                    </div>
                </div>
                ${t}
                <slot name="help-text"></slot>
            </div>
        `}}const pt=new Event("crowdDialogHide",{bubbles:!0,composed:!0}),ft=new Event("crowdDialogShow",{bubbles:!0,composed:!0});class mt extends X{static properties={open:{type:Boolean,reflect:!0},label:{type:String}};static styles=o`
        :host .dialog-container {
            pointer-events: none;
            visibility:hidden;
            background-color: transparent;
            transition-property: visibility;
            transition-duration: var(--crowd-dialog-container-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-dialog-container-transition-function, ease-in-out);
            transition-delay: var(--crowd-dialog-container-transition-delay, 0.3s);
        }
        :host([open]) .dialog-container {
            pointer-events: all;
            visibility:visible;
            transition-property: none;
        }
        :host .dialog-overlay {
            transition-property: background-color;
            transition-duration: var(--crowd-dialog-overlay-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-dialog-overlay-transition-function, ease-in-out);
            transition-delay: var(--crowd-dialog-overlay-transition-delay, 0.3s);
        }
        :host([open]) .dialog-overlay {
            background-color: var(--crowd-dialog-overlay-background, rgba(0,0,0,0.3));
        }
        dialog::backdrop {
            background-color: var(--crowd-dialog-overlay-background, rgba(0,0,0,0.3));
        }
        :host dialog {
            transform: scale(0);
        }
        :host([open]) dialog {
            transform: scale(1);
            transition-delay: var(--crowd-dialog-transition-delay, 0.2s);
        }
        dialog {
            all:unset;
            display: block;
        }
        dialog {
            background-color: var(--crowd-dialog-background, white);
            padding: var(--crowd-dialog-padding-vertical,1em) var(--crowd-dialog-padding-horizontal,1em);
            height: min(var(--height,calc(100% - (2 * var(--crowd-dialog-spacing-vertical,1em)))), 100%);
            width: min(var(--width,calc(100% - (2 * var(--crowd-dialog-spacing-horiztonal,1em)))), 100%);
            overflow-y: scroll;
            transition-property: transform;
            transition-duration: var(--crowd-dialog-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-dialog-transition-function, ease-in-out);
            position: relative;
            border-radius: var(--crowd-dialog-border-radius,0px);
            box-shadow: var(--crowd-dialog-box-shadow,
                0 1px 1px hsl(0deg 0% 0% / 0.075),
                0 2px 2px hsl(0deg 0% 0% / 0.075),
                0 4px 4px hsl(0deg 0% 0% / 0.075),
                0 8px 8px hsl(0deg 0% 0% / 0.075),
                0 16px 16px hsl(0deg 0% 0% / 0.075)
            );
                
        }
        h2 {
            margin: var(--crowd-dialog-heading-margin-top, 0px) 0 var(--crowd-dialog-heading-margin-bottom, 0px);
        }
        .dialog-container {
            display: grid;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            place-items: center;
            padding: var(--crowd-dialog-spacing-vertical, 2em) var(--crowd-dialog-spacing-horizontal, 2em);
            z-index: var(--crowd-dialog-z-index,9999);
        }
        .dialog-overlay {
            position:absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            cursor: pointer;
        }
        dialog > button.dialog-close {
            -webkit-appearance: none;
            background-color: var(--crowd-dialog-close-background,transparent);
            color: var(--crowd-dialog-close-color, black);
            border: var(--crowd-dialog-close-border-width,0px) var(--crowd-dialog-close-border-type,solid) var(--crowd-dialog-close-border-color,transparent);
            font-size: var(--crowd-dialog-close-font-size,2rem);
            line-height: 1;
            position: absolute;
            top: 0;
            right: 0;
            cursor: pointer;
        }
        @media (hover: hover) {
            dialog > button.dialog-close:hover {
                opacity: var(--crowd-dialog-close-hover-opacity, 0.6);
            }
        }
        @media (prefers-reduced-motion: reduce) {
            :host {
                --crowd-dialog-container-transition-duration: 0s;
                --crowd-dialog-container-transition-delay: 0s;
                --crowd-dialog-overlay-transition-duration: 0s;
                --crowd-dialog-overlay-transition-delay: 0s;
                --crowd-dialog-transition-duration: 0s;
                --crowd-dialog-transition-delay: 0s;
            }
        }
    `;constructor(){super(),this.open=!1}toggle(){this.open=!this.open,this.renderRoot.querySelector("dialog").show(),this.open?this.dispatchEvent(ft):this.dispatchEvent(pt)}show(){this.open=!0,this.renderRoot.querySelector("dialog").show(),this.dispatchEvent(ft)}hide(){this.open=!1,this.renderRoot.querySelector("dialog").close(),this.dispatchEvent(pt)}render(){let t;return this.label&&(t=M`<h2>${this.label}</h2>`),M`
            <div class='dialog-container' part="container">
            <div class='dialog-overlay' part="overlay" @click="${()=>this.hide()}"></div>
                <dialog ${this.open?"open":""} @close='${this.dispatchEvent(pt)}' @cancel='${this.dispatchEvent(pt)}'>
                    <button part="close" name='close dialog' aria-label='close dialog' class='dialog-close' @click="${()=>this.hide()}"
                        ><crowd-icon name='x'></crowd-icon></button>
                    ${t}
                    <slot></slot>
                </dialog>
            </div>
        `}}const gt=new Event("crowdDrawerHide",{bubbles:!0,composed:!0}),vt=new Event("crowdDrawerShow",{bubbles:!0,composed:!0});class yt extends X{static properties={open:{type:Boolean,reflect:!0},placement:{type:String}};static styles=o`
        :host .drawer-container {
            pointer-events: none;
            visibility:hidden;
            background-color: transparent;
            transition-property: visibility;
            transition-duration: var(--crowd-drawer-container-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-drawer-container-ease, ease-in-out);
            transition-delay: var(--crowd-drawer-container-transition-delay, 0.3s);
        }
        .drawer-container {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: var(--crowd-drawer-z-index,9999);
        }
        :host([open]) .drawer-container {
            pointer-events: all;
            visibility: visible;
        }
        .drawer-overlay {
            width: 100%;
            height: 100%;
        }
        :host .drawer-overlay {
            transition-property: background-color;
            transition-duration: var(--crowd-drawer-overlay-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-drawer-overlay-transition-function, ease-in-out);
            transition-delay: var(--crowd-drawer-overlay-transition-delay, 0.3s);
            cursor: pointer;
        }
        :host([open]) .drawer-overlay {
            background-color: var(--crowd-drawer-overlay-background, rgba(0,0,0,0.3));
        }
        .drawer {
            background-color: var(--crowd-drawer-background,white);
            position: absolute;
            top:var(--crowd-drawer-position-top, 0);
            left:var(--crowd-drawer-position-left, 0);
            right:var(--crowd-drawer-position-right, 0);
            bottom:var(--crowd-drawer-position-bottom, 0);
            padding: var(--crowd-drawer-padding-vertical, 2em) var(--crowd-drawer-padding-horizontal, 1em);
            transition-property: transform;
            transition-duration: var(--crowd-drawer-transition-duration, 0.2s);
            transition-timing-function: var(--crowd-drawer-transition-function, ease-in-out);
            overflow-y: scroll;
        }
        .drawer--top {
            transform: translateY(-100%);
        }
        .drawer--right {
            transform: translateX(100%);
        }
        .drawer--left {
            transform: translateX(-100%);
        }
        .drawer--bottom {
            transform: translateY(100%);
        }
        :host([open]) .drawer {
            transform: none;
            transition-delay: var(--crowd-drawer-transition-delay, 0.3s);
        }
    `;constructor(){super()}connectedCallback(){super.connectedCallback(),this.classList.add("hydrated")}toggle(){this.open=!this.open,this.open?this.dispatchEvent(vt):this.dispatchEvent(gt)}show(){this.open=!0,this.dispatchEvent(vt)}hide(){this.open=!1,this.dispatchEvent(gt)}render(){return M`
            <div part="container" class='drawer-container'>
                <div @click='${()=>this.hide()}' part="overlay" class='drawer-overlay'></div>
                <aside part="drawer" class='drawer drawer--${this.placement}'>
                    <slot></slot>
                </aside>
            </div>
        `}}class _t extends X{static properties={id:{type:String},show:{type:Boolean,reflect:!0},value:{type:String,reflect:!0},selectedLabel:{type:String},name:{type:String},placeholder:{type:String},required:{type:Boolean},label:{type:String},clearable:{type:Boolean},multiple:{type:Boolean},hoist:{type:Boolean},errorMessage:{type:String},showSuccess:{type:Boolean},success:{type:Boolean,reflect:!0},successMessage:{type:String},invalid:{type:Boolean,reflect:!0},_childIndex:{type:Number},_multiSelect:{type:Array},_multiLabel:{type:Array}};static styles=o`
        :host {
            display: inline-block;
        }
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        .wrapper {
            padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
            max-width: 100%;
            position:relative;
        }
        label {
            display: inline-block;
            color: var(--crowd-input-label-color, inherit);
            margin-bottom: var(--crowd-input-label-spacing, 0.5em);
        }
        .select-dropdown {
            box-sizing: border-box;
            position: absolute;
            top:calc(100% - var(--crowd-input-wrapper-padding-vertical, 0) + var(--crowd-select-dropdown-spacing,2px));
            left: 0;
            width: 100%;
            max-height: var(--crowd-select-dropdown-max-height, 50vh);
            overflow-y: scroll;
            padding: var(--crowd-select-dropdown-padding-vertical, 0.5em) var(--crowd-select-dropdown-padding-horizontal, 0.5em);
            pointer-events: none;
            transition-property: opacity, transform;
            transition-duration: var(--crowd-select-transition-duration, 0.15s);
            transition-timing-function: var(--crowd-select-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-transition-delay, 0s);
            opacity: 0;
            transform: scaleY(0.5);
            transform-origin: top center;
            background-color: var(--crowd-select-dropdown-background-color, white);
            z-index: var(--crowd-select-dropdown-z-index, 999);
            box-shadow: var(
                --crowd-select-dropdown-box-shadow,
                0 2px 8px rgba(0, 0, 0, 0.1)
            );
            border: var(--crowd-select-dropdown-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-select-dropdown-border-color, transparent);
        }
        :host([hoist]) .select-dropdown {
            position:fixed;
            top: auto;
            left: auto;
        }
        :host([show]) .select-dropdown {
            opacity: 1;
            transform: scale(1);
            pointer-events: all;
        }
        input,.multiple-items {
            -webkit-appearance: none;
            background-color: transparent;
            border: none;
            height: 100%;
            width: calc(100% - (2 * var(--crowd-input-padding-horizontal, 1em)));
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            padding: 0 var(--crowd-input-padding-horizontal, 1em);
            font-weight: var(--crowd-input-font-weight,400);
            height: calc(
                var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
            );
            caret-color: transparent;
            cursor: pointer;
            text-transform: var(--crowd-input-text-transform);
        }
        input.multi {
            opacity: 0;
        }
        input:focus-visible, input:active {
            outline: none;
        }
        input::placeholder {
            color: var(--crowd-input-placeholder-color, inherit);
        }
        .input-container {
            color: var(--crowd-input-color, inherit);
            background-color: var(--crowd-input-background, white);
            border: var(--crowd-input-border-width,0px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, transparent);
            border-radius: var(--crowd-input-border-radius, 0px);
            font-size: var(--crowd-input-font-size,1rem);
            padding: var(--crowd-input-padding-vertical, 1em) 0;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: stretch;
            position: relative;
        }
        .input-container:focus-within, .input-container:focus-visible {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        [part='overlay'] {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            pointer-events: none;
            z-index: calc(var(--crowd-select-dropdown-z-index, 999) - 1);
        }
        :host([show]) [part='overlay'] {
            pointer-events: all;
        }
        button {
            -webkit-appearance: none;
            background-color: transparent;
            color: var(--crowd-select-icon-color, inherit);
            border: none;
            padding: 0;
            display: grid;
            place-items: center;
            transition-property: transform,color;
            transition-duration: var(--crowd-select-icon-transition-duration,0.15s);
            transition-timing-function: var(--crowd-select-icon-transition-ease, ease-in-out);
            transition-delay: var(--crowd-select-icon-transition-delay, 0s);
            margin-right: var(--crowd-input-padding-horizontal, 1em);
            cursor: pointer;
            position:relative;
            z-index: 2;
        }
        .error,.success {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        .success {
            color: var(--crowd-input-success-message-color, lime);
        }
        @media (hover: hover) {
            button:hover {
                color: var(--crowd-select-icon-hover-color, inherit);
            }
        }
        :host([show]) button[part='toggle'] {
            transform: rotate(180deg);
        }
        :host([invalid]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-error-message-color, red);
        }
        :host([success]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-success-message-color, lime);
        }
        .multiple-items {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            bottom: 0;
            left: 0;
            right: 0;
            overflow-y: hidden;
            overflow-x: scroll;
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
            gap: 4px;
            z-index: 1;
        }
        @supports (::-moz-range-track) {
            .multiple-items {
                scrollbar-width: thin;
                scrollbar-color: var(--crowd-select-multi-scrollbar-color,#000);
            }
        }
        .multiple-items::-webkit-scrollbar {
            height: 2px;
            background-color: var(--crowd-select-multi-scrollbar-background,#aaa);
        }
        .multiple-items::-moz-scrollbar-button, .multiple-items::-webkit-scrollbar-button {
            width: 0px;
            display: none;
        }
        .multiple-items::-webkit-scrollbar-thumb {
            background: var(--crowd-select-multi-scrollbar-color,#000);
        }
        .multiple-items crowd-badge {
            pointer-events: auto;
            gap: 2px;
        }
    `;constructor(){super(),this.placeholder="Please select",this.show=!1,this.childIndex=-1,this.invalid=!1}connectedCallback(){super.connectedCallback(),this.id="select-"+Date.now(),new nt(this),this._multiSelect=[],this._multiLabel=[],this.multiple&&this.value&&(this._multiSelect=this.value.split(","),this._multiLabel=this.value.split(","),console.log(this._multiSelect),this._multiSelect.length>0?(this.value=this._multiSelect.join(),this.selectedLabel=this._multiLabel.join()):(this.value="",this.selectedLabel=""),console.log(this.value))}firstUpdated(){let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{t.addEventListener("click",(()=>{this._selectOption(t),this.multiple||this.close()}),!1),console.log(t.value.toUpperCase()),this.value&&this.value.toUpperCase().includes(t.value.toUpperCase())&&t.click()}))}updated(){let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{this.value&&this.value==t.value?(t.isActive=!0,this.selectedLabel!=t.innerHTML.replace(/(<([^>]+)>)/gi,"")&&(this.selectedLabel=t.innerHTML.replace(/(<([^>]+)>)/gi,""))):this.multiple&&this.value&&this._multiSelect.indexOf(t.value)>-1?t.isActive=!0:t.isActive=!1}))}open(){this.show=!0}close(){this.show=!1,this.childIndex=-1,this._blurOptions()}_blurOptions(){let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{t.blur()}))}validate(){this.invalid=!1,this.success=!1,this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0)),!this.invalid&&this.showSuccess&&(this.success=!0);const t=new CustomEvent("crowdValidate",{detail:{value:this.value}});this.dispatchEvent(t)}toggle(){this.show=!this.show,this.show||(this.childIndex=-1,this._blurOptions())}clear(){this._multiSelect=[],this._multiLabel=[],this.value="",this.selectedLabel="";let t=[].slice.call(this.querySelectorAll("crowd-option"));t&&t.forEach((t=>{t.isActive=!1})),this.validate(),this._dispatchChange()}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onInput(t){t.preventDefault(),this.invalid=!1}_keyDown(t){if("Tab"===t.key)return;t.preventDefault();let e=[].slice.call(this.querySelectorAll("crowd-option"));"Enter"===t.key?(this.open(),-1!=this.childIndex&&e[this.childIndex].click()):"ArrowDown"===t.key?(this.open(),e[this.childIndex]&&e[this.childIndex].blur(),e.length==this.childIndex+1?this.childIndex=0:this.childIndex=this.childIndex+1,e[this.childIndex]&&e[this.childIndex].focus()):"ArrowUp"===t.key?(this.open(),e[this.childIndex]&&e[this.childIndex].blur(),0==this.childIndex?this.childIndex=e.length-1:this.childIndex=this.childIndex-1,e[this.childIndex]&&e[this.childIndex].focus()):(this.open(),e&&e.forEach(((A,r)=>{if(A.value.toUpperCase()[0]==t.key.toUpperCase())return e.forEach((t=>t.blur())),A.focus(),void(this.childIndex=r)})))}_selectOption(t){t&&t.value&&t.innerHTML||console.warn("<crowd-select>: Could not find element for option",t);let e=t.value,A=t.innerHTML.replace(/(<([^>]+)>)/gi,"");this.multiple?(this._multiSelect.indexOf(e)<0?(this._multiSelect.push(e),this._multiLabel.push(A)):(this._multiSelect.splice(this._multiSelect.indexOf(e),1),this._multiLabel.splice(this._multiLabel.indexOf(A),1)),this._multiSelect.length>0?(this.value=this._multiSelect.join(),this.selectedLabel=this._multiLabel.join()):(this.value="",this.selectedLabel="")):(this.value=e,this.selectedLabel=A),this.validate(),this._dispatchChange()}_hoistPosition(){let t=this.renderRoot.querySelector(".select-dropdown"),e=this.renderRoot.querySelector(".input-container");t&&e&&(t.style.top=e.getBoundingClientRect().bottom+"px",t.style.left=e.getBoundingClientRect().left+"px",t.style.width=e.getBoundingClientRect().width+"px"),requestAnimationFrame((()=>{this._hoistPosition()}))}render(){let t="";this.clearable&&this.value&&(t=M`
                <button @click='${this.clear}' part="clear" name="clear" aria-label="clear select">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                        <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                    </svg>
                </button>
            `);let e=M`
            <div class='select-dropdown'>
                <slot></slot>
            </div>
        `;this.hoist&&requestAnimationFrame((()=>{this._hoistPosition()}));let A="";this.label&&(A=M`
                <label @click='${this.open}' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `);let r="";this.invalid&&(r=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let i="";this.success&&(i=M`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `);let n=M``;return this.multiple&&this._multiSelect.length&&(n=M`
                <div class='multiple-items' @click='${this.open}'>
                    ${this._multiSelect.map((t=>M`<crowd-badge pill @click='${e=>{e.stopImmediatePropagation(),this._selectOption(this.querySelector(`crowd-option[value='${t}']`))}}'>${t}<crowd-icon name='x'></crowd-icon></crowd-badge>`))}
                </div>
            `),M`
            <div part='wrapper' class='wrapper'>
                ${A}
                <div part='container' class='input-container' @click='${this.open}'>
                    <input inputmode='none' class='${this._multiSelect.length?"multi":""}' @change='${this._dispatchChange}' @keydown='${this._keyDown}' @input='${this._onInput}' id='${this.id}' type='text' name='${this.name}' value='${this.selectedLabel}' placeholder='${this.placeholder}' required='${this.required}' />
                    ${n}
                    ${t}
                    <slot name="icon"></slot>
                    <button @click='${this.toggle}' part="toggle" name="toggle" aria-label="toggle dropdown">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </button>
                </div>
                ${r}
                ${i}
                ${this.hoist?"":e}
            </div>
            <div @click='${this.close}' part='overlay'></div>
            ${this.hoist?e:""}
        `}}class bt extends X{static properties={value:{type:String},isFocus:{type:Boolean,reflect:!0,attribute:"focus"},isActive:{type:Boolean,reflect:!0,attribute:"active"}};static styles=o`
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        [part='container'] {
            padding: var(--crowd-option-padding-vertical, 0.2em) var(--crowd-option-padding-horizontal, 1em);
            font-family: var(--crowd-option-font-family, inherit);
            font-size: var(--crowd-option-font-size, inherit);
            font-weight: var(--crowd-option-font-weight, inherit);
            color: var(--crowd-option-color, inherit);
            background-color: var(--crowd-option-background-color, transparent);
            width: calc(100% - (1 * var(--crowd-option-padding-horizontal, 1em)));
            margin: 0 calc(-1 * var(--crowd-select-dropdown-padding-horizontal, 0.5em));
            cursor: pointer;
            transition-property: color, background-color;
            transition-duration: var(--crowd-option-transition-duration,0.15s);
            transition-timing-function: var(--crowd-option-transition-timing-function, ease-in-out);
            transition-delay: var(--crowd-option-transition-delay, 0s);
            border-radius: var(--crowd-option-border-radius,0px);
        }
        @media  (hover: hover) {
            [part='container']:hover {
                color: var(--crowd-option-hover-color, inherit);
                background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
            }
        }
        [part='container']:focus-visible, :host([focus]) [part='container'],:host([active]) [part='container'] {
            color: var(--crowd-option-hover-color, inherit);
            background-color: var(--crowd-option-hover-background-color, rgba(0,0,0,0.1));
        }
    `;constructor(){super(),this.isFocus=!1,this.isActive=!1}focus(){this.isFocus=!0}blur(){this.isFocus=!1}render(){return M`
            <div part='container'>
                <slot></slot>
            </div>
        `}}class wt extends X{static properties={show:{type:Boolean,reflect:!0},hoist:{type:Boolean},position:{type:String,reflect:!0}};static styles=[_t.styles,o`
            :host {
                transform: none !important;
            }
            .input-container {
                background-color: transparent;
                border: none;
            }
            .select-dropdown {
                min-width: 100%;
                width: max-content;
            }
            :host([hoist]) .select-dropdown {
                min-width: auto;
                width: max-content;
            }
            :host([position*="top"]) .select-dropdown {
                top: auto;
                bottom: calc(100% + var(--crowd-input-wrapper-padding-vertical, 0px) - var(--crowd-select-dropdown-spacing,2px));
                transform-origin: center bottom;
            }
            :host([position*="right"]) .select-dropdown {
                top: auto;
                left: auto;
                right: 0px;
                bottom: calc(100% + var(--crowd-input-wrapper-padding-vertical, 0px) - var(--crowd-select-dropdown-spacing,2px));
                transform-origin: center bottom;
            }
        `];open(){this.show=!0,this._dispatchOpen()}close(){this.show=!1,this._dispatchClose()}toggle(){this.show=!this.show,this.show?this._dispatchOpen():this._dispatchClose()}_dispatchOpen(){const t=new CustomEvent("crowdOpen");this.dispatchEvent(t)}_dispatchClose(){const t=new CustomEvent("crowdClose");this.dispatchEvent(t)}_hoistPosition(){let t=this.renderRoot.querySelector(".select-dropdown"),e=this.renderRoot.querySelector(".input-container");t&&e&&(t.style.top=e.getBoundingClientRect().bottom+"px",t.style.left=e.getBoundingClientRect().left+"px",t.style.width=e.getBoundingClientRect().width+"px"),requestAnimationFrame((()=>{this._hoistPosition()}))}constructor(){super(),this.position="bottom"}render(){let t=M`
            <div class='select-dropdown' part='dropdown'>
                <slot></slot>
            </div>
        `;return this.hoist&&requestAnimationFrame((()=>{this._hoistPosition()})),M`
            <div part='wrapper' class='wrapper'>
                <div @click='${this.open}' part='container' class='input-container'>
                    <slot name='trigger'></slot>
                </div>
                ${this.hoist?"":t}
            </div>
            <div @click='${this.close}' part='overlay'></div>
            ${this.hoist?t:""}
        `}}function kt(t){let e=[];return t.children.length&&Array.prototype.slice.call(t.children).forEach((t=>{e.push(t),t.children.length&&(e=[...e,...kt(t)])})),e}class xt extends X{static styles=o`
        :host {
            display: block;
        }
        .form-container {
            padding: var(--crowd-form-padding-vertical, 1em) var(--crowd-form-padding-horizontal, 1em);
        }
    `;constructor(){super()}connectedCallback(){super.connectedCallback();let t=[].slice.call(this.querySelectorAll("[submit]"));t&&t.forEach((t=>{t.addEventListener("click",(()=>{this.submit()}),!1)}))}validate(){return new Promise(((t,e)=>{this.getFormControls().then((A=>{let r=!0;A.forEach((t=>{t.validate&&(t.validate(),t.invalid&&(r=!1))})),r&&t(),e()}))}))}getFormControls(){let t=kt(this);return new Promise(((e,A)=>{e(t.filter((t=>t.name)))}))}getFormData(){return new Promise(((t,e)=>{let A=new FormData;this.getFormControls().then((e=>{e.forEach((t=>{("CROWD-CHECKBOX"==t.tagName&&t.checked||"CROWD-CHECKBOX"!=t.tagName)&&A.append(t.name,t.value)})),t(A)}))}))}submit(){this.validate().then((()=>{this.getFormData().then((t=>{const e=new CustomEvent("crowdFormSubmit",{detail:{formData:t}});this.dispatchEvent(e)}))}))}render(){return M`
            <div part='container' class='form-container'>
                <form part='form'>
                    <slot></slot>
                </form>
            </div>
        `}}class St extends it{static properties={...super.properties,name:{type:String},src:{type:String},count:{type:Number}};static styles=[it.styles,o`
            :host {
                line-height: 0;
            }
            button {
                background-color: transparent;
                border: none;
                padding: var(--crowd-icon-button-padding, 0.25em);
                width: 1em;
                height: 1em;
            }
            @media (hover: hover) {
                button:hover {
                    background-color: transparent;
                    border: none;
                }
            }
        `];constructor(){super()}render(){let t=M`
            <slot part='label' class='label'>
                <slot><crowd-icon count='${this.count}' name='${this.name}' src='${this.src}'></crowd-icon></slot>
            </slot>
        `,e=M`
            <button part='button'>
                ${t}
            </button>
        `;return this.href&&(e=M`
                <a part='button' href='${this.href}' target='${this.target}'>
                    ${t}
                </a>
            `),M`
            ${e}
        `}}const Ct=t=>(...e)=>({_$litDirective$:t,values:e});class Et{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,A){this._$Ct=t,this._$AM=e,this._$Ci=A}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}}const{H:Pt}=G,Tt=(t,e)=>{var A,r;const i=t._$AN;if(void 0===i)return!1;for(const t of i)null===(r=(A=t)._$AO)||void 0===r||r.call(A,e,!1),Tt(t,e);return!0},Ot=t=>{let e,A;do{if(void 0===(e=t._$AM))break;A=e._$AN,A.delete(t),t=e}while(0===(null==A?void 0:A.size))},Nt=t=>{for(let e;e=t._$AM;t=e){let A=e._$AN;if(void 0===A)e._$AN=A=new Set;else if(A.has(t))break;A.add(t),Mt(e)}};function $t(t){void 0!==this._$AN?(Ot(this),this._$AM=t,Nt(this)):this._$AM=t}function It(t,e=!1,A=0){const r=this._$AH,i=this._$AN;if(void 0!==i&&0!==i.size)if(e)if(Array.isArray(r))for(let t=A;t<r.length;t++)Tt(r[t],!1),Ot(r[t]);else null!=r&&(Tt(r,!1),Ot(r));else Tt(this,t)}const Mt=t=>{var e,A,r,i;2==t.type&&(null!==(e=(r=t)._$AP)&&void 0!==e||(r._$AP=It),null!==(A=(i=t)._$AQ)&&void 0!==A||(i._$AQ=$t))};class Dt extends Et{constructor(){super(...arguments),this._$AN=void 0}_$AT(t,e,A){super._$AT(t,e,A),Nt(this),this.isConnected=t._$AU}_$AO(t,e=!0){var A,r;t!==this.isConnected&&(this.isConnected=t,t?null===(A=this.reconnected)||void 0===A||A.call(this):null===(r=this.disconnected)||void 0===r||r.call(this)),e&&(Tt(this,t),Ot(this))}setValue(t){if((t=>void 0===this._$Ct.strings)())this._$Ct._$AI(t,this);else{const e=[...this._$Ct._$AH];e[this._$Ci]=t,this._$Ct._$AI(e,this,0)}}disconnected(){}reconnected(){}}class Lt{constructor(t){this.U=t}disconnect(){this.U=void 0}reconnect(t){this.U=t}deref(){return this.U}}class Bt{constructor(){this.Y=void 0,this.q=void 0}get(){return this.Y}pause(){var t;null!==(t=this.Y)&&void 0!==t||(this.Y=new Promise((t=>this.q=t)))}resume(){var t;null===(t=this.q)||void 0===t||t.call(this),this.Y=this.q=void 0}}const zt=t=>!(t=>null===t||"object"!=typeof t&&"function"!=typeof t)(t)&&"function"==typeof t.then,Rt=Ct(class extends Dt{constructor(){super(...arguments),this._$Cft=1073741823,this._$Cwt=[],this._$CG=new Lt(this),this._$CK=new Bt}render(...t){var e;return null!==(e=t.find((t=>!zt(t))))&&void 0!==e?e:D}update(t,e){const A=this._$Cwt;let r=A.length;this._$Cwt=e;const i=this._$CG,n=this._$CK;this.isConnected||this.disconnected();for(let t=0;t<e.length&&!(t>this._$Cft);t++){const o=e[t];if(!zt(o))return this._$Cft=t,o;t<r&&o===A[t]||(this._$Cft=1073741823,r=0,Promise.resolve(o).then((async t=>{for(;n.get();)await n.get();const e=i.deref();if(void 0!==e){const A=e._$Cwt.indexOf(o);A>-1&&A<e._$Cft&&(e._$Cft=A,e.setValue(t))}})))}return D}disconnected(){this._$CG.disconnect(),this._$CK.pause()}reconnected(){this._$CG.reconnect(this),this._$CK.resume()}});class Ft extends Et{constructor(t){if(super(t),this.it=L,2!==t.type)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===L||null==t)return this.vt=void 0,this.it=t;if(t===D)return t;if("string"!=typeof t)throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this.vt;this.it=t;const e=[t];return e.raw=e,this.vt={_$litType$:this.constructor.resultType,strings:e,values:[]}}}Ft.directiveName="unsafeHTML",Ft.resultType=1,Ct(Ft);class Ht extends Ft{}Ht.directiveName="unsafeSVG",Ht.resultType=2;const Ut=Ct(Ht);class Vt extends X{static properties={name:{type:String},style:{type:String},src:{type:String},count:{type:Number}};static styles=o`
        :host {
            display: inline-grid;
            place-items: center;
            position: relative;
            margin: var(--crowd-icon-vertical-spacing,0px) var(--crowd-icon-horizontal-spacing,0px);
        }
        img,i,svg {
            width: 1em;
            height: 1em;
            color: inherit;
        }
        img {
            width: 100%;
            height: 100%;
            aspect-ratio: 1/1;
            object-fit: contain;
        }
        .count {
            position:absolute;
            top: 0;
            right: 0;
            aspect-ratio: 1/1;
            display: inline-grid;
            place-items: center;
            border-radius: 50%;
            background-color: var(--crowd-icon-count-background-color, red);
            color: var(--crowd-icon-count-color, white);
            font-size: 0.5em;
            transform: translate(50%,-50%);
            width: 1em;
            height: 1em;
            padding: 0.2em;
            line-height: 1;
        }
    `;constructor(){super()}_fetchSrc(){return new Promise(((t,e)=>{this.src?t(M`<img src='${this.src}' />`):this.name&&fetch(`https://icons.getbootstrap.com/assets/icons/${this.name}.svg`).then((t=>t.text())).then((e=>t(M`${Ut(e)}`)))}))}render(){return M`
            ${Rt(this._fetchSrc().then((t=>M`${t}`)),M``)}
            ${this.count?M`<span class='count'>${this.count<10?this.count:"•"}</span>`:""}
        `}}class qt extends X{static properties={type:{type:String,reflect:!0},name:{type:String},value:{type:String,reflect:!0},placeholder:{type:String},label:{type:String},required:{type:Boolean},togglePassword:{type:Boolean},showPassword:{type:Boolean},errorMessage:{type:String},showSuccess:{type:Boolean},success:{type:Boolean,reflect:!0},successMessage:{type:String},invalid:{type:Boolean,reflect:!0},maxlength:{type:String}};static styles=o`
        :host {
            display: block;
        }
        :host,:host *,:host *::before, :host *::after {
            box-sizing: border-box;
        }
        input,textarea {
            -webkit-appearance: none;
            background-color: transparent;
            border: none;
            height: 100%;
            width: calc(100% - (2 * var(--crowd-input-padding-horizontal, 1em)));
            color: inherit;
            font-family: inherit;
            font-size: inherit;
            padding: 0 var(--crowd-input-padding-horizontal, 1em);
            font-weight: var(--crowd-input-font-weight,400);
            height: calc(
                var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
            );
            text-transform: var(--crowd-input-text-transform);
            flex: 1 1 auto;
        }
        textarea {
            height: auto;
            padding: var(--crowd-textarea-padding-vertical, 1em) var(--crowd-textarea-padding-horizontal, 1em);
        }
        input:focus-visible, input:active, textarea:focus-visible, textarea:active {
            outline: none;
        }
        input::placeholder,textarea::placeholder {
            color: var(--crowd-input-placeholder-color, inherit);
        }
        .input-container {
            color: var(--crowd-input-color, inherit);
            background-color: var(--crowd-input-background, white);
            border: var(--crowd-input-border-width,1px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, #eee);
            border-radius: var(--crowd-input-border-radius, 0px);
            font-size: var(--crowd-input-font-size,1rem);
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: stretch;
        }
        .input-container:focus-within {
            box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
        }
        label {
            display: inline-block;
            color: var(--crowd-input-label-color, inherit);
            margin-bottom: var(--crowd-input-label-spacing, 0.5em);
        }
        .wrapper {
            padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
            max-width: 100%;
        }
        .password-toggle {
            -webkit-appearance: none;
            background-color: transparent;
            color: var(--crowd-input-password-toggle-color, inherit);
            font-family: inherit;
            padding: 0;
            border: none;
            display: grid;
            place-items:center;
            margin-right: var(--crowd-input-padding-horizontal, 1em);
            cursor: pointer;
        }
        .error,.success {
            font-size: var(--crowd-input-error-message-font-size, 0.8em);
            color: var(--crowd-input-error-message-color, red);
        }
        .success {
            color: var(--crowd-input-success-message-color, lime);
        }
        :host([invalid]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-error-message-color, red);
        }
        :host([success]) .input-container {
            outline: var(--crowd-input-status-outline-width,1px) solid var(--crowd-input-success-message-color, lime);
        }
        slot[name='help-text'] {
            font-size:var(--crowd-input-error-message-font-size, 0.8em);
        }
    `;constructor(){super(),this.showPassword=!1,this.invalid=!1,this.showSuccess=!1,this.success=!1}connectedCallback(){super.connectedCallback(),this.id="input-"+Date.now(),this.classList.add("hydrated"),new nt(this)}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onInput(t){this.invalid=!1,this.value=t.currentTarget.value,this.validate(),this._dispatchChange()}_togglePassword(){this.showPassword=!this.showPassword}_getType(){return"password"===this.type&&this.showPassword?"text":this.type}_validateEmail(){this.value&&null==this.value.match(/^.+@\w+\.\w+/g)&&(this.invalid=!0)}validate(){this.invalid=!1,this.success=!1,"email"==this.type&&this._validateEmail(),this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0)),!this.invalid&&this.showSuccess&&(this.success=!0);const t=new CustomEvent("crowdValidate",{detail:{value:this.value}});this.dispatchEvent(t)}render(){let t="";"password"===this.type&&this.togglePassword&&(t=M`
                <button tabindex='-1' part='password-toggle' class='password-toggle' @click='${this._togglePassword}'>
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"></path>
                        <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"></path>
                    </svg>
                </button>
            `);let e="";this.invalid&&(e=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let A="";this.success&&(A=M`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `);let r="";this.maxlength&&(r=`maxlength='${this.maxlength}'`);let i="";return this.label&&(i=M`
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `),M`
            <div part='wrapper' class='wrapper'>
                ${i}
                <div part='container' class='input-container'>
                    <input ${r} @change='${this._dispatchChange}' @input='${this._onInput}' id='${this.id}' type='${this._getType()}' name='${this.name}' value='${this.value}' placeholder='${this.placeholder}' required='${this.required}' />
                    <slot name="icon"></slot>
                    ${t}
                </div>
                ${e}
                ${A}
                <slot name="help-text"></slot>
            </div>
        `}}class Yt extends X{static styles=[o`
            :host {
                display: inline-block;
            }
            li {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                padding: var(--crowd-menu-item-padding-vertical,0.2em) var(--crowd-menu-item-padding-horizontal,1em);
                color: var(--crowd-menu-item-color, inherit);
                background-color: var(--crowd-menu-item-background-color, transparent);
                transition-property: color, background-color;
                transition-duration: var(--crowd-menu-item-transition-duration,0.15s);
                transition-timing-function: var(--crowd-menu-item-transition-ease,ease-in-out);
                transition-delay: var(--crowd-menu-item-transition-delay, 0s);
                cursor: pointer;
                position: relative;
                container-type: inline-size;
                container-name: submenu-container;
            }
            slot[name='submenu'] {
                display: block;
                position: absolute;
                top: 0;
                left: 100%;
                transform: scale(0);
                opacity: 0;
                transition-duration: var(--crowd-menu-item-submenu-transition-duration, 0.15s);
                transition-timing-function: var(--crowd-menu-item-submenu-transition-ease, ease-in-out);
                transition-delay: var(--crowd-menu-item-submenu-transition-delay, 0s);
            }
            @media (hover: hover) {
                li:hover {
                    background-color: var(--crowd-menu-item-hover-background-color, rgba(0,0,0,0.1));
                    color: var(--crowd-menu-item-hover-color, inherit);
                }
                li:hover slot[name='submenu'] {
                    transform: scale(1);
                    opacity: 1;
                }
            }
            li:focus-visible,li:focus-within {
                outline: none;
                background-color: var(--crowd-menu-item-hover-background-color, rgba(0,0,0,0.1));
                color: var(--crowd-menu-item-hover-color, inherit);
            }

            li:focus-visible slot[name='submenu'],li:active slot[name='submenu'] {
                transform: scale(1);
                opacity: 1;
            }

            span.shortcut {
                opacity: 0.5;
                padding-left: 0.5em;
            }
            :host([position*="left"]) slot[name='submenu'] {
                left: auto;
                right: 100%;
            }
            :host([position*="bottom"]) slot[name='submenu'] {
                top: auto;
                bottom: 0;
            }
        `];static properties={shortcut:{type:String},position:{type:String,reflect:!0}};_executeShortcut(t){(t.ctrlKey||t.metaKey)&&t.key==this.shortcut&&(t.preventDefault(),this.triggerFocus(),this.click())}_listenForShortcut(){this.parentMenu&&document.addEventListener("keydown",this.bindExecuteShortcut,!1)}_stopListenForShortcut(){this.parentMenu&&document.removeEventListener("keydown",this.bindExecuteShortcut,!1)}triggerFocus(){this.renderRoot.querySelector("li").focus()}_executeNavigation(t){const e=()=>{this.nextElementSibling&&"CROWD-MENU-ITEM"==this.nextElementSibling.nodeName&&(this.blur(),this.nextElementSibling.triggerFocus())},A=()=>{this.previousElementSibling&&"CROWD-MENU-ITEM"==this.previousElementSibling.nodeName&&(this.blur(),this.previousElementSibling.triggerFocus())},r=()=>{this.querySelector("crowd-menu crowd-menu-item")&&(this.blur(),this.querySelector("crowd-menu crowd-menu-item").triggerFocus())};switch(t.key){case"Down":case"ArrowDown":t.preventDefault(),e();break;case"Up":case"ArrowUp":t.preventDefault(),A();break;case"Right":case"ArrowRight":t.preventDefault(),r();break;case"Enter":t.preventDefault(),this.click();default:return}}_listenForNavigation(){this.triggerFocus(),document.addEventListener("keydown",this.bindExecuteNavigation,!1)}_stopListenForNavigation(){document.removeEventListener("keydown",this.bindExecuteNavigation,!1)}constructor(){super(),this.position="right"}connectedCallback(){super.connectedCallback(),this.bindExecuteShortcut=this._executeShortcut.bind(this),this.bindExecuteNavigation=this._executeNavigation.bind(this),this.parentMenu=this.parentElement,this.parentMenu&&"CROWD-MENU"==this.parentMenu.nodeName&&this.shortcut&&(this.parentMenu.addEventListener("focusin",(()=>this._listenForShortcut()),!1),this.parentMenu.addEventListener("mouseover",(()=>this._listenForShortcut()),!1),this.parentMenu.addEventListener("focusout",(()=>this._stopListenForShortcut()),!1),this.parentMenu.addEventListener("mouseleave",(()=>this._stopListenForShortcut()),!1)),this.addEventListener("focusin",(()=>this._listenForNavigation()),!1),this.addEventListener("focus",(()=>this._listenForNavigation()),!1),this.addEventListener("focusout",(()=>this._stopListenForNavigation()),!1),this.addEventListener("mouseover",(()=>this._listenForNavigation()),!1),this.addEventListener("mouseleave",(()=>this._stopListenForNavigation()),!1)}render(){return M`
            <li part='item' tabindex='0'>
                <span><slot></slot></span>
                <div class='submenu'>
                    <slot name='submenu'>
                </div>
                </slot>
                <span class='shortcut'>
                    ${this.shortcut?`Ctrl+${this.shortcut.toUpperCase()}`:""}
                </span>
            </li>
        `}}class jt extends X{static styles=[o`
            :host, :host * {
                box-sizing: inherit;
            }
            ul {
                list-style: none;
                margin: 0;
                padding: var(--crowd-menu-padding-vertical,0.5em) var(--crowd-menu-padding-horizontal,0.5em);
                background-color: var(--crowd-menu-background-color,white);
                border: var(--crowd-menu-border-width, 0px) var(--crowd-menu-border-style, solid) var(--crowd-menu-border-color, black);
                border-radius: var(--crowd-menu-border-radius, 0px);
                display: flex;
                flex-flow: column nowrap;
                justify-content: flex-start;
                align-items: stretch;
                max-height: 50vh;
                overflow: visible;
            }
        `];render(){return M`
            <ul part='menu'>
                <slot></slot>
            </ul>
        `}}class Qt extends X{static properties={value:{type:String,reflect:!0},_selectedItems:{type:Array},name:{type:String},label:{type:String},required:{type:Boolean},errorMessage:{type:String},invalid:{type:Boolean,reflect:!0}};static styles=[o`
            :host {
                display: inline-block;
            }
            .input-container {
                color: var(--crowd-input-color, inherit);
                font-size: var(--crowd-input-font-size,1rem);
                display: inline-flex;
                flex-flow: row wrap;
                justify-content: flex-start;
                align-items: center;
                gap: var(--crowd-multi-toggle-row-gap, 1em);
            }
            label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .input-container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
        `];_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_setValue(){this.value=this._selectedItems.join(","),this._dispatchChange()}constructor(){super(),this._selectedItems=[]}connectedCallback(){super.connectedCallback(),new nt(this);let t=[].slice.call(this.querySelectorAll("crowd-multi-toggle-item"));t&&t.forEach((t=>{t.addEventListener("crowdChange",(()=>{if(t.checked)this._selectedItems.indexOf(t.value)<0&&this._selectedItems.push(t.value);else if(-1!=this._selectedItems.indexOf(t.value)){let e=this._selectedItems.indexOf(t.value);this._selectedItems.splice(e,1)}this._setValue()}),!1)}))}validate(){this.invalid=!1,this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0))}render(){let t="";this.invalid&&(t=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let e="";return this.label&&(e=M`
                <label part='label'>
                    ${this.label}
                </label>
            `),M`
            <div part='wrapper' class='wrapper'>
                ${e}
                <div part='container' class='input-container'>
                    <slot></slot>
                </div>
                ${t}
                <slot name="help-text"></slot>
            </div>
        `}}class Zt extends X{static properties={checked:{type:Boolean,reflect:!0},"show-icon":{type:Boolean,converter:(t,e)=>("string"==typeof t&&(t="false"!=t),t)},"icon-position":{type:String},value:{type:String}};static styles=[o`
            *,*::before, *::after {
                box-sizing: border-box;
                transition-property: none;
                transition-duration: var(--crowd-multi-toggle-transition-duration, 0.15s);
                transition-delay: var(--crowd-multi-toggle-transition-delay, 0s);
                transition-timing-function: var(--crowd-multi-toggle-transition-ease, ease-in-out);
            }

            :host {
                display: inline-block;
                width: auto;
                line-height: 1;
                height:min-content;
            }

            button {
                -webkit-appearance: none;
                background-color: var(--crowd-multi-toggle-off-background-color, transparent);
                color: var(--crowd-multi-toggle-off-color, inherit);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-off-border-color, #000);
                border-radius: var(--crowd-button-border-radius, 3px);
                font-family: inherit;
                font-size: inherit;
                font-weight: inherit;
                display: inline-flex;
                justify-content: center;
                align-items: center;
                gap: var(--crowd-button-gap, 0.5em);
                padding: var(--crowd-button-padding-vertical,0.5em) var(--crowd-button-padding-horizontal, 1em);
                cursor: pointer;
                transition-property: color, border-color, background-color;
            }

            button > span {
                display: inherit;
                min-width: 1ch;
            }

            @media (hover: hover) {
                button:hover {
                    background-color: var(--crowd-multi-toggle-off-hover-background-color, transparent);
                    color: var(--crowd-multi-toggle-off-hover-color, inherit);
                    border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-off-hover-border-color, #000);
                }
            }

            button:focus-visible,button:active {
                background-color: var(--crowd-multi-toggle-off-hover-background-color, transparent);
                color: var(--crowd-multi-toggle-off-hover-color, inherit);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-off-hover-border-color, #000);
                box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-multi-toggle-off-focus-color, rgba(0,0,0,0.3));
            }

            button.toggled {
                background-color: var(--crowd-multi-toggle-on-background-color, rgba(133, 255, 102,0.3));
                color: var(--crowd-multi-toggle-on-color, #85ff66);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-on-border-color, #85ff66);
                border-radius: var(--crowd-multi-toggle-border-radius, 3px);
            }

            @media (hover: hover) {
                button.toggled:hover {
                    background-color: var(--crowd-multi-toggle-on-hover-background-color, rgba(133, 255, 102,0.3));
                    color: var(--crowd-multi-toggle-on-hover-color, #85ff66);
                    border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-on-hover-border-color, #85ff66);
                }
            }

            button.toggled:focus-visible, button.toggled:active {
                background-color: var(--crowd-multi-toggle-on-hover-background-color, rgba(133, 255, 102,0.3));
                color: var(--crowd-multi-toggle-on-hover-color, #85ff66);
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-multi-toggle-on-hover-border-color, #85ff66);
                box-shadow: 0px 0px 0px var(--crowd-button-focus-width, 2px) var(--crowd-multi-toggle-on-focus-color, rgba(133, 255, 102,0.3));
            }
        `];_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}toggle(){this.checked=!this.checked,this._dispatchChange()}constructor(){super(),this["icon-position"]="after",this["show-icon"]=!0}render(){let t="";return this["show-icon"]&&(t=M`<span>${this.checked?M`<slot name='on'><crowd-icon name='x'></crowd-icon></slot>`:M`<slot name='off'><crowd-icon name='plus'></crowd-icon></slot>`}</span>`),M`
            <button part='button' class='${this.checked?"toggled":""}' @click='${()=>this.toggle()}'>
                ${"before"==this["icon-position"]?t:""}
                <slot></slot>
                ${"after"==this["icon-position"]?t:""}
            </button>
        `}}class Gt extends X{static properties={value:{type:Gt,reflect:!0},name:{type:String},min:{type:Gt},max:{type:Gt},step:{type:Gt},fixedPoint:{type:Gt},label:{type:String},required:{type:Boolean},errorMessage:{type:String},invalid:{type:Boolean,reflect:!0},placement:{type:String,hasChanged:(t,e)=>"before"==t||"after"==t?t:e},unit:{type:String}};static styles=[o`
            :host {
                display: block;
            }
            *,*::before, *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .input-container {
                color: var(--crowd-input-color, inherit);
                background-color: var(--crowd-input-background, white);
                border: var(--crowd-input-border-width,1px) var(--crowd-input-border-type, solid) var(--crowd-input-border-color, #eee);
                border-radius: var(--crowd-input-border-radius, 0px);
                font-size: var(--crowd-input-font-size,1rem);
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: stretch;
                width: max-content;
            }
            .input-container:focus-within {
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .input-container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            input {
                -webkit-appearance: none;
                background-color: transparent;
                border: none;
                height: 100%;
                width: max-content;
                min-width: 2ch;
                color: inherit;
                font-family: inherit;
                font-size: inherit;
                padding: 0 var(--crowd-input-padding-horizontal, 1em);
                font-weight: var(--crowd-input-font-weight,400);
                height: calc(
                    var(--crowd-input-height, 2em) - (var(--crowd-input-border-width,0px) * 2)
                );
                text-transform: var(--crowd-input-text-transform);
                flex: 1 1 auto;
            }
            input:focus-visible {
                outline: none;
            }
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
            }

            /* Firefox */
            input[type=number] {
            -moz-appearance: textfield;
            }

            .controls {
                display: flex;
                flex-flow: column;
                font-size: 0.5em;
                justify-content: space-around;
                margin: 0.5em 0 0.5em 0.5em;
                padding: 2px;
                background-color: var(--crowd-number-controls-background, #efefef);
                color: var(--crowd-number-controls-color, #dedede);
                --crowd-button-hover-color: #cecece;
            }
            :host([placement="after"]) .controls {
                margin: 0.5em 0.5em 0.5em 0;
            }

            .unit {
                display: flex;
                flex-flow: column;
                justify-content: center;
                padding: 0 0.3em;
                color: var(--crowd-number-unit-color, #dedede);
                font-size: 0.8em;
            }
        `];connectedCallback(){super.connectedCallback(),new nt(this),this.id="input-"+Date.now()}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onInput(t){this.invalid=!1,this.value=parseFloat(t.currentTarget.value).toFixed(this.fixedPoint)}_increment(){this.value=(parseFloat(this.value)+parseFloat(this.step)).toFixed(this.fixedPoint)}_decrement(){this.value=(parseFloat(this.value)-parseFloat(this.step)).toFixed(this.fixedPoint)}validate(){this.invalid=!1,this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0))}shouldUpdate(t){if(t.has("value")){if(!(null==this.max||parseFloat(this.value)<=parseFloat(this.max))||!(null==this.min||parseFloat(this.value)>=parseFloat(this.min)))return this.value=t.get("value"),this._syncValue(t.get("value")),!1;this._dispatchChange()}return!0}_syncValue(t){let e=this.renderRoot.querySelector("input");e&&(e.value=t)}updated(t){t.has("value")&&this._syncValue(this.value)}constructor(){super(),this.placement="before",this.value=0,this.step=1,this.fixedPoint=1}render(){let t="";this.invalid&&(t=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let e="";this.maxlength&&this.maxlength;let A="";this.label&&(A=M`
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `);let r=M`
            <div class='controls' part='controls'>
                <crowd-icon-button @click='${this._increment}' name='caret-up-fill'></crowd-icon-button>
                <crowd-icon-button @click='${this._decrement}' name='caret-down-fill'></crowd-icon-button>
            </div>
        `,i="";return this.unit&&(i=M`
                <div class='unit'>${this.unit}</div>
            `),M`
            <div part='wrapper' class='wrapper'>
                ${A}
                <div part='container' class='input-container'>
                    ${"before"==this.placement?r:""}
                    <input @change='${this._dispatchChange}' @input='${this._onInput}' id='${this.id}' type='text' min='${this.min}' max='${this.max}' step='${this.step}' name='${this.name}' value='${this.value}' required='${this.required}' />
                    ${i}
                    ${"after"==this.placement?r:""}
                </div>
                ${t}
                <slot name="help-text"></slot>
            </div>
        `}}class Wt extends X{static properties={percentage:{type:String},size:{type:String},trackWidth:{type:String,attribute:"stroke-width"}};static styles=[o`
            :host, :host * {
                box-sizing: inherit;
            }
            .progress-ring {
                display: inline-flex;
                align-items: center;
                justify-content: center;
                position: relative;
            }
            .progress-ring__image {
                width: var(--size,1em);
                height: var(--size,1em);
                transform: rotate(-90deg);
                transform-origin: 50% 50%;
            }
            .progress-ring__track {
                stroke: var(--track-color,rgba(0,0,0,0.2));
            }
            .progress-ring__track, .progress-ring__indicator {
                --radius: calc(var(--size,1em) / 2 - var(--track-width,2px) * 2);
                --circumference: calc(var(--radius) * 2 * 3.14159);
                fill: none;
                stroke-width: var(--track-width,2px);
                r: var(--radius);
                cx: calc(var(--size,1em) / 2);
                cy: calc(var(--size,1em) / 2);
            }
            .progress-ring__indicator {
                stroke: var(--indicator-color,black);
                stroke-linecap: round;
                transition: stroke-dashoffset 0.35s ease 0s;
                stroke-dasharray: var(--circumference) var(--circumference);
                stroke-dashoffset: calc(var(--circumference) - var(--percentage) * var(--circumference));
            }
            .progress-ring__label {
                display: flex;
                align-items: center;
                justify-content: center;
                position: absolute;
                top: 0px;
                left: 0px;
                width: 100%;
                height: 100%;
                text-align: center;
                user-select: none;
            }
        `];render(){return M`
            <div part="base" class="progress-ring" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="${this.percentage}">
                <svg class="progress-ring__image" style='${this.size?`--size: ${this.size};`:""} ${this.trackWidth?`--track-width:${this.trackWidth}`:""}'>
                    <circle class="progress-ring__track"></circle>
                    <circle class="progress-ring__indicator" style="--percentage: ${this.percentage/100};"></circle>
                </svg>

                <span part="label" class="progress-ring__label">
                    <slot></slot>
                </span>
            </div>
        `}}class Jt extends X{static properties={value:{type:Number,reflect:!0},min:{type:Number},max:{type:Number},step:{type:Number},notches:{type:Boolean},numbers:{type:Boolean},tooltip:{type:Boolean},required:{type:Boolean},label:{type:String},invalid:{type:Boolean},errorMessage:{type:String},hideMinMax:{type:Boolean}};static styles=[o`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
                width: var(--crowd-range-width,300px);

            }
            .container {
                position: relative;
                display: flex;
                flex-flow: row nowrap;
                justify-content: stretch;
                align-items: center;
                gap: var(--crowd-range-spacing,0.2em);
            }
            .container > span {
                flex: 0 1 auto;
            }
            .track {
                flex: 1 0 auto;
                position: relative;
                height: var(--crowd-range-track-height, 0.5em);
                border: var(--crowd-range-track-border-width, 1px) var(--crowd-range-track-border-style, solid) var(--crowd-range-track-border-color, #000);
                border-radius: var(--crowd-range-track-border-radius, 0px);
                background-color: var(--crowd-range-track-background-color,#eee);
                cursor: pointer;
            }
            .track::before {
                content: '';
                display: block;
                position:absolute;
                top: 0;
                right: calc(100% - var(--value));
                bottom: 0;
                left: 0;
                background-color: var(--crowd-range-indicator-color,#000);
            }
            .thumb {
                height: calc(var(--crowd-range-thumb-scale, 2) * var(--crowd-range-track-height, 0.5em));
                width: calc(var(--crowd-range-thumb-scale, 2) * var(--crowd-range-track-height, 0.5em));
                border: var(--crowd-range-track-border-width, 1px) var(--crowd-range-thumb-border-style, solid) var(--crowd-range-thumb-border-color, #000);
                border-radius: var(--crowd-range-thumb-border-radius, 50%);
                background-color: var(--crowd-range-thumb-background-color,#fff);
                position: absolute;
                top:50%;
                left: var(--value);
                transform: translate(-50%,-50%);
                pointer-events: none;
            }
            .track:focus-within .thumb {
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            input {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                -webkit-appearance: none;
                padding: 0;
                margin: 0;
                height: 0;
                width: 0;
                opacity: 0;
            }
            :host([notches]) .container {
                margin-top: calc(1ex + var(--crowd-range-notch-height,0.5em));
                
            }
            .notches {
                width: 100%;
                position: absolute;
                bottom: 100%;
                height: var(--crowd-range-notch-height,0.5em);
                left: 0;
                right: 0;
                pointer-events:none;
            }
            .notches > div:first-child,.notches > div:last-child {
                opacity: 0;
            }
            .notches > div {
                position: absolute;
                top: 0;
                height: 100%;
                left: var(--left);
                color: var(--crowd-range-number-color, inherit);
            }
            .notches > div.notched::before {
                content: '';
                display:block;
                position: absolute;
                top: 0;
                height: 100%;
                width: var(--crowd-range-notch-width,1px);
                background-color: var(--crowd-range-notch-color,#000);
            }
            .notches > div span {
                position: absolute;
                left: 50%;
                bottom: 100%;
                transform: translateX(-50%);
            }
            label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
        `];constructor(){super(),this.min="0",this.max="100",this.step=1,this.notches=!1,this.value=this.min,this.tooltipTimer=null,this.invalid=!1,this.hideMinMax=!1}connectedCallback(){super.connectedCallback(),this.id="range-"+Date.now(),new nt(this)}_dispatchPreChange(){const t=new CustomEvent("crowdMove");this.dispatchEvent(t)}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_roundToStep(t,e){return Math.round(t/e)*e}_setValue(t){this.value=this._roundToStep(t.offsetX/t.currentTarget.getBoundingClientRect().width*this.max,this.step),this.value>this.max&&(this.value=this.max),this.value<this.min&&(this.value=this.min);let e=this.renderRoot.querySelector("input");e&&e.focus(),this.invalid=!1,this._showTooltip(),this._dispatchChange()}_onMove(t){1==Math.round(t.pressure)&&(this._dispatchPreChange(),this._setValue(t))}_showTooltip(){let t=this.renderRoot.querySelector("crowd-tooltip");t&&(t.setAttribute("show",!0),this.tooltipTimer&&clearTimeout(this.tooltipTimer),this.tooltipTimer=setTimeout((()=>{t.removeAttribute("show")}),4e3))}_keyDown(t){switch(t.preventDefault(),t.key){case"ArrowLeft":this._dispatchPreChange(),this.value>this.min&&(this.value=this._roundToStep(this.value-this.step,this.step),this._dispatchChange());break;case"ArrowRight":this._dispatchPreChange(),this.value<this.max&&(this.value=this._roundToStep(this.value+this.step,this.step),this._dispatchChange())}}validate(){this.invalid=!1,this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0))}render(){let t="";this.hideMinMax||(t=M`<span>${this.min}</span>`);let e="";this.max&&!this.hideMinMax&&(e=M`<span>${this.max}</span>`);let A="";if(this.notches||this.numbers){let t=[];for(let e=0;e<=this.max;e+=this.step)t.push(e);A=M`
                <div class='notches' part='notches'>
                    ${t.map((t=>{let e="";return this.numbers&&(e=M`<span>${t}</span>`),M`<div class='${this.notches?"notched":""}' style='--left: ${t/this.max*100}%;'>${e}</div>`}))}
                </div>
            `}let r="";r=this.tooltip?M`
                <crowd-tooltip content='${this.value}'>
                    <div aria-label='Slider thumb' class='thumb' part='thumb'></div>
                </crowd-tooltip>
            `:M`
                <div aria-label='Slider thumb' class='thumb' part='thumb'></div>
            `;let i="";this.label&&(i=M`
                <label for='${this.id}'>
                    ${this.label}
                </label>
            `);let n="";return this.invalid&&(n=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `),M`
            <div part='wrapper' class='wrapper'>
                ${i}
                <div class='container' part='container'>
                    ${t}
                    <div @pointermove='${t=>this._onMove(t)}' @pointerdown='${t=>this._setValue(t)}' class='track' part='track' style='--value: ${this.value/this.max*100}%;'>
                        ${A}
                        ${r}
                        <input inputmode='none' id='${this.id}' @keydown='${t=>this._keyDown(t)}' type='text' value='${this.value}' />
                    </div>
                    ${e}
                </div>
                ${n}
            </div>
        `}}class Kt extends X{static styles=[o`
            :host {
                display: inline-block;
            }
            :host,:host * {
                box-sizing: inherit;
            }
            figure {
                margin: 0;
                width: 1em;
                height: 1em;
            }
            svg {
                width: 100%;
                height: 100%;
                transform-origin: 50% 50%;
                animation: spin var(--crowd-spinner-animation-duration, 1s) forwards infinite linear;
            }
            .track,.indicator {
                fill: none;
                stroke-width: var(--track-width,2px);
                r: calc(0.5em - var(--track-width,2px) / 2);
                cx: 0.5em;
                cy: 0.5em;
                stroke: var(--track-color,rgba(0,0,0,0.2));
            }
            .indicator {
                stroke: var(--indicator-color,black);
                stroke-linecap: round;
                stroke-dasharray: 25 250;
                transform-origin: 50% 50%;
                animation: pulse var(--crowd-spinner-animation-duration, 1s) forwards infinite alternate linear;
            }
            @keyframes pulse {
                0% {
                    stroke-dasharray: 25 250;
                }
                100% {
                    stroke-dasharray: 3 250;
                }
            }
            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }
                50% {
                    transform: rotate(180deg);
                }
                0% {
                    transform: rotate(360deg);
                }
            }
        `];render(){return M`
            <figure>
                <svg>
                    <circle class='track'></circle>
                    <circle class='indicator'></circle>
                </svg>
            </figure>
        `}}class Xt extends X{static properties={_content:{type:String}};static styles=[o`
            :host {
                display: block;
            }
            pre {
                padding: 1em;
                border: 1px solid #eeeeee;
            }
        `];_loop(){}_updateStylesheet(){this._content=document.documentElement.style.cssText.replace(/; /g,";\n")}connectedCallback(){super.connectedCallback();let t=[].slice.call(document.querySelectorAll("crowd-customiser"));t&&t.forEach((t=>{t.addEventListener("crowdChange",(()=>this._updateStylesheet()))}))}render(){return M`
            <pre>${this._content}</pre>
        `}}class te extends X{static properties={checked:{type:Boolean,reflect:!0},label:{type:String},value:{type:String,reflect:!0},name:{type:String},true:{type:String},false:{type:String}};static styles=[o`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .wrapper {
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            .container {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                gap: var(--crowd-switch-spacing, 0.5em);
            }
            .container > span {
                cursor: pointer;
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            :host label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin: 0 0 var(--crowd-input-label-spacing, 0.5em);
            }
            .track {
                position:relative;
                padding: var(--crowd-switch-track-spacing, 3px);
                border-radius: var(--crowd-switch-border-radius, 999px);
                width: calc((2 * var(--crowd-switch-size, 1em)) + (2 * var(--crowd-switch-track-spacing, 3px)));
                height: calc((var(--crowd-switch-size, 1em)) + (2 * var(--crowd-switch-track-spacing, 3px)));
                background-color: var(--crowd-switch-track-background-color, #eeeeee);
                cursor: pointer;
                transition-property: background-color;
                transition-duration: var(--crowd-switch-transition-duration,0.15s);
                transition-delay: var(--crowd-switch-transition-delay, 0s);
                transition-timing-function: var(--crowd-switch-ease, ease-in-out);
            }
            .track:focus-within {
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            .thumb {
                position: absolute;
                display: block;
                width: var(--crowd-switch-size, 1em);
                height: var(--crowd-switch-size, 1em);
                border-radius: var(--crowd-switch-border-radius, 999px);
                left: var(--crowd-switch-track-spacing, 3px);
                right: auto;
                background-color: var(--crowd-switch-thumb-background-color, #ffffff);
                transition-property: left, right;
                transition-duration: var(--crowd-switch-transition-duration,0.15s);
                transition-delay: var(--crowd-switch-transition-delay, 0s);
                transition-timing-function: var(--crowd-switch-ease, ease-in-out);
            }
            .on {
                color: var(--crowd-switch-unswitched-text-color, rgba(0,0,0,0.5));
            }
            :host([checked]) .on {
                color: inherit;
            }
            :host([checked]) .off {
                color: var(--crowd-switch-unswitched-text-color, rgba(0,0,0,0.5));
            }

            :host([checked]) .thumb {
                left: 50%;
            }
            :host([checked]) .track {
                background-color: var(--crowd-switch-track-background-color-active, #000000);
            }
            input[type='text'] {
                -webkit-appearance: none;
                opacity: 0;
                position: absolute;
                width: 0;
                height: 0;
            }
        `];_updateValue(){this.checked&&this.true?this.value=this.true:this.checked?this.value="true":!this.checked&&this.false?this.value=this.false:this.checked||(this.value="false")}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_toggle(){this.checked=!this.checked;let t=this.renderRoot.querySelector("input");t&&t.focus(),this._updateValue(),this._dispatchChange()}_check(){this.checked=!0;let t=this.renderRoot.querySelector("input");t&&t.focus(),this._updateValue(),this._dispatchChange()}_unCheck(){this.checked=!1;let t=this.renderRoot.querySelector("input");t&&t.focus(),this._updateValue(),this._dispatchChange()}_keyDown(t){switch(t.preventDefault(),t.key){case" ":this._toggle();break;case"ArrowLeft":this._unCheck();break;case"ArrowRight":this._check()}}constructor(){super(),this.checked=!1,this.value="false"}connectedCallback(){super.connectedCallback(),this.id="switch-"+Date.now()}render(){let t="";this.invalid&&(t=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let e="";this.label&&(e=M`
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `);let A="";this.false&&(A=M`
                <span class='off' part='off' @click='${()=>this._unCheck()}'>${this.false}</span>
            `);let r="";return this.true&&(r=M`
                <span class='on' part='on' @click='${()=>this._check()}'>${this.true}</span>
            `),M`
            <div part='wrapper' class='wrapper'>
                ${e}
                <div class='container' part='container'>
                    ${A}
                    <div @click='${()=>this._toggle()}' class='track' part='track'>
                        <span class='thumb' part='thumb'></span>
                        <input inputmode='none' aria-label='Toggle switch' @keydown='${t=>this._keyDown(t)}' id='${this.id}' type='text' />
                    </div>
                    ${r}
                </div>
                ${t}
                <slot name="help-text"></slot>
            </div>
        `}}class ee extends X{static properties={title:{type:String},_tabIndex:{type:Number},active:{type:Boolean,reflect:!0}};static styles=[o`
            :host {
                display: contents;
            }
            * {
                box-sizing: border-box;
            }
            :host(:not([active])) .container {
                opacity: 0;
                pointer-events: none;
            }
            :host(:not([active])) button {
                opacity: 0.4;
            }
            article {
                position: relative;
                grid-area: 1 / 1;
                max-width: 100%;
            }
        `];_dispatchChange(){const t=new CustomEvent("crowdShowTab");this.dispatchEvent(t)}show(){this.active=!0,this._dispatchChange()}hide(){this.active=!1}render(){return M`
            <article part='wrapper'>
                <div class='container' part='container'>
                    <slot></slot>
                </div>
            </article>
        `}}class Ae extends X{static properties={_tabs:{type:Array},_activeTab:{type:Number}};static styles=[o`
            :host {
                display: block;
            }
            * {
                box-sizing: border-box;
            }
            .panels {
                display: grid;
            }
            .buttons {
                display: flex;
                flex-flow: row nowrap;
                justify-content: flex-start;
                align-items: center;
                overflow-x: scroll;
                scrollbar-width: none;
                gap: var(--crowd-tabs-gap,0px);
                width:100%;
                border-bottom: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
            }
            .buttons::-webkit-scrollbar {
                display: none;
                width: 0px;
                height: 0px;
                opacity: 0;
            }
            .buttons button {
                -webkit-appearance: none;
                background-color: var(--crowd-tab-button-background-color, #fff);
                border: var(--crowd-tab-button-border-width, 0px) var(--crowd-tab-button-border-style, solid) var(--crowd-tab-button-border-color, #eee);
                padding: var(--crowd-tab-button-padding-vertical, 0.3em) var(--crowd-tab-button-padding-horizontal, 0.5em);
                font-size: var(--crowd-tab-button-font-size, inherit);
                cursor: pointer;
                border-top-left-radius: var(--crowd-tab-button-border-radius, 3px);
                border-top-right-radius: var(--crowd-tab-button-border-radius, 3px);
                opacity: 0.4;
            }
            @media (hover: hover) {
                .buttons button:hover {
                    background-color: var(--crowd-tab-button-hover-background-color, #fafafa);
                }
            }
            .buttons button:active,.buttons button:focus-visible {
                outline: none;
                background-color: var(--crowd-tab-button-hover-background-color, #fafafa);
                box-shadow: 0 0 0 var(--crowd-input-focus-width, 2px) var(--crowd-input-focus-color, rgba(0,0,0,0.3));
            }
            .buttons button.active {
                opacity: 1;
                background-color: var(--crowd-tab-button-active-background-color, #fff);
                border-top: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
                border-left: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
                border-right: var(--crowd-tab-button-active-border-width, 1px) var(--crowd-tab-button-active-border-style, solid) var(--crowd-tab-button-active-border-color, #ccc);
                border-bottom: none;
                position: relative;
            }
            // .buttons button.active::after {
            //     content: '';
            //     display: inline-block;
            //     width: 100%;
            //     left: 0;
            //     height: var(--crowd-tab-button-active-border-width, 1px);
            //     background-color: var(--crowd-tab-button-active-background-color, #fff);
            //     position: absolute;
            //     bottom: 0;
            //     transform: translateY(100%);
            // }
        `];_hideOtherTabs(t){[].slice.call(this.querySelectorAll("crowd-tab")).forEach((e=>{e!=t&&e.hide()}))}showTab(t){let e=[].slice.call(this.querySelectorAll("crowd-tab"));e&&(this._activeTab=t,e[t].show())}connectedCallback(){super.connectedCallback();let t=[].slice.call(this.querySelectorAll("crowd-tab"));t&&(this._tabs=t,t.forEach((t=>{t.addEventListener("crowdShowTab",(()=>this._hideOtherTabs(t)),!1)})),this.showTab(0))}constructor(){super(),this._activeTab=0}render(){return M`
            <article class='tab-group'>
                <div class='buttons'>
                    ${this._tabs.map(((t,e)=>M`
                        <button class='${e===this._activeTab?"active":""}' @click='${()=>this.showTab(e)}'>${t.title}</button>
                    `))}
                </div>
                <div class='panels'>
                    <slot></slot>
                </div>
            </article>
        `}}class re extends qt{static styles=o`
            ${qt.styles}
            .input-container {
                position: relative;
            }
            .icon {
                position: absolute;
                top: var(--crowd-textarea-icon-top,1ex);
                right: var(--crowd-textarea-icon-right,1ex);
                bottom: var(--crowd-textarea-icon-bottom,auto);
                left: var(--crowd-textarea-icon-left,auto);
            }
        `;constructor(){super()}render(){let t="";this.invalid&&(t=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let e="";this.success&&(e=M`
                <div part='success' class='success'>
                    ${this.successMessage}
                </div>
            `);let A="";return this.maxlength&&(A=`maxlength='${this.maxlength}'`),M`
            <div part='wrapper' class='wrapper'>
                <label part='label' for='${this.id}'>
                    ${this.label}
                </label>
                <div part='container' class='input-container'>
                    <textarea rows='4' ${A} @change='${this._dispatchChange}' @input='${this._onInput}' id='${this.id}' name='${this.name}' value='${this.value}' placeholder='${this.placeholder}' required='${this.required}'>${this.value}</textarea>
                    <div class='icon'>
                        <slot name="icon"></slot>
                    </div>
                </div>
                ${t}
                ${e}
                <slot name="help-text"></slot>
            </div>
        `}}const{DateTime:ie}=A(490);class ne extends X{static properties={value:{type:String,reflect:!0},open:{type:Boolean,reflect:!0},_angle:{type:Number},hours:{type:Number},minutes:{type:Number},meridiem:{type:String},selecting:{type:String},format:{type:String,reflect:!0},label:{type:String},required:{type:Boolean},errorMessage:{type:String},invalid:{type:Boolean,reflect:!0}};static styles=[o`
            :host {
                display: inline-block;
            }
            :host,:host *,:host *::before, :host *::after {
                box-sizing: border-box;
                padding: 0;
                margin: 0;
            }
            .container {
                position: relative;
                padding: var(--crowd-input-wrapper-padding-vertical, 0) var(--crowd-input-wrapper-padding-horizontal, 0);
                max-width: 100%;
            }
            label.label {
                display: inline-block;
                color: var(--crowd-input-label-color, inherit);
                margin-bottom: var(--crowd-input-label-spacing, 0.5em);
            }
            .error {
                font-size: var(--crowd-input-error-message-font-size, 0.8em);
                color: var(--crowd-input-error-message-color, red);
            }
            :host([invalid]) .container {
                outline: 1px solid var(--crowd-input-error-message-color, red);
            }
            slot[name='help-text'] {
                font-size:var(--crowd-input-error-message-font-size, 0.8em);
            }
            .picker {
                position: absolute;
                top: 100%;
                left: 0;
                transform-origin: top center;
                transform: scaleY(0);
                transition-property: transform;
                transition-duration: var(--crowd-time-picker-transition-duration, 0.15s);
                transition-delay: var(--crowd-time-picker-transition-delay, 0s);
                transition-timing-function: var(--crowd-time-picker-transition-ease, ease-in-out);
                background-color: var(--crowd-time-picker-background-color, white);
                padding: var(--crowd-time-picker-padding-vertical, 1em) var(--crowd-time-picker-padding-horizontal, 1em);
                box-shadow: var(
                    --crowd-select-dropdown-box-shadow,
                    0 2px 8px rgba(0, 0, 0, 0.1)
                );
                z-index: var(--crowd-time-picker-z-index, 999);
            }
            :host([open]) .picker {
                transform: scaleY(1);
            }

            .time-display {
                display: flex;
                flex-flow: row nowrap;
                gap: 0.5em;
                padding-bottom: 0.5em;
            }

            .time-digital {
                flex: 1 0 auto;
                display: flex;
                flex-flow: row nowrap;
                align-items: center;
                gap: 0.25em;
                height: 100%;
            }
            .time-digital crowd-button {
                font-size: 2em;
                min-width: 3em;
                --crowd-button-padding-vertical: 0.25em
                --crowd-button-padding-horizontal: 0.5em
            }
            .time-display crowd-button.active {
                --crowd-button-background-color: var(--crowd-time-picker-active-button-background-color, #ccc);
            }
            .time-digital span {
                font-size: 2em;
            }

            .meridiem {
                flex: 0 1 0;
                height: 100%;
                border: var(--crowd-button-border-width, 2px) var(--crowd-button-border-style, solid) var(--crowd-button-border-color, #aeaeae);
                border-radius: var(--crowd-button-border-radius, 3px);
            }

            .meridiem crowd-button {
                --crowd-button-border-width: 0px;
                --crowd-button-border-radius: 0px;
            }

            .clock {
                width: 100%;
                aspect-ratio: 1/1;
                display: grid;
                grid-template-area: 100% / 100%;
            }

            .clock-face,.clock-hand,.clock-thumb {
                grid-area: 1/1;
            }

            .clock-face {
                width: 100%;
                height: 100%;
                background-color: var(--crowd-time-picker-clock-face-background-color, #eee);
                border-radius: 50%;
                position: relative;
                display: grid;
            }

            .clock-face > span {
                grid-area: 1/1;
                place-self: center;
                display: block;
                width: 100%;
                aspect-ratio: 1/1;
                position: relative;
                z-index: 2;
                pointer-events: none;
                user-select: none;
            }

            .clock-face > span > span {
                display: inline-block;
            }

            .clock-hand {
                place-self: center;
                height: 50%;
                width: var(--crowd-time-picker-clock-hand-width, 2px);
                background-color: var(--crowd-time-picker-clock-hand-background-color, #ccc);
            }

            .clock-thumb {
                aspect-ratio: 1/1;
                transform-origin:center;
                position: relative;
                place-self: center;
                cursor: pointer;
            }

            .clock-thumb::before {
                content: '';
                position: absolute;
                width: var(--crowd-time-picker-clock-thumb-size, 2em);
                background-color: var(--crowd-time-picker-clock-hand-background-color, #ccc);
                aspect-ratio: 1/1;
                transform: translate(-25%,-25%);
                border-radius: 50%;
                top: 0;
                left: 0;
            }
            .action-buttons {
                margin-top: 0.5em;
                display: flex;
                gap: 1em;
                justify-content: space-between;
            }
        `];_setValue(){let t=this.hours;"pm"==this.meridiem?t=12==this.hours?this.hours:this.hours+12:12==this.hours&&(t=0);let e=ie.fromObject({hour:t,minute:this.minutes});this.value=e.toFormat(this.format),this._dispatchChange()}_showPicker(){this.open=!0}_hidePicker(){this.open=!1}_onPointerDown(t){this._onMouseMove(t),t.target.addEventListener("pointermove",this.bindMouseMove,!1)}_onPointerUp(t){t.target.removeEventListener("pointermove",this.bindMouseMove,!1)}_onMouseMove(t){let e=this.shadowRoot.querySelector(".clock"),A=e.getBoundingClientRect().x+e.getBoundingClientRect().width/2,r=e.getBoundingClientRect().y+e.getBoundingClientRect().height/2,i=t.clientX-A,n=r-t.clientY,o=Math.PI/2-Math.atan2(n,i);o*=180/Math.PI,o=(o+360)%360,this._angle=o,"hours"==this.selecting&&(this.hours=Math.round(o/360*12),0==this.hours&&(this.hours=12)),"minutes"==this.selecting&&(this.minutes=Math.round(o/360*60),60==this.minutes&&(this.minutes=0))}_getHoursDegree(t){return(t+1)/12*360+45}_getMinutesDegree(t){return(t+1)/6*360+45}_addPaddingZero(t){return t<10?"0"+t:t}_dispatchChange(){const t=new CustomEvent("crowdChange");this.dispatchEvent(t)}_onInput(t){this.invalid=!1,this._dispatchChange()}_onKeydown(t){"Tab"!=t.key&&t.preventDefault()}validate(){this.invalid=!1,"email"==this.type&&this._validateEmail(),this.required&&(""!==this.value&&null!=this.value||(this.invalid=!0))}connectedCallback(){super.connectedCallback(),new nt(this),this.bindMouseMove=this._onMouseMove.bind(this),this.textInput=this.renderRoot.querySelector("crowd-input")}constructor(){super();let t=(new Date).getHours();t>12?(this.meridiem="pm",t-=12):this.meridiem="am",this.hours=t,this.minutes=(new Date).getMinutes(),this._angle=t/12*360,this.selecting="hours",this.format="HH:mm a",this._setValue()}render(){let t="";this.invalid&&(t=M`
                <div part='error' class='error'>
                    ${this.errorMessage}
                </div>
            `);let e="";return this.label&&(e=M`
                <label class='label' part='label' for='${this.id}'>
                    ${this.label}
                </label>
            `),M`
            <div class='container' @focusin='${this._showPicker}' @focusout='${this._hidePicker}'>
                ${e}
                <div part='input' class='input'>
                    <crowd-input @keydown='${this._onKeydown}' @crowdChange='${this._onInput}' type='text' value='${this.value}'></crowd-input>
                </div>
                <div part='picker' class='picker' tabindex='0'>
                    <div class='time-display'>
                        <div class='time-digital'>
                            <div class='hours'>
                                <crowd-button class='${"hours"==this.selecting?"active":""}' @click=${()=>this.selecting="hours"}>${this._addPaddingZero(this.hours)}</crowd-button>
                            </div>
                            <span>:</span>
                            <div class='minutes'>
                                <crowd-button class='${"minutes"==this.selecting?"active":""}' @click=${()=>this.selecting="minutes"}>${this._addPaddingZero(this.minutes)}</crowd-button>
                            </div>
                        </div>
                        <div class='meridiem'>
                            <crowd-button @click='${()=>this.meridiem="am"}' class='${"am"==this.meridiem?"active":""}'>AM</crowd-button>
                            <crowd-button @click='${()=>this.meridiem="pm"}' class='${"pm"==this.meridiem?"active":""}'>PM</crowd-button>
                        </div>
                    </div>
                    <div class='clock' @pointerdown='${this._onPointerDown}' @pointerup='${this._onPointerUp}' @pointerout='${this._onPointerUp}' @pointerleave='${this._onPointerUp}'>
                        <div class='clock-face'>
                            ${"hours"==this.selecting?new Array(12).fill(0).map(((t,e)=>M`
                                    <span style='
                                        width: ${Math.sqrt(5e3)}%;
                                        transform: rotate(${this._getHoursDegree(e)}deg);
                                    '>
                                        <span style='transform: rotate(${-1*this._getHoursDegree(e)}deg);'>
                                            ${e+1}
                                        </span>
                                    </span>
                                `)):""}
                            ${"minutes"==this.selecting?new Array(6).fill(0).map(((t,e)=>M`
                                    <span style='
                                        width: ${Math.sqrt(5e3)}%;
                                        transform: rotate(${this._getMinutesDegree(e)}deg);
                                    '>
                                        <span style='transform: rotate(${-1*this._getMinutesDegree(e)}deg);'>
                                            ${10*(e+1)}
                                        </span>
                                    </span>
                                `)):""}
                        </div>
                        <div class='clock-hand' style='
                            transform: rotate(${this._angle}deg) translateY(-50%);
                        '></div>
                        <div class='clock-thumb' style='
                            width: ${Math.sqrt(5e3)}%;
                            transform: rotate(${this._angle+45}deg);
                        '></div>
                    </div>
                    <div class='action-buttons'>
                        <crowd-button @click='${this._hidePicker}'>CANCEL</crowd-button>
                        <crowd-button @click='${()=>{this._setValue(),this._hidePicker()}}'>OK</crowd-button>
                    </div>
                </div>
                ${t}
                <slot name="help-text"></slot>
            </div>
        `}}class oe extends X{static styles=[o`
            :host {
                display: block;
            }
            .toast-stack {
                position: fixed;
                top: 1rem;
                right: 1rem;
                display: flex;
                flex-flow: column nowrap;
                justify-content: flex-start;
                align-items: stretch;
                gap: var(--toast-stack-gap,1rem);
                z-index: var(--toast-stack-z-index, 9999);
                max-width: var(--toast-stack-max-width, min(500px,100%));
                pointer-events: none;
            }
        `];render(){return M`
            <article class='toast-stack'>
                <slot></slot>
            </article>
        `}}class se extends X{static properties={_hidden:{type:Boolean,reflect:!0},show:{type:Boolean},content:{type:String},delay:{type:Number},hoist:{type:Boolean}};static styles=[o`
            :host {
                display: contents;
                position:relative;
            }
            :host, :host * {
                box-sizing: inherit;
            }
            .positioner {
                position: absolute;
                z-index: var(--crowd-tooltip-z-index,1);
                pointer-events: none;
            }
            :host([hoist]) .positioner {
                position: fixed;
            }
            .tooltip {
                display: inline-block;
                font-size: 0.8em;
                padding: var(--crowd-tooltip-padding-vertical,0.5em) var(--crowd-tooltip-padding-horizontal,0.8em);
                color: var(--crowd-tooltip-color, inherit);
                background-color: var(--crowd-tooltip-background-color, white);
                border: var(--crowd-tooltip-border-width, 1px) var(--crowd-tooltip-border-style, solid) var(--crowd-tooltip-border-color, #eee);
                border-radius: var(--crowd-tooltip-border-radius, 4px);
                transform: translate(-50%,calc(-100% - 1em)) scale(1);
                opacity: 1;
                transition-property: transform, opacity;
                transition-duration: var(--crowd-tooltip-transition-duration, 0.15s);
                transition-timing-function: var(--crowd-tooltip-transition-ease, ease-in-out);
                transition-delay: var(--tooltip-delay);
            }
            :host([_hidden]) .tooltip {
                pointer-events: none;
                transform: translate(-50%,calc(-100% - 1em)) scale(0.5);
                opacity: 0;
                transition-delay: 0s;
            }
            :host([show]) .tooltip {
                pointer-events: all !important;
                transform: translate(-50%,calc(-100% - 1em)) scale(1) !important;
                opacity: 1 !important;
            }
        `];constructor(){super(),this._hidden=!0,this.delay=0}_positionTooltip(){let t=this.renderRoot.querySelector(".positioner");if(!t)return;let e=null,A=null;if(this.hoist)for(let t of this.children){let r=t.getBoundingClientRect();(null===e||r.top<e)&&(e=r.top);let i=r.left+r.width/2;(null===A||i>A)&&(A=i)}else for(let t of this.children){(null===e||t.offsetTop<e)&&(e=t.offsetTop);let r=t.offsetLeft+t.offsetWidth/2;(null===A||r>A)&&(A=r)}t.style.top=e+"px",t.style.left=A+"px",requestAnimationFrame((()=>this._positionTooltip()))}connectedCallback(){super.connectedCallback();for(let t of this.children)t.addEventListener("pointerover",(()=>this._show()),!1),t.addEventListener("pointerleave",(()=>this._hide()),!1)}_show(){this._hidden=!1}_hide(){this._hidden=!0}render(){return this._positionTooltip(),M`
            <div class='positioner' part='positioner'>
                <article part='tooltip' style='--tooltip-delay: ${this.delay/1e3}s;' class='tooltip' ${this._hidden?'hidden aria-hidden="true"':""}>
                    ${this.content}
                </article>
            </div>
            <slot></slot>
        `}}const ae=t=>null!=t?t:L;A(123);class le extends X{static properties={src:{type:String},"src-youtube":{type:String},"src-vimeo":{type:String},type:{type:String},playState:{type:Boolean,reflect:!0},initialPlay:{type:Boolean},progress:{type:Number},volume:{type:Number},muted:{type:Boolean,reflect:!0},loop:{type:Boolean,reflect:!0},fullscreen:{type:Boolean},_hideControls:{type:Boolean,reflect:!0},controlHideTimeoutDuration:{type:Number},playbackRate:{type:Number},pip:{type:Boolean},poster:{type:String},_embedWidth:{type:Number},playsupported:{type:Boolean},controls:{type:Boolean}};static styles=[o`

            *,*::before, *::after {
                box-sizing: border-box;
                transition-property: none;
                transition-duration: var(--crowd-video-transition-duration, 0.15s);
                transition-delay: var(--crowd-video-transition-delay, 0s);
                transition-timing-function: var(--crowd-video-transition-ease, ease-in-out);
            }

            :host {
                display: inline-block;
                --crowd-range-indicator-color: var(--crowd-video-theme-color, #000);
                --crowd-button-background-color: var(--crowd-video-theme-color, #000);
                --crowd-button-border-color: var(--crowd-video-theme-color, #000);
                --crowd-button-color: var(--crowd-video-alt-color, #fff);
                --crowd-button-hover-background-color: var(--crowd-video-button-hover-color, #aaa);
                --crowd-button-hover-border-color: var(--crowd-video-button-hover-color, #000);
                --crowd-menu-item-color: var(--crowd-video-theme-color, #000);
            }

            :host([src-youtube]) {
                display: block;
                width: 100%;
            }

            video {
                display: inline-block;
                max-width: 100%;
                width:100%;
            }

            .container {
                display: grid;
                place-items: center;
                position: relative;
                overflow: hidden;
            }
            :host([src-youtube]) .container {
                width: 100%;
            }

            .media, .play, .pause {
                grid-area: 1/1;
            }

            .media {
                width: 100%;
            }

            .pause {
                opacity: 0;
                transition-property: opacity;
            }

            @media (hover: hover) {
                .container:hover .pause {
                    opacity: 1;
                }
                :host([_hideControls]) .container:hover .pause {
                    opacity: 0;
                }
            }

            .controls {
                position: absolute;
                bottom: 0;
                left: 0;
                right: 0;
                color: #fff;
                padding: var(--crowd-video-controls-padding, 1em);
                background: var(--crowd-video-controls-background,linear-gradient(0deg, var(--crowd-video-controls-background-color,rgba(0,0,0,1)) 0%, rgba(255,255,255,0) 100%));
                transition-property: transform opacity;
                transform: translateY(100%);
                opacity: 0;
                display: flex;
                align-items: center;
                gap: var(--crowd-video-controls-gap, 1em);
            }

            @media (hover: hover) {
                .container:hover .controls {
                    transform: translateY(0);
                    opacity: 1;
                }
                :host([_hideControls]) .container:hover .controls {
                    transform: translateY(100%);
                    opacity: 0;
                }
            }

            .controls crowd-icon-button {
                font-size: var(--crowd-video-controls-button-size, 1.2em);
            }

            .controls .progress {
                width: 100%;
                --crowd-range-width: 100%;
            }
            .controls .volume {
                --crowd-range-width: 75px;
            }
            .audio {
                display: flex;
                align-items: center;
                gap: 0.5em;
            }
            crowd-icon-button {
                --crowd-button-hover-color: var(--crowd-video-button-hover-color, #aaa);
            }

            .settings {
                --crowd-input-padding-vertical: 0px;
                --crowd-input-wrapper-padding-vertical: 5px;
            }

            .settings::part(dropdown) {
                overflow: visible;
            }

            .playback span {
                display: inline-block;
                padding-left: 0.5em;
                font-style: italic;
                opacity: 0.5;
            }
            .container:not(.no-play) google-youtube {
                pointer-events: none;
            }
            .container.no-play .play {
                display: none;
            }
        `];async play(){this.video=this.renderRoot.querySelector(".video"),this.youtube=this.renderRoot.querySelector("google-youtube"),!this.initialPlay&&this.video&&(this.seeking=!1),this.video?(this.initialPlay=!0,this.video.play().then((()=>{this.playState=!0}))):this.youtube&&(this.initialPlay=!0,this.youtube.playsupported?(this.youtube.play(),this.playState=!0):this.renderRoot.querySelector(".container").classList.add("no-play"))}pause(){this.video=this.renderRoot.querySelector(".video"),this.youtube=this.renderRoot.querySelector("google-youtube"),this.video?(this.video.pause(),this.playState=!1):this.youtube&&(this.youtube.pause(),this.playState=!1,this.youtube.playsupported||this.renderRoot.querySelector(".container").classList.add("no-play"))}_saveVolume(){localStorage.setItem("crowd-video-volume",this.volume),localStorage.setItem("crowd-video-muted",this.muted)}_toggleFullscreen(){let t=this.renderRoot.querySelector(".container");this.fullscreen?document.exitFullscreen():t.requestFullscreen()}_startControlTimeout(){this._hideControls=!1,this._clearControlTimeout(),this.controlTimeout=setTimeout((()=>{this._hideControls=!0}),this.controlHideTimeoutDuration)}_clearControlTimeout(){this.controlTimeout&&clearTimeout(this.controlTimeout)}_setPlaybackRate(t){this.playbackRate=t}_togglePiP(){this.pip&&document.pictureInPictureElement?document.exitPictureInPicture().then((()=>{this.pip=!1})):(this.video=this.renderRoot.querySelector(".video"),this.video&&(this.video.requestPictureInPicture().then((()=>{this.pip=!0})),this.video.addEventListener("leavepictureinpicture",(()=>{this.pip=!1}))))}_checkPlaySupported(){var t,e=document.createElement("video");if("play"in e){e.id="playtest",e.style.position="absolute",e.style.top="-9999px",e.style.left="-9999px";var A=document.createElement("source");A.src="data:video/mp4;base64,AAAAFGZ0eXBNU05WAAACAE1TTlYAAAOUbW9vdgAAAGxtdmhkAAAAAM9ghv7PYIb+AAACWAAACu8AAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAnh0cmFrAAAAXHRraGQAAAAHz2CG/s9ghv4AAAABAAAAAAAACu8AAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAFAAAAA4AAAAAAHgbWRpYQAAACBtZGhkAAAAAM9ghv7PYIb+AAALuAAANq8AAAAAAAAAIWhkbHIAAAAAbWhscnZpZGVBVlMgAAAAAAABAB4AAAABl21pbmYAAAAUdm1oZAAAAAAAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAVdzdGJsAAAAp3N0c2QAAAAAAAAAAQAAAJdhdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAFAAOABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAAEmNvbHJuY2xjAAEAAQABAAAAL2F2Y0MBTUAz/+EAGGdNQDOadCk/LgIgAAADACAAAAMA0eMGVAEABGjuPIAAAAAYc3R0cwAAAAAAAAABAAAADgAAA+gAAAAUc3RzcwAAAAAAAAABAAAAAQAAABxzdHNjAAAAAAAAAAEAAAABAAAADgAAAAEAAABMc3RzegAAAAAAAAAAAAAADgAAAE8AAAAOAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA0AAAANAAAADQAAAA4AAAAOAAAAFHN0Y28AAAAAAAAAAQAAA7AAAAA0dXVpZFVTTVQh0k/Ou4hpXPrJx0AAAAAcTVREVAABABIAAAAKVcQAAAAAAAEAAAAAAAAAqHV1aWRVU01UIdJPzruIaVz6ycdAAAAAkE1URFQABAAMAAAAC1XEAAACHAAeAAAABBXHAAEAQQBWAFMAIABNAGUAZABpAGEAAAAqAAAAASoOAAEAZABlAHQAZQBjAHQAXwBhAHUAdABvAHAAbABhAHkAAAAyAAAAA1XEAAEAMgAwADAANQBtAGUALwAwADcALwAwADYAMAA2ACAAMwA6ADUAOgAwAAABA21kYXQAAAAYZ01AM5p0KT8uAiAAAAMAIAAAAwDR4wZUAAAABGjuPIAAAAAnZYiAIAAR//eBLT+oL1eA2Nlb/edvwWZflzEVLlhlXtJvSAEGRA3ZAAAACkGaAQCyJ/8AFBAAAAAJQZoCATP/AOmBAAAACUGaAwGz/wDpgAAAAAlBmgQCM/8A6YEAAAAJQZoFArP/AOmBAAAACUGaBgMz/wDpgQAAAAlBmgcDs/8A6YEAAAAJQZoIBDP/AOmAAAAACUGaCQSz/wDpgAAAAAlBmgoFM/8A6YEAAAAJQZoLBbP/AOmAAAAACkGaDAYyJ/8AFBAAAAAKQZoNBrIv/4cMeQ==",e.appendChild(A);var r=document.createElement("source");r.src="data:video/webm;base64,GkXfo49CgoR3ZWJtQoeBAUKFgQEYU4BnAQAAAAAAF60RTZt0vE27jFOrhBVJqWZTrIIQA027jFOrhBZUrmtTrIIQbE27jFOrhBFNm3RTrIIXmU27jFOrhBxTu2tTrIIWs+xPvwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFUmpZuQq17GDD0JATYCjbGliZWJtbCB2MC43LjcgKyBsaWJtYXRyb3NrYSB2MC44LjFXQY9BVlNNYXRyb3NrYUZpbGVEiYRFnEAARGGIBc2Lz1QNtgBzpJCy3XZ0KNuKNZS4+fDpFxzUFlSua9iu1teBAXPFhL4G+bmDgQG5gQGIgQFVqoEAnIEAbeeBASMxT4Q/gAAAVe6BAIaFVl9WUDiqgQEj44OEE95DVSK1nIN1bmTgkbCBULqBPJqBAFSwgVBUuoE87EQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9DtnVB4eeBAKC4obaBAAAAkAMAnQEqUAA8AABHCIWFiIWEiAICAAamYnoOC6cfJa8f5Zvda4D+/7YOf//nNefQYACgnKGWgQFNANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQKbANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQPoANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQU1ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQaDANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQfQANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQkdANEBAAEQEBRgAGFgv9AAIiGAAPuC/rOgnKGWgQprANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQu4ANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ0FANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgQ5TANEBAAEQEAAYABhYL/QACIhgAPuC/rKgnKGWgQ+gANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRDtANEBAAEQEAAYABhYL/QACIhgAPuC/rOgnKGWgRI7ANEBAAEQEAAYABhYL/QACIhgAPuC/rIcU7trQOC7jLOBALeH94EB8YIUzLuNs4IBTbeH94EB8YIUzLuNs4ICm7eH94EB8YIUzLuNs4ID6LeH94EB8YIUzLuNs4IFNbeH94EB8YIUzLuNs4IGg7eH94EB8YIUzLuNs4IH0LeH94EB8YIUzLuNs4IJHbeH94EB8YIUzLuNs4IKa7eH94EB8YIUzLuNs4ILuLeH94EB8YIUzLuNs4INBbeH94EB8YIUzLuNs4IOU7eH94EB8YIUzLuNs4IPoLeH94EB8YIUzLuNs4IQ7beH94EB8YIUzLuNs4ISO7eH94EB8YIUzBFNm3SPTbuMU6uEH0O2dVOsghTM",e.appendChild(r),document.body.appendChild(e),e.onplaying=A=>{clearTimeout(t),this.playsupported=A&&"playing"===A.type||0!==e.currentTime,e.onplaying=null,document.body.removeChild(e)},t=setTimeout(e.onplaying,500),e.play().catch((t=>{}))}else this.playsupported=!1}constructor(){super(),this.playState=!1,this.initialPlay=!1,this.loop=!1,this.controls=!1,localStorage.getItem("crowd-video-volume")?this.volume=Number(localStorage.getItem("crowd-video-volume")):this.volume=1,localStorage.getItem("crowd-video-muted")&&(this.muted="true"==localStorage.getItem("crowd-video-muted")),this._saveVolume(),this.fullscreen=!1,this.controlHideTimeoutDuration=5e3,this.playbackRate=1,this.pip=!1}connectedCallback(){super.connectedCallback(),document.addEventListener("fullscreenchange",(()=>{document.fullscreenElement?this.fullscreen=!0:this.fullscreen=!1})),this._checkPlaySupported(),console.log(this.controls)}firstUpdated(){let t=this.renderRoot.querySelector(".container");this._embedWidth=t.getBoundingClientRect().width,new ResizeObserver((t=>{for(let e of t)this._embedWidth=e.contentRect.width})).observe(t)}render(){let t="";return this.src?t=M`
                <video class='video' muted=${ae(this.muted?this.muted:void 0)} loop=${ae(this.loop?this.loop:void 0)} poster='${this.poster}' loading='lazy' preload="metadata" playsinline part='video' @timeupdate='${t=>{this.video&&requestAnimationFrame((()=>{this.seeking||(this.progress=this.video.currentTime/this.video.duration*100,this.video.volume=this.volume,this.video.muted=this.muted,this.video.playbackRate=this.playbackRate)}))}}'>
                    <source src='${this.src}' type='${this.type}' />
                </video>
            `:this["src-youtube"]&&(t=M`
                <google-youtube
                    video-id="${this["src-youtube"]}"
                    height="${this._embedWidth/16*9}px"
                    width="${this._embedWidth}px"
                    autoplay="0"
                    playsupported="${this.playsupported}"
                    chromeless
                    class='youtube'
                    part='youtube'
                    @google-youtube-state-change='${t=>{0===t.detail.data&&(this.pause(),this.youtube._videoIdChanged(),this.loop&&this.play())}}'
                    @playbackstarted-changed='${t=>{this.renderRoot.querySelector(".container").classList.remove("no-play"),this.playState=t.detail.value}}'
                    @currenttime-changed='${t=>{this.initialPlay=!0,this.youtube?requestAnimationFrame((()=>{this.seeking||(this.progress=this.youtube.currenttime/this.youtube.duration*100,this.youtube.setVolume(100*this.volume),this.muted?this.youtube.mute():this.youtube.unMute(),this.youtube.setPlaybackRate(this.playbackRate))})):this.youtube=this.renderRoot.querySelector("google-youtube")}}'
                >
                </google-youtube>
            `),M`
            <div class='container' part='container' @pointerleave='${()=>this._clearControlTimeout()}' @pointerenter='${()=>this._startControlTimeout()}' @pointermove='${()=>this._startControlTimeout()}' @pointerover='${()=>this._startControlTimeout()}'>
                <div class='media' part='media'>
                    ${t}
                </div>
                ${!this.playState&&this.controls?M`
                        <crowd-button @click='${()=>this.play()}' circle part='play' class='play'>
                            <slot name='play'>
                                <crowd-icon name='play-fill'></crowd-icon>
                            </slot>
                        </crowd-button>
                    `:""}
                ${this.playState&&this.controls?M`
                        <crowd-button @click='${()=>this.pause()}' circle part='pause' class='pause'>
                            <slot name='pause'>
                                <crowd-icon name='pause-fill'></crowd-icon>
                            </slot>
                        </crowd-button>
                    `:""}
                ${this.initialPlay&&this.controls?M`
                        <div class='controls' part='controls'>
                            <crowd-icon-button @click='${()=>{this.playState?this.pause():this.play()}}' name='${this.playState?"pause-fill":"play-fill"}'></crowd-icon-button>
                            <crowd-range part='progress' class='progress' hideMinMax='true' min='0' max='100' value='${this.progress}' step='1' @crowdMove='${t=>{if(this.pause(),this.seeking=!0,this.video&&(this.video.currentTime=t.currentTarget.value/100*this.video.duration),this.youtube&&3!=this.youtube.state){let e=(t.currentTarget.value/100*this.youtube.duration-this.progress)/100*this.youtube.duration;e=Math.round(e),this.youtube.seekTo(e)}setTimeout((()=>{this.seeking=!1,this.play()}),500)}}'></crowd-range>
                            <div class='audio' part='audio'>
                                <crowd-icon-button @click='${()=>{this.muted=!this.muted,this._saveVolume()}}' name='${0==this.volume||this.muted?"volume-mute-fill":""}${this.volume>0&&this.volume<.5&&!this.muted?"volume-down-fill":""}${this.volume>=.5&&!this.muted?"volume-up-fill":""}'></crowd-icon-button>
                                <crowd-range part='volume' class='volume' hideMinMax='true' min='0' max='1' value='${this.volume}' step='0.1' @crowdMove='${t=>{this.volume=t.currentTarget.value,this._saveVolume()}}'></crowd-range>
                            </div>
                            <crowd-dropdown position="top right" class='settings' part='settings'>
                                <crowd-icon-button name='gear-fill' slot='trigger'></crowd-icon-button>
                                <crowd-menu>
                                    <crowd-menu-item class='playback' position='left bottom'>
                                        Playback Speed <span>${this.playbackRate}x</span>
                                        <crowd-menu slot="submenu">
                                            <crowd-menu-item @click='${()=>this._setPlaybackRate(.25)}'>0.25x</crowd-menu-item>
                                            <crowd-menu-item @click='${()=>this._setPlaybackRate(.5)}'>0.5x</crowd-menu-item>
                                            <crowd-menu-item @click='${()=>this._setPlaybackRate(1)}'>1x</crowd-menu-item>
                                            <crowd-menu-item @click='${()=>this._setPlaybackRate(1.5)}'>1.5x</crowd-menu-item>
                                            <crowd-menu-item @click='${()=>this._setPlaybackRate(2)}'>2x</crowd-menu-item>
                                        </crowd-menu>
                                    </crowd-menu-item>
                                    <crowd-menu-item @click='${()=>this._togglePiP()}'>Picture in picture</crowd-menu-item>
                                </crowd-menu>
                            </crowd-dropdown>
                            <crowd-icon-button @click='${()=>this._toggleFullscreen()}' name='${this.fullscreen?"fullscreen-exit":"fullscreen"}'></crowd-icon-button>
                        </div>
                    `:""}
            </div>
        `}}customElements.define("crowd-accordion-group",tt),customElements.define("crowd-accordion-item",et),customElements.define("crowd-alert",At),customElements.define("crowd-badge",rt),customElements.define("crowd-button",it),customElements.define("crowd-checkbox",ot),customElements.define("crowd-color-picker",lt),customElements.define("crowd-context-menu",ct),customElements.define("crowd-customiser",dt),customElements.define("crowd-date-picker",ht),customElements.define("crowd-dialog",mt),customElements.define("crowd-drawer",yt),customElements.define("crowd-dropdown",wt),customElements.define("crowd-form",xt),customElements.define("crowd-icon-button",St),customElements.define("crowd-icon",Vt),customElements.define("crowd-input",qt),customElements.define("crowd-menu-item",Yt),customElements.define("crowd-menu",jt),customElements.define("crowd-multi-toggle",Qt),customElements.define("crowd-multi-toggle-item",Zt),customElements.define("crowd-number",Gt),customElements.define("crowd-progress-ring",Wt),customElements.define("crowd-range",Jt),customElements.define("crowd-select",_t),customElements.define("crowd-stylesheet",Xt),customElements.define("crowd-switch",te),customElements.define("crowd-option",bt),customElements.define("crowd-spinner",Kt),customElements.define("crowd-tab",ee),customElements.define("crowd-tab-group",Ae),customElements.define("crowd-textarea",re),customElements.define("crowd-time-picker",ne),customElements.define("toast-stack",oe),customElements.define("crowd-tooltip",se),customElements.define("crowd-video",le),(new class{constructor(){}_initEl(t){const e=t.getAttribute("cc-element"),A=document.createElement(e);[].slice.call(t.attributes).forEach((t=>{A.setAttribute(t.nodeName,t.nodeValue)})),A.innerHTML=t.innerHTML,A.removeAttribute("cc-element"),t.parentElement.replaceChild(A,t)}init(){const t=[].slice.call(document.querySelectorAll("[cc-element]"));t&&t.reverse().forEach((t=>{this._initEl(t)}))}}).init()})()})();