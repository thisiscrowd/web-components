# Dropdown

This component allows content to be hidden in a dropdown opened by a trigger element.

## Example

```
<crowd-dropdown>
    <crowd-button caret slot='trigger'>Open</crowd-button>
    Presto!
</crowd-dropdown>
```
<crowd-dropdown>
    <crowd-button caret slot='trigger'>Open</crowd-button>
    Presto!
</crowd-dropdown>

Use the `trigger` slot to define an element as the trigger element. Clicking this element will open the dropdown.

### Hoist

Add the `hoist` attribute to bring the dropdown out of any overflow: hidden parents

```
<div style='overflow:hidden;'>
    <crowd-dropdown>
        <crowd-button caret slot='trigger'>Open</crowd-button>
        <p>You</p>
        <p>Can't</p>
        <p>See</p>
        <p>What</p>
        <p>I'm</p>
        <p>Saying</p>
    </crowd-dropdown>
    <crowd-dropdown hoist>
        <crowd-button caret slot='trigger'>Open</crowd-button>
        <p>You</p>
        <p>Can</p>
        <p>See</p>
        <p>What</p>
        <p>I'm</p>
        <p>Saying</p>
    </crowd-dropdown>
</div>
```
<div style='overflow:hidden;'>
    <crowd-dropdown>
        <crowd-button caret slot='trigger'>Open</crowd-button>
        <p>You</p>
        <p>Can't</p>
        <p>See</p>
        <p>What</p>
        <p>I'm</p>
        <p>Saying</p>
    </crowd-dropdown>
    <crowd-dropdown hoist>
        <crowd-button caret slot='trigger'>Open</crowd-button>
        <p>You</p>
        <p>Can</p>
        <p>See</p>
        <p>What</p>
        <p>I'm</p>
        <p>Saying</p>
    </crowd-dropdown>
</div>

## Methods

|Method|Action|
|---|---|
|`open`|Open the dropdown|
|`close`|Close the dropdown|
|`toggle`|Toggle the dropdown|

## Events

|Events|Called when|
|---|---|
|`crowdOpen`|Dropdown opened|
|`crowdClose`|Dropdown closed|

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-input-wrapper-padding-vertical`|Spacing above/below dropdown|<crowd-customiser property='--crowd-input-wrapper-padding-vertical' type='Number' value='1' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-input-wrapper-padding-horizontal`|Spacing above/below dropdown|<crowd-customiser property='--crowd-input-wrapper-padding-horizontal' type='Number' value='1' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-select-dropdown-max-height`|Dropdown tray max height|<crowd-customiser property='--crowd-select-dropdown-max-height' type='Number' value='50' unit='vh' min='0' max='100' step='10'></crowd-customiser>|
|`--crowd-select-dropdown-padding-vertical`|Dropdown tray padding top/bottom|<crowd-customiser property='--crowd-select-dropdown-padding-vertical' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-select-dropdown-padding-horizontal`|Dropdown tray padding top/bottom|<crowd-customiser property='--crowd-select-dropdown-padding-horizontal' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-select-dropdown-background-color`|Dropdown tray background color|<crowd-customiser property='--crowd-select-dropdown-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-select-dropdown-z-index`|Dropdown tray z-index|<crowd-customiser property='--crowd-select-dropdown-z-index' type='Number' unit='' value='999' min='0' max='999' step='1'></crowd-customiser>|
|`--crowd-select-dropdown-box-shadow`|Dropdown tray box shadow|<crowd-customiser property='--crowd-select-dropdown-box-shadow' type='Text' value='0 2px 8px rgba(0, 0, 0, 0.1)'></crowd-customiser>|