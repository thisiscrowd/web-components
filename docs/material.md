# Material

Material Design inspired theme.

<style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap');
    :root {
        --general-border-radius: 2em;
        --crowd-accordion-border-radius: var(--general-border-radius);
        --crowd-alert-border-radius: var(--general-border-radius);
        --crowd-alert-border-width: 1px;
        --crowd-alert-outline-color: #ddd;
        --crowd-alert-border-color: var(--crowd-alert-outline-color,#eee);
        --crowd-badge-border-radius: var(--general-border-radius);
        --crowd-button-border-radius: var(--general-border-radius);
        --crowd-input-border-radius: var(--general-border-radius);
        --crowd-button-border-color: transparent;
        --crowd-button-hover-border-color: transparent;
        --crowd-button-focus-width: 0px;
        --crowd-input-focus-width: 0px;
        --crowd-date-picker-day-border-width: 0px;
        --crowd-dialog-border-radius: var(--general-border-radius);
        --crowd-select-dropdown-box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
        --crowd-input-padding-vertical: 0px;
        --crowd-menu-item-padding-vertical: 1em;
    }
    #main {
        font-family: 'Open Sans', sans-serif;
    }
    crowd-button:focus-visible,crowd-button:focus-within {
        outline: 2px solid #eee;
        outline-offset: 2px;
        border-radius: var(--general-border-radius);
    }
    crowd-checkbox::part(box) {
        border-radius: 0.3em;
    }
    crowd-checkbox:focus-within::part(box) {
        outline: 2px solid #eee;
        outline-offset: 2px;
    }
    crowd-date-picker::part(day) {
        border-radius: 50%;
        display: grid;
        place-items: center;
        text-align:center;
    }
    crowd-date-picker::part(input):focus-within, 
    crowd-input:focus-within::part(container),
    crowd-number:focus-within::part(container),
    crowd-textarea:focus-within::part(container) {
        outline: 2px solid #eee;
        outline-offset: 2px;
        border-radius: var(--general-border-radius);
    }
    crowd-dialog {
        --width: 35ch;
        --height: max-content;
    }
    crowd-drawer::part(drawer) {
        border-top-right-radius: var(--general-border-radius);
        border-bottom-right-radius: var(--general-border-radius);
    }
    crowd-dropdown::part(dropdown) {
        border-radius: 4px;
    }
    crowd-select {
        --crowd-input-border-width: 1px;
        --crowd-input-border-color: #eee;
    }
    crowd-number::part(controls) {
        border-radius: var(--general-border-radius);
    }
    crowd-menu-item::part(item) {
        border-top-right-radius: var(--general-border-radius);
        border-bottom-right-radius: var(--general-border-radius);
    }
    crowd-menu[slot="submenu"] {
        box-shadow:var(--crowd-select-dropdown-box-shadow);
    }
</style>

<crowd-accordion-group>
    <crowd-accordion-item title='Playing the accordion'>
        <p>The accordion is played by compressing or expanding the bellows while pressing buttons or keys, causing pallets to open, which allow air to flow across strips of brass or steel, called reeds. These vibrate to produce sound inside the body. Valves on opposing reeds of each note are used to make the instrument's reeds sound louder without air leaking from each reed block.[notes 1] The performer normally plays the melody on buttons or keys on the right-hand manual, and the accompaniment, consisting of bass and pre-set chord buttons, on the left-hand manual.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Origins'>
        <p>The accordion is widely spread across the world because of the waves of immigration from Europe to the Americas and other regions. In some countries (for example: Brazil,[2][3] Colombia, the Dominican Republic, Mexico and Panama) it is used in popular music (for example: gaucho, forró and sertanejo in Brazil, vallenato in Colombia, merengue in the Dominican Republic, and norteño in Mexico), whereas in other regions (such as Europe, North America and other countries in South America) it tends to be more used for dance-pop and folk music and is often used in folk music in Europe, North America and South America.</p>
    </crowd-accordion-item>
    <crowd-accordion-item title='Usage'>
        <p>In Europe and North America, some popular music acts also make use of the instrument. Additionally, the accordion is used in cajun, zydeco, jazz music and in both solo and orchestral performances of classical music. The piano accordion is the official city instrument of San Francisco, California. Many conservatories in Europe have classical accordion departments. The oldest name for this group of instruments is harmonika, from the Greek harmonikos, meaning "harmonic, musical". Today, native versions of the name accordion are more common. These names refer to the type of accordion patented by Cyrill Demian, which concerned "automatically coupled chords on the bass side".</p>
    </crowd-accordion-item>
</crowd-accordion-group>


<crowd-alert open title="Alert">
    <crowd-icon slot="icon" name='exclamation-triangle'></crowd-icon>
    <p>Draw attention to some content with an icon</p>
</crowd-alert>

<crowd-badge type='success'>Success</crowd-badge>
<crowd-badge type='warning'>Warning</crowd-badge>
<crowd-badge type='danger'>Danger</crowd-badge>

<crowd-button>Click Me!</crowd-button>

<crowd-checkbox>Toggle Me!</crowd-checkbox>

<crowd-context-menu>
    <crowd-menu>
        <crowd-menu-item shortcut="s" onclick="alert('I like spaghetti!')">Spaghetti</crowd-menu-item>
        <crowd-menu-item>Pizza</crowd-menu-item>
        <crowd-menu-item>Creme Bruleé</crowd-menu-item>
    </crowd-menu>
</crowd-context-menu>

<crowd-date-picker weekFormat='ccccc' range label="Pick a day"></crowd-date-picker>

<crowd-button onClick='dialog.show()'>Open Dialog</crowd-button>
<crowd-dialog id='dialog' label='Title'><p>Showcase content in a modal/dialog</p></crowd-dialog>
<crowd-button onClick='drawer.show()'>Open Drawer</crowd-button>
<crowd-drawer style='--crowd-drawer-position-right: 70vw;' id='drawer' placement='left'><p>Content in a drawer</p><p>Good for menus</p></crowd-drawer>

<crowd-dropdown>
    <crowd-button slot='trigger'>Open Dropdown</crowd-button>
    <crowd-menu>
        <crowd-menu-item>Items in a menu</crowd-menu-item>
        <crowd-menu-item>These use the crowd-menu and crowd-menu-item components</crowd-menu-item>
    </crowd-menu>
</crowd-dropdown>

<crowd-form>
    <crowd-input label='Text Field' placeholder='Type something'></crowd-input>
    <crowd-select label='Select' value='Option 1'>
        <crowd-option value='Option 1'>Option 1</crowd-option>
        <crowd-option value='Option 2'>Option 2</crowd-option>
        <crowd-option value='Option 3'>Option 3</crowd-option>
    </crowd-select>
    <crowd-textarea label='Textarea' placeholder></crowd-textarea>
    <crowd-number label="Number" unit="px"></crowd-number>
</crowd-form>

<div style="max-width: 400px">
    <crowd-menu>
        <crowd-menu-item>Spaghetti</crowd-menu-item>
        <crowd-menu-item>
            Pizza
            <crowd-menu slot="submenu">
                <crowd-menu-item>Pepperoni</crowd-menu-item>
                <crowd-menu-item>Proscuitto</crowd-menu-item>
                <crowd-menu-item>Diavolo</crowd-menu-item>
            </crowd-menu>
        </crowd-menu-item>
        <crowd-menu-item>Creme Bruleé</crowd-menu-item>
    </crowd-menu>
</div>

<crowd-progress-ring style='--size: 3em;' percentage='50'></crowd-progress-ring>

<crowd-range errorMessage='This is required' invalid label='Slide me!' step='10' max='50' notches tooltip></crowd-range>

<crowd-spinner></crowd-spinner>

<crowd-switch false='😞' true='😄' label='Flick me!'></crowd-switch>

<crowd-tab-group>
    <crowd-tab title='Tab #1'>
        <p>Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Etiam rhoncus. Nam commodo suscipit quam. Praesent egestas tristique nibh. Fusce neque.</p>
    </crowd-tab>
    <crowd-tab title='Tab #2'>
        <p>Vivamus quis mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Fusce commodo aliquam arcu. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Quisque ut nisi.</p>
    </crowd-tab>
    <crowd-tab title='Tab #3'>
        <p>Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Etiam rhoncus. Nam commodo suscipit quam. Praesent egestas tristique nibh. Fusce neque.</p>
    </crowd-tab>
    <crowd-tab title='Tab #4'>
        <p>Vivamus quis mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Fusce commodo aliquam arcu. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Quisque ut nisi.</p>
    </crowd-tab>
    <crowd-tab title='Tab #5'>
        <p>Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Etiam rhoncus. Nam commodo suscipit quam. Praesent egestas tristique nibh. Fusce neque.</p>
    </crowd-tab>
    <crowd-tab title='Tab #6'>
        <p>Vivamus quis mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Fusce commodo aliquam arcu. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Quisque ut nisi.</p>
    </crowd-tab>
</crowd-tab-group>

<crowd-time-picker label="Pick a time" class="time-picker"></crowd-time-picker>

<crowd-tooltip content='Wassup?'>
    <crowd-button>Hover me!</crowd-button>
</crowd-tooltip>