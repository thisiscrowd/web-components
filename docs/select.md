# Select

Stylised dropdown for forms.

## Example

Use `<crowd-option>` to define options.

```
<crowd-select label='Select an option' placeholder='Choose something'>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>
```
<crowd-select label='Select an option' placeholder='Choose something'>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>

### Standard Attributes

The Select component has the standard input attributes: `name`,`required`,`invalid`, `errorMessage`.

### Clearable

Add the `clearable` attribute if you want the value to be clearable.

<crowd-select label='Select an option' placeholder='Choose something' clearable>
    <crowd-option value='Option 1'>Option 1</crowd-option>    
    <crowd-option value='Option 2'>Option 2</crowd-option>
    <crowd-option value='Option 3'>Option 3</crowd-option>
</crowd-select>

### Multiple

Add the `multiple` attribute if you want the user to be able to select multple items. Returns the value as a comma-separated string.

```
<crowd-select placeholder='Select multiple' label='Multi Select' multiple>
    <crowd-option value='Select'>Select</crowd-option>
    <crowd-option value='a'>a</crowd-option>
    <crowd-option value='few'>few</crowd-option>
    <crowd-option value='values'>values</crowd-option>
</crowd-select>
```
<crowd-select placeholder='Select multiple' label='Multi Select' multiple>
    <crowd-option value='Select'>Select</crowd-option>
    <crowd-option value='a'>a</crowd-option>
    <crowd-option value='few'>few</crowd-option>
    <crowd-option value='values'>values</crowd-option>
</crowd-select>

### Hoist

Use `hoist` to pull the dropdown out of any parents with `overflow: hidden`.

```
<div style='overflow:hidden;'>
    <crowd-select label='No hoist' placeholder='Choose something'>
        <crowd-option value='Option 1'>You</crowd-option>    
        <crowd-option value='Option 2'>Can't</crowd-option>
        <crowd-option value='Option 3'>See</crowd-option>
    </crowd-select>
    <crowd-select label='Hoisted' placeholder='Choose something' hoist>
        <crowd-option value='Option 1'>You</crowd-option>    
        <crowd-option value='Option 2'>Can</crowd-option>
        <crowd-option value='Option 3'>See!</crowd-option>
    </crowd-select>
</div>
```
<div style='overflow:hidden;'>
    <crowd-select label='No hoist' placeholder='Choose something'>
        <crowd-option value='Option 1'>You</crowd-option>    
        <crowd-option value='Option 2'>Can't</crowd-option>
        <crowd-option value='Option 3'>See</crowd-option>
    </crowd-select>
    <crowd-select label='Hoisted' placeholder='Choose something' hoist>
        <crowd-option value='Option 1'>You</crowd-option>    
        <crowd-option value='Option 2'>Can</crowd-option>
        <crowd-option value='Option 3'>See!</crowd-option>
    </crowd-select>
</div>

## Events

|Event|Called by|
|---|---|
|`crowdChange`|When the element's value is changed|

## Methods

|Method|Action|
|---|---|
|`validate`|Validates the input, showing any error messages|
|`open`|Opens the dropdown|
|`close`|Closes the dropdown|
|`toggle`|Toggle open/closed|
|`clear`|Clears value|

## Customisation

### Select

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-input-wrapper-padding-vertical`|Spacing above/below the element|<crowd-customiser property='--crowd-input-wrapper-padding-vertical' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-input-wrapper-padding-horizontal`|Spacing above/below the element|<crowd-customiser property='--crowd-input-wrapper-padding-horizontal' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-input-label-color`|Color of the select label|<crowd-customiser type='Color' value='#000000' property='--crowd-input-label-color'></crowd-customiser>|
|`--crowd-input-label-spacing`|Spacing below the label|<crowd-customiser property='--crowd-input-label-spacing' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-select-dropdown-max-height`|Max height of the dropdown tray|<crowd-customiser type='Number' unit='vh' min='0' max='100' value='50' step='10' property='--crowd-select-dropdown-max-height'></crowd-customiser>|
|`--crowd-select-dropdown-padding-vertical`|Top/bottom padding of the dropdown tray|<crowd-customiser property='--crowd-select-dropdown-padding-vertical' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-select-dropdown-padding-horizontal`|Left/right padding of the dropdown tray|<crowd-customiser property='--crowd-select-dropdown-padding-horizontal' type='Number' value='0' min='0' max='5' unit='em' step='0.5'></crowd-customiser>|
|`--crowd-select-dropdown-background-color`|Background color of the dropdown tray|<crowd-customiser type='Color' value='#ffffff' property='--crowd-select-dropdown-background-color'></crowd-customiser>|
|`--crowd-select-dropdown-z-index`|Z-index of the dropdown tray|<crowd-customiser property='--crowd-select-dropdown-z-index' value='999' min='0' max='1000' step='1' unit='' type='Number'></crowd-customiser>|
|`--crowd-select-dropdown-box-shadow`|Box shadow of the dropdown tray|<crowd-customiser property='--crowd-select-dropdown-box-shadow' value='0 2px 8px rgba(0, 0, 0, 0.1)' type='Text'></crowd-customiser>|
|`--crowd-input-padding-horizontal`|Left/right padding inside the select input|<crowd-customiser property='--crowd-input-padding-horizontal' type='Number' min='0' max='5' unit='em' step='0.5' value='1'></crowd-customiser>|
|`--crowd-input-padding-vertical`|top/bottom padding inside the select input|<crowd-customiser property='--crowd-input-padding-vertical' type='Number' min='0' max='5' unit='em' step='0.5' value='1'></crowd-customiser>|
|`--crowd-input-font-weight`|Font weight inside the select input|<crowd-customiser property='--crowd-input-font-weight' type='Number' value='400' min='100' max='900' step='100' unit=''></crowd-customiser>|
|`--crowd-input-height`|Height of the select input|<crowd-customiser property='--crowd-input-height' type='Number' min='0' max='1' unit='em' step='0.5' value='2'></crowd-customiser>|
|`--crowd-input-placeholder-color`|Color of the select input placeholder|<crowd-customiser type='Color' value='#000000' property='--crowd-input-placeholder-color'></crowd-customiser>|
|`--crowd-input-color`|Color of the select input|<crowd-customiser type='Color' value='#000000' property='--crowd-input-color'></crowd-customiser>|
|`--crowd-input-border-width`|Width of the select input border|<crowd-customiser property='--crowd-input-border-width' type='Number' unit='px' value='0' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-input-border-type`|Style of the select input border|<crowd-customiser property='--crowd-input-border-type' type='Select' options='["solid", "dotted", "dashed"]' unit='' value='solid'></crowd-customiser>|
|`--crowd-input-border-color`|Color of the select input border|<crowd-customiser type='Color' value='#000000' property='--crowd-input-border-color'></crowd-customiser>|
|`--crowd-input-border-radius`|Border radius of the select input|<crowd-customiser property='--crowd-input-border-radius' type='Number' unit='px' value='0' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-select-dropdown-border-width`|Width of the select dropdown border|<crowd-customiser property='--crowd-select-dropdown-border-width' type='Number' unit='px' value='0' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-select-dropdown-border-type`|Style of the select dropdown border|<crowd-customiser property='--crowd-select-dropdown-border-type' type='Select' options='["solid", "dotted", "dashed"]' unit='' value='solid'></crowd-customiser>|
|`--crowd-select-dropdown-border-color`|Color of the select dropdown border|<crowd-customiser type='Color' value='#000000' property='--crowd-select-dropdown-border-color'></crowd-customiser>|
|`--crowd-select-dropdown-border-radius`|Border radius of the select dropdown|<crowd-customiser property='--crowd-select-dropdown-border-radius' type='Number' unit='px' value='0' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-input-font-size`|Font size of the select input|<crowd-customiser property='--crowd-input-font-size' type='Number' min='0' max='5' unit='rem' step='0.5' value='1'></crowd-customiser>|
|`--crowd-select-icon-color`|Color of the caret icon|<crowd-customiser property='--crowd-select-icon-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-input-error-message-font-size`|Font size of the error message|<crowd-customiser property='--crowd-input-error-message-font-size' type='Number' min='0' max='3' unit='em' step='0.1' value='0.8'></crowd-customiser>|
|`--crowd-input-error-message-color`|Color of the error message|<crowd-customiser property='--crowd-input-error-message-color' type='Color' value='#FF0000'></crowd-customiser>|