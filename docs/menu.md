# Menu

List items in a menu. Good for dropdowns, and pairs well with [Context Menu](/context-menu.md)

## Example

```
<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>
```
<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>

### Shortcuts

Use the `shortcut` prop on `<crowd-menu-item>` to add a keyboard shortcut when the menu is in focus/hovered.
Use the key code, but all shortcuts will be triggered with Ctrl+key. Triggering the shortcut calls the `click` event on that item.

#### Example

```
<crowd-menu>
    <crowd-menu-item shortcut="s" onclick="alert(`I'll have the spaghetti!`)">Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>
```
<crowd-menu>
    <crowd-menu-item shortcut="s" onclick="alert(`I'll have the spaghetti!`)">Spaghetti</crowd-menu-item>
    <crowd-menu-item>Pizza</crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>

### Sub-enus

Add a sub-menu that will show on hover/focus by putting content in a `submenu` slot

```
<crowd-menu>
    <crowd-menu-item>Spaghetti</crowd-menu-item>
    <crowd-menu-item>
        Pizza
        <crowd-menu slot="submenu">
            <crowd-menu-item>Pepperoni</crowd-menu-item>
            <crowd-menu-item>Proscuitto</crowd-menu-item>
            <crowd-menu-item>Diavolo</crowd-menu-item>
        </crowd-menu>
    </crowd-menu-item>
    <crowd-menu-item>Creme Bruleé</crowd-menu-item>
</crowd-menu>
```
<div style="max-width: 400px">
    <crowd-menu>
        <crowd-menu-item>Spaghetti</crowd-menu-item>
        <crowd-menu-item>
            Pizza
            <crowd-menu slot="submenu">
                <crowd-menu-item>Pepperoni</crowd-menu-item>
                <crowd-menu-item>Proscuitto</crowd-menu-item>
                <crowd-menu-item>Diavolo</crowd-menu-item>
            </crowd-menu>
        </crowd-menu-item>
        <crowd-menu-item>Creme Bruleé</crowd-menu-item>
    </crowd-menu>
</div>

## Customisation

### Menu

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-menu-padding-vertical`|Top/bottom padding|<crowd-customiser property='--crowd-menu-padding-vertical' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-menu-padding-horizontal`|Left/right padding|<crowd-customiser property='--crowd-menu-padding-horizontal' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-menu-background-color`|Background color of the menu|<crowd-customiser property='--crowd-menu-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-menu-border-width`|Border width of the menu|<crowd-customiser property='--crowd-menu-border-width' type='Number' value='0' unit='px' min='0' max='5' step='1'></crowd-customiser>|
|`--crowd-menu-border-style`|Border style|<crowd-customiser property='--crowd-menu-border-style' type='Select' options='["solid","dashed","dotted","none"]' value='solid'></crowd-customiser>|
|`--crowd-menu-background-color`|Background color of the menu|<crowd-customiser property='--crowd-menu-border-color' type='Color' value='#000000'></crowd-customiser>|

### Menu Item

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-menu-item-padding-vertical`|Top/bottom padding|<crowd-customiser property='--crowd-menu-item-padding-vertical' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-menu-item-padding-horizontal`|Left/right padding|<crowd-customiser property='--crowd-menu-item-padding-horizontal' type='Number' value='0.5' unit='em' min='0' max='5' step='0.5'></crowd-customiser>|
|`--crowd-menu-item-color`|Text color of the menu item|<crowd-customiser property='--crowd-menu-item-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-menu-item-background-color`|Background color of the menu item|<crowd-customiser property='--crowd-menu-item-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-menu-item-hover-background-color`|Background color of the menu item on hover|<crowd-customiser property='--crowd-menu-item-hover-background-color' type='Color' value='#000000'></crowd-customiser>|
|`--crowd-menu-item-hover-color`|Text color of the menu item on hover|<crowd-customiser property='--crowd-menu-item-hover-color' type='Color' value='#000000'></crowd-customiser>|

