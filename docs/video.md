# Video

A wrapper for HTML <video> Elements, as well as embedding YouTube.

## Examples

```
<crowd-video src="./path-to-local-video.mp4" type="video/mp4" controls></crowd-video>
<crowd-video src-youtube="TWTfhyvzTx0" controls></crowd-video>
```

<crowd-video src-youtube="TWTfhyvzTx0" controls></crowd-video>

## Attributes

### `muted`

Use the `muted` attribute to start the player muted.

### `loop`

Use the `loop` attribute to loop the videio back to the start after it finishes.

### `controls`

Set `controls` to show controls. Is `false` by default.

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-video-theme-color`|Main UI color for the player|<crowd-customiser property='--crowd-video-theme-color' value='#000' type='Color'></crowd-customiser>|
|`--crowd-video-alt-color`|Secondary Color|<crowd-customiser property='--crowd-video-alt-color' value='#fff' type='Color'></crowd-customiser>|
|`--crowd-video-button-hover-color`|Hover color of the main buttons|<crowd-customiser property='--crowd-video-button-hover-color' value='#aaa' type='Color'></crowd-customiser>|
|`--crowd-video-controls-padding`|Padding around the controls bar|<crowd-customiser property='--crowd-video-controls-padding' type='Number' unit='em' step='0.1' min='0' max='3' value='1'></crowd-customiser>|
|`--crowd-video-controls-background-color`|Gradient color of the controls background|<crowd-customiser property='--crowd-video-controls-background-color' value='#000' type='Color'></crowd-customiser>|
|`--crowd-video-controls-background`|Background property of the controls area. Changes the entire block background||
|`--crowd-video-controls-gap`|Gap between control elements|<crowd-customiser property='--crowd-video-controls-gap' type='Number' unit='em' step='0.1' min='0' max='3' value='1'></crowd-customiser>|
|`--crowd-video-controls-button-size`|Size of the control buttons|<crowd-customiser property='--crowd-video-controls-button-size' type='Number' unit='em' step='0.1' min='0' max='3' value='1.2'></crowd-customiser>|

This component uses many other components from this library within it. Use the customisation tokens on those components to customise further.