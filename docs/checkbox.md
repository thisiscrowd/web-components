# Checkbox

The checkbox is a standard form component, used to toggle single choices.

## Example

```
<crowd-checkbox>Tick me!</crowd-checkbox>
```
<crowd-checkbox>Tick me!</crowd-checkbox>

### Attributes

Like a HTML5 checkbox, there are attributes such as `name`, `checked`, `required`, `invalid` which affect it's behavior in a form.

```
<crowd-checkbox checked>Tick me!</crowd-checkbox>
```
<crowd-checkbox checked>Tick me!</crowd-checkbox>

### Invalid & Error message

Use the attribute `errorMessage` to show a message when the `invalid` attribute is present.

```
<crowd-checkbox errorMessage="Something's not right..." invalid>Tick me!</crowd-checkbox>
```
<crowd-checkbox errorMessage="Something's not right..." invalid>Tick me!</crowd-checkbox>

## Customisation

|Property|Changes|Adjust|
|---|---|---|
|`--crowd-input-color`|Label color|<crowd-customiser property='--crowd-input-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-checkbox-spacing`|Gap between label and checkbox|<crowd-customiser property='--crowd-checkbox-spacing' type='Number' unit='em' min='0' max='3' step='0.5' value='0.5'></crowd-customiser>|
|`--crowd-checkbox-border-width`|Checkbox border width|<crowd-customiser property='--crowd-checkbox-border-width' type='Number' unit='px' min='0' max='5' step='1' value='1'></crowd-customiser>|
|`--crowd-checkbox-border-style`|Checkbox border style|<crowd-customiser property='--crowd-checkbox-border-style' type='Select' value='solid' options='["solid","dashed","dotted","none"]'></crowd-customiser>|
|`--crowd-checkbox-border-color`|Checkbox border color|<crowd-customiser property='--crowd-checkbox-border-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-checkbox-background-color`|Background Color|<crowd-customiser property='--crowd-checkbox-background-color' type='Color' value='#ffffff'></crowd-customiser>|
|`--crowd-checkbox-color`|Tick Color|<crowd-customiser property='--crowd-checkbox-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-checkbox-checked-background-color`|Background Color (when ticked)|<crowd-customiser property='--crowd-checkbox-checked-background-color' type='Color' value='#000'></crowd-customiser>|
|`--crowd-checkbox-checked-color`|Tick Color (when ticked)|<crowd-customiser property='--crowd-checkbox-checked-color' type='Color' value='#fff'></crowd-customiser>|
|`--crowd-checkbox-transition-duration`|Color transition duration|
|`--crowd-checkbox-transition-ease`|Color transition ease|
|`--crowd-checkbox-transition-delay`|Color transition delay|
|`--crowd-input-focus-width`|Focus shadow width|<crowd-customiser property='--crowd-checkbox-focus-width' type='Number' unit='px' min='0' max='5' step='1' value='2'></crowd-customiser>|
|`--crowd-input-focus-color`|Focus shadow color|<crowd-customiser property='--crowd-checkbox-focus-color' type='Color' value='rgba(0,0,0,0.3)'></crowd-customiser>|
|`--crowd-input-error-message-font-size`|Error message font size|<crowd-customiser property='--crowd-input-error-message-font-size' type='Number' unit='em' min='0' max='3' step='0.1' value='0.8'></crowd-customiser>|
|`--crowd-input-error-message-color`|Error message font color|<crowd-customiser property='--crowd-checkbox-error-message-color' type='Color' value='red'></crowd-customiser>|
